import logging
import os
from urllib import request

from celery import shared_task
from django.db.models import Count
from passporteye import read_mrz
import random

from Visa.models import PassportVision
from officeha import settings

logger = logging.getLogger(__name__)

def cleaning(content: str):

    index = content.find("  ")

    if index != -1:
        content = content[:index]

    index = content.find("<")
    if index != -1:
        content = content[:index]

    index = content.find(" ")
    if index != -1:
        content = content[:index]

    return content



@shared_task(name="image-recognization")
def image_recognization(id,request):
    obj = PassportVision.objects.get(id= id)

    image_path = os.path.join(str(settings.MEDIA_ROOT), str(obj.image))

    mrz = read_mrz(image_path)

    dic = mrz.to_dict()

    respondic = {}
    last_name = dic['surname']
    passport_type = dic['type']
    country = dic['country']
    number = dic['number']
    nationality = dic['nationality']
    sex = dic['sex']
    first_name = dic['names']
    personal_number = dic['personal_number']

    # cleaning
    first_name = cleaning(first_name)
    last_name = cleaning(last_name)
    passport_type = cleaning(passport_type)
    country = cleaning(country)
    number = cleaning(number)
    nationality = cleaning(nationality)
    sex = cleaning(sex)
    personal_number = cleaning(personal_number)

    # Create String
    respondic['first_name'] = first_name
    respondic['last_name'] = last_name
    respondic['passport_type'] = passport_type
    respondic['country'] = country
    respondic['number'] = number
    respondic['nationality'] = nationality
    respondic['sex'] = sex
    respondic['personal_number'] = personal_number
    respondic['id'] = obj.id
    print(request.META['PATH_INFO'])
    respondic['full_path'] = 'http://'+request.META['HTTP_HOST']+request.META['PATH_INFO'].replace('-vision','')
    obj.result = respondic
    obj.save()

    return respondic


