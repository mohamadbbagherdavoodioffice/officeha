
from Hr.serializers import ConsultTeamSerializer
from Organization.serializers import DeclarationFormsSerializer, AccountNumberSerializer
from Raw_root.models import HeardAboutUs, Translation
from Raw_root.serializers import CountrySerializer, CountryBreifSerializer, HeardAboutUsSerializer, ProvidingModeSerializer, TimeFrameSerializer
from Raw_root.serializers import CurrencySerializer , ModeOfPaymentSerializer, RefundConditionSerializer
from Raw_root.serializers import DocumnetSerializer
from Raw_root.serializers import TranslationSerializer
from Raw_root.serializers import TserviceSerializer


from rest_framework import serializers

from Tour.models import TourIncludedExcluded
from .models import *
from Inbox.models import WorkshopsParticipant

# class ServiceCategorySerializer(serializers.ModelSerializer):
#     country = CountrySerializer()
#
#     class Meta:
#         model = ServiceCategory
#         fields = '__all__'



class ServiceCategoryBreifSerializer(serializers.ModelSerializer):
    country = CountryBreifSerializer()
    class Meta:
        model = ServiceCategory
        fields = ['id','country','title_latin','title_farsi','thumbnail','expiry_date_emergency_note',
                  'availability_from_date_time','availability_to_date_time','alt_text_thumb']

class ServiceCategorySerializer(serializers.ModelSerializer):
    country = CountryBreifSerializer()
    currency = CurrencySerializer()
    class Meta:
        model = ServiceCategory
        fields = ['title_latin','title_farsi','processing_time_latin','processing_time_farsi','country','currency','fee','banner_image','alt_text_banner','alt_text','emergency_note']


class AbouteSerializer(serializers.ModelSerializer):
    # stid = serializers.SerializerMethodField()

    # service_category = serializers.SerializerMethodField()
    class Meta:
        model = Aboute
        fields = '__all__'

    # def get_stid(self, instance):
    #     ids = instance.stid
    #     data = VisaStream.objects.filter(pk=ids)
    #     serializer = VisaStreamSerializer(data, many=True)
    #     return serializer.data
    #
    # def get_service_category(self, instance):
    #     ids = instance.id
    #     data = ServiceCategory.objects.filter(pk=ids)
    #     serializer = ServiceCategorySerializer(data, many=True)
    #     return serializer.data


class IncludedExcludedSerializer(serializers.ModelSerializer):
    # tour = serializers.SerializerMethodField()
    t_service = TserviceSerializer(read_only=True, many=True)

    # service_category = serializers.SerializerMethodField()
    class Meta:
        model = IncludedExcluded
        fields = ['t_service','id']


    # def get_tour(self, instance):
    #     ids = instance.tour
    #     data = Tour.objects.filter(pk=ids)
    #     serializer = TourSerializer(data, many=True)
    #     return serializer.data

    # def get_t_service(self, instance):
    #
    #     ids = instance.t_service
    #     print(ids,"||||")
    #     data = Tservice.objects.filter(pk=ids)
    #     serializer = TserviceSerializer(data, many=True)
    #     return serializer.data
    #
    # def get_service_category(self, instance):
    #     ids = int(instance.id)
    #     print(ids,"+++")
    #     data = ServiceCategory.objects.filter(pk=ids)
    #     serializer = ServiceCategorySerializer(data, many=True)
    #     return serializer.data


class RequiredDocumnetSerializer(serializers.ModelSerializer):
    doc = DocumnetSerializer(many=True)
    # guarantee = serializers.SerializerMethodField()
    # visa_forms = serializers.SerializerMethodField()
    translation = TranslationSerializer()
    # tour = serializers.SerializerMethodField()
    # stid = serializers.SerializerMethodField()
    provide_mode = ProvidingModeSerializer()

    # service_category = serializers.SerializerMethodField()
    class Meta:
        model = RequiredDocumnet
        fields = ['id','provide_text_latin','provide_text_farsi','translation','doc','provide_mode']

    # def get_tour(self, instance):
    #     ids = int(instance.id)
    #     data = Tour.objects.filter(pk=ids)
    #     serializer = TourSerializer(data, many=True)
    #     return serializer.data
    #
    # def get_stid(self, instance):
    #     ids = int(instance.id)
    #     data = VisaStream.objects.filter(pk=ids)
    #     serializer = VisaStreamSerializer(data, many=True)
    #     return serializer.data
    #
    # def get_doc(self, instance):
    #     ids = int(instance.id)
    #     data = Document.objects.filter(pk=ids)
    #     serializer = DocumnetSerializer(data, many=True)
    #     return serializer.data
    #
    # def get_guarantee(self, instance):
    #     ids = int(instance.id)
    #     data = AppGuaranteeOpt.objects.filter(pk=ids)
    #     serializer = AppGuaranteeOptSerializer(data, many=True)
    #     return serializer.data
    #
    # def get_visa_forms(self, instance):
    #     ids = int(instance.id)
    #     data = VisaForms.objects.filter(pk=ids)
    #     serializer = VisaFormsSerializer(data, many=True)
    #     return serializer.data

    def get_translation(self, instance):
        ids = int(instance.id)
        data = Translation.objects.filter(pk=ids)
        serializer = TranslationSerializer(data, many=True)
        return serializer.data

    # def get_service_category(self, instance):
    #     ids = int(instance.id)
    #     data = ServiceCategory.objects.filter(pk=ids)
    #     serializer = ServiceCategorySerializer(data, many=True)
    #     return serializer.data




class SubServiceCategorySerializer(serializers.ModelSerializer):
    country = CountryBreifSerializer()
    currency = CurrencySerializer()
    class Meta:
        model = SubServiceCategory
        fields = '__all__'


class SubServiceCategoryTitleSerializer(serializers.ModelSerializer):
    class Meta:
        model = SubServiceCategory
        fields = ['title_latin','title_farsi']


class GuideSerializer(serializers.ModelSerializer):
    class Meta:
        model = Guide
        fields = '__all__'


class ConsultCapacitySerializer(serializers.ModelSerializer):
    time_frame = TimeFrameSerializer(many=True)
    currency = CurrencySerializer()
    account_number = AccountNumberSerializer()
    class Meta:
        model = ConsultCapacity
        fields = ['id','consultation_date','time_frame','currency','fee','account_number']


class TourIncludedExcludedSerializer(serializers.ModelSerializer):
    # tour = serializers.SerializerMethodField()
    included_tour_service_id = TserviceSerializer(many=True)
    excluded_tour_service_id = TserviceSerializer(many=True)
    # service_category = serializers.SerializerMethodField()
    class Meta:
        model = TourIncludedExcluded
        fields = ['included_tour_service_id','excluded_tour_service_id','id']


class ServiceCategoryTitleSerializer(serializers.ModelSerializer):

    class Meta:
        model = ServiceCategory
        fields = ['title_latin','title_farsi','id']



class InstalmentSerializer(serializers.ModelSerializer):
    mode_of_payment = ModeOfPaymentSerializer()
    refund_policy = RefundConditionSerializer()
    currency = CurrencySerializer()

    class Meta:
        model = Instalment
        fields = ['seq','title_latin','title_farsi','full_fee','currency','unit_fee','account_number','refundable','refund_policy','mode_of_payment']


class WorkshopSerializer(serializers.ModelSerializer):
    class Meta:
        model = Workshop
        fields = '__all__'


class WorkshopParticipantSerializer(serializers.ModelSerializer):
    class Meta:
        model = WorkshopsParticipant
        fields = [
            'workshop',
            'first_name',
            'last_name',
            'call_number',
            'email',
            'debit_card',
            'bank_receipt_file',
            'created_at',
        ]
        read_only_fields = ['created_at']
    
class ConsultScheduleGeneralSerializer(serializers.ModelSerializer):
    consult_team = ConsultTeamSerializer()
    time_frames = TimeFrameSerializer(many=True)
    heard_about_us = serializers.SerializerMethodField()  # Add this line

    class Meta:
        model = ConsultScheduleGeneral
        fields = ['consult_team', 'from_date', 'to_date', 'time_frames', 'heard_about_us']

    def get_heard_about_us(self, obj):
        # Logic to retrieve HeardAboutUs data
        # For example, you can return a static value or fetch it from a related model
        heard_about_us_instance = HeardAboutUs.objects.first()  # Example: Get the first instance
        return HeardAboutUsSerializer(heard_about_us_instance).data if heard_about_us_instance else None