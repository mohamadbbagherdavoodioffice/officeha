from django.db.models import Count
from rest_framework import generics
from rest_framework.generics import RetrieveAPIView
from rest_framework.response import Response
from rest_framework.views import APIView
from OfficeHauser.models import OfficeUser
from PublicPages.models import StaticText
from Raw_root.models import AgeRange, Gender
from .serializers import *
from .models import *
from rest_framework.generics import CreateAPIView, get_object_or_404, ListAPIView, RetrieveAPIView, ListCreateAPIView
from Hr.serializers import HrSkillSerializer
# Create your views here.

class JobFamilyRetriveAPIView(RetrieveAPIView):
    queryset = JobFamily.objects.all()
    serializer_class = JobFamilySerializer
    lookup_url_kwarg = "jbf_id"


class JobRetriveAPIView(RetrieveAPIView):
    queryset = Job.objects.all()
    serializer_class = JobSerializer
    lookup_url_kwarg = "job_id"


class SelfAssessmentToolsStaticAPIView(APIView):
    def get(self, request):
        dic = {}

        age_range = AgeRange.objects.all().values()
        dic["age_range"] = list(age_range)

        gender = Gender.objects.all().values()
        dic["gender"] = list(gender)

        job_family = JobFamily.objects.all().values()
        dic["job_family"] = list(job_family)

        hr_skill = HrSkill.objects.all().values('skill_latin','skill_farsi','id')
        dic["hr_skill"] = list(hr_skill)


        return Response({"message": "success", "status": "OK", "data": dic}, status=200)

class CareerStaticAPIView(APIView):
    def get(self, request):
        dic = {}

        static_text = StaticText.objects.filter(type='Careers').values('banner','title_latin','title_farsi','text_latin','text_farsi')
        dic["static_text"] = list(static_text)

        job_family = JobFamily.objects.all().values()
        dic["job_family"] = list(job_family)

        job_application_process = JobApplicationProcess.objects.all().values()
        dic["job_application_process"] = list(job_application_process)

        career_page_config = CareerPageConfig.objects.all().values('first_text_latin','first_text_farsi','job_families_text_latin','job_families_text_farsi',
                                                                   'self_assessment_title_latin','self_assessment_title_farsi',
                                                                   'job_process_title_latin','job_process_title_farsi','job_openings_title_latin',
                                                                   'job_openings_title_farsi','job_openings_text_latin','job_openings_text_farsi',
                                                                   'job_application_page_text_latin','job_application_page_text_farsi'
                                                                   ,'job_application_pdf_form')
        dic["career_page_config"] = list(career_page_config)

        return Response({"message": "success", "status": 200, "data": dic}, status=200)


class SearchJobOpeningsStaticAPIView(APIView):
    def get(self, request):
        dic = {}
        job_family = JobFamily.objects.all().values('id','Job_fam_title_latin','Job_fam_title_farsi')
        dic["job_family"] = list(job_family)

        age_range = DutyStation.objects.all().values('id','title_latin','title_farsi')
        dic["duty_station"] = list(age_range)

        age_range = AgeRange.objects.all().values()
        dic["age_range"] = list(age_range)

        return Response({"message": "success", "status": "OK", "data": dic}, status=200)

class SearchJobOpeningsQueryAPIView(APIView):
    def post(self, request):
        job_family = self.request.GET.get("job_family")
        duty_station = self.request.GET.get("duty_station")
        age_range = self.request.GET.get("age_range")
        dic = {}
        job = Job.objects.filter(job_cat__job_fam_id=job_family,duty_station_id=duty_station,age_range_id=age_range).values(
            'id','job_title_latin','job_title_farsi','job_code','job_cat__title_latin','job_cat__title_farsi'
            ,'job_cat__job_fam__job_fam_title_latin','job_cat__job_fam__job_fam_title_farsi','duty_station__title_latin','duty_station__title_farsi'
            ,'expiry_date'
        )
        dic["job"] = list(job)

        return Response({"message": "success", "status": "OK", "data": dic}, status=200)

class RegionGetListForDutyStation(APIView):
    def get(self, request):
        dic = {}

        region = DutyStation.objects.annotate(num_offerings=Count('id')).values('region__title_latin',
                                                'region__title_farsi',
                                                'region__id').distinct()
        dic["region"] = list(region)

        return Response({"message": "success", "status": "OK", "data": dic}, status=200)



class HrSkillListAPIView(ListAPIView):
    serializer_class = HrSkillSerializer
    queryset = HrSkill.objects.all()



class SkillOfJobAPIView(APIView):
    def get(self, request):
        dic = {}
        job_id = self.request.GET.get("id")
        print(job_id)
        skills = Job.objects.filter(id=job_id).values('id','job_skill__skill_latin','job_skill__skill_farsi')
        dic=skills


        return Response({"message": "success", "status": "OK", "data": dic}, status=200)




class JobSkillListAPIView(generics.ListAPIView):
    serializer_class = HrSkillSerializer

    def get_queryset(self):
        return HrSkill.objects.filter(job__isnull=False).values('id', 'skill_latin', 'skill_farsi').distinct()


class JobFamilyAllAPIView(APIView):
    def get(self, request):
        dic = {}
        job_family = JobFamily.objects.all().values('id','job_fam_title_latin','job_fam_title_farsi')
        dic["job_family"] = list(job_family)

        return Response({"message": "success", "status": "OK", "data": dic}, status=200)
    
class AgeRangeHasJobAPIView(generics.ListAPIView):
    serializer_class = AgeRangeSerializer

    def get_queryset(self):
        return AgeRange.objects.filter(job__isnull=False).distinct()

from django_filters.rest_framework import DjangoFilterBackend

from django.db.models import Count, Sum


class JobListAPIView(generics.ListAPIView):
    serializer_class = JobForSkillsQuerySerializer
    filter_backends = [DjangoFilterBackend]
    filterset_fields = {
        'job_cat__job_fam': ['exact'],
        'job_skill': ['exact'],
        
    }
    # 'age_range': ['exact'],

    # jobs = [int(i) for i in jobs.split(',')]

    def get_queryset(self):
        queryset = Job.objects.all()
        return queryset
        
    def get(self, request, *args, **kwargs):
        queryset = self.get_queryset()

        
        # if 'job_skill' in request.GET:
        #     return Response({'percent': 0, 'jobs': []})
        
        job_fam = self.request.query_params.get('job_cat__job_fam', None)
        job_skills = self.request.query_params.getlist('job_skill')
        # age_range = self.request.query_params.get('age_range', None)
        queryset = Job.objects.all()
        if job_fam is not None:
            queryset = queryset.filter(job_cat__job_fam=job_fam)
        if job_skills:
            queryset = queryset.filter(job_skill__id__in=job_skills)


        # Get the flat list of job_cat__job_fam values
        job_fam_list = list(queryset.values_list('job_cat__job_fam', flat=True).distinct())
        print(job_fam_list)
        Job_Skill_exist =Job.objects.filter(job_cat__job_fam__in= job_fam_list).values_list('job_skill')
        # Job_Skill_exist =Job.objects.filter(age_range= age_range).filter(job_cat__job_fam__in= job_fam_list).values_list('job_skill')
        count_exist=0

        
        for job in job_skills:
            print(job,Job_Skill_exist)
            if Job_Skill_exist.filter(job_skill=job).exists():
                count_exist +=1
        percent= (count_exist/len(Job_Skill_exist))*100
        queryset = Job.objects.all()

        serializer = self.get_serializer(queryset, many=True)
        

        return Response({'percent': percent, 'jobs': serializer.data})


