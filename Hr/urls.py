from django.urls import path, include, re_path
from .views import *
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [

    re_path(r'^retrieve_job_family/(?P<jbf_id>\d+)$', JobFamilyRetriveAPIView.as_view()),
    re_path(r'^retrieve_job/(?P<job_id>\d+)$', JobRetriveAPIView.as_view()),
    path('get_self_assessment', SelfAssessmentToolsStaticAPIView.as_view()),
    path('get_career', CareerStaticAPIView.as_view()),
    path('search_job_opening', SearchJobOpeningsQueryAPIView.as_view()),
    path('get_region_for_duty_station', RegionGetListForDutyStation.as_view()),
    path('get_list_hr_skill', HrSkillListAPIView.as_view()),
    path('get_list_skill_of_job', SkillOfJobAPIView.as_view()),
    path('get_list_skill_has_job', JobSkillListAPIView.as_view()),
    path('get_list_job_family', JobFamilyAllAPIView.as_view()),
    path('get_age_has_job', AgeRangeHasJobAPIView.as_view()),
    path('get_list_skill_match', JobListAPIView.as_view()),
    
    


]

urlpatterns = urlpatterns + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
