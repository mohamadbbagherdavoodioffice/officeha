from django.db import models
from passporteye import read_mrz


def get_file_visa_stream_thumbnail(instance, filename):
    return f"file_visa_stream_thumbnail/{filename}"


def get_file_visa_stream_banner(instance, filename):
    return f"file_visa_stream_banner/{filename}"


def get_visastream_icon(instance, filename):
    return f"file_visastream_icon/{filename}"


class VisaStream(models.Model):
    alt_farsi = models.CharField(max_length=300, null=True, blank=True)
    abbreviation_code = models.CharField(max_length=300)
    publicity = models.BooleanField(default=False)
    is_popular = models.BooleanField(default=False)
    appointment = models.BooleanField(default=False)
    icon = models.FileField(upload_to=get_visastream_icon, null=True, blank=True)
    last_edit = models.DateTimeField(null=True, blank=True)
    visa_sub_cat = models.ForeignKey('Raw_root.VisaSubCat', on_delete=models.PROTECT, null=True, blank=True)
    title_latin = models.CharField(max_length=300, null=True, blank=True)
    title_farsi = models.CharField(max_length=300, null=True, blank=True)
    country = models.ForeignKey('Raw_root.Country', on_delete=models.PROTECT, )
    sub_class_nr = models.CharField(max_length=50, null=True, blank=True)
    entry_type = models.ForeignKey('Raw_root.EntryType', on_delete=models.PROTECT, null=True, blank=True)
    stay_time_cat = models.ForeignKey('Raw_root.StayTimeCat', on_delete=models.PROTECT, null=True, blank=True)
    processing_time_latin = models.CharField(max_length=500, null=True, blank=True)
    processing_time_farsi = models.CharField(max_length=500, null=True, blank=True)
    work_permit = models.ForeignKey('Raw_root.WorkPermit', on_delete=models.PROTECT, null=True, blank=True)
    interview = models.BooleanField(default=False)
    contract = models.ForeignKey('Organization.ContractProfile', on_delete=models.PROTECT, null=True, blank=True)
    declaration_forms = models.ForeignKey('Organization.DeclarationForms', on_delete=models.PROTECT, null=True,
                                          blank=True)
    guarantee = models.ForeignKey('Organization.GuaranteeAmount', on_delete=models.PROTECT, null=True, blank=True)
    agn_currency = models.ForeignKey('Raw_root.Currency', on_delete=models.PROTECT, null=True, blank=True,
                                     related_name="stream_agn_currency")
    agn_share_amount = models.DecimalField(max_digits=15, decimal_places=1, null=True, blank=True)
    embassy_currency = models.ForeignKey('Raw_root.Currency', on_delete=models.PROTECT, null=True, blank=True)
    embassy_fee_main_applicant = models.DecimalField(max_digits=10, decimal_places=2, null=True, blank=True)
    emergency_note = models.TextField(null=True, blank=True)
    expiry_date_emergency_note = models.DateTimeField(null=True, blank=True)
    banner = models.FileField(upload_to=get_file_visa_stream_banner, null=True, blank=True)
    thumbnail = models.FileField(upload_to=get_file_visa_stream_thumbnail, null=True, blank=True)
    alt_text = models.TextField(null=True, blank=True)
    tags = models.TextField(null=True, blank=True)

    def __str__(self):
        return f"{self.id},{self.title_latin},{self.title_farsi}"

    def show_embassy_currency(self):
        if self.embassy_currency:
            print(self.embassy_currency)
            return f"{self.embassy_currency.currency_latin__set()},{self.embassy_currency.currency_farsi}"


class VisaAvailability(models.Model):
    stid = models.ForeignKey(VisaStream, on_delete=models.PROTECT, null=True, blank=True, default=None)
    from_date = models.DateField(null=True, blank=True)
    to_date = models.DateField(null=True, blank=True)

    def __str__(self):
        return "%s " % (self.stid.title_latin)


class SubVisaStream(models.Model):
    stid = models.ForeignKey(VisaStream, on_delete=models.PROTECT, null=True, blank=True, default=None)
    service_category = models.ForeignKey('Services.ServiceCategory', on_delete=models.CASCADE, null=True, blank=True, )
    title_latin = models.CharField(max_length=300, null=True, blank=True)
    title_farsi = models.CharField(max_length=300, null=True, blank=True)
    currency = models.ForeignKey('Raw_root.Currency', on_delete=models.CASCADE, null=True, blank=True, default=None)
    vat = models.ForeignKey('Organization.Vat', on_delete=models.PROTECT, null=True, blank=True)
    fee = models.DecimalField(max_digits=15, decimal_places=2, null=True, blank=True)
    guarantee = models.ForeignKey('Organization.GuaranteeAmount', on_delete=models.CASCADE, null=True, blank=True, )
    work_permit = models.ForeignKey('Raw_root.WorkPermit', on_delete=models.PROTECT, null=True, blank=True)
    stay_time_cat = models.ForeignKey('Raw_root.StayTimeCat', on_delete=models.PROTECT, null=True, blank=True)
    pay_online = models.BooleanField(default=False)
    validity_from_date = models.DateField(null=True, blank=True)
    validity_to_date = models.DateField(null=True, blank=True)
    tags = models.TextField(null=True, blank=True)

    def __str__(self):
        return "%s / %s " % (self.title_latin, self.stid)


def get_file_passport(instance, filename):
    return f"passport/{filename}"


class PassportVision(models.Model):
    image = models.ImageField(upload_to=get_file_passport, null=True, blank=True)
    result = models.JSONField(null=True, blank=True)
