from django.db import models
from django.contrib.auth.models import AbstractUser
from django.contrib.auth.hashers import (
    check_password, is_password_usable, make_password,
)
from django.contrib.auth import password_validation


def get_file_user_office_avatar(instance, filename):
    return f"file_user_office_avatar/{filename}"




class Role(models.Model):
    name = models.CharField(max_length=50, null=True, blank=True)

    # group = models.ManyToManyField(TahoorUser, null=True, blank=True)
    # admin, tourguide, driver, hotel,agency,notarry
    def __str__(self):
        return str(self.name)


    
class OfficeUser(AbstractUser):
    # business
    dob = models.DateField(null=True, blank=True)
    nid = models.CharField(max_length=10, null=True, blank=True)
    position = models.CharField(max_length=100, null=True, blank=True)
    created = models.DateField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)
    LanguageS = (
        ('English', 'English'),
        ('Farsi', 'Farsi'),
    )
    language = models.CharField(max_length=20, choices=LanguageS, null=True, blank=True, )
    GENDER_CHOICES = (
        ('Male', 'Male'),
        ('Female', 'Female'),
    )
    gender = models.CharField(max_length=10, choices=GENDER_CHOICES, null=True, blank=True, )
    date_of_birth = models.DateField(null=True, blank=True)
    default_language = models.ForeignKey('Raw_root.Language', on_delete=models.CASCADE, null=True, blank=True, )
    occupation = models.ForeignKey('Raw_root.Occupation', on_delete=models.CASCADE, null=True, blank=True, )
    ADDRESS_TYPE = (
        ('Residential', 'Residential'),
        ('Business', 'Business'),
    )
    address_type = models.CharField(max_length=30, choices=ADDRESS_TYPE, null=True, blank=True, )
    country = models.ForeignKey('Raw_root.Country', on_delete=models.CASCADE, null=True, blank=True, )
    province = models.ForeignKey('Raw_root.Province', on_delete=models.CASCADE, null=True, blank=True, )
    region = models.ForeignKey('Raw_root.Region', on_delete=models.CASCADE, null=True, blank=True, )
    postal_code = models.CharField(max_length=100, null=True, blank=True, )
    land_line_number = models.CharField(max_length=100, null=True, blank=True, )
    cell_number = models.CharField(max_length=100, null=True, blank=True, )
    instagram_1 = models.CharField(max_length=200, null=True, blank=True, )
    instagram_2 = models.CharField(max_length=200, null=True, blank=True, )
    website_url = models.CharField(max_length=100, null=True, blank=True, )
    linkedin = models.CharField(max_length=400, null=True, blank=True, )
    role = models.ForeignKey(Role, on_delete=models.PROTECT, null=True, blank=True, default=None)


    STATUS = (
        ('Active', 'Active'),
        ('Locked-Out', 'Locked-Out'),
        ('Warning', 'Warning'),
        ('Suspended', 'Suspended'),
        ('Deactivated', 'Deactivated')
    )

    status = models.CharField(max_length=40, choices=STATUS, null=True, blank=True, )
    avatar = models.ImageField(upload_to=get_file_user_office_avatar, null=True, blank=True)
    token_temporary = models.CharField(max_length=500, null=True, blank=True, )
    activation_code = models.CharField(max_length=50, null=True, blank=True, )
    recovery_question = models.ForeignKey('Raw_root.RecoveryQuestions', on_delete=models.CASCADE, null=True, blank=True, )
    recovery_answer = models.CharField(max_length=200, null=True, blank=True)


    class Meta:
        verbose_name = "User"

    def __str__(self):
        return "%s " % (self.username)

    # def save(self, *args, **kwargs):
    #     if self.created == None:
    #         self.password = make_password(self.password, None)
    #         self.admin = True
    #         self.is_active = True
    #         self.is_staff = True
    #         # self.is_superuser = False
    #         super(OfficeUser, self).save(*args, **kwargs)
    #     else:
    #         super(OfficeUser, self).save(*args, **kwargs)


class UserCredit(models.Model):
    user = models.ForeignKey(OfficeUser, on_delete=models.PROTECT, null=True, blank=True, )
    point = models.PositiveIntegerField(null=True, blank=True)
    date_time = models.DateTimeField(null=True, blank=True)


class UserTmile(models.Model):
    user = models.ForeignKey(OfficeUser, on_delete=models.PROTECT, null=True, blank=True, )
    point = models.PositiveIntegerField(null=True, blank=True)
    date_time = models.DateTimeField(null=True, blank=True)


class UserBankData(models.Model):
    user = models.ForeignKey(OfficeUser, on_delete=models.PROTECT, null=True, blank=True, )
    iban_nr = models.CharField(max_length=24, null=True, blank=True)
    dc_number = models.CharField(max_length=16, null=True, blank=True)

    class Meta:
        verbose_name = "User Bank Data"


class UsersPersonality(models.Model):
    personality = models.ForeignKey('Raw_root.Personality', on_delete=models.CASCADE, null=True, blank=True, )
    helpful = models.ForeignKey('Raw_root.Helpful', on_delete=models.CASCADE, null=True, blank=True, )
    color_code = models.CharField(max_length=20, null=True, blank=True)
    note = models.TextField(null=True, blank=True)
    user = models.ForeignKey(OfficeUser, on_delete=models.PROTECT, null=True, blank=True, )

    class Meta:
        verbose_name = "Users Personality"


class UserProblem(models.Model):
    ADDRESS_TYPE = (
        ('Warning', 'Warning'),
        ('Med cases', 'Med cases'),
        ('Problem', 'Problem'),
        ('Complaint', 'Complaint')
    )
    type = models.CharField(max_length=200, choices=ADDRESS_TYPE, null=True, blank=True, )
    warning = models.ForeignKey('Raw_root.Warnings', on_delete=models.CASCADE, null=True, blank=True, )
    problem = models.ForeignKey('Raw_root.Problem', on_delete=models.CASCADE, null=True, blank=True, )
    complaint = models.ForeignKey('Raw_root.ComplaintCat', on_delete=models.CASCADE, null=True, blank=True, )
    by_staff = models.ForeignKey('OfficeHauser.OfficeUser', on_delete=models.CASCADE,
                                 related_name='user_problem_staff_user_set', null=True, blank=True)
    note = models.TextField(null=True, blank=True)

    class Meta:
        verbose_name = "User Problem"


class BusinessProfile(models.Model):
    BUSINESS_TYPE = (
        ('AGN', 'AGN'),
        ('ACC', 'ACCOM'),
        ('NOT', 'Notary'),
        ('TOG', 'Tour Guide'),
        ('DRI', 'Driver'),
        ('ADM', 'Admin'),
    )

    BUSINESS_STATUS = (
        ('Running', 'Running'),
        ('Open with limited services', 'Open with limited services'),
        ('Temporarily closed', 'Temporarily closed'),
        ('Permanently closed', 'Permanently closed'),
    )

    Language = (
        ('English', 'English'),
        ('Farsi', 'Farsi'),
    )
    holder_user = models.ForeignKey('OfficeHauser.OfficeUser', on_delete=models.CASCADE,
                                    related_name='user_business_staff_user_set', null=True, blank=True)
    created = models.DateField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)
    business_type = models.ForeignKey('Raw_root.BusinessType', null=True, blank=True, on_delete=models.PROTECT,
                                      related_name='business_language_1', default=None)
    business_name = models.CharField(max_length=300, null=True, blank=True)
    business_status = models.CharField(max_length=50, choices=BUSINESS_STATUS, null=True, blank=True)
    tax_nr = models.CharField(max_length=100, null=True, blank=True)
    co_reg_nr = models.CharField(max_length=100, null=True, blank=True)
    language_id_1 = models.ForeignKey('Raw_root.Language', null=True, blank=True, on_delete=models.PROTECT,
                                      related_name='business_language_1', default=None)
    language_id_2 = models.ForeignKey('Raw_root.Language', null=True, blank=True, on_delete=models.PROTECT,
                                      related_name='business_language_2', default=None)
    expertise_tour_type_1 = models.ForeignKey('Raw_root.ExpertiseCat', on_delete=models.PROTECT, null=True,
                                              blank=True, related_name='bussiness_expertise_tour_type_1_cat',
                                              default=None)
    expertise_tour_type_2 = models.ForeignKey('Raw_root.ExpertiseCat', on_delete=models.PROTECT, null=True,
                                              blank=True, related_name='bussiness_expertise_tour_type_2_cat',
                                              default=None)
    accomodation_type_id = models.ForeignKey('Raw_root.AccomType', on_delete=models.PROTECT, null=True, blank=True,
                                             default=None)
    notary_office_code = models.CharField(max_length=200, null=True, blank=True, )
    country = models.ForeignKey('Raw_root.Country', on_delete=models.CASCADE, null=True, blank=True)

    province = models.ForeignKey('Raw_root.Province', on_delete=models.CASCADE, null=True, blank=True, )
    region = models.ForeignKey('Raw_root.Region', on_delete=models.CASCADE, null=True, blank=True, )
    postal_code = models.CharField(max_length=10, null=True, blank=True)
    public_email = models.EmailField(null=True, blank=True)
    phone_number_1 = models.CharField(max_length=11, null=True, blank=True)
    phone_number_2 = models.CharField(max_length=11, null=True, blank=True)

    def __str__(self):
        return "%s " % (self.holder_user.username)


# class UserBalanceRequest(models.Model):
#     user = models.ForeignKey(OfficeUser, null=True, blank=True, on_delete=models.PROTECT)
#     date_time = models.DateTimeField(null=True, blank=True)

def get_file_bussiness_icn_validity_image(instance, filename):
    return f"file_bussiness_icn_validity_image/{filename}"


class BussinessICNValidity(models.Model):
    lcn = models.ImageField(upload_to=get_file_bussiness_icn_validity_image, null=True, blank=True)
    lcn_number = models.CharField(max_length=30, null=True, blank=True)
    expiry_date = models.DateTimeField(auto_now_add=True, blank=True)


class UserAuthentication(models.Model):
    s_user = models.ForeignKey(OfficeUser, null=True, blank=True, on_delete=models.PROTECT)
    signup_device = models.CharField(max_length=100, null=True, blank=True)
    signup_ip = models.CharField(max_length=100, null=True, blank=True)
    password = models.CharField(max_length=100, null=True, blank=True)
    recovery_question = models.CharField(max_length=200, null=True, blank=True)
    recovery_answer = models.CharField(max_length=200, null=True, blank=True)
    last_login = models.DateTimeField(null=True, blank=True)
    

class UserWarranty(models.Model):
    TYPE = (
        ('Bank', 'Bank'),
        ('Notary', 'Notary'),
        ('Cheque', 'Cheque'),
        ('Promissory Note', 'Promissory Note'),
    )

    STATUS = (
        ('Waiting', 'Waiting'),
        ('Provided', 'Provided'),
        ('Expired', 'Expired'),
        ('Deactivate', 'Deactivate'),
    )

    user = models.ForeignKey('OfficeHauser.OfficeUser', on_delete=models.CASCADE, related_name='UserWarranty_user',
                             null=True, blank=True, default=None)
    registrar_staff = models.ForeignKey('OfficeHauser.OfficeUser', on_delete=models.CASCADE,
                                        related_name='UserWarranty_registrar_staff', null=True, blank=True,
                                        default=None)
    registrar_notary = models.ForeignKey('OfficeHauser.OfficeUser', on_delete=models.CASCADE,
                                         related_name='UserWarranty_registrar_notary', null=True, blank=True,
                                         default=None)

    type = models.CharField(max_length=40, choices=TYPE, null=True, blank=True)
    notary_text = models.ForeignKey('Organization.NotaryText', on_delete=models.CASCADE,
                                    related_name='UserWarranty_registrar_notary', null=True, blank=True, default=None)
    amount = models.DecimalField(max_digits=6, decimal_places=2, null=True, blank=True)
    doc_code = models.CharField(max_length=100, null=True, blank=True)
    bank = models.ForeignKey('Organization.Bank', on_delete=models.CASCADE, null=True, blank=True, default=None)
    date_of_issue = models.DateTimeField(null=True, blank=True)
    expiry_date = models.DateTimeField(null=True, blank=True)
    status = models.CharField(max_length=40, choices=STATUS, null=True, blank=True)


def get_file_customer_support(instance, filename):
    return f"get_file_customer_support/{filename}"


class CustomerSupport(models.Model):
    user = models.ForeignKey('OfficeHauser.OfficeUser', on_delete=models.CASCADE, null=True, blank=True, default=None)
    date_time = models.DateTimeField(null=True, blank=True)
    CAT = (
        ('AGN', 'AGN'),
        ('Financial', 'Financial'),
        ('Tour', 'Tour'),
        ('Visas', 'Visas'),
        ('Other', 'Other'),
    )
    PRIORITY = (
        ('Normal', 'Normal'),
        ('Urgent', 'Urgent'),
    )

    STATUS = (
        ('New', 'New'),
        ('Read', 'Read'),
    )

    cat = models.CharField(max_length=10, choices=CAT, null=True, blank=True)
    text = models.CharField(max_length=10, null=True, blank=True)
    priority = models.CharField(max_length=40, choices=PRIORITY, null=True, blank=True)
    dateTime = models.DateTimeField(null=True, blank=True)
    status = models.CharField(max_length=40, choices=STATUS, null=True, blank=True)
    file_attach = models.ImageField(upload_to=get_file_customer_support, null=True, blank=True)


class SupportChat(models.Model):
    customer_support = models.ForeignKey(CustomerSupport, on_delete=models.CASCADE, null=True, blank=True,
                                         related_name='customer_support_Userchat_staff')
    staff_user = models.ForeignKey('OfficeHauser.OfficeUser', on_delete=models.CASCADE,
                                   related_name='userchat_staff_user_set', null=True, blank=True, default=None)
    user = models.ForeignKey('OfficeHauser.OfficeUser', on_delete=models.CASCADE, related_name='userchat_user_set',
                             null=True, blank=True, default=None)
    date_time = models.DateTimeField(null=True, blank=True)
    text_from_user = models.TextField(blank=True, null=True)
    text_from_staff = models.TextField(blank=True, null=True)
    pagination_nr = models.PositiveIntegerField(null=True, blank=True)


class UserSubscribe(models.Model):
    status = models.CharField(max_length=40, choices=(('Subscribed', 'Subscribed'), ('unsubscribed', 'unsubscribed')),
                              null=True, blank=True)
    email = models.EmailField(null=True, blank=True)
    tell = models.CharField(max_length=20, null=True, blank=True)
    service_cat = models.ForeignKey('Services.ServiceCategory', on_delete=models.CASCADE,
                                    null=True, blank=True, )
    event_cat = models.ForeignKey('Raw_root.EventCat', on_delete=models.CASCADE,
                                  related_name='user_subscribe_event_cat_2', null=True,
                                  blank=True, )
    # event_cat_3 = models.ForeignKey('Raw_root.EventCat', on_delete=models.CASCADE, related_name='user_subscribe_event_cat_3', null=True,
    #                                 blank=True, )

    join_date = models.DateField(null=True, blank=True)
    unsubscribe_date = models.DateField(null=True, blank=True)


class RefundList(models.Model):
    tour_pax = models.ForeignKey('Tour.TourPAX', on_delete=models.CASCADE, null=True, blank=True, )
    cancellation_date_time = models.DateTimeField(null=True, blank=True)
    cancellation_rate = models.ForeignKey('Raw_root.CancellationRate', on_delete=models.CASCADE, null=True,
                                          blank=True, )
    total_fee = models.DateTimeField(null=True, blank=True)
    deducted_amount_admin = models.DateTimeField(null=True, blank=True)
    refundable_amount = models.DateTimeField(null=True, blank=True)


def get_file_evidence_file(instance, filename):
    return f"file_evidence_file/{filename}"


class Balance(models.Model):
    user_balance_req = models.ForeignKey(OfficeUser, on_delete=models.CASCADE, null=True, blank=True, )
    currency = models.ForeignKey("Raw_root.Currency", on_delete=models.CASCADE, null=True, blank=True, default=None)
    balance_amount = models.DateTimeField(null=True, blank=True)
    # User id business
    admin_approval = models.BooleanField(default=False)
    approval_date_time = models.DateTimeField(null=True, blank=True)
    bank_trace_code = models.CharField(max_length=100, null=True, blank=True)
    remittance_date_time = models.DateTimeField(null=True, blank=True)
    evidence_file = models.ImageField(upload_to=get_file_evidence_file, null=True, blank=True)
    note = models.TextField(blank=True, null=True)
    status = models.CharField(max_length=20,
                              choices=(('Requested', 'Requested'), ('Waiting', 'Waiting'), ('Approved', 'Approved'),
                                       ('Remittance queue', 'Remittance queue'), ('PAID', 'PAID')), null=True,
                              blank=True, )


# class UserLedger(models.Model):
#     tour_pax = models.ForeignKey('Tour.TourPAX', on_delete=models.CASCADE, null=True, blank=True, )
#     user = models.ForeignKey(OfficeUser, on_delete=models.CASCADE, null=True, blank=True, )
#     status = models.CharField(max_length=20, choices=(('Auto', 'Auto'), ('manual', 'manual')),null=True, blank=True, )
#     note = models.TextField(blank=True,null=True)
#     currency = models.ForeignKey("Raw_root.Currency", on_delete=models.CASCADE, null=True, blank=True, default=None)
#     code = models.CharField(max_length=20, choices=(('Balance id', 'Balance id'), ('Refund list id', 'Refund list id')
#                                                     ,('App. Our buy Opt.id','App. Our buy Opt.id'),('Tour-Operation id','Tour-Operation id')),null=True, blank=True, )
#     debit_amount = models.DecimalField(max_digits=10, decimal_places=2,null=True,blank=True)
#     credit_amount = models.DecimalField(max_digits=10, decimal_places=2,null=True,blank=True)
#
#
#


class StaffUser(OfficeUser):
    type_user = models.CharField(max_length=10, default='staff')
    event_cat = models.ManyToManyField('Raw_root.EventCat', blank=True, )
    service_cat = models.ManyToManyField("Services.ServiceCategory", blank=True)
    stid = models.ManyToManyField('Visa.VisaStream', blank=True, )

    class Meta:
        verbose_name = "Staff User"

    def __str__(self):
        return "%s " % (self.username)

    # def save(self, *args, **kwargs):
    #     if self.created == None:
    #         self.password = make_password(self.password, None)
    #         self.admin = True
    #         self.is_active = True
    #         self.is_staff = True
    #         # self.is_superuser = False
    #         super(OfficeUser, self).save(*args, **kwargs)
    #     else:
    #         super(OfficeUser, self).save(*args, **kwargs)
