from rest_framework import serializers

from Raw_root.serializers import EduProgramDegreeSerializer, EduProgramDegreeSerializer
from .models import *


class SchoolSerializer(serializers.ModelSerializer):
    class Meta:
        model = School
        fields = '__all__'


class ProgramSerializer(serializers.ModelSerializer):
    class Meta:
        model = Program
        fields = '__all__'


class ProgramRetreiveSerializer(serializers.ModelSerializer):
    # required_degree = DegreeSerializer()
    degree = EduProgramDegreeSerializer()
    class Meta:
        model = Program
        fields = ['id','banner','name_latin','name_farsi','degree']


class ProgramListSerializer(serializers.ModelSerializer):

    class Meta:
        model = Program
        fields = ['id','name_latin','name_farsi']

class SchoolListSerializer(serializers.ModelSerializer):
    class Meta:
        model = School
        fields = ['id','name_latin','name_farsi']
