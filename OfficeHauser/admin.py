from django.contrib import admin
from django.contrib.auth.admin import GroupAdmin
from django.contrib.auth.models import Group
from django.apps import apps
from django.contrib import admin
from django.contrib.admin.sites import AlreadyRegistered
from import_export import resources
from import_export.admin import ImportExportModelAdmin
from django.apps import apps
from django.contrib import admin
from django.contrib.admin.sites import AlreadyRegistered
from .models import *

app_models = apps.get_app_config('OfficeHauser').get_models()
for model in app_models:

    try:

        class AAAResource(resources.ModelResource):
            class Meta:
                model = model


        class AAAAdmin(ImportExportModelAdmin):
            resource_class = AAAResource


        admin.site.register(model, AAAAdmin)
    except AlreadyRegistered:
        pass


admin.site.unregister(Group)

class GroupAdmin(admin.ModelAdmin):
    search_fields = ('name',)
    ordering = ('name',)


class GroupResource(resources.ModelResource):
    class Meta:
        model = Group
class GroupAdmin(ImportExportModelAdmin):
    resource_class = GroupResource

admin.site.register(Group, GroupAdmin)
