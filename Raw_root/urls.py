from django.urls import path, include, re_path
from .views import *
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    # path('', include(router.urls)),
    path('get_countries', CountryAPIView.as_view()),
    path('get_contanct_us_type', ContactUsTypeAPIView.as_view()),
    path('get_tell_what_you_think_cat', TellUsWhatYouThinkCatAPIView.as_view()),
    path('get_list_age_range', AgeRangeAPIView.as_view()),
    path('get_list_degree', EduProgramDegreeAPIView.as_view()),
    path('get_list_edu_subject', EduSubjectAPIView.as_view()),
    path('get_list_available_fund', AvailableFundAPIView.as_view()),
    path('get_list_province', ProvinceAPIView.as_view()),
    path('get_list_followup_status', FollowUpStatusAPIView.as_view()),
    path('get_list_visa_main_cat', VisaMainCatAPIView.as_view()),
    # path('retrieve_get_external_payment/<int:pk>/', ExternalPaymentStatusGetRetrieveAPIView),
    re_path(r'^retrieve_external_payment/(?P<payment_id>\d+)$', ExternalPaymentStatusGetRetrieveAPIView.as_view()),
    # re_path(r'^retrieve_external_payment/(?P<payment_id>\d+)$', ExternalPaymentStatusGetRetrieveAPIView.as_view()),
    path('get_list_external_payment', ExternalPaymentStatusGetAPIView.as_view()),
    path('get_age_range_for_hr', AgeRangeAPIViewForHr.as_view()),
    path('get_region_iran', RegionIranAPIView.as_view()),
    path('get_list_tour_element', TourElementAPIView.as_view()),
    path('get_list_province_by_country', ProvinceByCountryAPIView.as_view()),
    path('get_list_region_by_country', RegionByCountryAPIView.as_view()),
    path('get_list_event_cat', EventCatSerializerGetAPIView.as_view()),
    path('min_employment_years', MinEmploymentYearGetAPIView.as_view()),
    path('get_list_maritals', MaritalGetAPIView.as_view()),
    path('get_list_occupations', OccupationGetAPIView.as_view()),
    path('get_list_lang', LanguageGetAPIView.as_view()),
    path('room_type', RoomTypeGetAPIView.as_view()),
    # path('get_list_visa_asia_middle', GlobalZonesArabianAPiView.as_view()),
    
    

]

urlpatterns = urlpatterns + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
