from django.shortcuts import render
from django.db.models import Q
from rest_framework import status, viewsets, pagination
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet
from rest_framework.generics import CreateAPIView, get_object_or_404, ListAPIView, RetrieveAPIView, ListCreateAPIView

from SpecificPages.models import BiometricConfig, BookWizardConfig, AboutUsCert, AboutUs, AboutUsTeam, ConsultWizardConfig, EvaluationConfig
from SpecificPages.serializers import BiometricConfigSerializer, BookWizardConfigSerializer, AboutUsCertSerializer, AboutUsSerializer, \
    AboutUsTeamSerializer, ConsultWizardConfigSerializer, EvaluationConfigSerializer
# from rest_framework_swagger import swagger_auto_schema
from drf_yasg.utils import swagger_auto_schema
from drf_yasg import openapi


class BookWizardConfigListAPIView(ListAPIView):
    serializer_class = BookWizardConfigSerializer

    def list(self, request):
        dict = {}
        result_list = []
        queryset = BookWizardConfig.objects.all().values()
        query = Q()
        print(self.request.GET.get('type'))
        if self.request.GET.get('service_cat'):
            query |= Q(service_cat__in=self.request.GET.get('service_cat').split(','))
        if self.request.GET.get('visa_stream'):
            query |= Q(visa_stream__in=self.request.GET.get('visa_stream').split(','))
        if self.request.GET.get('tour'):
            query |= Q(tour__in=self.request.GET.get('tour').split(','))
        if self.request.GET.get('type'):
            query |= Q(general_type=self.request.GET.get('type'))


        book_wizard = BookWizardConfig.objects.filter(query).values().distinct()


        dict=book_wizard


        return Response({"message": "success", "status": "OK", "data":dict }, status=200)



class AboutUsCertALLAPIView(ListAPIView):
    serializer_class = AboutUsCertSerializer
    queryset = AboutUsCert.objects.all().order_by('seq')


class AboutUsALLAPIView(ListAPIView):
    serializer_class = AboutUsSerializer
    queryset = AboutUs.objects.all()


class AboutUsTeamALLAPIView(ListAPIView):
    serializer_class = AboutUsTeamSerializer
    queryset = AboutUsTeam.objects.all()

class EvaluationConfigAPIView(APIView):
    serializer_class = EvaluationConfigSerializer

    @swagger_auto_schema(
        operation_description="Evaluation Config APIView description",
        operation_summary="Evaluation Config APIView summary",
        manual_parameters=[
            openapi.Parameter(
                name='evaluation_id',
                in_=openapi.IN_QUERY,
                type=openapi.TYPE_INTEGER,
                description='Evaluation ID',
                required=True,
            ),
        ],
        responses={
            200: EvaluationConfigSerializer(many=True),
            400: "Bad Request",
            401: "Unauthorized",
            404: "Not Found",
            500: "Internal Server Error"
        }
    )
    def get(self, request):
        """
        Evaluation Config APIView GET method
        """
        evaluation_id = request.query_params.get('evaluation_id')
        queryset = EvaluationConfig.objects.filter(evaluation=evaluation_id)
        serializer = EvaluationConfigSerializer(queryset, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)
    

class ConsultWizardConfigListView(ListAPIView):
    queryset = ConsultWizardConfig.objects.all()
    serializer_class = ConsultWizardConfigSerializer

class GetBiometricConfigView(APIView):
    def get(self, request, *args, **kwargs):
        biometric_configs = BiometricConfig.objects.all() 
        serializer = BiometricConfigSerializer(biometric_configs, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)