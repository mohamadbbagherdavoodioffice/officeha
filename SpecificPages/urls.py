from django.urls import path, include, re_path
from .views import *
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('get_list_book_wizard', BookWizardConfigListAPIView.as_view()),
    path('get_list_aboutus_cert', AboutUsCertALLAPIView.as_view()),
    path('get_list_aboutus', AboutUsALLAPIView.as_view()),
    path('get_list_aboutus_team', AboutUsTeamALLAPIView.as_view()),
    path('get_list_evaluation_config', EvaluationConfigAPIView.as_view()),
    path('consult-wizard-configs/', ConsultWizardConfigListView.as_view(), name='consult-wizard-config-list'),
    path('get_list_bio_metric_config/', GetBiometricConfigView.as_view(), name='get-biometric-config'),
    

]

urlpatterns = urlpatterns + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
