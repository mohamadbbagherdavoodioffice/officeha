from django.shortcuts import render
from PublicPages.models import Hint
from PublicPages.models import StaticText
from Raw_root.models import RespondNote
from rest_framework import generics
from rest_framework.generics import RetrieveAPIView
from rest_framework.response import Response
from rest_framework.views import APIView
from .serializers import *

class ExternalPaymentCreateAPIView(generics.CreateAPIView):
    queryset = ExternalPayment.objects.all()
    serializer_class = ExternalPaymentDetailsSerializer

    def create(self, request, *args, **kwargs):
        response = super().create(request, *args, **kwargs)
        # print(response.data['payment_code'])
        response.data = {"payment_code":response.data['payment_code']}


        respond_success = RespondNote.objects.filter(category='External Payment/Create').values(
            'successful_title_latin',
            'successful_title_farsi',
            'successful_text_latin',
            'successful_text_farsi',
            'successful_signature_latin',
            'successful_signature_farsi')
        respond_fail = RespondNote.objects.filter(category='External Payment/Create').values('failed_title_latin',
                                                                                             'failed_title_farsi',
                                                                                             'failed_text_latin',
                                                                                             'failed_text_farsi',
                                                                                             'failed_signature_latin',
                                                                                             'failed_signature_farsi')
        if response.status_code == 201:
            return Response({
                'status': 201,
                'message': 'success',
                'respond': respond_success,
                'data': response.data
            })
        else:
            return Response({
                'status': 404,
                'message': 'fail',
                'respond': respond_fail,
                'data': response.data
            })

class ExternalPaymentStaticAPIView(APIView):
    def get(self, request):
        dic = {}

        static_text = StaticText.objects.filter(type='payment Status').values('banner','title_latin','title_farsi','text_latin','text_farsi')
        dic["static_text"] = list(static_text)

        hint = Hint.objects.filter(item='payment Status').all()
        dic["hint"] = list(hint)

        return Response({"message": "success", "status": "OK", "data": dic}, status=200)

class ExternalPaymentRetriveAPIView(RetrieveAPIView):
    serializer_class = ExternalPaymentSerializer
    

    def get(self,request):
        payment_code= self.request.GET.get('payment_code')
        print(payment_code)
        try:
            
            app = ExternalPayment.objects.get(payment_code=payment_code)
            serializer = ExternalPaymentStatusSerializer(app)
            respond_success = RespondNote.objects.filter(category='payment status').values(
                        'successful_title_latin',
                        'successful_title_farsi',
                        'successful_text_latin',
                        'successful_text_farsi',
                        'successful_signature_latin',
                        'successful_signature_farsi')
            

            return Response({
                'status': 200,
                'message': 'success',
                'respond': respond_success,
                'data': serializer.data
            })
        except :
            respond_fail = RespondNote.objects.filter(category='payment status').values('failed_title_latin',
                                                                                    'failed_title_farsi',
                                                                                    'failed_text_latin',
                                                                                    'failed_text_farsi',
                                                                                    'failed_signature_latin',
                                                                                    'failed_signature_farsi')
            return Response({
                'status': 404,
                'message': 'fail',
                'respond': respond_fail,
                'data': ''
            })

class HintApplyFormAPIView(APIView):
    def get(self, request):
        dic = {}

        hint = Hint.objects.filter(item='Apply form').all()
        dic["hint"] = list(hint)

        return Response({"message": "success", "status": "OK", "data": dic}, status=200)

class ExternalPaymentRetriveQueryAPIView(APIView):
    def get(self,request):
        try:
            payment_code = self.request.GET.get("payment_code")
            app = ExternalPayment.objects.get(payment_code=payment_code)
            serializer = ExternalPaymentStatusSerializer(app)
            respond_success = RespondNote.objects.filter(category='payment status').values(
                        'successful_title_latin',
                        'successful_title_farsi',
                        'successful_text_latin',
                        'successful_text_farsi',
                        'successful_signature_latin',
                        'successful_signature_farsi')

            return Response({
                'status': 200,
                'message': 'success',
                'respond': respond_success,
                'data': serializer.data
            })
        except :
            respond_fail = RespondNote.objects.filter(category='payment status').values('failed_title_latin',
                                                                                    'failed_title_farsi',
                                                                                    'failed_text_latin',
                                                                                    'failed_text_farsi',
                                                                                    'failed_signature_latin',
                                                                                    'failed_signature_farsi')
            return Response({
                'status': 404,
                'message': 'fail',
                'respond': respond_fail,
                'data': ''
            })


class InvoiceCreateAPIView(generics.CreateAPIView):
    queryset = Invoice.objects.all()
    serializer_class = InvoiceSerializer