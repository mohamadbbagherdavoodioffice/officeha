from django.shortcuts import render
import random
from django.http import JsonResponse, HttpResponse
from django.contrib.auth.hashers import check_password
from django.db.models import Q
from rest_framework import status, viewsets, pagination
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet
from rest_framework.generics import CreateAPIView, get_object_or_404, ListAPIView, RetrieveAPIView, ListCreateAPIView
from .models import *
from .serializers import *
from Raw_root.serializers import CountryFlagSerializer
from Raw_root.models import Country


class HintNewsAPIView(ListAPIView):
    serializer_class = HintSerializer
    queryset = Hint.objects.filter(Q(item="News cat") | Q(item="news share")).all()


class BranchNumbersAPIView(ListAPIView):
    serializer_class = BranchNumbersSerializer
    queryset = BranchNumbers.objects.all()


class StaticTextFeedbackAPIView(ListAPIView):
    serializer_class = StaticTextSerializer
    queryset = StaticText.objects.filter(type='feedback').all()


class StaticTextOurServicesAPIView(ListAPIView):
    serializer_class = StaticTextSerializer
    queryset = StaticText.objects.filter(type='Our services').all()

class HintApplyFormAPIView(ListAPIView):
    serializer_class = HintSerializer
    queryset = Hint.objects.filter(Q(item="Apply form") ).all()


class StaticTextVisaFormsAPIView(ListAPIView):
    serializer_class = StaticTextSerializer
    queryset = StaticText.objects.filter(type='Visa forms').all()

class StaticAsiaMiddleAPIView(ListAPIView):
    serializer_class = StaticTextSerializer
    queryset = StaticText.objects.filter(type='Asia & Middle').all()

class HintWizardAPIView(APIView):
    def get(self, request):
        dic = {}

        hint = Hint.objects.filter(item=self.request.GET.get('item')).values()
        dic["hint"] = hint

        return Response({"message": "success", "status": "OK", "data": dic}, status=200)


class StaticTextListTypeAPIView(APIView):
    def get(self, request):
        dic = {}
        dic =[ 'Our services',
            'Passenger right main',
            'feedback',
            'Claim your rights',
            'Submit payments',
            'payment Status',
            'Retrieve Application Number',
            'Application Status Check',
            'Explore programs',
            'Travel requirements upper',
            'Travel requirements lower',
            'Careers', 'Careers',
            'Visa forms',
            'Asia & Middle',
            'Articles',
            'Things to do',
            'Training',
            'PAX-topics',
            'Consultation',
            'Evaluation']
        return Response({"message": "success", "status": "OK", "data": dic}, status=200)

    
    
class StaticDyanamicAPIView(ListAPIView):

    serializer_class = StaticTextSerializer

    def get_queryset(self):
        # send me type of each static text as dyanamic you can get list type in api: api/public/get_static_text_with_type
        text_type = self.request.GET.get('type')
        return StaticText.objects.filter(type=text_type)



class BranchNewAPI(APIView):
    def get(self, request):
        branches = Branch.objects.all()
        data = []

        for branch in branches:
            branch_data = {
                "id": branch.id,
                "type": branch.type,
                "branch_title_latin": branch.branch_title_latin,
                "branch_title_farsi": branch.branch_title_farsi,
                "address_title_latin": branch.address_title_latin,
                "address_title_farsi": branch.address_title_farsi,
                "postal_code": branch.postal_code,
                "working_days": branch.working_days,
                "hours_of_operation": branch.hours_of_operation,
                "branch_numbers": []
            }

            branch_numbers = BranchNumbers.objects.filter(branch=branch)
            
            for branch_number in branch_numbers:
                branch_data["branch_numbers"].append({
                    "tel_number": branch_number.tel_number
                })

            data.append(branch_data)

        return Response(data)
    




class EvaluationAPIView(ListAPIView):
    serializer_class = EvaluationSerializer
    queryset = Evaluation.objects.all()



class EvaluationCountryAPIView(APIView):
    def get(self, request, format=None):
        evaluations = Evaluation.objects.all()
        country_ids = evaluations.values_list('country_id', flat=True).distinct()
        countries = Country.objects.filter(id__in=country_ids)
        serializer = CountryFlagSerializer(countries, many=True)
        return Response(serializer.data)
    