import datetime
import random
from django.db import models
from django.utils.crypto import get_random_string
from persiantools.jdatetime import JalaliDateTime
from ApplicationOperation.models import ApplicationProfile
from django.utils import timezone

def get_payment_receipt_file(instance, filename):
    return f"file_payment_receipt_file/{filename}"


class ExternalPayment(models.Model):
    application_nr = models.ForeignKey('ApplicationOperation.ApplicationProfile', on_delete=models.PROTECT, null=True,
                                       blank=True)
    payment_code = models.CharField(max_length=300, null=True, blank=True)
    first_name = models.CharField(max_length=300, null=True, blank=True)
    last_name = models.CharField(max_length=300, null=True, blank=True)
    nid = models.CharField(max_length=300, null=True, blank=True)
    tel = models.CharField(max_length=300, null=True, blank=True)
    customer_note = models.TextField(null=True, blank=True)
    debit_card_number = models.CharField(max_length=16, null=True, blank=True)
    currency = models.ForeignKey('Raw_root.Currency', on_delete=models.CASCADE, null=True, blank=True)
    amount = models.DecimalField(max_digits=10, decimal_places=2, null=True, blank=True)
    # invoice = models.ForeignKey('Financial.Invoice', on_delete=models.CASCADE, null=True, blank=True)
    payment_receipt_file = models.ImageField(upload_to=get_payment_receipt_file, null=True, blank=True)
    # app_invoices = models.ForeignKey('ApplicationOperation.AppInvoice', on_delete=models.CASCADE, null=True, blank=True)
    external_payment_status = models.ForeignKey('Raw_root.ExternalPaymentStatus', on_delete=models.CASCADE, null=True,
                                                blank=True)
    refused_note = models.TextField(null=True, blank=True)
    action = models.ForeignKey('Raw_root.Action', on_delete=models.CASCADE, null=True, blank=True)

    def __str__(self):
        return self.payment_code

    # def save(self, *args, **kwargs):
    #     if not self.payment_code:
    #     # if self.payment_code is None:
    #         not_unique = True
    #         while not_unique:
    #             uni_random = "X" + str(datetime.datetime.now().year)[2:4] \
    #                          + str(self.tel)[-3:] + str(get_random_string(length=6, allowed_chars='123456789'))
    #             if not ExternalPayment.objects.filter(payment_code=uni_random):
    #                 not_unique = False
    #                 self.payment_code = str(uni_random)
    #                 super(ExternalPayment, self).save(*args, **kwargs)
    #     else:
    #         super(ExternalPayment, self).save(*args, **kwargs)


    def save(self, *args, **kwargs):
        if not self.payment_code:
            year = datetime.datetime.now().strftime('%y')
            random_str = ''.join(random.choices('PYBAEK', k=1))
            rand_num = ''.join(random.choices('WQDVMJRUGZC', k=1))
            rand_digits = ''.join(random.choices('0123456789', k=2))
            self.payment_code = f'X{year}{random_str}00000{rand_num}{rand_digits}'
            print(self.payment_code)
            super().save(*args, **kwargs)
        else:
            super(ExternalPayment, self).save(*args, **kwargs)


def get_file_beneficiary_id_card(instance, filename):
    return f"file_beneficiary_id_card/{filename}"


def get_file_financial_evidence(instance, filename):
    return f"file_financial_evidence/{filename}"


class OurBuy(models.Model):
    type = models.CharField(choices=(('Application', 'Application'), ('Tour', 'Tour'), ('General', 'General')),
                            max_length=20, null=True, blank=True)
    transaction_group = models.ForeignKey('Raw_root.TransactionGroup', on_delete=models.CASCADE, null=True, blank=True)
    transaction_date_time = models.DateTimeField(null=True, blank=True)
    application_number = models.ForeignKey('ApplicationOperation.ApplicationProfile', on_delete=models.CASCADE,
                                           null=True, blank=True)
    applicant = models.ForeignKey('ApplicationOperation.Applicant', on_delete=models.CASCADE, null=True, blank=True, )
    tour_operation = models.ForeignKey('Organization.TourOperationTask', on_delete=models.CASCADE, null=True,
                                       blank=True, )
    task = models.ForeignKey('Organization.Task', on_delete=models.CASCADE, null=True, blank=True)
    description = models.TextField(null=True, blank=True)
    currency = models.ForeignKey('Raw_root.Currency', on_delete=models.CASCADE, null=True, blank=True)
    amount = models.DecimalField(max_digits=10, decimal_places=2, null=True, blank=True)
    financial_evidence = models.ImageField(upload_to=get_file_financial_evidence, null=True, blank=True)
    TYPE = (
        ('IBAN', 'IBAN'),
        ('CC', 'CC'),
        ('Online', 'Online'),
        ('Cash', 'Cash'),
    )
    payment_methods = models.CharField(max_length=30, choices=TYPE, null=True, blank=True)
    from_account_bank = models.ForeignKey('Organization.Bank', on_delete=models.CASCADE, null=True, blank=True,
                                          default=None)

    beneficiary_business_user = models.ForeignKey('OfficeHauser.OfficeUser', on_delete=models.PROTECT,
                                                  related_name='our_by_benefiencery_user_set', null=True,
                                                  blank=True)
    beneficiary_name = models.CharField(max_length=300, null=True, blank=True)
    beneficiary_id_card = models.ImageField(upload_to=get_file_beneficiary_id_card, null=True, blank=True)
    beneficiary_account_iban = models.CharField(max_length=300, null=True, blank=True)
    beneficiary_cc = models.CharField(max_length=300, null=True, blank=True)

    by_staff = models.ForeignKey('OfficeHauser.OfficeUser', on_delete=models.CASCADE, primary_key=False,
                                 related_name='our_by_by_user_set', null=True, blank=True)
    self_note = models.TextField(null=True, blank=True)
    last_updated_date = models.DateTimeField(auto_now=True)


def get_app_invoice(instance, filename):
    return f"file_app_invoice/{filename}"


def get_file_bank_receipt_file_image(instance, filename):
    return f"file_bank_receipt_file_image/{filename}"


class Invoice(models.Model):
    METHOD_OF_PAYMENT = (
        ('No action needed', 'No action needed'),
        ('In-office', 'In-office'),
        ('Online', 'Online'),
        ('Cash', 'Cash'),
        ('POS', 'POS'),
        ('Bank-Transfer', 'Bank-Transfer'),
    )

    STATUS = (
        ('Neutral', 'Neutral'),
        ('Pay Queue', 'Pay Queue'),
        ('Wait approval', 'Wait approval'),
        ('PAID', 'PAID'),
        ('Discarded', 'Discarded'),
    )
    TRACE_CODE = (
        ('Neutral', 'Neutral'),
        ('Pay Queue', 'Pay Queue'),
        ('Waiting Approval', 'Waiting Approval'),
        ('PAID', 'PAID'),
        ('Discarded', 'Discarded'),
    )

    def create_new_ref_number():
        not_unique = True
        while not_unique:
            # number = ['1', '2', '3', '4', '5', '6', '7', '8', '9']
            # nu1 = random.randint(0, len(number) - 1)
            # nu2 = random.randint(0, len(number) - 1)
            # nu3 = random.randint(0, len(number) - 1)
            # c = Invoice.objects.count()

            # rna = 0
            # if int(str(JalaliDateTime.now())[5:7]) <= 6:
            #     rna = random.randint(5, 9)
            # if int(str(JalaliDateTime.now())[5:7]) > 6:
            #     rna = random.randint(0, 5)

            # uni_random = "IV" + "USVP" + str(JalaliDateTime.now())[5:7] + number[nu1] + number[nu2] \
            #              + number[nu3] + str(c).zfill(4) + str(rna)

            year = timezone.now().strftime('%Y')
            rand_chars = ''.join(random.choices('HCTGEPUBMLSW', k=5))
            rand_nums = str(random.randint(0, 999)).zfill(3)
            rand_letters = ''.join(random.choices('AVZXQRODFKN', k=1))
            zeros = '00000'

            uni_random = f'INV{year}{rand_chars}{zeros}{rand_nums}{rand_letters}'

            if not Invoice.objects.filter(invoice_nr=uni_random):
                not_unique = False
            
        return str(uni_random)

    type = models.CharField(choices=(('Application', 'Application'), ('Tour', 'Tour'), ('Other', 'Other')),
                            max_length=20, null=True, blank=True)
    status = models.CharField(max_length=40, choices=STATUS, null=True, blank=True)
    status = models.CharField(max_length=400, choices=TRACE_CODE, null=True, blank=True)
    invoice_group = models.ForeignKey('Raw_root.InvoiceGroup', on_delete=models.CASCADE, null=True, blank=True,
                                      default=None)
    invoice_nr = models.CharField(max_length=50,
                                  blank=True,
                                  editable=False,
                                  unique=True,
                                  default=create_new_ref_number)
    application_profile = models.ForeignKey(ApplicationProfile, on_delete=models.CASCADE, null=True,
                                           blank=True, default=None)
    instalment = models.ForeignKey('Services.Instalment', on_delete=models.CASCADE, null=True, blank=True)
    t_service = models.ForeignKey('Raw_root.Tservice', on_delete=models.PROTECT, null=True, blank=True, default=None)
    mode_of_payment = models.ForeignKey('Raw_root.ModeOfPayment', on_delete=models.CASCADE, null=True, blank=True)
    action = models.ForeignKey('Raw_root.Action', on_delete=models.CASCADE, null=True, blank=True, default=None)
    currency = models.ForeignKey('Raw_root.Currency', on_delete=models.CASCADE, null=True, blank=True, default=None)
    unit_fee = models.DecimalField(max_digits=10, decimal_places=2, null=True, blank=True)
    # ex_rate = models.ForeignKey('Organization.ExRate', on_delete=models.CASCADE, null=True, blank=True, default=None)
    # irr_amount = models.DecimalField(max_digits=10, decimal_places=2, null=True, blank=True, default=None)
    # vat = models.ForeignKey('Organization.Vat', on_delete=models.PROTECT, null=True, blank=True)
    quantity = models.IntegerField(choices=((1, 1), (2, 2), (3, 3), (4, 4), (5, 5)), null=True, blank=True)
    total = models.DecimalField(max_digits=12, decimal_places=2, null=True, blank=True)
    account_number = models.ForeignKey('Organization.AccountNumber', on_delete=models.CASCADE, null=True, blank=True,
                                       default=None)
    pos_bank = models.ForeignKey('Organization.Bank', on_delete=models.CASCADE, null=True, blank=True)
    payment_date_time = models.DateTimeField(null=True, blank=True)
    discard_date_time = models.DateTimeField(null=True, blank=True)
    account_number = models.ForeignKey('Organization.AccountNumber', on_delete=models.CASCADE, null=True,
                                       blank=True)
    remittance_date_time = models.DateTimeField(null=True, blank=True)
    trace_code = models.CharField(max_length=20, null=True, blank=True)
    external_payments = models.ForeignKey('Financial.ExternalPayment', related_name='invoice_external_payment_set',
                                          on_delete=models.CASCADE, null=True, blank=True)
    bank_receipt_file_image = models.ImageField(upload_to=get_file_bank_receipt_file_image, null=True, blank=True)
    by_staff = models.ForeignKey('OfficeHauser.OfficeUser', on_delete=models.PROTECT,
                                 related_name='app_invoice_user_set', null=True, blank=True)
    self_note = models.TextField(null=True, blank=True)
    last_edit = models.DateTimeField(null=True, blank=True)

    def __str__(self):
        return "%s | %s , %s " % (self.invoice_nr, self.status, self.total)


def get_app_invoice_balance_payment_by_user(instance, filename):
    return f"file_invoice_balance_payment_by_user/{filename}"


class RefundCancellation(models.Model):
    tour_pax = models.ForeignKey('Tour.TourPAX', on_delete=models.CASCADE, null=True, blank=True, )
    cancellation_date_time = models.DateTimeField(null=True, blank=True)
    cancellation_rate = models.ForeignKey('Raw_root.CancellationRate', on_delete=models.CASCADE, null=True,
                                          blank=True, )
    total_fee = models.DateTimeField(null=True, blank=True)
    deducted_amount_admin = models.DateTimeField(null=True, blank=True)
    deducted_penalty = models.DateTimeField(null=True, blank=True)
    refundable_amount = models.DateTimeField(null=True, blank=True)


class UserLedger(models.Model):
    tour_pax = models.ForeignKey('Tour.TourPAX', on_delete=models.CASCADE, null=True, blank=True, )
    user = models.ForeignKey('OfficeHauser.OfficeUser', on_delete=models.CASCADE, null=True, blank=True, )
    status = models.CharField(max_length=20, choices=(('Auto', 'Auto'), ('manual', 'manual')), null=True, blank=True, )
    note = models.TextField(blank=True, null=True)
    currency = models.ForeignKey("Raw_root.Currency", on_delete=models.CASCADE, null=True, blank=True, default=None)
    # code = models.CharField(max_length=20, choices=(('Balance id', 'Balance id'), ('Refund list id', 'Refund list id')
    #                                                 ,('App. Our buy Opt.id','App. Our buy Opt.id'),('Tour-Operation id','Tour-Operation id')),null=True, blank=True, )
    balance = models.ForeignKey("OfficeHauser.Balance", on_delete=models.CASCADE, null=True, blank=True)
    refund = models.ForeignKey("Raw_root.RefundCondition", on_delete=models.CASCADE, null=True, blank=True)
    our_buy = models.ForeignKey("Financial.OurBuy", on_delete=models.CASCADE, null=True, blank=True)
    tour_arrangment = models.ForeignKey("Tour.TourArrangment", on_delete=models.CASCADE, null=True, blank=True)
    external_payment = models.ForeignKey("Financial.ExternalPayment", on_delete=models.CASCADE, null=True, blank=True)
    debit_amount = models.DecimalField(max_digits=10, decimal_places=2, null=True, blank=True)
    credit_amount = models.DecimalField(max_digits=10, decimal_places=2, null=True, blank=True)
    engaged_bank_account = models.ForeignKey('Organization.AccountNumber', on_delete=models.CASCADE, null=True,
                                             blank=True, default=None)
    invoice = models.ForeignKey('Financial.Invoice', on_delete=models.CASCADE, null=True, blank=True)
    refund_cancellation = models.ForeignKey("Financial.RefundCancellation", on_delete=models.CASCADE, null=True,
                                            blank=True)


def get_balance_request_request_by_user(instance, filename):
    return f"file_balance_request_request_by_user/{filename}"


class BalanceRequest(models.Model):
    user = models.ForeignKey('OfficeHauser.OfficeUser', on_delete=models.CASCADE, null=True, blank=True, )
    requested_date_time = models.DateTimeField(null=True, blank=True)
    admin_approval = models.BooleanField(default=False)
    approval_date_time = models.DateTimeField(null=True, blank=True)
    remitance_date_time = models.DateTimeField(null=True, blank=True)
    from_bank = models.ForeignKey('Organization.AccountNumber', on_delete=models.CASCADE, null=True, blank=True, )

    bank_trace_code = models.CharField(max_length=200, null=True, blank=True)
    evidence_file = models.ImageField(upload_to=get_balance_request_request_by_user, null=True, blank=True)
    note = models.TextField(null=True, blank=True)
    status = models.CharField(max_length=20,
                              choices=(('Requested', 'Requested'), ('Waiting', 'Waiting'), ('Approved', 'Approved'),
                                       ('Remittance queue', 'Remittance queue'), ('PAID', 'PAID')), null=True,
                              blank=True)

    expired_date_time = models.DateTimeField(null=True, blank=True)
    by_staff_user = models.ForeignKey('OfficeHauser.OfficeUser', on_delete=models.PROTECT,
                                      related_name='tour_balance_requested_by_user_set', null=True, blank=True)
    self_note = models.TextField(null=True, blank=True)
    last_updated_date = models.DateTimeField(auto_now=True)


def get_file_fund_circulation_recipt_file(instance, filename):
    return f"file_fund_circulation_recipt_file/{filename}"


class FundCirculation(models.Model):
    cash_man = models.ForeignKey('OfficeHauser.OfficeUser', on_delete=models.CASCADE, null=True, blank=True, )
    tour_crew = models.ForeignKey('Tour.TourCrew', on_delete=models.PROTECT, null=True, blank=True, )
    transaction_type = models.CharField(max_length=40, choices=(('Fund', 'Fund'), ('Expenses', 'Expenses')), null=True,
                                        blank=True, )
    date_time = models.DateTimeField(null=True, blank=True)
    receipt_file = models.ImageField(upload_to=get_file_fund_circulation_recipt_file, null=True, blank=True)
    mode_of_pay = models.CharField(max_length=40, choices=(('Cash', 'Cash'), ('Pos', 'Pos'), ('C2C', 'C2C')), null=True,
                                   blank=True, )
    note = models.TextField(null=True, blank=True)
    approval = models.BooleanField(default=False)

class InvoiceItem(models.Model):
    invoice = models.ForeignKey(Invoice, on_delete=models.CASCADE, null=True, blank=True, )
    instalment = models.ForeignKey('Services.instalment', on_delete=models.PROTECT, null=True, blank=True, )
    t_service = models.ForeignKey('Raw_root.Tservice', on_delete=models.PROTECT, null=True, blank=True, )
    
    unit_fee = models.FloatField(default=0, null=True, blank=True)
    quantity = models.IntegerField(default=0, null=True, blank=True)
    total = models.FloatField( null=True, blank=True)
    
    def save(self, *args, **kwargs):
        self.total = self.unit_fee * self.quantity
        super().save(*args, **kwargs)

    
