from django.shortcuts import render
from django.shortcuts import render
import random
from django.http import JsonResponse, HttpResponse
from django.contrib.auth.hashers import check_password
from django.db.models import Q
from rest_framework import status, viewsets, pagination
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet
from rest_framework.generics import CreateAPIView, get_object_or_404, ListAPIView, RetrieveAPIView, ListCreateAPIView
from .models import *
from .serializers import *

class CountryAPIView(ListAPIView):
    serializer_class = CountrySerializer
    queryset = Country.objects.all()


class TourElementAPIView(ListAPIView):
    serializer_class = TourElementSerializer
    queryset = TourElement.objects.all()

class ContactUsTypeAPIView(ListAPIView):
    serializer_class = ContactUsTypeSerializer
    queryset = ContactUsType.objects.all()

class TellUsWhatYouThinkCatAPIView(ListAPIView):
    serializer_class = TellUsWhatYouThinkCatSerializer
    queryset = TellUsWhatYouThinkCat.objects.all()

class AgeRangeAPIView(ListAPIView):
    serializer_class = AgeRangeSerializer
    queryset = AgeRange.objects.all()


class EduProgramDegreeAPIView(ListAPIView):
    serializer_class = EduProgramDegreeSerializer
    queryset = EduProgramDegree.objects.all().order_by('id')


class EduSubjectAPIView(ListAPIView):
    serializer_class = EduSubjectSerializer
    queryset = EduSubject.objects.all()


class AvailableFundAPIView(ListAPIView):
    serializer_class = AvailableFundSerializer
    queryset = AvailableFund.objects.all()


class ProvinceAPIView(ListAPIView):
    serializer_class = ProvinceSerializer
    queryset = Province.objects.all()


class FollowUpStatusAPIView(ListAPIView):
    serializer_class = FollowUpStatusSerializer
    queryset = FollowUpStatus.objects.all()


class VisaMainCatAPIView(ListAPIView):
    serializer_class = VisaMainCatSerializer
    queryset = VisaMainCat.objects.all()


class ExternalPaymentStatusGetRetrieveAPIView(RetrieveAPIView):
    serializer_class = ExternalPaymentStatusSerializer
    queryset = ExternalPaymentStatus.objects.all()
    lookup_url_kwarg = "payment_id"


class ExternalPaymentStatusGetAPIView( ListAPIView):
    serializer_class = ExternalPaymentStatusSerializer
    queryset = ExternalPaymentStatus.objects.all()





class AgeRangeAPIViewForHr(ListAPIView):
    serializer_class = AgeRangeSerializer
    queryset = AgeRange.objects.filter(id__gte=4).filter(id__lte=13)


class RegionIranAPIView(APIView):
    def get(self, request):
        dic = {}

        country = Region.objects.filter(country__name_latin="IRAN").values('id','title_latin','title_farsi')
        dic["country"] = list(country)

        return Response({"message": "success", "status": "OK", "data": dic}, status=200)

class ProvinceByCountryAPIView(APIView):
    def get(self, request):
        dic = {}
        country_id = self.request.GET.get("country_id")
        province = Province.objects.filter(country=country_id).values('id','title_latin','title_farsi')
        dic = province

        return Response({"message": "success", "status": "OK", "data": dic}, status=200)



class RegionByCountryAPIView(APIView):
    def get(self, request):
        dic = {}
        country_id = self.request.GET.get("country_id")
        region = Region.objects.filter(country=country_id).values('id','title_latin','title_farsi')
        dic= region

        return Response({"message": "success", "status": "OK", "data": dic}, status=200)

class EventCatSerializerGetAPIView( ListAPIView):
    serializer_class = EventCatSerializer
    queryset = EventCat.objects.all()


class MinEmploymentYearGetAPIView( ListAPIView):
    serializer_class = MinEmploymentYearSerializer
    queryset = MinEmploymentYear.objects.all()


class MaritalGetAPIView(ListAPIView):
    serializer_class = MaritalSerializer
    queryset = Marital.objects.all()

class OccupationGetAPIView(ListAPIView):
    serializer_class = OccupationSerializer
    queryset = Occupation.objects.all()

class LanguageGetAPIView(ListAPIView):
    serializer_class = LanguageSerializer
    queryset = Language.objects.all()

class RoomTypeGetAPIView(ListAPIView):
    serializer_class = RoomTypeSerializer
    queryset = RoomType.objects.all()

class GlobalZonesArabianAPiView(ListAPIView):
    serializer_class = GlobalZoneSerializer

    def get_queryset(self):
        return GlobalZone.objects.filter(country__visastream__isnull=False, title_latin='Arabian countries').distinct()