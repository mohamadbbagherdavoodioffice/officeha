from django.db import models

from Raw_root.models import Currency


def get_file_service_category_banner_image(instance, filename):
    return f"file_service_category_banner_image/{filename}"


def get_file_service_category_thumbnail(instance, filename):
    return f"file_service_category_thumbnail/{filename}"


def get_file_service_cat_file_icon(instance, filename):
    return f"_fileservice_cat_file_icon/{filename}"


class ServiceCategory(models.Model):
    show_priority = models.BooleanField(default=False)
    appointment = models.BooleanField(default=False)
    abbreviation_code = models.CharField(max_length=20, null=True, blank=True)
    icon = models.ImageField(upload_to=get_file_service_cat_file_icon, null=True, blank=True)
    country = models.ForeignKey('Raw_root.Country', on_delete=models.CASCADE,
                                related_name='service_category_country_set', null=True, blank=True, default=None)
    title_latin = models.CharField(max_length=500, null=True, blank=True)
    title_farsi = models.CharField(max_length=500, null=True, blank=True)
    account_number = models.ForeignKey("Organization.AccountNumber", on_delete=models.CASCADE, null=True, blank=True)
    processing_time_farsi = models.CharField(max_length=500, null=True, blank=True)
    processing_time_latin = models.CharField(max_length=500, null=True, blank=True)
    declaration_forms = models.ForeignKey('Organization.DeclarationForms', on_delete=models.CASCADE,
                                          related_name='service_category_decleration_form_set', null=True, blank=True)
    availability_from_date_time = models.DateTimeField(null=True, blank=True)
    availability_to_date_time = models.DateTimeField(null=True, blank=True)
    deadline_submission_of_application = models.PositiveIntegerField(null=True, blank=True)
    currency = models.ForeignKey("Raw_root.Currency", on_delete=models.PROTECT, null=True, blank=True, default=None)
    vat = models.DecimalField(max_digits=10, decimal_places=2, null=True, blank=True)
    fee = models.DecimalField(max_digits=10, decimal_places=2, null=True, blank=True)
    pay_online = models.BooleanField(default=False)
    emergency_note = models.TextField(null=True, blank=True)
    expiry_date_emergency_note = models.DateTimeField(null=True, blank=True)
    banner_image = models.ImageField(upload_to=get_file_service_category_banner_image, null=True, blank=True)
    alt_text_banner = models.TextField(null=True, blank=True)
    thumbnail = models.ImageField(upload_to=get_file_service_category_thumbnail, null=True, blank=True)
    alt_text_thumb = models.TextField(null=True, blank=True)
    alt_text = models.TextField(null=True, blank=True)
    tags = models.TextField(null=True, blank=True)

    def __str__(self):
        return "%s " % (self.title_latin)


class SubServiceCategory(models.Model):
    service_category = models.ForeignKey(ServiceCategory, on_delete=models.CASCADE, null=True, blank=True, default=None)
    country = models.ForeignKey('Raw_root.Country', on_delete=models.PROTECT, null=True, blank=True, default=None)
    title_latin = models.CharField(max_length=500, null=True, blank=True)
    title_farsi = models.CharField(max_length=500, null=True, blank=True)
    currency = models.ForeignKey("Raw_root.Currency", on_delete=models.PROTECT, null=True, blank=True, default=None)
    vat = models.DecimalField(max_digits=10, decimal_places=2, null=True, blank=True)
    fee = models.DecimalField(max_digits=10, decimal_places=2, null=True, blank=True)
    pay_online = models.BooleanField(default=False)
    validity_from = models.DateTimeField(null=True, blank=True)
    validity_to = models.DateTimeField(null=True, blank=True)
    tags = models.TextField(null=True, blank=True)

    def __str__(self):
        return "%s / %s / %s" % (self.service_category.title_latin, self.country.name_latin, self.title_latin)


class ServiceCatFeature(models.Model):
    service_category = models.ForeignKey('Services.ServiceCategory', on_delete=models.CASCADE, null=True, blank=True,
                                         default=None)
    seq = models.PositiveIntegerField(null=True, blank=True)
    title_latin = models.CharField(max_length=500, null=True, blank=True)
    title_farsi = models.CharField(max_length=500, null=True, blank=True)

    def __str__(self):
        if self.service_category == None:
            s2 = ""
        else:
            s2 = self.service_category.title_latin

        return "%s | %s" % (self.title_latin, s2)


class IncludedExcluded(models.Model):
    type = models.CharField(max_length=30, choices=(('Included', 'Included'), ('Excluded', 'Excluded')), null=True,
                            blank=True)
    # tour = models.ForeignKey('Tour.Tour', on_delete=models.PROTECT, null=True, blank=True,default=None)
    service_category = models.ForeignKey(ServiceCategory, on_delete=models.CASCADE, null=True, blank=True)
    sub_service_cat = models.ForeignKey('Services.SubServiceCategory', on_delete=models.CASCADE, null=True, blank=True)
    t_service = models.ManyToManyField('Raw_root.Tservice', blank=True)

    def __str__(self):
        return "%s | %s" % (self.type, self.service_category.title_latin)
        #     t- service latin , tour latin


class Instalment(models.Model):
    TYPE = (
        ('In-Office', 'In-Office'),
        ('Online', 'Online'),
        ('Cash', 'Cash'),
        ('POS', 'POS'),
        ('Bank_Transfer', 'Bank_Transfer'),
    )
    stid = models.ForeignKey('Visa.VisaStream', on_delete=models.PROTECT, null=True, blank=True, default=None)
    sub_stid = models.ForeignKey('Visa.SubVisaStream', on_delete=models.PROTECT, null=True, blank=True, default=None)
    service_category = models.ForeignKey(ServiceCategory, on_delete=models.CASCADE, null=True, blank=True)
    sub_service_cat = models.ForeignKey('Services.SubServiceCategory', on_delete=models.CASCADE, null=True, blank=True)
    tour = models.ForeignKey('Tour.Tour', on_delete=models.PROTECT, null=True, blank=True, default=None)
    seq = models.PositiveIntegerField(null=True, blank=True)
    title_latin = models.CharField(max_length=500, null=True, blank=True)
    title_farsi = models.CharField(max_length=500, null=True, blank=True)
    full_fee = models.BooleanField(default=False)

    currency = models.ForeignKey("Raw_root.Currency", on_delete=models.CASCADE, null=True, blank=True)
    unit_fee = models.DecimalField(max_digits=12, decimal_places=2, null=True, blank=True)
    account_number = models.ForeignKey("Organization.AccountNumber", on_delete=models.CASCADE, null=True, blank=True)
    refundable = models.BooleanField(default=False)
    refund_policy = models.ForeignKey("Raw_root.RefundCondition", on_delete=models.CASCADE, null=True, blank=True)
    # mode_of_payment = models.CharField(max_length=30, choices=TYPE, null=True, blank=True)
    mode_of_payment = models.ForeignKey("Raw_root.ModeOfPayment", on_delete=models.CASCADE, null=True, blank=True)

    def __str__(self):
        if self.stid == None:
            s1 = ""
        else:
            s1 = self.stid.title_latin
        if self.service_category == None:
            s2 = ""
        else:
            s2 = self.service_category.title_latin

        if self.tour == None:
            s3 = ""
        else:
            s3 = self.tour.title_latin

        return f'{s1}, {s2}, {s3},,{self.title_latin},{self.seq},{self.unit_fee} , '


class RequiredDocumnet(models.Model):
    PROVIDE_MODE = (
        ("Upload", "Upload"),
        ("Third Party", "Third Party"),
        ("Physically", "Physically"),
    )
    tour = models.ForeignKey('Tour.Tour', on_delete=models.PROTECT, null=True, blank=True, default=None)
    service_category = models.ForeignKey(ServiceCategory, on_delete=models.CASCADE, null=True, blank=True)
    stid = models.ForeignKey('Visa.VisaStream', on_delete=models.PROTECT, null=True, blank=True, default=None)
    doc = models.ManyToManyField("Raw_root.Document", blank=True)
    guarantee = models.ForeignKey("Organization.GuaranteeAmount", on_delete=models.CASCADE, null=True, blank=True,
                                  default=None)
    visa_forms = models.ManyToManyField('Content.VisaForms', blank=True)
    translation = models.ForeignKey("Raw_root.Translation", on_delete=models.PROTECT, null=True, blank=True,
                                    default=None)
    # provide_mode = models.CharField(max_length=300, choices=PROVIDE_MODE, null=True, blank=True)
    provide_mode = models.ForeignKey("Raw_root.ProvidingMode", on_delete=models.PROTECT, null=True, blank=True)
    provide_text_latin = models.TextField(null=True, blank=True)
    provide_text_farsi = models.TextField(null=True, blank=True)

    def __str__(self):
        if self.stid == None:
            s1 = ""
        else:
            s1 = self.stid.title_latin
        if self.service_category == None:
            s2 = ""
        else:
            s2 = self.service_category.title_latin

        if self.tour == None:
            s3 = ""
        else:
            s3 = self.tour.title_latin

        return f'{s1}, {s2}, {s3}, '


class Eligibility(models.Model):
    service_category = models.ForeignKey(ServiceCategory, on_delete=models.CASCADE, null=True, blank=True)
    stid = models.ForeignKey('Visa.VisaStream', on_delete=models.PROTECT, null=True, blank=True, default=None)
    title_latin = models.CharField(max_length=500, null=True, blank=True)
    title_farsi = models.CharField(max_length=500, null=True, blank=True)
    text_latin = models.TextField(null=True, blank=True)
    text_farsi = models.TextField(null=True, blank=True)

    title_latin2 = models.CharField(max_length=500, null=True, blank=True)
    title_farsi2 = models.CharField(max_length=500, null=True, blank=True)
    text_latin2 = models.TextField(null=True, blank=True)
    text_farsi2 = models.TextField(null=True, blank=True)

    title_latin3 = models.CharField(max_length=500, null=True, blank=True)
    title_farsi3 = models.CharField(max_length=500, null=True, blank=True)
    text_latin3 = models.TextField(null=True, blank=True)
    text_farsi3 = models.TextField(null=True, blank=True)

    title_latin4 = models.CharField(max_length=500, null=True, blank=True)
    title_farsi4 = models.CharField(max_length=500, null=True, blank=True)
    text_latin4 = models.TextField(null=True, blank=True)
    text_farsi4 = models.TextField(null=True, blank=True)

    def __str__(self):
        if self.stid == None:

            s1 = ""
        else:
            s1 = self.stid.title_latin
        if self.service_category == None:
            s2 = ""
        else:
            s2 = self.service_category.title_latin

        return f'{s1}, {s2}, {self.title_latin}, '


class Guide(models.Model):
    service_category = models.ForeignKey(ServiceCategory, on_delete=models.CASCADE, null=True, blank=True, default=None)
    stid = models.ForeignKey('Visa.VisaStream', on_delete=models.PROTECT, null=True, blank=True, default=None)
    title_latin = models.CharField(max_length=500, null=True, blank=True)
    title_farsi = models.CharField(max_length=500, null=True, blank=True)
    text_latin = models.TextField(null=True, blank=True)
    text_farsi = models.TextField(null=True, blank=True)

    title_latin2 = models.CharField(max_length=500, null=True, blank=True)
    title_farsi2 = models.CharField(max_length=500, null=True, blank=True)
    text_latin2 = models.TextField(null=True, blank=True)
    text_farsi2 = models.TextField(null=True, blank=True)

    title_latin3 = models.CharField(max_length=500, null=True, blank=True)
    title_farsi3 = models.CharField(max_length=500, null=True, blank=True)
    text_latin3 = models.TextField(null=True, blank=True)
    text_farsi3 = models.TextField(null=True, blank=True)

    title_latin4 = models.CharField(max_length=500, null=True, blank=True)
    title_farsi4 = models.CharField(max_length=500, null=True, blank=True)
    text_latin4 = models.TextField(null=True, blank=True)
    text_farsi4 = models.TextField(null=True, blank=True)

    def __str__(self):
        if self.stid == None:
            s1 = ""
        else:
            s1 = self.stid.title_latin
        if self.service_category == None:
            s2 = ""
        else:
            s2 = self.service_category.title_latin

        return f'{s1}, {s2}, {self.title_latin}, '


class Aboute(models.Model):
    service_category = models.ForeignKey(ServiceCategory, on_delete=models.CASCADE, null=True, blank=True)
    stid = models.ForeignKey('Visa.VisaStream', on_delete=models.PROTECT, null=True, blank=True, default=None)
    title_latin = models.CharField(max_length=500, null=True, blank=True)
    title_farsi = models.CharField(max_length=500, null=True, blank=True)
    text_latin = models.TextField(null=True, blank=True)
    text_farsi = models.TextField(null=True, blank=True)

    title_latin2 = models.CharField(max_length=500, null=True, blank=True)
    title_farsi2 = models.CharField(max_length=500, null=True, blank=True)
    text_latin2 = models.TextField(null=True, blank=True)
    text_farsi2 = models.TextField(null=True, blank=True)

    title_latin3 = models.CharField(max_length=500, null=True, blank=True)
    title_farsi3 = models.CharField(max_length=500, null=True, blank=True)
    text_latin3 = models.TextField(null=True, blank=True)
    text_farsi3 = models.TextField(null=True, blank=True)

    title_latin4 = models.CharField(max_length=500, null=True, blank=True)
    title_farsi4 = models.CharField(max_length=500, null=True, blank=True)
    text_latin4 = models.TextField(null=True, blank=True)
    text_farsi4 = models.TextField(null=True, blank=True)

    def __str__(self):
        if self.stid == None:
            s1 = ""
        else:
            s1 = self.stid.title_latin
        if self.service_category == None:
            s2 = ""
        else:
            s2 = self.service_category.title_latin

        return f'{s1}, {s2}, {self.title_latin}, '


class ConsultCapacity(models.Model):
    STATUS = (('Vacant', 'Vacant'), ('Booked', 'Booked'), ('Done', 'Done'), ('No Show', 'No Show'))
    # service_category = models.ForeignKey(ServiceCategory, on_delete=models.CASCADE, null=True, blank=True,default=None)
    staff_user = models.ForeignKey('OfficeHauser.OfficeUser', on_delete=models.CASCADE, primary_key=False,
                                   related_name='consult_capacity_staff_user_set', null=True,
                                   blank=True)
    consultation_date = models.DateField(null=True, blank=True)
    # from_time = models.DateTimeField(null=True, blank=True)
    # to_time = models.DateTimeField(null=True, blank=True)
    # consultation_mode = models.ForeignKey('Raw_root.ConsultationMode', on_delete=models.PROTECT, null=True, blank=True)
    # cons_cat = models.ForeignKey('Raw_root.ConsultationCategory', on_delete=models.PROTECT, null=True, blank=True,default=None)
    time_frame = models.ManyToManyField('Raw_root.TimeFrame', blank=True)
    center = models.ForeignKey('PublicPages.Branch', on_delete=models.PROTECT, null=True, blank=True, default=None)
    status = models.CharField(max_length=13, choices=STATUS, null=True, blank=True, default='Vacant')
    currency = models.ForeignKey('Raw_root.Currency', on_delete=models.CASCADE, null=True, blank=True, )
    fee = models.DecimalField(max_digits=10, decimal_places=2, null=True, blank=True)
    account_number = models.ForeignKey('Organization.AccountNumber', on_delete=models.CASCADE, null=True, blank=True)

    def __str__(self):
        return " %s  " % (self.consultation_date)
        # staff name  constl mode title  Centerlatin

# class ServiceCatAppointmentCapacity(models.Model):
#     STATUS = (('Vacant', 'Vacant'),('Booked', 'Booked'),('Done', 'Done'),('No Show', 'No Show'))
#
#     center = models.ForeignKey('Content.Centers', on_delete=models.PROTECT, null=True, blank=True,default=None)
#     service_category = models.ForeignKey(ServiceCategory, on_delete=models.CASCADE, null=True, blank=True,default=None)
#     staff_user = models.ForeignKey('OfficeHauser.OfficeUser', on_delete=models.CASCADE, primary_key=False,
#                                       related_name='service_cat_appointment_staff_user_set', null=True,
#                                       blank=True)
#     appointment_from_time = models.DateTimeField(null=True, blank=True)
#     appointment_to_time = models.DateTimeField(null=True, blank=True)
#     consultation_mode= models.ForeignKey('Raw_root.ConsultationMode', on_delete=models.PROTECT, null=True, blank=True,default=None)
#     status = models.CharField(max_length=13, choices=STATUS, null=True, blank=True, default='Vacant')
#
#     def __str__(self):
#         return "%s | , %s | %s , %s" % (self.service_category.title_latin, self.appointment_from_time, self.appointment_to_time,self.center,self.status)
#         # staff name

class Workshop(models.Model):
    workshop_title_latin = models.CharField(max_length=300, null=True, blank=True)
    workshop_title_farsi = models.CharField(max_length=300, null=True, blank=True)
    date_time = models.DateTimeField(null=True, blank=True)
    location_address = models.CharField(max_length=1000, null=True, blank=True)
    hotline = models.CharField(max_length=12, null=True, blank=True)
    capacity = models.IntegerField(null=True, blank=True)         
    delivery_in_person = models.BooleanField(default=False)
    delivery_online = models.BooleanField(default=False)
    has_invoice = models.BooleanField(default=False) 
    
    fee = models.CharField(max_length=300, null=True, blank=True)
    thumbnail_img = models.ImageField(null=True, blank=True)
    workshop_banner_img = models.ImageField(null=True, blank=True)
    workshop_booking_title_latin = models.CharField(max_length=300, null=True, blank=True)
    workshop_booking_title_farsi = models.CharField(max_length=300, null=True, blank=True)
    workshop_text_latin = models.TextField(max_length=1000, null=True, blank=True)
    workshop_text_farsi = models.TextField(max_length=1000, null=True, blank=True)

class ConsultScheduleGeneral(models.Model):
    consult_team = models.ForeignKey('Hr.ConsultTeam', on_delete=models.CASCADE)
    from_date = models.DateField()
    to_date = models.DateField()
    time_frames = models.ManyToManyField('Raw_root.TimeFrame', blank=True)
    def __str__(self):
        return " %s  " % (self.consult_team.full_name_latin)
