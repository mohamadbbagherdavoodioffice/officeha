from datetime import datetime
from django.apps import apps
from rest_framework.response import Response
from rest_framework.generics import ListAPIView, RetrieveAPIView, get_object_or_404
from django.db.models import Count
from rest_framework.views import APIView
from Tour.models import TourPageConfig, Tour, TourDepartureFare, TourToPack, DayTour
from Tour.serializers import TourPageConfigALLSerializer, TourDetailsSerializer, TourDepratureFareDetailsSerializer, \
    TourAllListSerializer, TourToPackSerializer,  DayTourDetails2Serializer
from django.db.models import CharField,TextField
from django.db.models import  Q
import json 
from Raw_root.models import EventCat
from django.db.models import F
from drf_yasg.utils import swagger_auto_schema
from drf_yasg import openapi
from .schemas import  * 

class TourPageConfigALLAPIView(ListAPIView):
    serializer_class = TourPageConfigALLSerializer
    queryset = TourPageConfig.objects.all()


class TourGetListCountryEventCatAPIView(APIView):
    def get(self, request):
        dic = {}
        tour_country = Tour.objects.annotate(num_offerings=Count('country' )).values('country__name_latin','country__name_farsi','country__id').distinct()
        dic["tour_country"] = list(tour_country)

        tour_province= Tour.objects.annotate(num_offerings=Count('province')).values('province__title_latin',
                                                                                    'province__title_farsi',
                                                                                    'province__id').distinct()
        dic["tour_province"] = list(tour_province)

        tour_event_cat = Tour.objects.annotate(num_offerings=Count('event_cat')).values('event_cat__title_latin',
                                                                                    'event_cat__title_farsi',
                                                                                    'event_cat__id').distinct()
        dic["tour_event_cat"] = list(tour_event_cat)

        return Response({"message": "success", "status": "OK", "data": dic}, status=200)


# class TourQueryRegionAndEventCatAPIVIEW(ListAPIView):
#     serializer_class = VisaStreamForCountryQuerySerializer
#
#     def get_queryset(self):
#         objs = VisaStream.objects.filter(country=self.request.GET.get('country')).all()
#         return objs


# class TourGetSearchCriteriaAPIView(APIView):
#     def get(self, request):
#         dic = {}
#         #
#         # \
#         # age_range
#         # get whats
#         tours = Tour.objects.annotate(num_offerings=Count('event_cat')).values('id','event_cat__title_latin',
#                             'event_cat__title_farsi','event_cat__id','title_latin','title_farsi',
#                             'trip_length_day','difficulty_level__title_latin','difficulty_level__title_farsi',
#                             'accom_type__title_latin','accom_type__title_farsi',
#                                                                                         )
#         dic["tour"] = list(tours)
#
#         return Response({"message": "success", "status": "OK", "data": dic}, status=200)


class TourGetSearchCriteriaAPIView(APIView):
    def get(self, request):
        dic = {}
        trip_length_day = Tour.objects.annotate(num_offerings=Count('trip_length_day')).values('trip_length_day',).distinct()
        dic["trip_length_day"] = list(trip_length_day)

        difficulty_level = Tour.objects.annotate(num_offerings=Count('difficulty_level')).values('difficulty_level__title_latin'
                                                                                                 ,'difficulty_level__title_farsi','difficulty_level__id').distinct()
        dic["difficulty_level"] = list(difficulty_level)


        accom_type = Tour.objects.annotate(num_offerings=Count('accom_type')).values('accom_type__title_latin'
                                                                  ,'accom_type__title_farsi'
                                                                 ,'accom_type__id').distinct()
        dic["accom_type"] = list(accom_type)

        age_range = Tour.objects.annotate(num_offerings=Count('age_range')).values('age_range__title_latin'
                                                                  ,'age_range__title_farsi'
                                                                 ,'age_range__id').distinct()
        dic["age_range"] = list(age_range)

        tour_event_cat = Tour.objects.annotate(num_offerings=Count('event_cat')).values('event_cat__title_latin',
                                                                                        'event_cat__title_farsi',
                                                                                        'event_cat__id').distinct()
        dic["tour_event_cat"] = list(tour_event_cat)

        return Response({"message": "success", "status": "OK", "data": dic}, status=200)


class TourQueryAPIView(APIView):
    def get(self, request):
        dic = {}

        # get whats
        query = Q()
        if self.request.GET.get('event_cat'):
            query &= Q(tour__event_cat__in=self.request.GET.get('event_cat').split(','))

        if self.request.GET.get('trip_length_day'):
            # Q(trip_length_day=self.request.GET.get('trip_length_day'))
            query &= Q(tour__trip_length_day=str(self.request.GET.get('trip_length_day')))
            query |=  Q(tour__trip_length_day=int(self.request.GET.get('trip_length_day'))-1)
            query |=  Q(tour__trip_length_day=int(self.request.GET.get('trip_length_day')) - 2)
            query |=  Q(tour__trip_length_day=int(self.request.GET.get('trip_length_day')) - 3)
            query |= Q(tour__trip_length_day=int(self.request.GET.get('trip_length_day'))+ 1)
            query |=  Q(tour__trip_length_day=int(self.request.GET.get('trip_length_day')) + 2)
            query |=  Q(tour__trip_length_day=int( self.request.GET.get('trip_length_day')) + 3)

        if self.request.GET.get('difficulty_level'):
            query |= Q(tour__difficulty_level=self.request.GET.get('difficulty_level'))

        if self.request.GET.get('accom_type'):
            query |= Q(tour__accom_type=self.request.GET.get('accom_type'))

        if self.request.GET.get('age_range'):
            query &= Q(tour__age_range__in=self.request.GET.get('age_range').split(','))


        tours = TourDepartureFare.objects.filter(query).values('id','tour__thumbnail','tour__title_seo','tour__slug',
                            'tour__trip_length_day','tour__event_cat__id','tour__title_latin','tour__title_farsi',
                            'tour__trip_length_day','tour__difficulty_level__title_latin','tour__difficulty_level__title_farsi',
                            'tour__accom_type__title_latin','tour__accom_type__title_farsi','departure_date_time','fee_dbl','currency__id',
                            'currency__currency_latin','currency__currency_farsi','tour__meal_pattern__title_latin','tour__meal_pattern__title_farsi').distinct('id')
        dic["tour"] = list(tours)

        return Response({"message": "success", "status": "OK", "data": dic}, status=200)




class TourDetailsAPIView(ListAPIView):
    serializer_class = TourDepratureFareDetailsSerializer
    
    def list(self, request, *args, **kwargs):
        id1= self.request.GET.get('id')

        if id1.isdigit():
            id1 = id1 
        else:
            # this is slug
            tour_find = Tour.objects.get(slug=id1)

            id1 = tour_find.id

        dict = {}

        data_sc = Tour.objects.filter(id=id1)
        serializer_sc = TourDetailsSerializer(data_sc, many=True)
        dict["tour"] = serializer_sc.data

        dt = datetime.today().strftime('%Y-%m-%d')
        data_sctd = TourDepartureFare.objects.filter(validity_to__gte=dt)\
            .filter(validity_from__lte=dt).filter(tour__id=id1)
        serializer_sctd = TourDepratureFareDetailsSerializer(data_sctd, many=True)
        if len(serializer_sctd.data)>=1:
            dict["tour_deprature_fare"] = serializer_sctd.data
            print(len(dict["tour_deprature_fare"] ))
        else:
            dict["tour_deprature_fare"] = []

        ontime = 0
        defvalue = 100000000000000
        xx=-1
        try:
            for tour in dict["tour_deprature_fare"]:
                keys = ['fee_dbl', 'fee_dbl_discounted', 'fee_sgl', 'fee_shared', 'fee_child', 'fee_infant', ]

                for key in keys:
                    ontime = ontime + 1


                    if tour.get(key) is None:
                        del tour[key]

                    else:
                        if ontime == 1:
                            defvalue = tour.get(key)
                            tour['price_from'] = tour.get(key)
                        else:
                            if float(tour.get(key)) < float(defvalue):
                                defvalue = tour.get(key)
                                tour['price_from'] = tour.get(key)

                        del tour[key]

                        # del tour['validity_to']
                        # del tour['validity_from']
                xx=xx+1
                dict["tour_deprature_fare"][xx] = tour
        except:
            pass

        data_tourtopack = TourToPack.objects.filter(tour__id=id1)
        serializer_tourtopack = TourToPackSerializer(data_tourtopack, many=True)
        dict["tour_to_pack"] = serializer_tourtopack.data

        return Response({
            'status': 200,
            'message': 'success',
            'data': dict
        })

class TourDepartureFareQueryRegionAndEventCatAPIVIEW(APIView):
    def post(self, request):
        dic = {}

        region = self.request.GET.get("region")
        event_cat = self.request.GET.get("event_cat")
        event_cat = event_cat.split(',')


        now = datetime.now()

        tours = TourDepartureFare.objects.filter(validity_to__gte=now)\
            .filter(validity_from__lte=now).filter(destination__id=region).\
            filter(tour__event_cat__in=event_cat).values('id','departure_date_time',
                              'fee_dbl',
                              'fee_dbl_discounted',
                              'fee_sgl', 'fee_shared',
                              'fee_child', 'fee_infant',
                              'tour__id', 'tour__thumbnail',
                              'tour__trip_length_day',
                              'tour__title_latin',
                              'tour__title_farsi',
                              'tour__event_cat__title_latin',
                              'tour__event_cat__title_farsi',
                              'tour__meal_pattern__title_latin',
                              'tour__meal_pattern__title_farsi',
                              'tour__accom_type__title_latin',
                              'tour__accom_type__title_farsi',
                              )
        defvalue = 100000000000000
        ontime = 0
        for tour in tours:
            keys = ['fee_dbl', 'fee_dbl_discounted', 'fee_sgl', 'fee_shared', 'fee_child', 'fee_infant', ]

            for key in keys:
                ontime = ontime + 1

                print(tour.get(key))
                if tour.get(key) is None:
                    del tour[key]

                else:
                    if ontime == 1:
                        defvalue = tour.get(key)
                        tour['fee'] = tour.get(key)
                    else:
                        if tour.get(key) < defvalue:
                            defvalue = tour.get(key)
                            tour['fee'] = tour.get(key)

                    del tour[key]

        dic["tour_dept_fare"] = list(tours)

        return Response({"message": "success", "status": "OK", "data": dic}, status=200)


from django.db.models import Q

class TourDepartureFareQueryRegionAndEventCatAPIVIEWOR(APIView):
    def post(self, request):
        dic = {}

        region = self.request.GET.get("region")
        event_cat = self.request.GET.get("event_cat")
        event_cat = event_cat.split(',')

        now = datetime.now()

        tours = TourDepartureFare.objects.filter(validity_from__lte=now).filter(validity_to__gte=now).filter(destination__id=region)\
            .filter(Q(tour__event_cat__in=event_cat) | Q(destination__id=region)).\
            filter(tour__event_cat__in=event_cat).values('id','departure_date_time',
                              'fee_dbl',
                              'fee_dbl_discounted',
                              'fee_sgl', 'fee_shared',
                              'fee_child', 'fee_infant',
                              'tour__id', 'tour__thumbnail',
                              'tour__trip_length_day',
                              'tour__title_latin',
                              'tour__title_farsi',
                              'tour__event_cat__title_latin',
                              'tour__event_cat__title_farsi',
                              'tour__meal_pattern__title_latin',
                              'tour__meal_pattern__title_farsi',
                              'tour__accom_type__title_latin',
                              'tour__accom_type__title_farsi',
                              )

        defvalue = 100000000000000
        ontime = 0
        for tour in tours:
            keys = ['fee_dbl', 'fee_dbl_discounted', 'fee_sgl', 'fee_shared', 'fee_child', 'fee_infant', ]

            for key in keys:
                ontime = ontime + 1

                print(tour.get(key))
                if tour.get(key) is None:
                    del tour[key]

                else:
                    if ontime == 1:
                        defvalue = tour.get(key)
                        tour['fee'] = tour.get(key)
                    else:
                        if tour.get(key) < defvalue:
                            defvalue = tour.get(key)
                            tour['fee'] = tour.get(key)

                    del tour[key]

        dic["tour_dept_fare"] = list(tours)

        return Response({"message": "success", "status": "OK", "data": dic}, status=200)


class TourAllListAPIView(ListAPIView):
    serializer_class = TourAllListSerializer
    queryset = Tour.objects.all()


class QueryFAQ(APIView):
    def get(self, request):
        SEARCH_TERM = self.request.GET.get("query")
        dic = {}

        models= ['SpecificFaq', 'GeneralFAQ','TravelRequirement','Article','Glossary','New','PassengerRight','PrivacyClauses','Program','School',
                 'Job','JobFamily','ServiceCategory','SubServiceCategory','Tour','VisaStream','SubVisaStream']
        appss=['Content','Content','Content','Content','Content','Content','Content','Content','Edu','Edu','Hr','Hr','Services','Services',
             'Tour','Visa','Visa']
        for data,app in zip(models,appss):
            print(data,app)
            a_works = apps.get_model(str(app), str(data))
            fields = [f for f in a_works._meta.fields if isinstance(f, CharField) or isinstance(f, TextField)]
            queries = [Q(**{f.name + '__icontains': SEARCH_TERM}) for f in fields]
            qs = Q()
            for query in queries:
                qs = qs | query
            name_fields=[]
            name_fields = [f.name for f in fields if isinstance(f, CharField) or isinstance(f, TextField)] + ['id']

            specific_faq = a_works.objects.filter(qs).values(*name_fields)

            dic[data] = list(specific_faq)

        return Response({"message": "success", "status": "OK", "data": dic}, status=200)


class TourDetails2IdAPIView(APIView):
    @swagger_auto_schema(
        operation_summary="Get tour details by ID",
        operation_description="Returns the tour details for the given ID.",
        manual_parameters=[
            openapi.Parameter(
                name="id",
                in_=openapi.IN_QUERY,
                type=openapi.TYPE_INTEGER,
                description="The ID of the tour to retrieve.",
                required=True
            )
        ],
        responses={
            200: openapi.Response(
                description="Success",
                schema=TourDetails2IdAPIView_response_schema
            ),
            400: openapi.Response(description="Bad Request"),
            401: openapi.Response(description="Unauthorized"),
            403: openapi.Response(description="Forbidden"),
            404: openapi.Response(description="Not Found"),
            500: openapi.Response(description="Internal Server Error")
        }
    )
    def get(self, request):
        dic = {}
        id = self.request.GET.get("id")

        data_sc = DayTour.objects.filter(tour=id)
        serializer_sc = DayTourDetails2Serializer(data_sc, many=True)
        dic["tour"] = serializer_sc.data

        return Response({"message": "success", "status": "OK", "data": dic}, status=200)


class TourDepartureFareQueryEventCatAPIVIEW(APIView):
    def get(self, request):
        dic = {}
        event_cat = self.request.GET.get("event_cat")
        event_cat = event_cat.split(',')

        now = datetime.now()
        toursd = TourDepartureFare.objects.filter(tour__event_cat__in=event_cat).values('id','departure_date_time',
                              'fee_dbl',
                              'fee_dbl_discounted',
                              'fee_sgl', 'fee_shared',
                              'fee_child', 'fee_infant',
                              'tour__id', 'tour__thumbnail',
                              'tour__trip_length_day',
                              'tour__title_latin',
                              'tour__title_farsi',
                              'tour__tour_code','departure_date_time','currency__currency_latin'
                              ,'currency__currency_farsi','trans_mode__title_latin','trans_mode__title_farsi',
                              'tour__country__name_farsi','tour__country__name_latin',
                              'tour__event_cat__title_latin',
                              'tour__event_cat__title_farsi',
                              'tour__meal_pattern__title_latin',
                              'tour__meal_pattern__title_farsi',
                              'tour__accom_type__title_latin',
                              'tour__accom_type__title_farsi',
                              )
    
        tours = Tour.objects.filter(event_cat__in=event_cat).values(                          
                              'id', 'thumbnail',
                              'trip_length_day',
                              'title_latin',
                              'title_farsi',
                              'tour_code',
                              'country__name_farsi','country__name_latin',
                              'event_cat__title_latin',
                              'event_cat__title_farsi',
                              'meal_pattern__title_latin',
                              'meal_pattern__title_farsi',
                              'accom_type__title_latin',
                              'accom_type__title_farsi'
                              )
        # defvalue = 100000000000000
        # ontime = 0
        # for tour in tours:
        #     keys = ['fee_dbl', 'fee_dbl_discounted', 'fee_sgl', 'fee_shared', 'fee_child', 'fee_infant', ]
        #
        #     for key in keys:
        #         ontime = ontime + 1
        #
        #         print(tour.get(key))
        #         if tour.get(key) is None:
        #             del tour[key]
        #
        #         else:
        #             if ontime == 1:
        #                 defvalue = tour.get(key)
        #                 tour['fee'] = tour.get(key)
        #             else:
        #                 if tour.get(key) < defvalue:
        #                     defvalue = tour.get(key)
        #                     tour['fee'] = tour.get(key)
        #
        #             del tour[key]

        dic["tour_dept_fare"] = list(toursd)
        dic["tours"] = list(tours)

        return Response({"message": "success", "status": "OK", "data": dic}, status=200)


class TourDetailsAPIViewQueryWithTourID(ListAPIView):
    serializer_class = TourDepratureFareDetailsSerializer
    def list(self, request, *args, **kwargs):
        id1= self.request.GET.get('id')
        dict = {}

        dt = datetime.today().strftime('%Y-%m-%d')
        data_sctd = TourDepartureFare.objects.filter(validity_to__gte=dt)\
            .filter(validity_from__lte=dt).filter(tour__id=id1)
        serializer_sctd = TourDepratureFareDetailsSerializer(data_sctd, many=True)
        dict["tour_deprature_fare"] = serializer_sctd.data

        return Response({
            'status': 200,
            'message': 'success',
            'data': dict
        })

class TourDepartureFareDetailsAPIView(ListAPIView):
    serializer_class = TourDepratureFareDetailsSerializer
    def list(self, request, *args, **kwargs):
        id1= self.request.GET.get('id')
        dict = {}

        dt = datetime.today().strftime('%Y-%m-%d')
        data_sctd = TourDepartureFare.objects.filter(pk=id1)
        serializer_sctd = TourDepratureFareDetailsSerializer(data_sctd, many=True)
        dict["tour_deprature_fare"] = serializer_sctd.data

        return Response({
            'status': 200,
            'message': 'success',
            'data': dict
        })


class EventCatAPIView(APIView):
    def get(self, request):
        event_cat_data = {}

        tours = Tour.objects.filter(event_cat__isnull=False)

        for tour in tours:
            event_cats = tour.event_cat.all()

            for event_cat in event_cats:
                event_cat_data[event_cat.id] = {
                    'event_cat__title_latin': event_cat.title_latin,
                    'event_cat__title_farsi': event_cat.title_farsi,
                    'tour__title_latin': tour.title_latin,
                    'tour__title_farsi': tour.title_farsi,
                    'TourDepartureFare': [],
                    'tour__id': tour.id,
                    'tour__title_latin': tour.title_latin,
                    'tour__title_farsi': tour.title_farsi,
                    'tour__country': {
                        'name_latin': tour.country.name_latin,
                        'name_farsi': tour.country.name_farsi,
                        'zone_nr': tour.country.zone_nr.id,
                        'travel_alert_note': tour.country.travel_alert_note,
                        'expiry_date': str(tour.country.expiry_date),
                        'flag': str(tour.country.flag),
                    },
                    'tour__tour_code': tour.tour_code,
                    'tour__thumbnail': str(tour.thumbnail),
                    'tour__trip_length_day': tour.trip_length_day,
                    'tour__meal_pattern': {
                        'title_latin': tour.meal_pattern.title_latin,
                        'title_farsi': tour.meal_pattern.title_farsi,
                        'abbreviation': tour.meal_pattern.abbreviation,
                    },
                    'tour__accom_type': {
                        'title_latin': tour.accom_type.title_latin,
                        'title_farsi': tour.accom_type.title_farsi,
                    },
                }

                

                tour_departure_fares = TourDepartureFare.objects.filter(
                    tour=tour,
                    departure_date_time__isnull=False
                ).order_by('departure_date_time')

                # for tdf in tour_departure_fares:
                #     event_cat_data[event_cat.id]['TourDepartureFare'].append({
                #         'id': tdf.id,
                #         'departure_date_time': str(tdf.departure_date_time)
                #     })

                for tdf in tour_departure_fares:
                    event_cat_data[event_cat.id]['TourDepartureFare'].append({
                        'id': tdf.id,
                        'departure_date_time': str(tdf.departure_date_time),
                        'fee_dbl' : str(tdf.fee_dbl)
                    })


        json_data = event_cat_data

        return Response(json_data)