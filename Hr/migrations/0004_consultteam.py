# Generated by Django 3.2.18 on 2024-07-18 08:13

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('Raw_root', '0006_autoemailtemplate'),
        ('Hr', '0003_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='ConsultTeam',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('full_name_latin', models.CharField(blank=True, max_length=300, null=True)),
                ('full_name_farsi', models.CharField(blank=True, max_length=300, null=True)),
                ('consultation_categories', models.ManyToManyField(blank=True, to='Raw_root.ConsultationCategory')),
                ('consultation_modes', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='Raw_root.consultationmode')),
            ],
        ),
    ]
