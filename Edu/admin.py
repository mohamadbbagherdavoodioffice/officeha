from django.apps import apps
from django.contrib import admin
from django.contrib.admin.sites import AlreadyRegistered
from import_export import resources
from import_export.admin import ImportExportModelAdmin
from .models import Program , ProgramSchool
app_models = apps.get_app_config('Edu').get_models()
for model in app_models:
    try:

        class AAAResource(resources.ModelResource):

            class Meta:
                model = model


        class AAAAdmin(ImportExportModelAdmin):
            resource_class = AAAResource
            if str(model) == "<class 'Edu.models.ProgramSchool'>":
                fields = (
                          ('program'), ('school'),
                          ('intake_month', 'days_1'),
                          # ('month_2', 'days_2'),
                          # ('month_3', 'days_3'),
                          # ('month_4', 'days_4'),
                          # ('month_5', 'days_5'),
                          # ('month_6', 'days_6'),

                          'currency',
                          'tuition',
                          'scholarship',
                          'teaching_lang_1',
                          'teaching_lang_2',
                          'admission_fee',
                          'all_degrees_internship',
                          'all_degrees_co_op',
                          'type',
                          'duration',
                          'apply_month'
                          )



        admin.site.register(model, AAAAdmin)

    except AlreadyRegistered:
        pass



admin.site.unregister(Program)
class ProgramAdmin( ImportExportModelAdmin,admin.ModelAdmin):
    # search_fields = [ 'Program__id','Program__title_latin','Program__title_farsi']
    ordering=['name_latin']

admin.site.register(Program, ProgramAdmin)


admin.site.unregister(ProgramSchool)
class ProgramSchoolAdmin( ImportExportModelAdmin,admin.ModelAdmin):
    search_fields = [ 'program__id','program__name_latin','program__name_farsi']
    list_filter = ('program__name_latin', )
admin.site.register(ProgramSchool, ProgramSchoolAdmin)

# class MyAuthenticationForm(AuthenticationForm):
#     # add your form widget here
#     widget =