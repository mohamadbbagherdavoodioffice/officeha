from django.db import models


class Currency(models.Model):
    currency_latin = models.CharField(max_length=200, null=True, blank=True)
    currency_farsi = models.CharField(max_length=200, null=True, blank=True)

    def __str__(self):
        return str(self.currency_farsi)


class AccomType(models.Model):
    title_latin = models.CharField(max_length=500, null=True, blank=True)
    title_farsi = models.CharField(max_length=500, null=True, blank=True)

    def __str__(self):
        return str(self.title_latin)


class Warnings(models.Model):
    text_latin = models.CharField(max_length=500, null=True, blank=True)
    text_farsi = models.CharField(max_length=500, null=True, blank=True)

    def __str__(self):
        return str(self.text_latin)


class Language(models.Model):
    title_latin = models.CharField(max_length=500, null=True, blank=True)
    title_farsi = models.CharField(max_length=500, null=True, blank=True)

    def __str__(self):
        return str(self.title_latin)


class Problem(models.Model):
    TYPE = (
        ("Criminal", "Criminal"),
        ("immigration", "immigration"),
        ("Incident", "Incident"),
        ("trouble", "trouble"),
        ("Medical", "Medical"),
        ("Operation", "Operation"),
    )
    title_latin = models.CharField(max_length=400, null=True, blank=True)
    title_farsi = models.CharField(max_length=400, null=True, blank=True)
    type = models.CharField(choices=TYPE, max_length=20, null=True, blank=True)

    def __str__(self):
        return str(self.title_latin)


class DifficultyLevel(models.Model):
    title_latin = models.CharField(max_length=500, null=True, blank=True)
    title_farsi = models.CharField(max_length=500, null=True, blank=True)

    def __str__(self):
        return self.title_latin


def get_file_event_cat_thumbnail(instance, filename):
    return f"file_event_cat_thumbnail/{filename}"


def get_file_event_cat_icon(instance, filename):
    return f"file_event_cat_icon/{filename}"


class EventCat(models.Model):
    seq = models.IntegerField(choices=list(zip(range(1, 30), range(1, 30))))
    title_latin = models.CharField(max_length=500, null=True, blank=True)
    title_farsi = models.CharField(max_length=500, null=True, blank=True)
    icon = models.ImageField(upload_to=get_file_event_cat_icon, null=True, blank=True)
    thumbnail = models.ImageField(upload_to=get_file_event_cat_thumbnail, null=True, blank=True)

    def __str__(self):
        return self.title_latin


class Meal(models.Model):
    title_latin = models.CharField(max_length=500, null=True, blank=True)
    title_farsi = models.CharField(max_length=500, null=True, blank=True)
    abbreviation = models.CharField(max_length=500, null=True, blank=True)
    note = models.CharField(max_length=500, null=True, blank=True)

    def __str__(self):
        return self.title_latin


class MealPattern(models.Model):
    title_latin = models.CharField(max_length=500, null=True, blank=True)
    title_farsi = models.CharField(max_length=500, null=True, blank=True)
    abbreviation = models.CharField(max_length=500, null=True, blank=True)

    def __str__(self):
        return self.title_latin


class Vehicle(models.Model):
    title_latin = models.CharField(max_length=500, null=True, blank=True)
    title_farsi = models.CharField(max_length=500, null=True, blank=True)

    def __str__(self):
        return self.title_latin


class ToPack(models.Model):
    title_latin = models.CharField(max_length=500, null=True, blank=True)
    title_farsi = models.CharField(max_length=500, null=True, blank=True)

    def __str__(self):
        return self.title_latin


class TransMode(models.Model):
    title_latin = models.CharField(max_length=500)
    title_farsi = models.CharField(max_length=500, null=True, blank=True)

    def __str__(self):
        return self.title_latin


def get_transco(instance, filename):
    return f"file_transco/{filename}"


class TransCo(models.Model):
    trans_mode = models.ForeignKey(TransMode, on_delete=models.CASCADE, null=True, blank=True, )
    title_latin = models.CharField(max_length=500)
    title_farsi = models.CharField(max_length=500, null=True, blank=True)
    logo = models.ImageField(upload_to=get_transco, null=True, blank=True, )

    def __str__(self):
        return self.title_latin


class ClassRanges(models.Model):
    title_latin = models.CharField(max_length=500, )
    abbreviation = models.CharField(max_length=500, null=True, blank=True)

    def __str__(self):
        return self.title_latin


def get_image_country(instance, filename):
    return f"file_recovery_questions/{filename}"


class Tservice(models.Model):
    title_latin = models.CharField(max_length=500, null=True, blank=True)
    title_farsi = models.CharField(max_length=500, null=True, blank=True)

    def __str__(self):
        return self.title_latin


class RefundCondition(models.Model):
    refund_condition_latin = models.CharField(max_length=500, null=True, blank=True)
    refund_condition_farsi = models.CharField(max_length=500, null=True, blank=True)
    Refund_of_instalment = models.DecimalField(max_digits=10, decimal_places=2, null=True, blank=True)

    def __str__(self):
        return self.refund_condition_latin


class CancellationRate(models.Model):
    from_30_to_7_days_before_start = models.DecimalField(max_digits=10, decimal_places=2)
    from_7_to_3_days_before_start = models.DecimalField(max_digits=10, decimal_places=2)
    from_3_to_1_days_before_start = models.DecimalField(max_digits=10, decimal_places=2)
    from_24h_to_3h_before_start = models.DecimalField(max_digits=10, decimal_places=2)

    def __str__(self):
        return "%s | %s | %s | %s" % (self.from_30_to_7_days_before_start, self.from_7_to_3_days_before_start,
                                      self.from_3_to_1_days_before_start,
                                      self.from_24h_to_3h_before_start,)


class StayTimeCat(models.Model):
    title_latin = models.CharField(max_length=500, null=True, blank=True)
    title_farsi = models.CharField(max_length=500, null=True, blank=True)

    def __str__(self):
        return self.title_latin


class Translation(models.Model):
    name_latin = models.CharField(max_length=500, null=True, blank=True)
    name_farsi = models.CharField(max_length=500, null=True, blank=True)

    def __str__(self):
        return str(self.name_latin)


class Degree(models.Model):
    title_latin = models.CharField(max_length=500, null=True, blank=True)
    title_farsi = models.CharField(max_length=500, null=True, blank=True)

    def __str__(self):
        return self.title_latin


class Relationship(models.Model):
    title_latin = models.CharField(max_length=500, null=True, blank=True)
    title_farsi = models.CharField(max_length=500, null=True, blank=True)

    def __str__(self):
        return self.title_latin


class WorkPermit(models.Model):
    title_latin = models.CharField(max_length=500, null=True, blank=True)
    title_farsi = models.CharField(max_length=500, null=True, blank=True)

    def __str__(self):
        return self.title_latin


class Marital(models.Model):
    title_latin = models.CharField(max_length=500, null=True, blank=True)
    title_farsi = models.CharField(max_length=500, null=True, blank=True)

    def __str__(self):
        return self.title_latin


class ResidencyStatus(models.Model):
    title_latin = models.CharField(max_length=500, null=True, blank=True)
    title_farsi = models.CharField(max_length=500, null=True, blank=True)

    def __str__(self):
        return self.title_latin


class AgeRange(models.Model):
    title_latin = models.CharField(max_length=500, null=True, blank=True)
    title_farsi = models.CharField(max_length=500, null=True, blank=True)

    def __str__(self):
        return self.title_latin


class JobLevelClassification(models.Model):
    title_latin = models.CharField(max_length=500, null=True, blank=True)
    title_farsi = models.CharField(max_length=500, null=True, blank=True)

    def __str__(self):
        return self.title_latin


class EntryType(models.Model):
    title_latin = models.CharField(max_length=250, null=True, blank=True)
    title_farsi = models.CharField(max_length=250, null=True, blank=True)

    def __str__(self):
        return self.title_latin


class IrGraduationZones(models.Model):
    title_latin = models.CharField(max_length=250, null=True, blank=True)
    title_farsi = models.CharField(max_length=250, null=True, blank=True)

    def __str__(self):
        return self.title_latin


class VisaMainCat(models.Model):
    title_latin = models.CharField(max_length=250, null=True, blank=True)
    title_farsi = models.CharField(max_length=250, null=True, blank=True)
    text_latin = models.TextField(null=True, blank=True)
    text_farsi = models.TextField(null=True, blank=True)

    def __str__(self):
        return str(self.title_latin)


class Vaccination(models.Model):
    title_latin = models.CharField(max_length=250, null=True, blank=True)

    def __str__(self):
        return str(self.title_latin)


class VisaSubCat(models.Model):
    seq = models.PositiveIntegerField(null=True, blank=True)
    main_cat = models.ForeignKey(VisaMainCat, on_delete=models.CASCADE, null=True, blank=True, )
    title_latin = models.CharField(max_length=500, null=True, blank=True)
    title_farsi = models.CharField(max_length=500, null=True, blank=True)
    text_latin = models.TextField(null=True, blank=True)
    text_farsi = models.TextField(null=True, blank=True)

    def __str__(self):
        return "%s" % (self.title_latin,)


def get_file_app_process_s(instance, filename):
    return f"file_application_process_status/{filename}"


class ApplicationProcessStatus(models.Model):
    title_latin = models.CharField(max_length=250, null=True, blank=True)
    title_farsi = models.CharField(max_length=250, null=True, blank=True)
    file = models.FileField(upload_to=get_file_app_process_s, null=True, blank=True)

    def __str__(self):
        return str(self.title_latin)


class Document(models.Model):
    CAT = (
        ('MED', 'MED'),
        ('EDU', 'EDU'),
        ('JOB', 'JOB'),
        ('ID', 'ID'),
    )
    type = models.ForeignKey('Raw_root.TravelDocType', on_delete=models.CASCADE, null=True, blank=True, )
    title_latin = models.CharField(max_length=250, null=True, blank=True)
    title_farsi = models.CharField(max_length=250, null=True, blank=True)
    text_latin = models.TextField(null=True, blank=True)
    text_farsi = models.TextField(null=True, blank=True)

    def __str__(self):
        return self.title_latin


class Occupation(models.Model):
    seq = models.PositiveIntegerField(null=True, blank=True)
    title_latin = models.CharField(max_length=500, null=True, blank=True)
    title_farsi = models.CharField(max_length=500, null=True, blank=True)

    def __str__(self):
        return self.title_latin


class Action(models.Model):
    title_latin = models.CharField(max_length=500, null=True, blank=True)
    title_farsi = models.CharField(max_length=500, null=True, blank=True)

    def __str__(self):
        return self.title_latin


class GlobalZone(models.Model):
    title_latin = models.CharField(max_length=500, null=True, blank=True)
    title_farsi = models.CharField(max_length=500, null=True, blank=True)

    def __str__(self):
        return self.title_latin


class RecoveryQuestions(models.Model):
    q_title_latin = models.CharField(max_length=500, null=True, blank=True)
    q_title_farsi = models.CharField(max_length=500, null=True, blank=True)

    def __str__(self):
        return str(self.q_title_latin)


def get_file_social_media_icon(instance, filename):
    return f"file_social_media_icon/{filename}"


class SocialMedias(models.Model):
    title_latin = models.CharField(max_length=40, null=True, blank=True)
    title_farsi = models.CharField(max_length=40, null=True, blank=True)
    icon = models.ImageField(upload_to=get_file_social_media_icon, null=True, blank=True)

    def __str__(self):
        return str(self.title_latin)

class ConsultationCategory(models.Model):
    title_latin = models.CharField(max_length=40, null=True, blank=True)
    title_farsi = models.CharField(max_length=40, null=True, blank=True)
    currency = models.ForeignKey(Currency, on_delete=models.CASCADE, null=True, blank=True, )
    fee = models.DecimalField(max_digits=10, decimal_places=2, null=True, blank=True)

    def __str__(self):
        return self.title_latin

class Country(models.Model):
    name_latin = models.CharField(max_length=30)
    name_farsi = models.CharField(max_length=30)
    zone_nr = models.ForeignKey("Raw_root.GlobalZone", on_delete=models.CASCADE, null=True, blank=True, )
    travel_alert_note = models.TextField(null=True, blank=True)
    expiry_date = models.DateField(null=True, blank=True)
    flag = models.FileField(upload_to=get_image_country, null=True, blank=True)
    our_coverage = models.BooleanField(default=False)

    def __str__(self):
        return str(self.name_latin)


class MinEmploymentYear(models.Model):
    title_latin = models.CharField(max_length=250, null=True, blank=True)
    title_farsi = models.CharField(max_length=250, null=True, blank=True)

    def __str__(self):
        return self.title_latin


class TravelDocType(models.Model):
    title_latin = models.CharField(max_length=250, null=True, blank=True)
    title_farsi = models.CharField(max_length=250, null=True, blank=True)

    def __str__(self):
        return str(self.title_latin)


class ConsultationMode(models.Model):
    title_latin = models.CharField(max_length=400, null=True, blank=True)
    title_farsi = models.CharField(max_length=400, null=True, blank=True)
    type = models.CharField(max_length=40, choices=(
        ('In-office', 'In-office'), ('Phone call', 'Phone call'), ('What’s App', 'What’s App')))

    def __str__(self):
        return self.title_latin


class Personality(models.Model):
    title_latin = models.CharField(max_length=400, null=True, blank=True)
    title_farsi = models.CharField(max_length=400, null=True, blank=True)

    def __str__(self):
        return self.title_latin


class Helpful(models.Model):
    title_latin = models.CharField(max_length=400, null=True, blank=True)
    title_farsi = models.CharField(max_length=400, null=True, blank=True)

    def __str__(self):
        return self.title_latin


class Province(models.Model):
    country = models.ForeignKey('Raw_root.Country', on_delete=models.CASCADE, null=True, blank=True, )
    title_latin = models.CharField(max_length=400, null=True, blank=True)
    title_farsi = models.CharField(max_length=400, null=True, blank=True)

    def __str__(self):
        return "%s" % (self.title_latin)


#
# class ExpertiseGuide(models.Model):
#     title_latin = models.CharField(max_length=160)
#     title_farsi = models.CharField(max_length=160)
#
#     def __str__(self):
#         return self.title_latin


class ComplaintCat(models.Model):
    title_latin = models.CharField(max_length=40, null=True, blank=True)
    title_farsi = models.CharField(max_length=40, null=True, blank=True)

    def __str__(self):
        return str(self.title_latin)


class MedicalCases(models.Model):
    TYPE = (
        ("Allergic", "Allergic"),
        ("surgery", "surgery"),
        ("Disease", "Disease"),
    )
    title_latin = models.CharField(max_length=50, null=True, blank=True)
    title_farsi = models.CharField(max_length=50, null=True, blank=True)
    type = models.CharField(choices=TYPE, max_length=10, null=True, blank=True)

    def __str__(self):
        return "%s " % (self.title_latin)


class RegionType(models.Model):
    title_latin = models.CharField(max_length=50, null=True, blank=True)
    title_farsi = models.CharField(max_length=50, null=True, blank=True)

    def __str__(self):
        return "%s / %s" % (self.title_latin, self.id)


class Region(models.Model):
    type = models.ForeignKey(RegionType, on_delete=models.CASCADE, null=True, blank=True, )
    country = models.ForeignKey(Country, on_delete=models.CASCADE, null=True, blank=True, )
    province = models.ForeignKey(Province, on_delete=models.CASCADE, null=True, blank=True, )
    title_latin = models.CharField(max_length=50)
    title_farsi = models.CharField(max_length=50)
    our_coverage = models.BooleanField(default=False)

    def __str__(self):
        return "%s" % (self.title_latin)


# class FinancialCat(models.Model):
#     title_latin = models.CharField(max_length=400, null=True, blank=True)
#     title_farsi = models.CharField(max_length=400, null=True, blank=True)
#
#     def __str__(self):
#         return self.title_latin

class ContactUsType(models.Model):
    title_latin = models.CharField(max_length=400, null=True, blank=True)
    title_farsi = models.CharField(max_length=400, null=True, blank=True)

    def __str__(self):
        return self.title_latin


class FollowUpStatus(models.Model):
    title_latin = models.CharField(max_length=400, null=True, blank=True)
    title_farsi = models.CharField(max_length=400, null=True, blank=True)

    def __str__(self):
        return self.title_latin


class EduSubject(models.Model):
    title_latin = models.CharField(max_length=400, null=True, blank=True)
    title_farsi = models.CharField(max_length=400, null=True, blank=True)

    def __str__(self):
        return self.title_latin


class AvailableFund(models.Model):
    title_latin = models.CharField(max_length=500, null=True, blank=True)
    title_farsi = models.CharField(max_length=500, null=True, blank=True)

    def __str__(self):
        return self.title_latin


class TellUsWhatYouThinkCat(models.Model):
    title_latin = models.CharField(max_length=500, null=True, blank=True)
    title_farsi = models.CharField(max_length=500, null=True, blank=True)

    def __str__(self):
        return self.title_latin


class RespondNote(models.Model):
    CAT = (('Contact Us/Create', 'Contact Us/Create'),
           ('Tell Us/Create', 'Tell Us/Create'),
           ('Edu Apply/Create', 'Edu Apply/Create'),
           ('Service cat. Order', 'Service cat. Order'),
           ('Feedback/Create', 'Feedback/Create'),
           ('Claim Pax right', 'Claim Pax right'),
           ('External Payment/Create', 'External Payment/Create'),
           ('Visa order', 'Visa order'),
           ('Job application', 'Job application'),
           ('Signup pax', 'Signup pax'),
           ('Signup agn', 'Signup agn'),
           ('Appointment', 'Appointment'),
           ('application status no record', 'application status no record'),
           ('payment status check no record', 'payment status check no record'),
           ('payment status', 'payment status'),
           ('Retrieve Application Number', 'RRetrieve Application Number'),
           ('Successful app create', 'Successful app create'),
           ('Failed app create', 'Failed app create'),
           ('Evaluation', 'Evaluation'),
           )

    category = models.CharField(choices=CAT, max_length=40, null=True, blank=True)
    successful_title_latin = models.CharField(max_length=500, null=True, blank=True)
    successful_title_farsi = models.CharField(max_length=500, null=True, blank=True)
    successful_text_latin = models.TextField(null=True, blank=True)
    successful_text_farsi = models.TextField(null=True, blank=True)
    successful_signature_latin = models.CharField(max_length=500, null=True, blank=True)
    successful_signature_farsi = models.CharField(max_length=500, null=True, blank=True)
    failed_title_latin = models.CharField(max_length=500, null=True, blank=True)
    failed_title_farsi = models.CharField(max_length=500, null=True, blank=True)
    failed_text_latin = models.TextField(null=True, blank=True)
    failed_text_farsi = models.TextField(null=True, blank=True)
    failed_signature_latin = models.CharField(max_length=500, null=True, blank=True)
    failed_signature_farsi = models.CharField(max_length=500, null=True, blank=True)

    def __str__(self):
        return self.category


def get_file_pax_cat_thumbnail(instance, filename):
    return f"file_pax_cat_thumbnail/{filename}"


def get_file_pax_cat_banner(instance, filename):
    return f"file_pax_cat_banner/{filename}"


class PassengerRelatedCategory(models.Model):
    title_latin = models.CharField(max_length=500, null=True, blank=True)
    title_farsi = models.CharField(max_length=500, null=True, blank=True)
    text_latin = models.TextField(null=True, blank=True)
    text_farsi = models.TextField(null=True, blank=True)
    thumbnail = models.FileField(upload_to=get_file_pax_cat_thumbnail, null=True, blank=True)
    banner = models.FileField(upload_to=get_file_pax_cat_banner, null=True, blank=True)

    def __str__(self):
        return str(self.title_latin)


class EduProgramDegree(models.Model):
    title_latin = models.CharField(max_length=500, null=True, blank=True)
    title_farsi = models.CharField(max_length=500, null=True, blank=True)

    def __str__(self):
        return str(self.title_latin)


class ExpertiseCat(models.Model):
    title_latin = models.CharField(max_length=500, null=True, blank=True)
    title_farsi = models.CharField(max_length=500, null=True, blank=True)

    def __str__(self):
        return str(self.title_latin)


class AppMainStatus(models.Model):
    title_latin = models.CharField(max_length=500, null=True, blank=True)
    title_farsi = models.CharField(max_length=500, null=True, blank=True)

    def __str__(self):
        return str(self.title_latin)


class MonthOfYear(models.Model):
    title_latin = models.CharField(max_length=300, null=True, blank=True)
    title_farsi = models.CharField(max_length=300, null=True, blank=True)

    def __str__(self):
        return str(self.title_latin)


class Gpa(models.Model):
    title = models.CharField(max_length=400, null=True, blank=True)


class ExternalPaymentStatus(models.Model):
    title_latin = models.CharField(max_length=300, null=True, blank=True)
    title_farsi = models.CharField(max_length=300, null=True, blank=True)

    def __str__(self):
        return str(self.title_latin)


class HrContractMode(models.Model):
    title_latin = models.CharField(max_length=300, null=True, blank=True)
    title_farsi = models.CharField(max_length=300, null=True, blank=True)

    def __str__(self):
        return str(self.title_latin)


class HrRequiredTest(models.Model):
    title_latin = models.CharField(max_length=300, null=True, blank=True)
    title_farsi = models.CharField(max_length=300, null=True, blank=True)

    def __str__(self):
        return str(self.title_latin)


# class HrDrugTestRequired(models.Model):
#     title_latin = models.CharField(max_length=300, null=True, blank=True)
#     title_farsi = models.CharField(max_length=300, null=True, blank=True)
#
#     def __str__(self):
#         return str(self.title_latin)

class HrPublicTrust(models.Model):
    title_latin = models.CharField(max_length=300, null=True, blank=True)
    title_farsi = models.CharField(max_length=300, null=True, blank=True)

    def __str__(self):
        return str(self.title_latin)


class HrPositionSensitivity(models.Model):
    title_latin = models.CharField(max_length=300, null=True, blank=True)
    title_farsi = models.CharField(max_length=300, null=True, blank=True)

    def __str__(self):
        return str(self.title_latin)


class HrTravelRequired(models.Model):
    title_latin = models.CharField(max_length=300, null=True, blank=True)
    title_farsi = models.CharField(max_length=300, null=True, blank=True)

    def __str__(self):
        return str(self.title_latin)


class Gender(models.Model):
    title_latin = models.CharField(max_length=300, null=True, blank=True)
    title_farsi = models.CharField(max_length=300, null=True, blank=True)

    def __str__(self):
        return str(self.title_latin)


class BusinessType(models.Model):
    title_latin = models.CharField(max_length=300, null=True, blank=True)
    title_farsi = models.CharField(max_length=300, null=True, blank=True)

    def __str__(self):
        return str(self.title_latin)


class TransactionGroup(models.Model):
    title_latin = models.CharField(max_length=300, null=True, blank=True)
    title_farsi = models.CharField(max_length=300, null=True, blank=True)

    def __str__(self):
        return str(self.title_latin)


class InvoiceGroup(models.Model):
    title_latin = models.CharField(max_length=300, null=True, blank=True)
    title_farsi = models.CharField(max_length=300, null=True, blank=True)

    def __str__(self):
        return str(self.title_latin)


class AllowedBaggage(models.Model):
    title_latin = models.CharField(max_length=400, null=True, blank=True)
    title_farsi = models.CharField(max_length=400, null=True, blank=True)

    def __str__(self):
        return self.title_latin


class ThingsToDo(models.Model):
    title_latin = models.CharField(max_length=400, null=True, blank=True)
    title_farsi = models.CharField(max_length=400, null=True, blank=True)

    def __str__(self):
        return self.title_latin

    class Meta:
        verbose_name = "Thing To Do Cat"


def get_each_tour_element(instance, filename):
    return f"file_each_tour_element/{filename}"


class TourElement(models.Model):
    icon = models.ImageField(upload_to=get_each_tour_element, null=True, blank=True)
    title_latin = models.CharField(max_length=400, null=True, blank=True)
    title_farsi = models.CharField(max_length=400, null=True, blank=True)

    def __str__(self):
        return self.title_latin


class Injury(models.Model):
    title_latin = models.CharField(max_length=400, null=True, blank=True)
    title_farsi = models.CharField(max_length=400, null=True, blank=True)

    def __str__(self):
        return self.title_latin


class TourService(models.Model):
    title_latin = models.CharField(max_length=500, null=True, blank=True)
    title_farsi = models.CharField(max_length=500, null=True, blank=True)

    def __str__(self):
        return self.title_latin


class ModeOfPayment(models.Model):
    title_latin = models.CharField(max_length=500, null=True, blank=True)
    title_farsi = models.CharField(max_length=500, null=True, blank=True)

    def __str__(self):
        return self.title_latin


class ProvidingMode(models.Model):
    title_latin = models.CharField(max_length=500, null=True, blank=True)
    title_farsi = models.CharField(max_length=500, null=True, blank=True)

    def __str__(self):
        return self.title_latin


class TimeFrame(models.Model):
    time_frame = models.CharField(max_length=400, null=True, blank=True)

    def __str__(self):
        return self.time_frame

def get_room_type_icon(instance, filename):
    return f"file_room_type_icon/{filename}"

class RoomType(models.Model):
    title_latin = models.CharField(max_length=500, null=True, blank=True)
    title_farsi = models.CharField(max_length=500, null=True, blank=True)
    quantity = models.IntegerField(choices=((1, 1), (2, 2), (3, 3), (4, 4), (5, 5),
                                            (6,6),(7,7),(8,8),(9,9),(10,10)), null=True, blank=True)

    icon = models.ImageField(upload_to=get_room_type_icon, null=True, blank=True)

       
    def __str__(self):
        return  self.title_latin
    

class UserGrade(models.Model):
    title_latin = models.CharField(max_length=500, null=True, blank=True)
    title_farsi = models.CharField(max_length=500, null=True, blank=True)
    icon = models.ImageField(upload_to=get_room_type_icon, null=True, blank=True)

       
    def __str__(self):
        return  self.title_latin
    


class CustomerServiceType(models.Model):
    icon = models.ImageField(upload_to=get_room_type_icon, null=True, blank=True)
    title_latin = models.CharField(max_length=500, null=True, blank=True)
    title_farsi = models.CharField(max_length=500, null=True, blank=True)
   
    def __str__(self):
        return "%s" % (self.title_latin,)

class CustomerServiceMode(models.Model):
    title_latin = models.CharField(max_length=500, null=True, blank=True)
    title_farsi = models.CharField(max_length=500, null=True, blank=True)
    desc_latin = models.TextField(null=True, blank=True)
    desc_farsi = models.TextField(null=True, blank=True)
    availablility = models.BooleanField(default=False)      
 
    def __str__(self):
        return "%s" % (self.title_latin,)




class AutoEmailTemplate(models.Model):
    TYPE = (
        ("Signup", "Signup"),
    )

    type = models.CharField(choices=TYPE, max_length=20, null=True, blank=True)
    text = models.TextField(null=True, blank=True)
 
    def __str__(self):
        return "%s" % (self.type,)


class HeardAboutUs(models.Model):
    title_latin = models.CharField(max_length=500)
    title_farsi = models.CharField(max_length=500, null=True, blank=True)

    def __str__(self):
        return self.title_latin

class Hotel(models.Model):
    hotel_name_latin = models.CharField(max_length=200)
    hotel_name_farsi = models.CharField(max_length=200, null=True, blank=True)
    region = models.ForeignKey('Raw_root.Region', on_delete=models.CASCADE)
    accom_type = models.ForeignKey('Raw_root.AccomType', on_delete=models.CASCADE)
    address = models.CharField(max_length=300, null=True, blank=True)
    telephone = models.CharField(max_length=20, null=True, blank=True)
    email = models.EmailField(null=True, blank=True)
    website = models.URLField(null=True, blank=True)
    staff_name = models.CharField(max_length=100, null=True, blank=True)

    def __str__(self):
        return self.hotel_name_latin

class HotelDistance(models.Model):
    hotel = models.ForeignKey('Raw_root.Hotel', on_delete=models.CASCADE, null=True, blank=True)
    to_center = models.ForeignKey('Content.Centers', on_delete=models.CASCADE, null=True, blank=True)
    distance_desc_latin = models.CharField(max_length=300, null=True, blank=True)
    distance_desc_farsi = models.CharField(max_length=300, null=True, blank=True)

    def __str__(self):
        return f"{self.hotel.hotel_name_latin} - {self.to_center}"


class BookingCondition(models.Model):
    title_latin = models.CharField(max_length=500, null=True, blank=True)
    title_farsi = models.CharField(max_length=500, null=True, blank=True)
  
 
    def __str__(self):
        return "%s" % (self.title_latin,)
