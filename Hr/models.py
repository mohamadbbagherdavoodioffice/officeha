from django.db import models


class HrTopic(models.Model):
    topic_latin = models.CharField(max_length=500, null=True, blank=True)
    topic_farsi = models.CharField(max_length=500, null=True, blank=True)

    def __str__(self):
        return self.topic_latin


class HrSkill(models.Model):
    skill_latin = models.CharField(max_length=500, null=True, blank=True)
    skill_farsi = models.CharField(max_length=500, null=True, blank=True)
    hr_topic = models.ManyToManyField(HrTopic, blank=True, )

    def __str__(self):
        return self.skill_latin


class HrTask(models.Model):
    task_name_latin = models.CharField(max_length=500, null=True, blank=True)
    task_name_farsi = models.CharField(max_length=500, null=True, blank=True)
    hr_skill = models.ManyToManyField(HrSkill, blank=True, )

    def __str__(self):
        return self.task_name_latin


class HrResponsibility(models.Model):
    title_latin = models.CharField(max_length=500, null=True, blank=True)
    title_farsi = models.CharField(max_length=500, null=True, blank=True)

    def __str__(self):
        return self.title_latin


def get_file_job_fam_thumbnail(instance, filename):
    return f"file_job_fam_thumbnail/{filename}"


def get_file_job_fam_banner(instance, filename):
    return f"file_job_fam_banner/{filename}"


class JobFamily(models.Model):
    job_fam_thumbnail = models.ImageField(upload_to=get_file_job_fam_thumbnail, null=True, blank=True)
    job_fam_banner = models.ImageField(upload_to=get_file_job_fam_banner, null=True, blank=True)
    job_fam_title_latin = models.CharField(max_length=500, null=True, blank=True)
    job_fam_title_farsi = models.CharField(max_length=500, null=True, blank=True)
    job_fam_text_latin = models.TextField(null=True, blank=True)
    job_fam_text_farsi = models.TextField(null=True, blank=True)
    tags = models.TextField( null=True, blank=True)

    def __str__(self):
        return self.job_fam_title_latin


class DutyStation(models.Model):
    title_latin = models.CharField(max_length=500, null=True, blank=True)
    title_farsi = models.CharField(max_length=500, null=True, blank=True)
    region = models.ForeignKey('Raw_root.Region', on_delete=models.CASCADE, null=True, blank=True, )

    def __str__(self):
        return self.title_latin


class HrQualification(models.Model):
    title_latin = models.CharField(max_length=500, null=True, blank=True)
    title_farsi = models.CharField(max_length=500, null=True, blank=True)

    def __str__(self):
        return self.title_latin


class WorkScheduleType(models.Model):
    title_latin = models.CharField(max_length=500, null=True, blank=True)
    title_farsi = models.CharField(max_length=500, null=True, blank=True)

    def __str__(self):
        return self.title_latin


class GenderPriority(models.Model):
    gender = models.ForeignKey('Raw_root.Gender', on_delete=models.CASCADE, null=True, blank=True, )
    title_latin = models.CharField(max_length=500, null=True, blank=True)
    title_farsi = models.CharField(max_length=500, null=True, blank=True)

    def __str__(self):
        return "%s | %s " % (self.title_latin,self.gender.title_latin)

class Job(models.Model):
    expiry_date = models.DateField(null=True, blank=True)
    job_code = models.CharField(max_length=500, null=True, blank=True)
    job_cat = models.ForeignKey('Hr.JobCategory', on_delete=models.CASCADE, null=True, blank=True, )
    # job_fam = models.ForeignKey(JobFamily, on_delete=models.CASCADE, primary_key=False, null=True, blank=True)
    age_range = models.ForeignKey('Raw_root.AgeRange', on_delete=models.PROTECT, null=True, blank=True, )
    guarantee = models.ForeignKey("Organization.GuaranteeAmount", on_delete=models.PROTECT, null=True,
                                  blank=True, )
    hr_required_test = models.ManyToManyField("Raw_root.HrRequiredTest",blank=True,related_name='job_hr_required_test_set' )
    public_trust = models.ForeignKey("Raw_root.HrPublicTrust", on_delete=models.PROTECT,   null=True , blank=True,  )
    contract_mode = models.ForeignKey("Raw_root.HrContractMode", on_delete=models.PROTECT, null=True,
                                           blank=True, )
    duty_station = models.ForeignKey('Hr.DutyStation', on_delete=models.CASCADE, null=True, blank=True, )
    starting_date = models.DateField(null=True, blank=True)
    work_schedule_type = models.ForeignKey(WorkScheduleType, on_delete=models.CASCADE, null=True, blank=True, )
    travel_required = models.ForeignKey('Raw_root.HrTravelRequired', on_delete=models.CASCADE, null=True, blank=True, )
    position_sensitivity = models.ManyToManyField('Raw_root.HrPositionSensitivity', blank=True, )
    job_task = models.ManyToManyField('Hr.HrTask', blank=True, )
    job_skill = models.ManyToManyField('Hr.HrSkill', blank=True, )
    job_reponsibility = models.ManyToManyField('Hr.HrResponsibility', blank=True, )
    job_qualification = models.ManyToManyField('Hr.HrQualification', blank=True, )
    gender_priority = models.ForeignKey(GenderPriority, on_delete=models.CASCADE, null=True, blank=True, )
    job_experience = models.ForeignKey('Hr.JobExpreience', on_delete=models.CASCADE, null=True,blank=True, )
    job_title_latin = models.CharField(max_length=500, null=True, blank=True)
    job_title_farsi = models.CharField(max_length=500, null=True, blank=True)
    job_description_latin = models.TextField( null=True, blank=True)
    job_description_farsi = models.TextField(null=True, blank=True)
    end_text_farsi = models.TextField(null=True, blank=True)
    end_text_latin = models.TextField(null=True, blank=True)
    tags = models.TextField( null=True, blank=True)


    def __str__(self):
        return "%s | %s" % (self.job_title_latin, self.duty_station)


# class JobQualification(models.Model):
#     job = models.ForeignKey(Job, on_delete=models.CASCADE, null=True, blank=True, )
#     hr_qualify = models.ManyToManyField(HrQualification, blank=True, )
#
#     def __str__(self):
#         return str(self.job)


# class JobTask(models.Model):
#     job = models.ForeignKey(Job, on_delete=models.CASCADE, null=True, blank=True, )
#     hr_task = models.ManyToManyField(HrTask, blank=True, )
#
#     def __str__(self):
#         return str(self.job)


# class JobResponsibility(models.Model):
#     job = models.ForeignKey(Job, on_delete=models.CASCADE, null=True, blank=True, )
#     hr_responsibility = models.ManyToManyField(HrResponsibility, blank=True, )
#
#     def __str__(self):
#         return str(self.job)


def get_file_form_pdf(instance, filename):
    return f"file_form_pdf/{filename}"


class JobForms(models.Model):
    form_pdf = models.FileField(upload_to=get_file_form_pdf)

    def __str__(self):
        return str(self.form_pdf)


class StaffWorkLog(models.Model):
    user = models.ForeignKey('OfficeHauser.OfficeUser', on_delete=models.CASCADE, primary_key=False,related_name='staff_work_log_by_user_set', null=True, blank=True)
    from_date_time = models.DateTimeField(null=True, blank=True)
    to_date_time = models.DateTimeField(null=True, blank=True)

    def __str__(self):
        return str(self.from_date_time)
        # firstname lastname


class StaffActionLog(models.Model):
    user = models.ForeignKey('OfficeHauser.OfficeUser', on_delete=models.CASCADE, primary_key=False,
                             related_name='staff_action_log_user_set', null=True, blank=True)
    from_date_time = models.TimeField(null=True, blank=True)
    application_number = models.ForeignKey('ApplicationOperation.ApplicationProfile', on_delete=models.CASCADE,
                                           null=True, blank=True)
    model_name = models.CharField(max_length=500, null=True, blank=True)

    def __str__(self):
        return "%s | %s | %s" % (self.model_name, self.application_number, self.from_date_time)
        # first name last name


class HrContract(models.Model):
    user = models.ForeignKey('OfficeHauser.OfficeUser', on_delete=models.CASCADE, primary_key=False,related_name='hrcontranct_user_set', null=True, blank=True)
    contract = models.ForeignKey('Organization.ContractProfile', on_delete=models.PROTECT, null=True, blank=True, )
    job = models.ForeignKey('Hr.Job', on_delete=models.CASCADE, related_name='hr_contract_job_set', null=True,blank=True, )
    guarantee = models.ForeignKey("ApplicationOperation.AppGuaranteeOpt", on_delete=models.PROTECT, null=True,blank=True, )
    notary_text = models.ForeignKey('Organization.NotaryText', on_delete=models.CASCADE,related_name='hr_contract_notary_text_set', null=True, blank=True, )
    user = models.ForeignKey('OfficeHauser.OfficeUser', on_delete=models.CASCADE, primary_key=False,related_name='hr_contract_office_user_set', null=True, blank=True)
    notary_sign_date_time = models.DateTimeField(null=True, blank=True)
    notary_trace_code = models.DateTimeField(null=True, blank=True)
    hourly_fee = models.DecimalField(max_digits=10, decimal_places=2, null=True, blank=True)
    currency = models.ForeignKey("Raw_root.Currency", on_delete=models.CASCADE, related_name="hr_contract_currency_set",null=True, blank=True, )
    insurance_hours_per_day_limit = models.PositiveIntegerField(null=True, blank=True)
    weekly_hours_limit = models.PositiveIntegerField(null=True, blank=True)
    monthly_hours_limit = models.PositiveIntegerField(null=True, blank=True)
    from_date = models.DateField(null=True, blank=True)
    to_date = models.DateField(null=True, blank=True)
    sign_date_time = models.DateTimeField(null=True, blank=True)
    staff_user = models.ForeignKey('OfficeHauser.OfficeUser', on_delete=models.CASCADE, primary_key=False,related_name='hr_contract_staff_user_set', null=True, blank=True)


class HrBalanceDuration(models.Model):
    days = models.PositiveIntegerField(null=True, blank=True)


class StaffSchedule(models.Model):
    hr_contract = models.ForeignKey(HrContract, on_delete=models.CASCADE, primary_key=False,
                                    related_name='staff_schedule_hr_contract', null=True, blank=True)
    WK = (('Sat', 'Sat'), ('Sun', 'Sun'),
                                         ('Mon', 'Mon'),
                                         ('Tue', 'Tue'),
                                         ('Wed', 'Wed'),
                                         ('Thu', 'Thu'),
                                         ('Fri', 'Fri'))
    week_day_1 = models.CharField(choices=WK, max_length=10, null=True, blank=True)
    from_time_1 = models.TimeField(null=True, blank=True)
    to_time_1 = models.TimeField(null=True, blank=True)

    week_day_2 = models.CharField(choices=WK, max_length=10, null=True, blank=True)
    from_time_2 = models.TimeField(null=True, blank=True)
    to_time_2 = models.TimeField(null=True, blank=True)

    week_day_3 = models.CharField(choices=WK, max_length=10, null=True, blank=True)
    from_time_3 = models.TimeField(null=True, blank=True)
    to_time_3 = models.TimeField(null=True, blank=True)

    week_day_4 = models.CharField(choices=WK, max_length=10, null=True, blank=True)
    from_time_4 = models.TimeField(null=True, blank=True)
    to_time_4 = models.TimeField(null=True, blank=True)

    week_day_5 = models.CharField(choices=WK, max_length=10, null=True, blank=True)
    from_time_5 = models.TimeField(null=True, blank=True)
    to_time_5 = models.TimeField(null=True, blank=True)

    week_day_6 = models.CharField(choices=WK, max_length=10, null=True, blank=True)
    from_time_6 = models.TimeField(null=True, blank=True)
    to_time_6 = models.TimeField(null=True, blank=True)

    week_day_7 = models.CharField(choices=WK, max_length=10, null=True, blank=True)
    from_time_7 = models.TimeField(null=True, blank=True)
    to_time_7 = models.TimeField(null=True, blank=True)


    def __str__(self):
        return str(self.week_day)


class JobApplicationProcess(models.Model):
    step_number = models.IntegerField(null=True, blank=True)
    title_latin = models.CharField(max_length=500, null=True, blank=True)
    title_farsi = models.CharField(max_length=500, null=True, blank=True)
    color_code_unread = models.CharField(max_length=500, null=True, blank=True)
    color_code_read = models.CharField(max_length=500, null=True, blank=True)
    text_latin = models.TextField(max_length=500, null=True, blank=True)
    text_farsi = models.TextField(max_length=500, null=True, blank=True)

    def __str__(self):
        return self.title_latin


class JobCategory(models.Model):
    job_fam = models.ForeignKey(JobFamily, on_delete=models.CASCADE, primary_key=False, null=True, blank=True)
    title_latin = models.CharField(max_length=500, null=True, blank=True)
    title_farsi = models.CharField(max_length=500, null=True, blank=True)

    # def __str__(self):
    #     return self.title_latin
    def __str__(self):
        return "%s | %s " % (self.title_latin,self.job_fam.job_fam_title_latin)

class ContractMode(models.Model):
    title_latin = models.CharField(max_length=500, null=True, blank=True)
    title_farsi = models.CharField(max_length=500, null=True, blank=True)

    def __str__(self):
        return self.title_latin


class JobExpreience(models.Model):
    title_latin = models.CharField(max_length=500, null=True, blank=True)
    title_farsi = models.CharField(max_length=500, null=True, blank=True)

    def __str__(self):
        return self.title_latin


def get_file_job_application_pdf_form(instance, filename):
    return f"job_application_pdf_form/{filename}"


def get_file_job_application_banner(instance, filename):
    return f"file_job_application_banner/{filename}"

class CareerPageConfig(models.Model):
    first_text_latin = models.CharField(max_length=500, null=True, blank=True)
    first_text_farsi = models.CharField(max_length=500, null=True, blank=True)
    job_families_text_latin = models.TextField(null=True, blank=True)
    job_families_text_farsi = models.TextField(null=True, blank=True)
    job_process_title_latin = models.CharField(max_length=500, null=True, blank=True)
    job_process_title_farsi = models.CharField(max_length=500, null=True, blank=True)

    self_assessment_title_latin = models.CharField(max_length=500, null=True, blank=True)
    self_assessment_title_farsi = models.CharField(max_length=500, null=True, blank=True)
    self_assessment_text_latin = models.TextField(null=True, blank=True)
    self_assessment_text_farsi = models.TextField(null=True, blank=True)

    job_openings_title_latin = models.CharField(max_length=500, null=True, blank=True)
    job_openings_title_farsi = models.CharField(max_length=500, null=True, blank=True)
    job_openings_text_latin = models.TextField(null=True, blank=True)
    job_openings_text_farsi = models.TextField(null=True, blank=True)

    job_application_page_text_latin = models.TextField(null=True, blank=True)
    job_application_page_text_farsi = models.TextField(null=True, blank=True)
    job_application_pdf_form = models.FileField(upload_to=get_file_job_application_pdf_form,null=True, blank=True)
    banner = models.ImageField(upload_to=get_file_job_application_banner, null=True, blank=True)

    def __str__(self):
        return self.first_text_latin

class ConsultTeam(models.Model):
    full_name_latin = models.CharField(max_length=300, null=True, blank=True)
    full_name_farsi = models.CharField(max_length=300, null=True, blank=True)
    consultation_categories = models.ManyToManyField('Raw_root.ConsultationCategory', blank=True)
    consultation_modes= models.ForeignKey('Raw_root.ConsultationMode', on_delete=models.CASCADE, null=True, blank=True, )
    def __str__(self):
        return self.full_name_latin