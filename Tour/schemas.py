from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema


TourDetails2IdAPIView_response_schema = openapi.Schema(
    type=openapi.TYPE_OBJECT,
    properties={
        "message": openapi.Schema(type=openapi.TYPE_STRING),
        "status": openapi.Schema(type=openapi.TYPE_STRING),
        "data": openapi.Schema(
            type=openapi.TYPE_OBJECT,
            properties={
                "tour": openapi.Schema(
                    type=openapi.TYPE_ARRAY,
                    items=openapi.Schema(
                        type=openapi.TYPE_OBJECT,
                        properties={
                            "id": openapi.Schema(type=openapi.TYPE_INTEGER),
                            "day_region": openapi.Schema(
                                type=openapi.TYPE_OBJECT,
                                properties={
                                    "id": openapi.Schema(type=openapi.TYPE_INTEGER),
                                    "title_latin": openapi.Schema(type=openapi.TYPE_STRING),
                                    "title_farsi": openapi.Schema(type=openapi.TYPE_STRING),
                                    "our_coverage": openapi.Schema(type=openapi.TYPE_BOOLEAN),
                                    "type": openapi.Schema(type=openapi.TYPE_INTEGER),
                                    "country": openapi.Schema(type=openapi.TYPE_INTEGER),
                                    "province": openapi.Schema(type=openapi.TYPE_INTEGER, nullable=True)
                                }
                            ),
                            "night_region": openapi.Schema(
                                type=openapi.TYPE_OBJECT,
                                properties={
                                    "id": openapi.Schema(type=openapi.TYPE_INTEGER),
                                    "title_latin": openapi.Schema(type=openapi.TYPE_STRING),
                                    "title_farsi": openapi.Schema(type=openapi.TYPE_STRING),
                                    "our_coverage": openapi.Schema(type=openapi.TYPE_BOOLEAN),
                                    "type": openapi.Schema(type=openapi.TYPE_INTEGER),
                                    "country": openapi.Schema(type=openapi.TYPE_INTEGER),
                                    "province": openapi.Schema(type=openapi.TYPE_INTEGER, nullable=True)
                                }
                            ),
                            "accom_type": openapi.Schema(
                                type=openapi.TYPE_OBJECT,
                                properties={
                                    "id": openapi.Schema(type=openapi.TYPE_INTEGER),
                                    "title_latin": openapi.Schema(type=openapi.TYPE_STRING),
                                    "title_farsi": openapi.Schema(type=openapi.TYPE_STRING)
                                }
                            ),
                            "meal": openapi.Schema(
                                type=openapi.TYPE_OBJECT,
                                properties={
                                    "id": openapi.Schema(type=openapi.TYPE_INTEGER),
                                    "title_latin": openapi.Schema(type=openapi.TYPE_STRING),
                                    "title_farsi": openapi.Schema(type=openapi.TYPE_STRING),
                                    "abbreviation": openapi.Schema(type=openapi.TYPE_STRING)
                                }
                            ),
                            "thing_to_do": openapi.Schema(
                                type=openapi.TYPE_ARRAY,
                                items=openapi.Schema(
                                    type=openapi.TYPE_OBJECT,
                                    properties={
                                        "id": openapi.Schema(type=openapi.TYPE_INTEGER),
                                        "title_latin": openapi.Schema(type=openapi.TYPE_STRING, nullable=True),
                                        "title_farsi": openapi.Schema(type=openapi.TYPE_STRING, nullable=True),
                                        "description": openapi.Schema(type=openapi.TYPE_STRING, nullable=True),
                                        "banner": openapi.Schema(type=openapi.TYPE_STRING, nullable=True),
                                        "visiting_time_in_min": openapi.Schema(type=openapi.TYPE_INTEGER, nullable=True),
                                        "ticket_fee": openapi.Schema(type=openapi.TYPE_INTEGER, nullable=True),
                                        "upload_date_time": openapi.Schema(type=openapi.TYPE_STRING, nullable=True),
                                        "approval": openapi.Schema(type=openapi.TYPE_BOOLEAN),
                                        "thing_to_do_cat": openapi.Schema(type=openapi.TYPE_INTEGER, nullable=True),
                                        "region": openapi.Schema(type=openapi.TYPE_INTEGER, nullable=True),
                                        "currency": openapi.Schema(type=openapi.TYPE_INTEGER, nullable=True),
                                        "photo_by": openapi.Schema(type=openapi.TYPE_STRING, nullable=True)
                                    }
                                )
                            ),
                            "attraction": openapi.Schema(
                                type=openapi.TYPE_ARRAY,
                                items=openapi.Schema(
                                    type=openapi.TYPE_OBJECT,
                                    properties={
                                        "id": openapi.Schema(type=openapi.TYPE_INTEGER),
                                        "title_latin": openapi.Schema(type=openapi.TYPE_STRING),
                                        "title_farsi": openapi.Schema(type=openapi.TYPE_STRING)
                                    }
                                )
                            ),
                            "local_guide": openapi.Schema(type=openapi.TYPE_OBJECT, nullable=True),
                            "seq": openapi.Schema(type=openapi.TYPE_INTEGER),
                            "activity": openapi.Schema(type=openapi.TYPE_STRING),
                            "day_image": openapi.Schema(type=openapi.TYPE_STRING),
                            "tour": openapi.Schema(type=openapi.TYPE_INTEGER)
                        }
                    )
                )
            }
        )
    }
)