from rest_framework import serializers
from Edu.serializers import ProgramListSerializer, SchoolListSerializer
from Hr.serializers import JobListSerializer
from OfficeHauser.serializers import OfficeUserBriefSerializer, OfficeUserUsernameSerializer ,OfficeUserDetailsSerializer
from Raw_root.models import VisaSubCat
from Services.models import ServiceCategory
from .models import *
from Tour.models import Tour

# from Visa.serializers import TourDetailsForArticlesSerializer
from Visa.models import VisaStream
from Raw_root.serializers import CountrySerializer, VisaSubCatSerializer


class TourSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tour
        fields = '__all__'



class ServiceCategoryAllSerializer(serializers.ModelSerializer):
    class Meta:
        model = ServiceCategory
        fields = ['id','title_latin','title_farsi']


class NewCatSerializer(serializers.ModelSerializer):
    class Meta:
        model = NewCat
        fields = '__all__'


class VisaFormsSerializer(serializers.ModelSerializer):
    class Meta:
        model = VisaForms
        fields = '__all__'


class SpecificFaqSerializer(serializers.ModelSerializer):
    class Meta:
        model = SpecificFaq
        fields = '__all__'


class SpecificFAQ2Serializer(serializers.ModelSerializer):
    tour = serializers.SerializerMethodField()
    stid = serializers.SerializerMethodField()

    class Meta:
        model = SpecificFaq
        fields = '__all__'

    def get_tour(self, instance):
        ids = int(instance.id)
        data = Tour.objects.filter(pk=ids)
        serializer = TourSerializer(data, many=True)
        return serializer.data

    def get_stid(self, instance):
        ids = int(instance.id)
        data = VisaStream.objects.filter(pk=ids)
        serializer = VisaStreamSerializer(data, many=True)
        return serializer.data


class ArticleServiceCatListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Article
        fields = ['title_latin','title_farsi','banner']


class ArticleServiceCatListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Article
        fields = ['title_latin','title_farsi','banner']


class ArticleProgramIDListSerializer(serializers.ModelSerializer):
    by_staff_user = OfficeUserBriefSerializer()
    class Meta:
        model = Article
        fields = ['title_latin','title_farsi','banner','program','by_staff_user']


class ArticleServiceCategoryIDListSerializer(serializers.ModelSerializer):
    by_staff_user = OfficeUserBriefSerializer()
    class Meta:
        model = Article
        fields = ['title_latin','title_farsi','banner','service_category','by_staff_user']

class ArticleSTIDListSerializer(serializers.ModelSerializer):
    by_staff_user = OfficeUserBriefSerializer()
    class Meta:
        model = Article
        fields = ['title_latin','title_farsi','banner','stid','by_staff_user']


class ArticleTourIDListSerializer(serializers.ModelSerializer):
    by_staff_user = OfficeUserBriefSerializer()
    class Meta:
        model = Article
        fields = ['title_latin','title_farsi','banner','tour','by_staff_user']


class ArticleSchoolIDListSerializer(serializers.ModelSerializer):
    by_staff_user = OfficeUserBriefSerializer()
    class Meta:
        model = Article
        fields = ['title_latin','title_farsi','banner','school','by_staff_user']


class ArticleJobIDListSerializer(serializers.ModelSerializer):
    by_staff_user = OfficeUserBriefSerializer()
    class Meta:
        model = Article
        fields = ['title_latin','title_farsi','banner','job','by_staff_user']


class LegalSectionSerializer(serializers.ModelSerializer):
    class Meta:
        model = LegalSection
        fields = '__all__'

class TermClausesSerializer(serializers.ModelSerializer):
    # term_section = LegalSectionSerializer()
    class Meta:
        model = TermClauses
        fields = '__all__'

class PrivacyClausesSerializer(serializers.ModelSerializer):
    # term_section = LegalSectionSerializer(many=True)
    privacy_section = LegalSectionSerializer(many=True)
    class Meta:
        model = PrivacyClauses
        fields = '__all__'

class ArticleListSerializer(serializers.ModelSerializer):
    by_staff_user= OfficeUserUsernameSerializer()
    class Meta:
        model = Article
        fields = ['id','title_latin','title_farsi','thumbnail','by_staff_user']

class VisaStreamSerializer(serializers.ModelSerializer):
    visa_sub_cat = serializers.SerializerMethodField()

    class Meta:
        model = VisaStream
        fields = '__all__'

    def get_visa_sub_cat(self, instance):
        ids = int(instance.id)
        data = VisaSubCat.objects.filter(pk=ids)
        serializer = VisaSubCatSerializer(data, many=True)
        return serializer.data

class TourDetailsForArticlesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tour
        fields = ['id','title_latin','title_farsi']

class ArticleEachIDSerializer(serializers.ModelSerializer):
    by_staff_user= OfficeUserBriefSerializer()
    service_category = ServiceCategoryAllSerializer( many=True)
    stid = VisaStreamSerializer( many=True)
    tour = TourDetailsForArticlesSerializer( many=True)
    program = ProgramListSerializer(many=True)
    school = SchoolListSerializer(many=True)
    job = JobListSerializer()
    class Meta:
        model = Article
        fields = ['id','title_latin', 'title_farsi', 'banner', 'service_category', 'stid', 'tour', 'program', 'school', 'job', 'by_staff_user', 'text', 'last_updated_date', ]




class NewSerializer(serializers.ModelSerializer):
    country = CountrySerializer()
    new_cat = NewCatSerializer()
    creator_staff =OfficeUserDetailsSerializer()
    class Meta:
        model = New
        fields = '__all__'

class ThingToDoSerializer(serializers.ModelSerializer):

    class Meta:
        model = ThingToDo
        fields = '__all__'
        
    
class ThingToDoTitleSerializer(serializers.ModelSerializer):

    class Meta:
        model = ThingToDo
        fields = ['id','title_latin','title_farsi']

class GeneralFAQSerializer(serializers.ModelSerializer):

    class Meta:
        model = GeneralFAQ
        fields = '__all__'


class SpecificFaqSerializer(serializers.ModelSerializer):

    class Meta:
        model = SpecificFaq
        fields = '__all__'



class PassengerRightSerializer(serializers.ModelSerializer):

    class Meta:
        model = PassengerRight
        fields = '__all__'
    
class PassengerRightBreifSerializer(serializers.ModelSerializer):

    class Meta:
        model = PassengerRight
        fields = ['id','title_latin','title_farsi','thumbnail','tags']