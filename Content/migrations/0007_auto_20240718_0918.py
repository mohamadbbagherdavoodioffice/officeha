# Generated by Django 3.2.18 on 2024-07-18 09:18

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('Raw_root', '0008_hotel_hoteldistance'),
        ('Content', '0006_generalfaq_approval'),
    ]

    operations = [
        migrations.AddField(
            model_name='centers',
            name='bio_in_country',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='bio_incountry_id_raw_root', to='Raw_root.country'),
        ),
        migrations.AlterField(
            model_name='centers',
            name='country',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='centers_history_country_set', to='Raw_root.country', verbose_name='Visa Country'),
        ),
    ]
