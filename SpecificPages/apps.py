from django.apps import AppConfig


class SpecificpagesConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'SpecificPages'
    verbose_name = "Specific Pages"
