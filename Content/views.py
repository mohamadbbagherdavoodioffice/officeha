from django.shortcuts import render
import random
from django.http import JsonResponse, HttpResponse
from django.contrib.auth.hashers import check_password
from django.db.models import Q, Count
from rest_framework import status, viewsets, pagination
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet
from rest_framework.generics import CreateAPIView, get_object_or_404, ListAPIView, RetrieveAPIView, ListCreateAPIView
from .models import *
from .serializers import *
from Hr.serializers import TravelRequirementSerializer

class CustomPagination(pagination.PageNumberPagination):
    page_size = 20
    page_size_query_param = 'page_size'
    max_page_size = 500
    page_query_param = 'p'

class NewAPIView(ListAPIView):
    serializer_class = NewSerializer
    queryset = New.objects.all().order_by('create_date_time')
    pagination_class = CustomPagination

class NewCatAPIView(ListAPIView):
    serializer_class = NewCatSerializer
    queryset = NewCat.objects.all()


class VisaFormsList(RetrieveAPIView):
    serializer_class = VisaFormsSerializer
    queryset = VisaForms.objects.all()
    def post(self, request, *args, **kwargs):
        qs = VisaForms.objects.filter(
            Q(country__id=self.request.GET.get('country'))
            & Q(main_cat__id=self.request.GET.get('main_cat'))).values()
        return Response(qs)




class ArticleProgramIDListAPIView(ListAPIView):
    serializer_class = ArticleProgramIDListSerializer
    queryset = Article.objects.all()
    def get_queryset(self):
        program = self.kwargs['program']
        return Article.objects.filter(program=program)



class ArticleServiceCategoryIDListAPIView(ListAPIView):
    serializer_class = ArticleServiceCategoryIDListSerializer
    queryset = Article.objects.all()
    def get_queryset(self):
        service_category = self.kwargs['service_category']
        return Article.objects.filter(service_category=service_category)



class ArticleSTIDListAPIView(ListAPIView):
    serializer_class = ArticleSTIDListSerializer
    queryset = Article.objects.all()
    def get_queryset(self):
        stid = self.kwargs['stid']
        return Article.objects.filter(stid=stid)


class ArticleTourIDListAPIView(ListAPIView):
    serializer_class = ArticleTourIDListSerializer
    queryset = Article.objects.all()

    def get_queryset(self):
        tour = self.kwargs['tour']
        return Article.objects.filter(tour=tour)

class ArticleSchoolIDListAPIView(ListAPIView):
    serializer_class = ArticleSchoolIDListSerializer
    queryset = Article.objects.all()

    def get_queryset(self):
        school = self.kwargs['school']
        return Article.objects.filter(school=school)

class ArticleJobIDListAPIView(ListAPIView):
    serializer_class = ArticleJobIDListSerializer
    queryset = Article.objects.all()
    def get_queryset(self):
        job = self.kwargs['job']
        return Article.objects.filter(job=job)




class CountryGetListForApplyVisaForm(APIView):
    def get(self, request):
        dic = {}

        country = VisaForms.objects.annotate(num_offerings=Count('id')).values('country__name_latin',
                                                                                    'country__name_farsi',
                                                                                    'country__id').distinct()
        dic["country"] = list(country)

        return Response({"message": "success", "status": "OK", "data": dic}, status=200)




class TermClausesListAPIView(ListAPIView):
    serializer_class = TermClausesSerializer
    queryset = TermClauses.objects.all().order_by('clause_seq')


class PrivacyClausesListAPIView(ListAPIView):
    serializer_class = PrivacyClausesSerializer
    queryset = PrivacyClauses.objects.all().order_by('clause_seq')


class ArticleUsersAPIVIEW(APIView):
    def get(self, request):
        dic = {}


        data_i = Article.objects.filter(by_staff_user=self.request.GET.get("user_id")).filter(approval=True)
        serializer_i = ArticleListSerializer(data_i, many=True)
        dic = serializer_i.data

        return Response({"message": "success", "status": "OK", "data": dic}, status=200)



class ArticleEachIDAPIView(ListAPIView):
    serializer_class = ArticleEachIDSerializer
    queryset = Article.objects.all()
    def get_queryset(self):
        job = self.kwargs['id']
        return Article.objects.filter(id=job).filter(approval=True)


class ArticleListIsNotNullAPIView(APIView):
    def get(self, request):
        dic = {}

        sc = Article.objects.exclude(Q(service_category__isnull=True) & Q(approval=True)).order_by('?').values("id","title_latin","title_farsi","thumbnail","by_staff_user__username")[:4]
        dic['service_category']= sc

        stid = Article.objects.exclude(Q(stid__isnull=True)& Q(approval=True)).order_by('?').values("id","title_latin","title_farsi","thumbnail","by_staff_user__username")[:4]
        dic['visa'] = stid

        tour = Article.objects.exclude(Q(tour__isnull=True)& Q(approval=True)).order_by('?').values("id","title_latin","title_farsi","thumbnail","by_staff_user__username")[:4]
        dic['tour'] = tour

        program = Article.objects.exclude(Q(program__isnull=True)& Q(approval=True)).order_by('?').values("id","title_latin","title_farsi","thumbnail","by_staff_user__username")[:4]
        dic['program'] = program

        school = Article.objects.exclude(Q(school__isnull=True)& Q(approval=True)).order_by('?').values("id","title_latin","title_farsi","thumbnail","by_staff_user__username")[:4]
        dic['school'] = school

        job = Article.objects.exclude(Q(job__isnull=True)& Q(approval=True)).order_by('?').values("id","title_latin","title_farsi","thumbnail","by_staff_user__username")[:4]
        dic['job'] = job

        return Response({"message": "success", "status": "OK", "data": dic}, status=200)




class ArticleListServiceCategoryVALUEAPIView(ListAPIView):
    serializer_class = ArticleListSerializer
    queryset = Article.objects.filter(Q(service_category__isnull=False)& Q(approval=True)).order_by('id')
    pagination_class = CustomPagination


class ArticleListStidVALUEAPIView(ListAPIView):
    serializer_class = ArticleListSerializer
    queryset = Article.objects.filter(Q(stid__isnull=False)& Q(approval=True)).order_by('id').distinct('id')
    pagination_class = CustomPagination


class ArticleListTourVALUEAPIView(ListAPIView):
    serializer_class = ArticleListSerializer
    queryset = Article.objects.filter(Q(tour__isnull=False)& Q(approval=True)).order_by('id')
    pagination_class = CustomPagination


class ArticleListProgramVALUEAPIView(ListAPIView):
    serializer_class = ArticleListSerializer
    queryset = Article.objects.filter(Q(program__isnull=False) & Q(approval=True)).order_by('id')
    pagination_class = CustomPagination


class ArticleListSchoolVALUEAPIView(ListAPIView):
    serializer_class = ArticleListSerializer
    queryset = Article.objects.filter(Q(school__isnull=False)& Q(approval=True)).order_by('id')
    pagination_class = CustomPagination

class ArticleListJobVALUEAPIView(ListAPIView):
    serializer_class = ArticleListSerializer
    queryset = Article.objects.filter(Q(job__isnull=False) & Q(approval=True)).order_by('id')
    pagination_class = CustomPagination


class ArticleEachTourElementAPIView(ListAPIView):
    serializer_class = ArticleListSerializer
    queryset = Article.objects.all()
    pagination_class = CustomPagination

    def get_queryset(self):
        te = self.request.GET.get('tour_element')

        return Article.objects.filter(tour_element__in=te.split(','))


class GeneralFAQListApiView(ListAPIView):
    serializer_class = GeneralFAQSerializer
    queryset = GeneralFAQ.objects.all()


# class GeneralFAQListApiView(ListAPIView):
#     serializer_class = GeneralFAQSerializer
#     queryset = GeneralFAQ



class SpecificFaqListApiView(APIView):
    def get(self, request):
        dic = {}

        sc = SpecificFaq.objects.filter(Q(tour=True) & Q(stid=True) | Q(service_category=True))
        dic= sc

        if self.request.GET.get('service_category'):
            dic = SpecificFaq.objects.filter(Q(service_category=self.request.GET.get('service_category'))).values()
        if self.request.GET.get('stid'):
            dic = SpecificFaq.objects.filter(Q(stid=self.request.GET.get('stid'))).values()
        if self.request.GET.get('tour'):
            dic = SpecificFaq.objects.filter(Q(tour=self.request.GET.get('tour'))).values()

        return Response({"message": "success", "status": "OK", "data": dic}, status=200)






class TravelRequirementDetailsAPIView(ListAPIView):
    def list(self, request, *args, **kwargs):
        id1 = self.request.GET.get('id')
        dict = {}
        data_sctd = TravelRequirement.objects.filter(country__id=id1)
        serializer_sctd = TravelRequirementSerializer(data_sctd, many=True)
        dict["travel_requirement"] = serializer_sctd.data

        return Response({
            'status': 200,
            'message': 'success',
            'data': dict
        })


class TravelRequirementListCountriesAPIView(ListAPIView):
    def list(self, request, *args, **kwargs):
        id1 = self.request.GET.get('id')
        dict = {}
        data_sctd = TravelRequirement.objects.all().values('country__id', 'country__name_latin', 'country__name_farsi').distinct()
        return Response(data_sctd)
    

class PassengerRightListApiView(ListAPIView):
    serializer_class = PassengerRightBreifSerializer
    queryset = PassengerRight.objects.all()

class PassengerRightRetrieveApiView(RetrieveAPIView):
    serializer_class = PassengerRightSerializer
    queryset = PassengerRight.objects.all()
    lookup_url_kwarg = "id"
    """get /1"""