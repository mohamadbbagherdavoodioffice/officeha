from django.urls import path, include, re_path
from .views import *
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    # path('', include(router.urls)),
    path('create_contact_us', ContactUsAPIView.as_view()),
    path('create_tell_us', TellUsWhatYouThinkAPIView.as_view()),
    path('create_feed_back', FeedbackCreateAPIView.as_view()),
    path('d', dataAPIView.as_view()),
    path('create_edu_apply_request', EduApplyRequestCreateAPIView.as_view()),
    path('get_job_application_forms_static', JobApplicationFormStaticAPIView.as_view()),
    path('create_job_seeker', JobSeekerCreateAPIView.as_view()),
    path('create_evaluation_form', EvaluationFormAPIView.as_view()),
    path('create_tour_request', TourRequestAPIView.as_view()),
    path('create_workshop_participant', CreateWorkshopParticipantView.as_view(), name='create_workshop_participant'),

]

urlpatterns = urlpatterns + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

