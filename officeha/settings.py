import os
from pathlib import Path
from decouple import config



BASE_DIR = Path(__file__).resolve().parent.parent

SECRET_KEY = 'django-insecure-rcp-1exm&jo^j56#)_ibd9)iob7o1!of+x$(^mo4n3)t5j)p$k'

DEBUG = True

AUTH_USER_MODEL = "OfficeHauser.OfficeUser"

print(AUTH_USER_MODEL)


INSTALLED_APPS = [
    # 'admin_interface',
    # 'colorfield',
    # 'adminlteui',
    'Financial',
    'OfficeHauser',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'rest_framework.authtoken',
    'PublicPages.apps.PublicpagesConfig',
    'Raw_root.apps.RawrootConfig',
    'SpecificPages.apps.SpecificpagesConfig',
    'Inbox',
    'Hr',
    'Organization',
    'Edu',
    'Content',
    'Services',
    'ApplicationOperation',
    'Tour',
    'Visa',
    'HomePageConfig',
    'ApplicantData',
    'django_extensions',
    'import_export',
    'rest_framework',
    'django_summernote',
    'corsheaders',
    'drf_yasg',
    'django_celery_results',
    'django_celery_beat',
    # 'rest_framework_swagger'
  
    'rest_framework_simplejwt',
    'rest_framework_simplejwt.token_blacklist',
    'rest_framework_swagger',

]


DBBACKUP_STORAGE = 'django.core.files.storage.FileSystemStorage'
DBBACKUP_STORAGE_OPTIONS = {'location': '/my/backup/dir/'}
#for colorfol ui
# X_FRAME_OPTIONS = 'SAMEORIGIN'
# SILENCED_SYSTEM_CHECKS = ['security.W019']

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    "corsheaders.middleware.CorsMiddleware",
    "django.middleware.common.CommonMiddleware",
]

ROOT_URLCONF = 'officeha.urls'




TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {

            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'officeha.wsgi.application'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'office_ha',
        'USER': 'office_ha',
        'PASSWORD': 'timnak123',
        'HOST': 'localhost',
        'PORT': '5432',
        'DISABLE_SERVER_SIDE_CURSORS': True,

    }
}

DATA_WIZARD = {
    'BACKEND': 'data_wizard.backends.threading',
    'LOADER': 'data_wizard.loaders.FileLoader',
    'IDMAP': 'data_wizard.idmap.never',  # 'data_wizard.idmap.existing' in 2.0
    'AUTHENTICATION': 'rest_framework.authentication.SessionAuthentication',
    'PERMISSION': 'rest_framework.permissions.IsAdminUser',
}

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True

STATIC_URL = '/static/'

DEFAULT_AUTO_FIELD = 'django.db.models.BigAutoField'

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
STATIC_URL = '/static/'
STATIC_ROOT = os.path.join(BASE_DIR, 'static/')

MEDIA_URL = '/media/'
MEDIA_ROOT = os.path.join(BASE_DIR, 'media')



ALLOWED_HOSTS = ['www.timnak.ir','timnak.ir','api.timnak.ir', 'office.timnak.ir',
    'localhost',
    '157.90.138.115:8345',
                 '157.90.138.115',
                 'localhost:3000',
                 '192.168.1.106:3000',
                 '127.0.0.1',
                 '192.168.1.105:8000',
                 '192.168.1.105:7001',
                 '192.168.1.105',
                 '178.173.147.89',
                 '192.168.1.103:7001',
                 '192.168.1.103',
                 '192.168.1.100:7001',
                 '192.168.1.100',
                 ]

# only if django version >= 3.0
X_FRAME_OPTIONS = 'SAMEORIGIN'
SILENCED_SYSTEM_CHECKS = ['security.W019']
# STATICFILES_STORAGE = 'django.contrib.staticfiles.storage.CachedStaticFilesStorage'

REST_FRAMEWORK = {
    'DEFAULT_AUTHENTICATION_CLASSES': (

        'rest_framework_simplejwt.authentication.JWTAuthentication',
    )
    , 'DEFAULT_PERMISSION_CLASSES': (
       'rest_framework.permissions.AllowAny',
    )
    , 'TEST_REQUEST_RENDERER_CLASSES': [
        'rest_framework.renderers.MultiPartRenderer',
        'rest_framework.renderers.JSONRenderer',
        'rest_framework.renderers.TemplateHTMLRenderer'
    ],
    'DEFAULT_FILTER_BACKENDS': ['django_filters.rest_framework.DjangoFilterBackend']
    # 'DEFAULT_RENDERER_CLASSES': [
    #     'rest_framework.renderers.JSONRenderer',
    # ]
}

PASSWORD_RESET_TIMEOUT = 259200 # 3 days
CORS_ORIGIN_ALLOW_ALL = True
#1m25650668
CORS_ALLOW_CREDENTIALS = True
CORS_ALLOW_ALL_ORIGINS = True 



DATA_UPLOAD_MAX_MEMORY_SIZE = 1024 * 1024 * 25  # 25M
FILE_UPLOAD_MAX_MEMORY_SIZE = DATA_UPLOAD_MAX_MEMORY_SIZE



from celery import Celery
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'sciences.settings')
app = Celery('sciences')

# Celery settings
CELERY_BROKER_URL = "redis://localhost:6379"
CELERY_RESULT_BACKEND = "redis://localhost:6379"

CELERY_RESULT_BACKEND = 'django-db'
CELERY_TASK_TRACK_STARTED = True
CELERY_RESULT_EXTENDED = True
CELERY_BEAT_SCHEDULER = 'django_celery_beat.schedulers:DatabaseScheduler'



EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
EMAIL_HOST = 'smtp.gmail.com'
EMAIL_PORT = 587
EMAIL_USE_TLS = True
DEFAULT_FROM_EMAIL = 'default from email'
DOMAIN = config('DOMAIN')
EMAIL_VERIFY_URL = f"{DOMAIN}/api/users/account/verify_account"
EMAIL_LOGIN_URL = f"{DOMAIN}/api/users/account/login"

EMAIL_HOST_USER = config('EMAIL_HOST_USER')
EMAIL_HOST_PASSWORD = config('EMAIL_HOST_PASSWORD')

# EMAIL_HOST_USER = 'donotreply.timacc@gmail.com'
# EMAIL_HOST_PASSWORD = 'pyjwidsxhmzozezn'  # past the key or password app here
