
from django.dispatch import receiver
from django.db.models.signals import post_save, m2m_changed, pre_save

import random
import datetime
from django.db import models
from persiantools.jdatetime import JalaliDateTime

def get_tour_thumbnail(instance, filename):
    return f"file_tour_thumbnail/{filename}"


def get_tour_banner(instance, filename):
    return f"file_tour_banner/{filename}"

def get_tour_icon(instance, filename):
    return f"file_tour_icon/{filename}"


def get_tour_magazine_cover(instance, filename):
    return f"file_tour_magazine_cover/{filename}"

class Attraction(models.Model):
    title_latin = models.CharField(max_length=500)
    title_farsi = models.CharField(max_length=500, null=True, blank=True)

    def __str__(self):
        return "%s " % (self.title_latin)


class Tour(models.Model):
    icon = models.FileField(upload_to=get_tour_icon, null=True, blank=True)
    title_latin = models.CharField(max_length=500)
    title_farsi = models.CharField(max_length=500, null=True, blank=True)
    title_seo = models.CharField(max_length=40,null=True, blank=True)
    slug = models.SlugField(null=True, blank=True)
    tour_code = models.CharField(max_length=500, null=True, blank=True)
    stid = models.ForeignKey('Visa.VisaStream', on_delete=models.PROTECT, null=True, blank=True, )
    zone_nr = models.ForeignKey("Raw_root.GlobalZone", on_delete=models.CASCADE, null=True, blank=True, )
    country = models.ForeignKey('Raw_root.Country', on_delete=models.PROTECT, null=True, blank=True, )
    province = models.ForeignKey('Raw_root.Province', on_delete=models.CASCADE, null=True, blank=True, )
    trip_length_day = models.IntegerField(choices=list(zip(range(1, 90), range(1, 90))), null=True, blank=True)
    difficulty_level = models.ForeignKey('Raw_root.DifficultyLevel', on_delete=models.PROTECT, null=True, blank=True, )
    meal_pattern = models.ForeignKey('Raw_root.MealPattern', on_delete=models.PROTECT, null=True, blank=True, )
    accom_type = models.ForeignKey('Raw_root.AccomType', on_delete=models.CASCADE, null=True, blank=True, )
    age_range =  models.ManyToManyField('Raw_root.AgeRange', blank=True, )
    account_number = models.ForeignKey('Organization.AccountNumber', on_delete=models.CASCADE, null=True, blank=True)
    arrvial_pick_up = models.BooleanField(default=False)
    departure_drop = models.BooleanField(default=False)
    night_drive = models.BooleanField(default=False)
    brief_text = models.TextField(null=True, blank=True)
    gathering_address = models.CharField(max_length=300, null=True, blank=True)
    hot_line = models.CharField(max_length=14, null=True, blank=True)
    calling_time_from = models.TimeField(null=True, blank=True)
    calling_time_to = models.TimeField(null=True, blank=True)
    hash_tags = models.CharField(max_length=500, null=True, blank=True)
    #  meal parent
    group = models.BooleanField(default=False)
    outbound = models.BooleanField(default=False)
    vechile = models.ForeignKey('Raw_root.Vehicle', on_delete=models.PROTECT, null=True, blank=True, )
    event_cat = models.ManyToManyField('Raw_root.EventCat', blank=True, )
    declaration_forms = models.ForeignKey('Organization.DeclarationForms', on_delete=models.PROTECT, null=True, blank=True)
    thumbnail = models.FileField(upload_to=get_tour_thumbnail, null=True, blank=True)
    alt_text_thumb = models.CharField(max_length=500, null=True, blank=True)
    banner = models.FileField(upload_to=get_tour_banner, null=True, blank=True)
    alt_text_banner = models.CharField(max_length=500, null=True, blank=True)
    magazine_cover = models.ImageField(upload_to=get_tour_magazine_cover, null=True, blank=True)
    video_upload = models.FileField(upload_to=get_tour_magazine_cover, null=True, blank=True)
    video_title = models.CharField(max_length=500, null=True, blank=True)
    video_desc = models.CharField(max_length=500, null=True, blank=True)
    video_url = models.CharField(max_length=500, null=True, blank=True)
    supervisor_staff_user = models.ForeignKey('OfficeHauser.OfficeUser', on_delete=models.PROTECT,
                                              related_name='tour_supervisor_user_set', null=True, blank=True)
    tour_guide = models.ForeignKey('OfficeHauser.OfficeUser', on_delete=models.PROTECT,
                                  related_name='tour_tour_user_set', null=True, blank=True)
    publish_date_time = models.DateTimeField(null=True, blank=True)
    thing_to_do = models.ManyToManyField("Content.ThingToDo",blank=True,)
    attractions = models.ManyToManyField('Tour.Attraction', blank=True, )
    tags = models.TextField( null=True, blank=True)

    def __str__(self):
        return "%s  " % (self.title_latin)



def get_day_tour_image(instance, filename):
    return f"file_day_tour_image/{filename}"

class DayTour(models.Model):
    tour = models.ForeignKey(Tour, on_delete=models.PROTECT, )
    seq = models.PositiveIntegerField(choices=[(x, x) for x in range(1, 90)], )
    day_region = models.ForeignKey('Raw_root.Region',related_name='day_tour_region_day_set', on_delete=models.CASCADE )

    night_region = models.ForeignKey('Raw_root.Region',related_name='day_tour_region_night_set', on_delete=models.CASCADE, null=True, blank=True, )
    accom_type = models.ForeignKey('Raw_root.AccomType',on_delete=models.CASCADE,  )
    meal = models.ForeignKey('Raw_root.MealPattern',on_delete=models.CASCADE,  )
    thing_to_do = models.ManyToManyField("Content.ThingToDo" , blank=True,  )
    attraction =  models.ManyToManyField("Tour.Attraction" , blank=True,  )
    activity = models.TextField(null=True, blank=True)
    local_guide = models.ForeignKey('OfficeHauser.OfficeUser', on_delete=models.PROTECT,
                                  related_name='day_tour_users_set', null=True, blank=True)
    day_image = models.ImageField(upload_to=get_day_tour_image, null=True, blank=True)
    def __str__(self):
        if self.tour ==None:
            s1 = ""
        else:
            s1=self.tour.title_latin
        if self.seq ==None:
            s2 = ""
        else:
            s2 = self.seq

        if self.day_region == None:
            s3 = ""
        else:
            s3 = self.day_region.title_latin

        return f'{s1}, {s2}, {s3},,{self.accom_type.title_latin},{self.meal.title_latin} , '

# class TourAgeRange(models.Model):
#     tour = models.ForeignKey(Tour, on_delete=models.PROTECT, null=True, blank=True, )
#     age_range = models.ManyToManyField('Raw_root.AgeRange', blank=True, )

class TourMeal(models.Model):
    tour = models.ForeignKey(Tour, on_delete=models.PROTECT, null=True, blank=True, )
    meal = models.ManyToManyField('Raw_root.MealPattern',  blank=True, )

class TourToPack(models.Model):
    tour = models.ForeignKey(Tour, on_delete=models.PROTECT, null=True, blank=True, )
    to_pack = models.ManyToManyField('Raw_root.ToPack', blank=True, )

    def __str__(self):
        return "%s " % (self.tour.title_latin,)

# class TourDeparture(models.Model):
#     tour = models.ForeignKey(Tour, on_delete=models.PROTECT, null=True, blank=True, )
#     status = models.CharField(max_length=30,
#                               choices=(('Pending', 'Pending'), ('Active', 'Active'), ('Expired', 'Expired')), null=True,
#                               blank=True)
#     dept_date_time = models.DateTimeField(null=True, blank=True)
#     return_date_time = models.DateTimeField(null=True, blank=True)
#     reg_start_date_time = models.DateTimeField(null=True, blank=True)
#     reg_end_date_time = models.DateTimeField(null=True, blank=True)

def get_file_tour_operation_request(instance, filename):
    return f"file_tour_operation_request/{filename}"

class TourArrangment(models.Model):
    tour_element = models.ForeignKey('Raw_root.TourElement', on_delete=models.CASCADE,related_name='tour_arrangement_element', null=True, blank=True, )
    region = models.ForeignKey('Raw_root.Region', on_delete=models.CASCADE, null=True, blank=True, )
    tour_dept = models.ForeignKey('Tour.TourDepartureFare', on_delete=models.PROTECT, null=True, blank=True, )
    business_user = models.ForeignKey('OfficeHauser.OfficeUser', on_delete=models.PROTECT,
                                      related_name='tour_operation_business_user_set', null=True, blank=True)
    # tour_leg = models.ForeignKey('Tour.TourDepartureFare', on_delete=models.PROTECT, null=True, blank=True, )
    region = models.ForeignKey('Raw_root.Region', on_delete=models.CASCADE, null=True, blank=True, )
    due = models.DateTimeField(null=True, blank=True)
    check_in = models.DateTimeField(null=True, blank=True)
    check_out = models.DateTimeField(null=True, blank=True)
    transc_co = models.ForeignKey('Raw_root.TransCo', on_delete=models.PROTECT, null=True, blank=True, )
    request = models.FileField(upload_to=get_file_tour_operation_request, null=True, blank=True)
    confirmation = models.FileField(upload_to=get_file_tour_operation_request, null=True, blank=True)
    currency = models.ForeignKey("Raw_root.Currency", on_delete=models.CASCADE, null=True, blank=True, )
    unit_fee = models.DecimalField(max_digits=10, decimal_places=2, null=True, blank=True)
    quantity = models.PositiveIntegerField(null=True, blank=True)
    total_amount = models.DecimalField(max_digits=10, decimal_places=2, null=True, blank=True)
    discount = models.DecimalField(max_digits=10, decimal_places=2, null=True, blank=True)
    balance_amount = models.DecimalField(max_digits=10, decimal_places=2, null=True, blank=True)
    staff_user = models.ForeignKey('OfficeHauser.OfficeUser', on_delete=models.PROTECT,
                                   related_name='tour_operation_business_staff_user_set', null=True, blank=True)

    last_updated_date = models.DateTimeField(auto_now=True)
    note = models.TextField(null=True, blank=True)
    by_staff_user = models.ForeignKey('OfficeHauser.OfficeUser', on_delete=models.PROTECT,
                                      related_name='tour_operation_by_user_set', null=True, blank=True)
    status = models.CharField(max_length=30, choices=(
        ('Waiting', 'Waiting'), ('Accepted', 'Accepted'), ('Rejected', 'Rejected'), ('Done', 'Done'),
        ('Failed', 'Failed'), ('Canceled', 'Canceled')), null=True, blank=True)



    def save(self, *args, **kwargs):

        if self.unit_fee is not None and self.unit_fee is not None:
            self.total_amount = self.unit_fee*self.unit_fee
            super(TourArrangment, self).save(*args, **kwargs)

class TourTicketCenter(models.Model):
    # applicant = models.ForeignKey('ApplicationOperation.Applicant', on_delete=models.CASCADE, null=True, blank=True,
    #                               default=None)
    tour_dept = models.ForeignKey('Tour.TourDepartureFare', on_delete=models.PROTECT, null=True, blank=True, )

    def create_new_ref_number():
        return str("uni_random")

    ticket_number = models.CharField(max_length=20,blank=True,editable=False)
    status = models.CharField(max_length=30, choices=(
        ('Vacant', 'Vacant'),('Temporary Secured', 'Temporary Secured'), ('Paid & Reserved', 'Paid & Reserved'),
        ('Full Booked', 'Full Booked'), ('Canceled', 'Canceled')), null=True, blank=True,default="Vacant")

    def save(self, *args, **kwargs):
        if self.ticket_number is not None:
            not_unique = True
            while not_unique:

                s1 = str(datetime.date.today())[2:4]
                # tt1 = TourDepartureFare.objects.filter(pk=self.tour_dept).values('tour_leg')
                s2 = str(datetime.date.today())[5:7]

                c = TourTicketCenter.objects.count()
                s3 = str(c).zfill(4);

                number = ['1', '2', '3', '4', '5', '6', '7', '8', '9']
                s4 = random.randint(0, len(number) - 1)
                number = ['1', '2', '3', '4', '5', '6', '7', '8', '9']
                s5 = random.randint(0, len(number) - 1)

                uni_random = str(s1) + str(s2) + str(s3) + str(s4) + str(s5)

                if not TourTicketCenter.objects.filter(ticket_number=uni_random):
                    not_unique = False
                    self.ticket_number = str(uni_random)
                    super(TourTicketCenter, self).save(*args, **kwargs)
        else:
            super(TourTicketCenter, self).save(*args, **kwargs)


    def __str__(self):
        return "%s " % (self.ticket_number)

def get_file_tour_our_buy_financial_evidence(instance, filename):
    return f"file_tour_our_buy_financial_evidence/{filename}"

class TourOurBuy(models.Model):
    tour_operation = models.ForeignKey(TourArrangment, on_delete=models.PROTECT, null=True, blank=True, )
    beneficiary_business_user = models.ForeignKey('OfficeHauser.OfficeUser', on_delete=models.PROTECT,
                                                  related_name='tour_ourby_beneficiary_business_user_set', null=True,
                                                  blank=True)
    TYPE = (
        ('IBAN', 'IBAN'),
        ('CC', 'CC'),
        ('Online', 'Online'),
        ('Cash', 'Cash'),
    )
    payment_methods = models.CharField(max_length=30, choices=TYPE, null=True, blank=True)
    currency = models.ForeignKey('Raw_root.Currency', on_delete=models.CASCADE, null=True, blank=True, default=None)
    amount = models.DecimalField(max_digits=10, decimal_places=2, null=True, blank=True)
    from_account_number = models.ForeignKey('Organization.AccountNumber', on_delete=models.CASCADE, null=True,
                                            blank=True,
                                            default=None)

    financial_evidence = models.ImageField(upload_to=get_file_tour_our_buy_financial_evidence, null=True, blank=True)
    beneficiary_name = models.CharField(max_length=300, null=True, blank=True)
    beneficiary_account_iban = models.CharField(max_length=300, null=True, blank=True)
    remittance_date_time = models.DateTimeField(null=True, blank=True)
    by_staff_user = models.ForeignKey('OfficeHauser.OfficeUser', on_delete=models.PROTECT,
                                      related_name='tour_our_by_staff_user_set', null=True, blank=True)

    self_note = models.TextField(null=True, blank=True)
    last_updated_date = models.DateTimeField(auto_now=True)

class TourPAX(models.Model):
    user = models.ForeignKey('OfficeHauser.OfficeUser', on_delete=models.PROTECT,
                             related_name='tour_pax_staff_user_set', null=True, blank=True)
    pnr_code = models.CharField(max_length=200, null=True, blank=True)
    tour_dept = models.ForeignKey('Tour.TourTicketCenter', on_delete=models.PROTECT, null=True, blank=True, )

    fee = models.DecimalField(max_digits=10, decimal_places=2, null=True, blank=True)
    first_name = models.CharField(max_length=200, null=True, blank=True)
    last_name = models.CharField(max_length=200, null=True, blank=True)
    nid = models.CharField(max_length=200, null=True, blank=True)


    Status_TYPES = (
        ('Vacant', 'Vacant'), ('Temporary Secured', 'Temporary Secured'), ('Paid & Reserved', 'Paid & Reserved'),
        ('Full Booked', 'Full Booked'), ('Canceled', 'Canceled')
    )
    status = models.CharField(max_length=30, choices=Status_TYPES,null=True, blank=True)
    GENDER_TYPES = (
        ("Male", "Male"),
        ("Female", "Female"),
        ("None", "None")
    )
    gender = models.CharField(max_length=10, choices=GENDER_TYPES,null=True, blank=True)
    postal_code = models.CharField(max_length=200, null=True, blank=True)
    dob = models.DateField(null=True, blank=True)
    landline = models.CharField(max_length=200, null=True, blank=True)
    email = models.CharField(max_length=200, null=True, blank=True)
    cell_number = models.CharField(max_length=200, null=True, blank=True)
    residential_province = models.CharField(max_length=200, null=True, blank=True)
    residential_city = models.CharField(max_length=200, null=True, blank=True)
    father_name = models.CharField(max_length=200, null=True, blank=True)
    mother_name = models.CharField(max_length=200, null=True, blank=True)

def get_file_tour_invoice_balance_bank_receipt_file(instance, filename):
    return f"file_tour_invoice_balance_bank_receipt_file/{filename}"

class TourInvoiceBalance(models.Model):
    # pos_bank id
    account_number = models.ForeignKey('Organization.AccountNumber', on_delete=models.CASCADE, null=True,
                                       blank=True)
    payment_date_time = models.DateTimeField(null=True, blank=True)
    bank_receipt_file = models.ImageField(upload_to=get_file_tour_invoice_balance_bank_receipt_file, null=True,
                                          blank=True)
    trace_code = models.CharField(max_length=200, null=True, blank=True)
    # external_payment = models.ForeignKey('ApplicationOperation.ExternalPayment', on_delete=models.CASCADE, null=True,
    #                                      blank=True, )
    currency = models.ForeignKey('Raw_root.Currency', on_delete=models.CASCADE, null=True, blank=True, )
    total = models.DecimalField(max_digits=10, decimal_places=2, null=True, blank=True)
    TYPE = (('Online', 'Online'), ('Cash', 'Cash'), ('POS', 'POS'), ('Bank-Transfer', 'Bank-Transfer'),)
    payment_methods = models.CharField(max_length=30, choices=TYPE, null=True, blank=True)
    by_staff_user = models.ForeignKey('OfficeHauser.OfficeUser', on_delete=models.PROTECT,
                                      related_name='tour_invoice_balance_user_set', null=True, blank=True)
    self_note = models.TextField(null=True, blank=True)
    last_updated_date = models.DateTimeField(auto_now=True)


def get_file_tour_report_upload_file(instance, filename):
    return f"file_tour_report_upload_file/{filename}"


def get_file_tour_report_doc_upload(instance, filename):
    return f"file_tour_report_doc_upload/{filename}"


class TourReport(models.Model):
    creator_user = models.ForeignKey('OfficeHauser.OfficeUser', on_delete=models.CASCADE, null=True, blank=True, default=None)
    tour_deprature_fare = models.ForeignKey('Tour.TourDepartureFare', on_delete=models.PROTECT, null=True, blank=True, )
    date_time = models.DateTimeField(null=True, blank=True)

    TYPE = (('Criminal', 'Criminal'), ('immigration', 'immigration'), ('Incident', 'Incident'), ('trouble', 'trouble'),
            ('Medical', 'Medical'), ('Operation', 'Operation'), ('Other', 'Other'))
    type = models.CharField(max_length=30, choices=TYPE, null=True, blank=True)
    problem = models.ForeignKey('Raw_root.Problem', on_delete=models.CASCADE, null=True, blank=True, )
    injury = models.ForeignKey('Raw_root.Injury', on_delete=models.CASCADE, null=True, blank=True, )
    triage_level = models.IntegerField(choices=list(zip(range(1, 5), range(1, 5))), null=True, blank=True)
    ems_report = models.BooleanField(default=False)
    ems_report_code = models.CharField(max_length=200, null=True, blank=True)
    ems_staff_full_name = models.CharField(max_length=200, null=True, blank=True)
    police_report = models.BooleanField(default=False)
    police_full_name = models.CharField(max_length=200, null=True, blank=True)
    police_cell_number = models.CharField(max_length=200, null=True, blank=True)
    witness_1_full_name = models.CharField(max_length=200, null=True, blank=True)
    witness_1_cell_number = models.CharField(max_length=200, null=True, blank=True)
    witness_2_full_name = models.CharField(max_length=200, null=True, blank=True)
    witness_2_cell_number = models.CharField(max_length=200, null=True, blank=True)
    note = models.TextField(null=True, blank=True)

    upload_date_time =  models.DateTimeField(null=True, blank=True)
    file_upload = models.FileField(upload_to=get_file_tour_report_upload_file, null=True,blank=True)
#
# def get_file_tour_report_doc_upload(instance, filename):
#     return f"file_tour_report_doc_upload/{filename}"
#
# class TourReportDoc(models.Model):
#     date_time = models.DateTimeField(null=True, blank=True)
#     file_upload = models.FileField(upload_to=get_file_tour_report_doc_upload, null=True, blank=True)

class TourReservation(models.Model):
    def create_new_ref_number():
        alpha = ['A', 'C', 'D', 'E', 'F', 'G', 'H', 'J', 'K', 'L', 'M', 'N', 'Q', 'S', 'T',
                 'U', 'V', 'W', 'X', 'Y', 'Z']
        al1 = random.randint(0, len(alpha) - 1)
        al2 = random.randint(0, len(alpha) - 1)

        number = ['1', '2', '3', '4', '5', '6', '7', '8', '9']
        nu1 = random.randint(0, len(number) - 1)
        al3 = random.randint(0, len(alpha) - 1)
        nu2 = random.randint(0, len(number) - 1)
        nu3 = random.randint(0, len(number) - 1)
        al4 = random.randint(0, len(alpha) - 1)

        not_unique = True
        while not_unique:
            unique_ref = str(alpha[al1]) + str(alpha[al2]) + number[nu1] + str(alpha[al3]) + number[nu2] + number[
                nu3] + str(alpha[al4])
            if True:
                not_unique = False
        return str(unique_ref)

    pnr_code = models.CharField(
        max_length=20,
        blank=True,
        editable=False,
        unique=True,
        default=create_new_ref_number
    )
    tour_leg = models.ForeignKey('Tour.TourDepartureFare', on_delete=models.PROTECT, null=True, blank=True, )
    user = models.ForeignKey('OfficeHauser.OfficeUser', on_delete=models.PROTECT,
                             related_name='tour_reservation_user_set', null=True, blank=True)
    booking_date_time = models.DateTimeField(null=True, blank=True)
    currency_1 = models.ForeignKey('Raw_root.Currency', on_delete=models.CASCADE,
                                   related_name='tour_reservation_currency1_set', null=True, blank=True, )
    total_1 = models.DecimalField(max_digits=10, decimal_places=2, null=True, blank=True)
    currency_2 = models.ForeignKey('Raw_root.Currency', on_delete=models.CASCADE,
                                   related_name='tour_reservation_currency2_set', null=True, blank=True, )
    total_2 = models.DecimalField(max_digits=10, decimal_places=2, null=True, blank=True)
    currency_3 = models.ForeignKey('Raw_root.Currency', on_delete=models.CASCADE,
                                   related_name='tour_reservation_currency3_set', null=True, blank=True, )
    total_3 = models.DecimalField(max_digits=10, decimal_places=2, null=True, blank=True)


    def __str__(self):
        return "%s " % (self.pnr_code)

# class TourInvoice(models.Model):
#     tour = models.ForeignKey(Tour, on_delete=models.PROTECT, null=True, blank=True, )
#     tour_reservation = models.ForeignKey(TourReservation, on_delete=models.PROTECT, null=True, blank=True, )
#     instalment = models.ForeignKey('Services.Instalment', on_delete=models.CASCADE, null=True, blank=True)
#     t_service = models.ForeignKey('Raw_root.Tservice', on_delete=models.PROTECT, null=True, blank=True)
#     method_of_payment = models.CharField(max_length=30, choices=(
#         ('No action needed', 'No action needed'), ('In-Office', 'In-Office'), ('Online', 'Online'),
#         ('Cash', 'Cash'), ('POS', 'POS'), ('Bank-Transfer', 'Bank-Transfer')), null=True, blank=True)
#
#     status = models.CharField(max_length=30, choices=(
#         ('Neutral', 'Neutral'), ('Pay Queue', 'Pay Queue'),
#         ('Waiting Approval', 'Waiting Approval'), ('PAID', 'PAID'),
#         ('Discarded', 'Discarded')), null=True, blank=True)
#     action = models.ForeignKey('Raw_root.Action', on_delete=models.CASCADE, null=True, blank=True)
#     currency_1 = models.ForeignKey('Raw_root.Currency', related_name='app_invoice_currency_1', on_delete=models.CASCADE,
#                                    null=True, blank=True, )
#     total_fee_vat_1 = models.IntegerField(null=True, blank=True)
#     currency_2 = models.ForeignKey('Raw_root.Currency', related_name='app_invoice_currency_2', on_delete=models.CASCADE,
#                                    null=True, blank=True, )
#     total_fee_vat_2 = models.IntegerField(null=True, blank=True)
#     currency_3 = models.ForeignKey('Raw_root.Currency', related_name='app_invoice_currency_3', on_delete=models.CASCADE,
#                                    null=True, blank=True, )
#     total_fee_vat_3 = models.IntegerField(null=True, blank=True)
#
#     by_staff_user = models.ForeignKey('OfficeHauser.OfficeUser', on_delete=models.PROTECT,
#                                       related_name='tour_invoice_by_user_set', null=True, blank=True)
#     self_note = models.TextField(null=True, blank=True)
#     last_updated_date = models.DateTimeField(auto_now=True)

def get_file_tour_crew_breifing_voice(instance, filename):
    return f"file_tour_crew_breifing_voice/{filename}"

def get_file_tour_crew_assignment_file(instance, filename):
    return f"file_tour_crew_assignment_file/{filename}"

def get_file_tour_crew_debreifing_voice(instance, filename):
    return f"file_tour_crew_debreifing_voice/{filename}"

class TourCrew(models.Model):

    tour_deprature_fare = models.ForeignKey('Tour.TourDepartureFare', on_delete=models.PROTECT, null=True, blank=True, )
    hr_contract = models.ForeignKey('Hr.HrContract', on_delete=models.CASCADE, related_name='hr_contract_job_set',null=True,blank=True, )
    assignment_date_time = models.DateTimeField(null=True, blank=True)
    briefing_mode = models.CharField(max_length=30,
                                     choices=(('Meeting', 'Meeting'), ('Online', 'Online')), null=True, blank=True)
    briefing_voice = models.FileField(upload_to=get_file_tour_crew_breifing_voice, null=True, blank=True)
    briefing_date_time = models.DateTimeField(null=True, blank=True)
    assignment_note = models.TextField(null=True, blank=True)
    assignment_file = models.FileField(upload_to=get_file_tour_crew_assignment_file, null=True, blank=True)

    debriefing_date_time = models.DateTimeField(null=True, blank=True)
    debriefing_voice = models.FileField(upload_to=get_file_tour_crew_debreifing_voice, null=True, blank=True)
    debriefing_note = models.TextField(null=True, blank=True)
    approval_by_supervisor = models.BooleanField(default=False)
    status = models.CharField(max_length=30, choices=(
        ('Waiting', 'Waiting'), ('Accepted', 'Accepted'), ('Rejected', 'Rejected'), ('Briefed', 'Briefed'), ('Done', 'Done')
    , ('DeBriefed', 'DeBriefed'), ('Approved', 'Approved'), ('Resign', 'Resign'),), null=True, blank=True)


class TourCrewToPack(models.Model):
    tour_crew = models.ForeignKey(TourCrew, on_delete=models.PROTECT, null=True, blank=True, )
    to_pack = models.ForeignKey('Raw_root.ToPack', on_delete=models.CASCADE, null=True, blank=True, )

# class TravelingLeg(models.Model):
#     dept_region = models.ForeignKey('Raw_root.Region', related_name='traveling_leg_region_set',on_delete=models.PROTECT )
#     trans_mode = models.ForeignKey('Raw_root.TransMode', on_delete=models.PROTECT )
#     trans_co = models.ForeignKey('Raw_root.TransCo', on_delete=models.PROTECT, )
#     arrival_region = models.ForeignKey('Raw_root.Region', related_name='traveling_leg_arrival_regions_set', on_delete=models.PROTECT )
#     traveling_duration = models.PositiveIntegerField(null=True, blank=True,)
#     class_range = models.ForeignKey('Raw_root.ClassRanges', on_delete=models.PROTECT )
#     allowed_luggage = models.ManyToManyField('Raw_root.AllowedBaggage', blank=True, )
#     allowed_carry_on = models.ForeignKey('Raw_root.AllowedBaggage',related_name='allowed_carry_luggage_luggage_set', on_delete=models.PROTECT, null=True, blank=True, )
#     cabin_wifi = models.BooleanField(null=True,default=False )
#     lounge_vip = models.BooleanField(null=True,default=False )
#
#     def __str__(self):
#         return "%s | %s | %s | %s" % (self.dept_region.title_latin, self.trans_mode.title_latin,
#                                       self.arrival_region.title_latin,
#                                       self.class_range.title_latin,)


# class TourLeg(models.Model):
#     tour = models.ForeignKey(Tour, on_delete=models.PROTECT,)
#     traveling_leg = models.ForeignKey(TravelingLeg, on_delete=models.PROTECT, null=True, blank=True, )
#     departure_date_time = models.DateTimeField()
#     arrival_local_date_time = models.DateTimeField(null=True, blank=True)
#     status = models.CharField(max_length=30,choices=(('Pending', 'Pending'), ('Active', 'Active'), ('Expired', 'Expired')),default='Active')
#
#
#     def __str__(self):
#         return "%s | %s | %s" % (self.tour.title_latin,
#                                       self.departure_date_time,
#                                       self.status,)

class TourDepartureFare(models.Model):
    # tour_dept = models.ForeignKey(TourDeparture, on_delete=models.PROTECT, null=True, blank=True, )
    tour = models.ForeignKey(Tour, on_delete=models.PROTECT, )
    destination = models.ForeignKey('Raw_root.Region', on_delete=models.PROTECT, null=True, blank=True, )
    # tour_leg = models.ForeignKey(TourLeg, on_delete=models.PROTECT, null=True, blank=True, )

    departure_date_time = models.DateTimeField()

    # validity_before_date = models.DateTimeField(null=True, blank=True)
    validity_from = models.DateTimeField(null=True, blank=True)
    validity_to = models.DateTimeField(null=True, blank=True)
    trans_mode = models.ForeignKey('Raw_root.TransMode', on_delete=models.PROTECT,null=True, blank=True)
    trans_co = models.ForeignKey('Raw_root.TransCo', on_delete=models.PROTECT, null=True, blank=True)

    class_range = models.ForeignKey('Raw_root.ClassRanges', on_delete=models.PROTECT, null=True, blank=True )
    capacity = models.IntegerField(choices=list(zip(range(1, 50), range(1, 50))))
    currency = models.ForeignKey('Raw_root.Currency', on_delete=models.CASCADE, null=True, blank=True, )
    cancellation_rate = models.ForeignKey('Raw_root.CancellationRate', on_delete=models.PROTECT, null=True,
                                          blank=True, )
    fee_dbl = models.DecimalField(max_digits=10, decimal_places=2, null=True, blank=True)
    fee_dbl_discounted = models.DecimalField(max_digits=10, decimal_places=2, null=True, blank=True)
    fee_sgl = models.DecimalField(max_digits=10, decimal_places=2, null=True, blank=True)
    fee_shared = models.DecimalField(max_digits=10, decimal_places=2, null=True, blank=True)
    fee_child = models.DecimalField(max_digits=10, decimal_places=2, null=True, blank=True)

    fee_infant = models.DecimalField(max_digits=15, decimal_places=2, null=True, blank=True)
    flight_adult = models.DecimalField(max_digits=15, decimal_places=2, null=True, blank=True)
    flight_chd = models.DecimalField(max_digits=15, decimal_places=2, null=True, blank=True)
    comment = models.TextField(null=True, blank=True)


    allowed_luggage = models.ForeignKey('Raw_root.AllowedBaggage', on_delete=models.PROTECT, null=True, blank=True,related_name="tour_fare_allowed_luggage_set" )
    allowed_carry_on = models.ForeignKey('Raw_root.AllowedBaggage', on_delete=models.PROTECT, null=True, blank=True,related_name="tour_fare_allowed_carry_on_set" )

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    def __str__(self):
        return  str(self.tour)
    # def save(self, *args, **kwargs):
    #     print(self.created_at)
    #     if self.created_at is None:
    #         print("aaaaaaaaaaaaaa222")
    #         p = TourTicketCenter.objects.create(tour_dept=self.id)
    #         print("aaaaaaaaaaaaaa")
    #         p.save()
    #         print("aaaaaadfgdfgdfga")
    #
    #     else:
    #         super(TourDepartureFare, self).save(*args, **kwargs)


def get_tour_pageconfig_banner(instance, filename):
    return f"file_tour_pageconfig_banner/{filename}"

def get_tour_pageconfig_belt_image_background(instance, filename):
    return f"file_tour_pageconfig_belt_image_background/{filename}"

def get_tour_pageconfig_belt_title_latin(instance, filename):
    return f"file_tour_pageconfig_belt_title_latin/{filename}"

def get_tour_pageconfig_belt_title_farsi(instance, filename):
    return f"file_tour_pageconfig_belt_title_farsi/{filename}"

def get_tour_pageconfig_belt_image_2(instance, filename):
    return f"file_tour_pageconfig_belt_image_2/{filename}"

class TourPageConfig(models.Model):
    title_latin = models.CharField(max_length=500)
    title_farsi = models.CharField(max_length=500, null=True, blank=True)
    banner = models.ImageField(upload_to=get_tour_pageconfig_banner, null=True, blank=True)
    slogan_title_1_latin = models.CharField(max_length=500, null=True, blank=True)
    slogan_title_1_farsi = models.CharField(max_length=500, null=True, blank=True)
    slogan_desc_1_latin = models.TextField( null=True, blank=True)
    slogan_desc_1_farsi = models.TextField(null=True, blank=True)
    article_1 = models.ForeignKey('Content.Article', on_delete=models.CASCADE, null=True, blank=True,
                                  related_name='tourpage_config_article_1_set')
    article_2 = models.ForeignKey('Content.Article', on_delete=models.CASCADE, null=True, blank=True,
                                  related_name='tourpage_config_article_2_set')
    slogan_title_2_latin = models.CharField(max_length=500, null=True, blank=True)
    slogan_title_2_farsi = models.CharField(max_length=500, null=True, blank=True)
    slogan_desc_2_latin = models.TextField( null=True, blank=True)
    slogan_desc_2_farsi = models.TextField(null=True, blank=True)
    event_cat = models.ManyToManyField('Raw_root.EventCat', blank=True, )
    magazine_background = models.ImageField(upload_to=get_tour_pageconfig_belt_image_background, null=True, blank=True)
    magazine_title_latin = models.CharField(max_length=500, null=True, blank=True)
    magazine_title_farsi =  models.CharField(max_length=500, null=True, blank=True)
    magazine_title_color_code = models.CharField(max_length=500, null=True, blank=True)
    popular_tour_1 = models.ForeignKey('Tour.Tour',related_name='tourpage_config_tour_1_set', on_delete=models.PROTECT, null=True, blank=True)
    popular_tour_2 = models.ForeignKey('Tour.Tour', related_name='tourpage_config_tour2_set',
                                              on_delete=models.PROTECT, null=True, blank=True)
    popular_tour_3 = models.ForeignKey('Tour.Tour', related_name='tourpage_config_tour3_set',
                                           on_delete=models.PROTECT, null=True, blank=True)
    popular_tour_4 = models.ForeignKey('Tour.Tour', related_name='tourpage_config_tour4_set',
                                              on_delete=models.PROTECT, null=True, blank=True)
    popular_tour_5 = models.ForeignKey('Tour.Tour', related_name='tourpage_config_tour5_set',
                                              on_delete=models.PROTECT, null=True, blank=True)
    expert_block_title_latin = models.CharField(max_length=500, null=True, blank=True)
    expert_block_title_farsi = models.CharField(max_length=500, null=True, blank=True)
    expert_block_1_latin = models.TextField(null=True, blank=True)
    expert_block_1_farsi = models.TextField(null=True, blank=True)
    guide_1 = models.ForeignKey("OfficeHauser.BusinessProfile",related_name='tour_page_business_user_config_guide_1', on_delete=models.PROTECT, blank=True,null=True)
    guide_2 = models.ForeignKey("OfficeHauser.BusinessProfile", related_name='tour_page_business_user_config_guide_2', on_delete=models.PROTECT, blank=True, null=True)
    guide_3 = models.ForeignKey("OfficeHauser.BusinessProfile", related_name='tour_page_business_user_config_guide_3', on_delete=models.PROTECT, blank=True, null=True)
    guide_4 = models.ForeignKey("OfficeHauser.BusinessProfile", related_name='tour_page_business_user_config_guide_4', on_delete=models.PROTECT, blank=True, null=True)
    guide_5 = models.ForeignKey("OfficeHauser.BusinessProfile", related_name='tour_page_business_user_config_guide_5', on_delete=models.PROTECT, blank=True, null=True)
    guide_6 = models.ForeignKey("OfficeHauser.BusinessProfile", related_name='tour_page_business_user_config_guide_6', on_delete=models.PROTECT, blank=True, null=True)
    staff_1 = models.ForeignKey("OfficeHauser.StaffUser", related_name='tour_page_staff_user_config_staff_1',
                                on_delete=models.PROTECT, blank=True, null=True)
    staff_2 = models.ForeignKey("OfficeHauser.StaffUser", related_name='tour_page_staff_user_config_staff_2',
                                on_delete=models.PROTECT, blank=True, null=True)
    staff_3 = models.ForeignKey("OfficeHauser.StaffUser", related_name='tour_page_staff_user_config_staff_3',
                                on_delete=models.PROTECT, blank=True, null=True)
    staff_4 = models.ForeignKey("OfficeHauser.StaffUser", related_name='tour_page_staff_user_config_staff_4',
                                on_delete=models.PROTECT, blank=True, null=True)
    staff_5 = models.ForeignKey("OfficeHauser.StaffUser", related_name='tour_page_staff_user_config_staff_5',
                                on_delete=models.PROTECT, blank=True, null=True)
    staff_6 = models.ForeignKey("OfficeHauser.StaffUser", related_name='tour_page_staff_user_config_staff_6',
                                on_delete=models.PROTECT, blank=True, null=True)
    routing_block_img = models.ImageField(upload_to=get_tour_pageconfig_belt_image_2, null=True,
                                         blank=True)
    button_routing =  models.CharField(max_length=50,choices = (('service cat','service cat'),
                                                                ('visa','visa'), ('article','article'),
                                                                ('things to do','things to do'),
                                                                ('training','training'),
                                                                ('pax topic','pax topic')), null=True, blank=True)
    service_cat_1 = models.ForeignKey('Services.ServiceCategory', on_delete=models.CASCADE, null=True, blank=True,
                                      related_name='tour_page_config_service_cat_1')
    visa = models.ForeignKey('Visa.VisaStream', on_delete=models.CASCADE, null=True, blank=True,
                                      related_name='tour_page_config_visa_stream_set1')
    article = models.ForeignKey('Content.Article', on_delete=models.CASCADE, null=True, blank=True,
                                  related_name='tourpage_config_article_set')
    thing_to_do_cat = models.ForeignKey("Raw_root.ThingsToDo", on_delete=models.CASCADE, null=True, blank=True, )
    routing_title_title = models.CharField(max_length=500, null=True, blank=True)
    routing_title_farsi = models.CharField(max_length=500, null=True, blank=True)
    routing_desc_latin = models.TextField(null=True, blank=True)
    routing_desc_farsi = models.TextField(null=True, blank=True)
    photos_of_client_title_latin = models.CharField(max_length=500, null=True, blank=True)
    photos_of_client_title_farsi = models.CharField(max_length=500, null=True, blank=True)
    photos_of_client_desc_title_latin = models.TextField(null=True, blank=True)
    photos_of_client_desc_title_farsi = models.TextField(null=True, blank=True)
    no_of_random_images = models.PositiveIntegerField(null=True, blank=True)
    random_images_from = models.ManyToManyField('OfficeHauser.OfficeUser', blank=True)
    subscribe_title_latin = models.CharField(max_length=500, null=True, blank=True)
    subscribe_title_farsi = models.CharField(max_length=500, null=True, blank=True)
    subscribe_desc_latin = models.TextField(null=True, blank=True)
    subscribe_desc_farsi = models.TextField(null=True, blank=True)

@receiver(post_save, sender=TourDepartureFare)
def create_tour_ticket_center(sender, instance, created=False, **kwargs):
    if created:
        for i in range(0,instance.capacity):
            TourTicketCenter.objects.create(tour_dept_id=instance.id)

class TourIncludedExcluded(models.Model):
    tour = models.ForeignKey('Tour.Tour', on_delete=models.PROTECT, null=True, blank=True)
    included_tour_service_id = models.ManyToManyField('Raw_root.TourService', blank=True,related_name="tour_include_service1_set")
    excluded_tour_service_id = models.ManyToManyField('Raw_root.TourService', blank=True,related_name="tour_include_service2_set")

    def __str__(self):
        return "%s " % (str(self.tour))

def get_tour_sponsor_sponsor_logo(instance, filename):
    return f"file_tour_sponsor_sponsor_logo/{filename}"

class TourSponsors(models.Model):
    tour = models.ForeignKey('Tour.Tour', on_delete=models.PROTECT, null=True, blank=True)
    sponsor_name = models.CharField(max_length=500, null=True, blank=True)
    sponsor_logo = models.ImageField(upload_to=get_tour_sponsor_sponsor_logo, null=True, blank=True)

    def __str__(self):
        return "%s " % (self.tour)


class TourPricing(models.Model):
    tour_dept = models.ForeignKey(TourDepartureFare, on_delete=models.PROTECT, null=True, blank=True, )
    t_service = models.ForeignKey('Raw_root.Tservice', on_delete=models.PROTECT, null=True, blank=True, default=None)
    currency = models.ForeignKey("Raw_root.Currency", on_delete=models.CASCADE, null=True, blank=True, )
    fee_dbl = models.DecimalField(max_digits=10, decimal_places=2, null=True, blank=True)
    fee_sgl = models.DecimalField(max_digits=10, decimal_places=2, null=True, blank=True)
    fee_shared = models.DecimalField(max_digits=10, decimal_places=2, null=True, blank=True)
    fee_child = models.DecimalField(max_digits=10, decimal_places=2, null=True, blank=True)
    fee_infant = models.DecimalField(max_digits=10, decimal_places=2, null=True, blank=True)

