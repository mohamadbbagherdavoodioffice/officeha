from django.db import models
import uuid
import random 
import string

class ContactUs(models.Model):
    date_time = models.DateTimeField(null=True, blank=True)
    contact_us_type = models.ForeignKey('Raw_root.ContactUsType', on_delete=models.CASCADE, null=True, blank=True, )
    followup_status = models.ForeignKey('Raw_root.FollowUpStatus', on_delete=models.CASCADE, null=True, blank=True, )
    center = models.ForeignKey('Content.Centers', on_delete=models.CASCADE, null=True, blank=True,
                               related_name='contactus_center_set')
    service_category = models.ForeignKey('Services.ServiceCategory', on_delete=models.CASCADE, null=True, blank=True)
    tour = models.ForeignKey('Tour.Tour', on_delete=models.PROTECT, null=True, blank=True)
    visa = models.ForeignKey('Visa.VisaStream', on_delete=models.CASCADE, null=True, blank=True,
                                      related_name='contactus_visa_stream_set1')
    name = models.CharField(max_length=200, null=True, blank=True)
    tel = models.CharField(max_length=200, null=True, blank=True)
    email = models.EmailField(null=True,blank=True)
    note = models.TextField(null=True, blank=True)
    star_rate = models.IntegerField(null=True, blank=True, choices=[(i, str(i)) for i in range(1, 6)])  # 1 to 5 rating
    digital_code = models.CharField(max_length=200, null=True, blank=True)
    
    def __str__(self):
        contact_type = self.contact_us_type.name if self.contact_us_type else 'N/A'
        followup_status = self.followup_status.name if self.followup_status else 'N/A'
        name = self.name if self.name else 'N/A'
        tel = self.tel if self.tel else 'N/A'
        email = self.email if self.email else 'N/A'
        service_category_title = self.service_category.title if self.service_category else 'N/A'
        tour_title = self.tour.title if self.tour else 'N/A'
        visa_title = self.visa.title if self.visa else 'N/A'
        
        return f"{contact_type} | {followup_status} | {name} | {tel} | {email} | {service_category_title} | {tour_title} | {visa_title}"


def get_file_job_seeker_upload_id(instance, filename):
    return f"file_job_seeker_upload_id/{filename}"


def get_file_job_seeker_upload_cv(instance, filename):
    return f"file_job_seeker_upload_cv/{filename}"


def get_file_job_seeker_upload_app_form(instance, filename):
    return f"file_job_seeker_upload_app_form/{filename}"

class JobSeeker(models.Model):
    date_time = models.DateTimeField(null=True, blank=True)
    job_code = models.CharField(max_length=500, null=True, blank=True)

    first_name = models.CharField(max_length=200, null=True, blank=True)
    last_name = models.CharField(max_length=200, null=True, blank=True)
    match_percenet = models.DecimalField(max_digits=10, decimal_places=2, null=True, blank=True)
    region = models.ForeignKey('Raw_root.Region', on_delete=models.CASCADE,null=True,blank=True )
    cell_number = models.CharField(max_length=200, null=True, blank=True)
    skills = models.TextField( null=True, blank=True,)
    upload_id = models.FileField(upload_to=get_file_job_seeker_upload_id,null=True,blank=True)
    upload_cv = models.FileField(upload_to=get_file_job_seeker_upload_cv, null=True, blank=True)
    upload_app_form = models.FileField(upload_to=get_file_job_seeker_upload_app_form, null=True, blank=True)
    followup_status = models.ForeignKey('Raw_root.FollowUpStatus', on_delete=models.CASCADE, null=True, blank=True, )

# class JobSeekerSkill(models.Model):
#     hr_skill
#

class EduApplyRequest(models.Model):
    date_time = models.DateTimeField(null=True, blank=True)
    program = models.ForeignKey('Edu.Program', on_delete=models.CASCADE,null=True,blank=True,)
    full_name = models.CharField(max_length=200, null=True, blank=True)
    age_range = models.ForeignKey('Raw_root.AgeRange', on_delete=models.PROTECT, null=True, blank=True, )
    degree = models.ForeignKey('Raw_root.Degree', on_delete=models.PROTECT, null=True, blank=True,)
    gpa = models.ForeignKey('Raw_root.Gpa', on_delete=models.PROTECT, null=True, blank=True,)
    edu_subject = models.ForeignKey('Raw_root.EduSubject', on_delete=models.PROTECT, null=True, blank=True,)
    available_fund = models.ForeignKey('Raw_root.AvailableFund', on_delete=models.PROTECT, null=True, blank=True,)
    province = models.ForeignKey('Raw_root.Province', on_delete=models.CASCADE,null=True,blank=True, )
    tel = models.CharField(max_length=200, null=True, blank=True)
    email = models.EmailField(null=True,blank=True)
    followup_status = models.ForeignKey('Raw_root.FollowUpStatus', on_delete=models.CASCADE, null=True, blank=True, )


class TellUsWhatYouThink(models.Model):
    date_time = models.DateTimeField(null=True, blank=True)
    star_number = models.PositiveIntegerField(null=True, blank=True)
    tell_us_what_you_think = models.ForeignKey('Raw_root.TellUsWhatYouThinkCat', on_delete=models.PROTECT, null=True, blank=True, )
    comment_note = models.TextField(null=True, blank=True)

    def __str__(self):
        return self.tell_us_what_you_think

class Feedback(models.Model):
    date_time = models.DateTimeField(null=True, blank=True)
    name = models.CharField(max_length=200, null=True, blank=True)
    tel = models.CharField(max_length=200, null=True, blank=True)
    email = models.EmailField(null=True,blank=True)
    note = models.TextField(null=True, blank=True)
    followup_status = models.ForeignKey('Raw_root.FollowUpStatus', on_delete=models.CASCADE, null=True, blank=True, )



def get_evaluation_form(instance, filename):
    return f"file_evaluation_form/{filename}"

class EvaluationForm(models.Model):
    trace_code = models.CharField(max_length=200, null=True, blank=True)
    followup_status = models.ForeignKey('Raw_root.FollowUpStatus', on_delete=models.PROTECT, null=True, blank=True)
    staff = models.ForeignKey('OfficeHauser.OfficeUser', on_delete=models.CASCADE, null=True, blank=True)
    staff_note = models.TextField(null=True, blank=True)
    
    applicant_note = models.TextField(null=True, blank=True)
    cv = models.FileField(upload_to=get_evaluation_form, null=True, blank=True)
    fullname = models.CharField(max_length=200, null=True, blank=True)
    cellnumber = models.CharField(max_length=200, null=True, blank=True)
    email = models.EmailField(null=True,blank=True)
    age =  models.ForeignKey('Raw_root.AgeRange', on_delete=models.CASCADE, null=True, blank=True, )
    degree = models.ForeignKey("Raw_root.Degree", on_delete=models.CASCADE, null=True, blank=True, )
    min_employe_years = models.ForeignKey('Raw_root.MinEmploymentYear', on_delete=models.PROTECT, null=True, blank=True, default=None)
    occupation = models.ForeignKey('Raw_root.Occupation', on_delete=models.CASCADE, null=True, blank=True, )
    education = models.ForeignKey('Raw_root.EduSubject', on_delete=models.CASCADE, null=True, blank=True, )
    visa_chance_nr = models.CharField(max_length=200, null=True, blank=True)
    language_answer  = models.TextField(null=True, blank=True)
    military_answer  = models.TextField(null=True, blank=True)
    paper_answer  = models.TextField(null=True, blank=True)
    occupation_answer = models.TextField(null=True, blank=True)
    min_emp_answer = models.TextField(null=True, blank=True)
    degree_answer = models.TextField(null=True, blank=True)
    fund_answer = models.TextField(null=True, blank=True)
    edu_answer = models.TextField(null=True, blank=True)
    marital_answer = models.TextField(null=True, blank=True)
    family_answer = models.TextField(null=True, blank=True)
    refusal_answer = models.TextField(null=True, blank=True)
    travel_history = models.TextField(null=True, blank=True)
    

    def save(self, *args, **kwargs):
        if not self.trace_code:
            while True:
                generated_code = random.randint(1000000000, 9999999999)
                if not EvaluationForm.objects.filter(trace_code=str(generated_code)).exists():
                    self.trace_code = str(generated_code)
                    break
        super().save(*args, **kwargs)

    def __str__(self):
        return str(self.fullname) + " | " + str(self.cellnumber) +"   |   "+str(self.trace_code) 



class TourRequest(models.Model):

    tour_deprature_fare = models.ForeignKey('Tour.TourDepartureFare', on_delete=models.PROTECT, null=True, blank=True, )
    date_time = models.DateTimeField(null=True, blank=True)
    followup_status = models.ForeignKey('Raw_root.FollowUpStatus', on_delete=models.CASCADE, null=True, blank=True, )
    delegate_first_name = models.CharField(max_length=300, null=True, blank=True)
    delegate_last_name = models.CharField(max_length=300, null=True, blank=True)
    delegate_tel = models.CharField(max_length=300, null=True, blank=True)
    delegate_land_line = models.CharField(max_length=300, null=True, blank=True)
    
    name_1 = models.CharField(max_length=300, null=True, blank=True)
    dob_1 = models.DateTimeField(null=True, blank=True)
    room_type_1 = models.ForeignKey('Raw_root.RoomType', on_delete=models.CASCADE, null=True, blank=True, related_name='room_type_1_d1')
    
    name_2 = models.CharField(max_length=300, null=True, blank=True)
    dob_2 = models.DateTimeField(null=True, blank=True)
    room_type_2 = models.ForeignKey('Raw_root.RoomType', on_delete=models.CASCADE, null=True, blank=True, related_name='room_type_2_d1')
    
    name_3 = models.CharField(max_length=300, null=True, blank=True)
    dob_3 = models.DateTimeField(null=True, blank=True)
    room_type_3 = models.ForeignKey('Raw_root.RoomType', on_delete=models.CASCADE, null=True, blank=True, related_name='room_type_3_d1')
        
    name_4 = models.CharField(max_length=300, null=True, blank=True)
    dob_4 = models.DateTimeField(null=True, blank=True)
    room_type_4 = models.ForeignKey('Raw_root.RoomType', on_delete=models.CASCADE, null=True, blank=True, related_name='room_type_4_d1' )
        
    name_5 = models.CharField(max_length=300, null=True, blank=True)
    dob_5 = models.DateTimeField(null=True, blank=True)
    room_type_5 = models.ForeignKey('Raw_root.RoomType', on_delete=models.CASCADE, null=True, blank=True,  related_name='room_type_5_d1')
    def __str__(self):
        return f"{self.followup_status} | {self.delegate_first_name} | {self.delegate_tel}"

class WorkshopsParticipant(models.Model):
    workshop = models.ForeignKey('Services.Workshop', on_delete=models.CASCADE)
    status = models.CharField(max_length=20, choices=[('Pending', 'Pending'), ('Paid&Confirmed', 'Paid&Confirmed'), ('Canceled', 'Canceled')])
    mode_of_payment = models.ForeignKey('Raw_root.ModeOfPayment', on_delete=models.CASCADE)
    first_name = models.CharField(max_length=100, null=True, blank=True)
    last_name = models.CharField(max_length=100, null=True, blank=True)
    cell_number = models.CharField(max_length=20, null=True, blank=True)
    email_address = models.EmailField(max_length=100, null=True, blank=True)
    pos_bank_id = models.CharField(max_length=100, null=True, blank=True)
    debit_card_number = models.CharField(max_length=16, null=True, blank=True)
    payment_date_time = models.DateTimeField(null=True, blank=True)
    account_number = models.ForeignKey('Organization.AccountNumber', on_delete=models.CASCADE, null=True, blank=True)
    trace_code = models.CharField(max_length=15, null=True, blank=True)
    bank_receipt_file = models.FileField(null=True, blank=True)
    by_staff = models.ForeignKey('OfficeHauser.OfficeUser', on_delete=models.CASCADE, null=True, blank=True)
    staff_note = models.TextField(max_length=1000, null=True, blank=True)

    def __str__(self):
        return f"{self.status} | {self.first_name} "
