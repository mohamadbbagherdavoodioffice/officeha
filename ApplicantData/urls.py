from django.urls import path, include, re_path
from .views import *
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [

    path('app_answer_declaration', AppAnsweredDeclarationCreateViewset.as_view()),
]

urlpatterns = urlpatterns + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)


