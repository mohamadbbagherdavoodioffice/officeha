from django.urls import path, include, re_path
from .views import *
from django.conf import settings
from django.conf.urls.static import static

from . import views

urlpatterns = [
    path('get_home_page', views.HomePageAPIView.as_view()),
    path('get_header_footer_menu', views.HeaderFooterAPIView.as_view()),
    path('get_home_page_internationalser', views.HomePageAPIInternationalServiceView.as_view()),
    path('immigration-config/', views.GenericDataListView.as_view(), name='immigration-config-list'),
]

urlpatterns = urlpatterns + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

# Apply Now
# Find out more
# Instalments     FAQ
# عنوان ها ثابت ولی مقدیر خاکستری زیر  داینامیک
# #5E5E5E
# #004D8E
# #004D8E
# Not Available
# API get all visa details by visa id   if  Publicity 1
# Jsons:
# From model Visa stream :+Visa main cat :latin farsi
# +Visa subcat: latin farsi+Title_farsi/Title_latin+subclass nr.
# +Entry type_latin farsi+Stay_time_cat latin farsi
# +Processing time
# +Work_permit latin /farsi
# +Interview 0/1
# +Embassy_currency ilatin/farsi
# +Embassy_fee main_applicant
# +BANNER IMG
# +THUMBNAIL IMG
# +Emergency note
# +Expiry date ems note job check current date before expiry date then send emergency textFrom model visa validity :
# +Visa availability 0/1  Job check current date between validity datesFrom model Tour :
# +Related_TOUR img thumbnail + tour title latin /farsi
# +From model About
# +Title latin
# +Title farsi
# +Txt latin  HTML
# +Txt farsi HTML
# +From model eligibility +Title latin
# +Title farsi +Txt latin  HTML