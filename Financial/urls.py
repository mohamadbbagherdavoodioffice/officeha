from django.urls import path, include, re_path
from .views import *
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('create_external_payment', ExternalPaymentCreateAPIView.as_view()),
    path('get_external_payment_statics', ExternalPaymentStaticAPIView.as_view()),
    path('retrieve_external_payment', ExternalPaymentRetriveAPIView.as_view()),
    path('get_hint_apply_form', HintApplyFormAPIView.as_view()),
    path('get_external_payment_query', ExternalPaymentRetriveQueryAPIView.as_view()),
    path('invoice', InvoiceCreateAPIView.as_view()),

]

urlpatterns = urlpatterns + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
