from django.shortcuts import render
from OfficeHauser.serializers import *
from OfficeHauser.models import *
from rest_framework.response import Response
from rest_framework import status
from rest_framework import generics
from rest_framework import status, viewsets
from .serializers import OfficeUserSerializer, \
    ChangePasswordSerializer, OfficeUserBriefSerializer, OfficeUserRoleSerializer, OfficeUserInfoSerializer, \
    OfficeUserForPostSerializer, RoleSerializer, OfficeUserForCreateUserSerializer, OfficeUserBriefForUpdateSerializer
import datetime
import json

import jwt
from rest_framework import status, generics
from rest_framework.exceptions import AuthenticationFailed
from rest_framework.generics import UpdateAPIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework_simplejwt.tokens import RefreshToken
from rest_framework import serializers
from .models import OfficeUser, Role
from .permissions import IsStaff, IsMy

from rest_framework.generics import GenericAPIView
from .permissions import IsOwner, IsVisitor, IsAdmin
import random

from django.shortcuts import render, redirect
from django.contrib.auth import get_user_model
from django.http import HttpResponseBadRequest
from officeha.settings import EMAIL_LOGIN_URL
from django.http import HttpResponse


class UserSubscribeGeneric(generics.CreateAPIView,):
    queryset = UserSubscribe.objects.all()
    serializer_class = UserSubscribeSerializer


def get_random_number(n):
    digits = "0123456789ABCDEFGHJKLMNOP"
    return ''.join(random.choice(digits) for _ in range(n))

class UserSubscribeDeleteGeneric(generics.DestroyAPIView):
    queryset = UserSubscribe.objects.all()
    serializer_class = UserSubscribeSerializer
    lookup_url_kwarg = "pk"

    def delete(self, request, pk, *args, **kwargs):
        queryset = self.get_queryset()
        queryset.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)



class OfficeUserCreateViewSet(generics.CreateAPIView,):
    """
     An endpoint for Create user.
    """
    
    queryset = OfficeUser.objects.all().order_by('-id')
    serializer_class = OfficeUserForCreateUserSerializer

    # def get_serializer_class(self):
    #     if str(self.request.method) == 'GET':
    #         pass
    #         # return OfficeUserInfoSerializer
    #     return OfficeUserForCreateUserSerializer

    def post(self, request, *args, **kwargs):

        # request.serializer_class.set_password(request.data['password'])
        # request.save()
        print(request)
        # instance = serializer.save()
        # instance.verification_code = get_random_number(10)
        # instance.save()



        return self.create(request, *args, **kwargs)



def verify_account(request):
    activation_code = request.GET.get('activation_code')
    if not activation_code:
        return HttpResponseBadRequest('Activation code not provided')
    user = get_user_model().objects.filter(activation_code=activation_code).first()
    user.is_active=True
    user.save()
    if not user:
        return HttpResponseBadRequest('Invalid activation code')
    user.is_active = True
    user.save()
    
    
    # return Response({"message": "success", "status": "OK", }, status=200)
    return HttpResponse(json.dumps({"message": "success", "status": "OK", }), content_type='application/json')


class ChangePasswordView(UpdateAPIView):
    """
    An endpoint for changing password with old password and 2 new password.
    Also you need to header Authorization
    """

    serializer_class = ChangePasswordSerializer
    model = OfficeUser

    permission_classes = (IsAuthenticated,)

    def get_object(self, queryset=None):
        obj = self.request.user
        return obj

    def update(self, request, *args, **kwargs):
        self.object = self.get_object()
        serializer = self.get_serializer(data=request.data)

        if serializer.is_valid():

            # Check old password
            if not self.object.check_password(request.data['old_password']):
                return Response({"old_password": ["Wrong password."]}, status=status.HTTP_400_BAD_REQUEST)
            # set_password also hashes the password that the user will get
            print(self.object)
            self.object.set_password(request.data['password'])
            self.object.save()
            response = {
                'status': 'success',
                'code': status.HTTP_200_OK,
                'message': 'Password updated successfully',
                'data': []
            }
            return Response(response)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


# class LogoutView(APIView):
#     """
#     endpoint for logout with refresh token & header Authorization
#
#
#       ## Example:row json body
#         {
#        "refresh_token": ""
#        }
#     """
#     permission_classes = (IsAuthenticated,)
#
#     def post(self, request):
#         try:
#             refresh_token = request.data["refresh_token"]
#             token = RefreshToken(refresh_token)
#             token.blacklist()
#
#             return Response(status=status.HTTP_205_RESET_CONTENT)
#         except Exception as e:
#             return Response(status=status.HTTP_400_BAD_REQUEST)


class OfficeUserRoleListAPIView(generics.ListAPIView):
    """
     An endpoint for list of user with role.
     """
    serializer_class = OfficeUserInfoSerializer
    queryset = OfficeUser.objects.all()


class OfficeUserRoleCreateAPIView(generics.CreateAPIView):
    """
     An endpoint for list of user with role.
     """
    serializer_class = OfficeUserRoleSerializer
    queryset = Role.objects.all()


# class LogoutAPIView(GenericAPIView):
#     serializer_class = LogoutSerializer

#     permission_classes = (IsAuthenticated,)

#     def post(self, request):
#         """ Validate token and save.  Logout"""
#         serializer = self.serializer_class(data=request.data)
#         serializer.is_valid(raise_exception=True)
#         serializer.save()

#         return Response(status=status.HTTP_204_NO_CONTENT)


class OfficeUserRoleCreateAPIView(generics.CreateAPIView):
    """
     An endpoint for list of user with role.
     """
    serializer_class = OfficeUserRoleSerializer
    queryset = Role.objects.all()


class OfficeUserGetInfo(generics.ListAPIView, UpdateAPIView):
    permission_classes = (IsMy,)
    serializer_class = OfficeUserBriefSerializer
    queryset = OfficeUser.objects.all()


    def get_serializer_class(self):
        if str(self.request.method) == 'PUT':
            return OfficeUserBriefForUpdateSerializer
        return OfficeUserBriefSerializer

    def get(self, request, *args, **kwargs):
        queryset = OfficeUser.objects.filter(id=request.user.id)
        self.queryset = queryset
        return self.list(request, *args, **kwargs)


    def put(self, request, *args, **kwargs, ):
        req = json.loads(request.body)
        first_name = req.get('first_name')
        last_name = req.get('last_name')
        username = req.get('username')

        find_user = OfficeUser.objects.filter(username=username)
        if len(find_user) >= 1:
            for f in find_user:
                if request.user.id != f.id:
                    return Response({"details": "cannot set this username, please set another username"
                                     }, status=400)

        users = OfficeUser.objects.get(id=request.user.id)
        users.first_name = first_name
        users.last_name = last_name
        users.username = username
        users.save()

        return Response({"first_name": users.first_name,
                         "last_name": users.last_name,
                         "username": users.username})



class OfficeUserGetUserInfo(generics.RetrieveAPIView):
    permission_classes = (IsMy,)

    def get(self, request):
        user = OfficeUserInfoSerializer(OfficeUser.objects.filter(pk=request.user.id), many=True).data
        return Response(user)


class OfficeUserDeleteUpdateRetrieveAccount(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = [IsAdmin]
    serializer_class = OfficeUserSerializer
    queryset = OfficeUser.objects.all()

    def get_serializer_class(self):
        if str(self.request.method) == 'PUT':
            return OfficeUserForCreateUserSerializer
        return OfficeUserSerializer

    def delete(self, request, pk, *args, **kwargs):
        try:
            user = OfficeUser.objects.get(id=pk)
            user.delete()
            return Response({"details": "user delete"}, status=200)
        except:
            return Response({"details": "user not found"}, status=404)

    def update(self, request, pk, *args, **kwargs):
        usr = OfficeUser.objects.get(pk=pk)
        if 'password' in request.data:
            usr.set_password((request.data)['password'])
        usr.first_name = (request.data)['first_name']
        usr.last_name = (request.data)['last_name']
        usr.username = (request.data)['username']
        usr.role_id = (request.data)['role']
        usr.save()
        return Response({"details": "success"}, status=200)


class GetRoleListAPIView(generics.ListAPIView):
    permission_classes = [IsAdmin]
    """
     An endpoint for list of user with role.
     """
    serializer_class = RoleSerializer
    queryset = Role.objects.all()


class SetUserDeactivate(generics.RetrieveAPIView):
    permission_classes = [IsAdmin]

    def put(self, request, pk, *args, **kwargs, ):
        users = OfficeUser.objects.get(id=pk)
        users.is_active = False
        users.save()
        return Response({"details": "User Deactivated"}, status=200)


class SetUserActivate(generics.RetrieveAPIView):
    permission_classes = [IsAdmin]

    def put(self, request, pk, *args, **kwargs, ):
        users = OfficeUser.objects.get(id=pk)
        users.is_active = True
        users.save()
        return Response({"details": "User Activated"}, status=200)