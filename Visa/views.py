import os
import urllib
from passporteye import read_mrz
from rest_framework import generics, status
from rest_framework.generics import ListAPIView

from officeha import settings
from .serializers import *
from rest_framework.generics import ListAPIView
from rest_framework.views import APIView
from .serializers import *
from rest_framework.response import Response
from datetime import datetime
from Raw_root.models import Country
from Raw_root.serializers import CountryFlagSerializer

from .tasks import image_recognization


class VisaStreamAPIVIEW(ListAPIView):
    serializer_class = VisaStreamForCountryQuerySerializer

    def get_queryset(self):
        objs = VisaStream.objects.filter(country=self.request.GET.get('country')).all()
        return objs


class VisaStreamPopularAPIVIEW(ListAPIView):
    serializer_class = VisaStreamDetailsSerializer

    def get_queryset(self):
        objs = VisaStream.objects.filter(is_popular=True)
        return objs


class VisaStreamSubAllDetailsAPIVIEW(APIView):
    def get(self, request):
        dic = {}
        dt = datetime.today().strftime('%Y-%m-%d')
        data_a = VisaStream.objects.filter(pk=self.request.GET.get("id"))
        serializer_a = VisaStreamSubAllDetailsSerializer(data_a, many=True)
        dic["visa_stream"] = serializer_a.data
        valid1 = VisaStream.objects.filter(pk=self.request.GET.get("id")).filter(
            expiry_date_emergency_note__gte=dt).values()
        if len(valid1) >= 1:
            del dic["visa_stream"][0]["expiry_date_emergency_note"]
        else:
            del dic["visa_stream"][0]["expiry_date_emergency_note"]
            del dic["visa_stream"][0]["emergency_note"]

        data_b = VisaAvailability.objects.filter(stid=self.request.GET.get("id"))
        validitb1 = VisaAvailability.objects.filter(stid=self.request.GET.get("id")).filter(from_date__lte=dt).filter(
            to_date__gte=dt)
        serializer_b = VisaAvailabilitySerializer(data_b, many=True)
        dic["visa_availability"] = serializer_b.data
        if len(data_b) >= 1:
            # dic["visa_availability"][0]["availability"]=True
            del dic["visa_availability"][0]["from_date"]
            del dic["visa_availability"][0]["id"]
            del dic["visa_availability"][0]["to_date"]

        if len(validitb1) >= 1:
            dic["visa_availability"][0]["availability"] = True
        # else:
        #
        #     dic["visa_availability"][0]["availability"] = False

        # data_c = Instalment.objects.filter(stid=self.request.GET.get("id"))
        # serializer_c = InstalmentVisaStreamCatdataSerializer(data_c, many=True)
        # dic["instalment"] = serializer_c.data

        return Response({"message": "success", "status": "OK", "data": dic}, status=200)


class VisaStreamListAllAPIVIEW(ListAPIView):
    serializer_class = VisaStreamTitleSerializer
    queryset = VisaStream.objects.all()


class InstalmentGETServiceCatDetailsAPIVIEW(ListAPIView):
    serializer_class = InstalmentGETSerializer

    def get_queryset(self):
        pkk = self.kwargs['idd']
        return Instalment.objects.filter(service_category=pkk)


class InstalmentGETStidDetailsAPIVIEW(ListAPIView):
    serializer_class = InstalmentGETSerializer

    def get_queryset(self):
        pkk = self.kwargs['idd']
        return Instalment.objects.filter(stid=pkk)


class VisaStreamAlertNoteAPIView(APIView):
    def get(self, request):
        dic = {}
        now = datetime.datetime.now()
        data_a = VisaStream.objects.filter(pk=self.request.GET.get("id")).values('country__travel_alert_note')
        dic = data_a
        return Response({"message": "success", "status": "OK", "data": dic}, status=200)


class VisaStreamAlertNoteAPIView(APIView):
    def get(self, request):
        dic = {}
        data_a = VisaStream.objects.filter(pk=self.request.GET.get("id")).values('country__travel_alert_note')
        dic = data_a
        return Response({"message": "success", "status": "OK", "data": dic}, status=200)


class VisaStreamGuaranteeAPIView(APIView):
    def get(self, request):
        dic = {}

        data_a = VisaStream.objects.filter(pk=self.request.GET.get("id")).values('guarantee__id',
                                                                                 'guarantee__date_time',
                                                                                 'guarantee__currency__currency_latin',
                                                                                 'guarantee__currency__currency_farsi',
                                                                                 'guarantee__amount')
        dic = data_a
        return Response({"message": "success", "status": "OK", "data": dic}, status=200)


class InstalmentSubAllDetailsAPIVIEW(APIView):
    def get(self, request):
        dict = {}

        if self.request.GET.get("service_category"):
            data_sc = Instalment.objects.filter(
                service_category__id=self.request.GET.get("service_category")).order_by("seq")
            serializer_sc = InstalmentVisaStreamCatdataSerializer(data_sc, many=True)
            dict = serializer_sc.data
        if self.request.GET.get("tour"):
            data_sc = Instalment.objects.filter(tour__id=self.request.GET.get("tour")).order_by("seq")
            serializer_sc = InstalmentVisaStreamCatdataSerializer(data_sc, many=True)
            dict = serializer_sc.data

        if self.request.GET.get("stid"):
            data_sc = Instalment.objects.filter(stid__id=self.request.GET.get("stid")).order_by("seq")
            serializer_sc = InstalmentVisaStreamCatdataSerializer(data_sc, many=True)
            dict = serializer_sc.data

        if self.request.GET.get("sub_stid"):
            data_sc = Instalment.objects.filter(
                sub_stid__id=self.request.GET.get("sub_stid")).order_by("seq")
            serializer_sc = InstalmentVisaStreamCatdataSerializer(data_sc, many=True)
            dict = serializer_sc.data

        if self.request.GET.get("sub_service_cat"):
            data_sc = Instalment.objects.filter(
                sub_service_cat__id=self.request.GET.get("sub_service_cat")).order_by("seq")
            serializer_sc = InstalmentVisaStreamCatdataSerializer(data_sc, many=True)
            dict = serializer_sc.data

        return Response({"message": "success", "status": "OK", "data": dict}, status=200)


#
# class VisaStreamSubAllDetailsAPIVIEW(APIView):
#     def get(self,request):
#         dic={}
#         # else:
#         #
#         #     dic["visa_availability"][0]["availability"] = False
#
#         data_c = Instalment.objects.filter(stid=self.request.GET.get("id"))
#         serializer_c = InstalmentVisaStreamCatdataSerializer(data_c, many=True)
#         dic["instalment"] = serializer_c.data
#
#
#         return Response({"message": "success", "status": "OK", "data": dic}, status=200)


class SubVisaStreamDetailsAPIView(APIView):
    def get(self, request):
        dic = {}

        if self.request.GET.get("stid"):
            data_c = SubVisaStream.objects.filter(stid=self.request.GET.get("stid"))
            serializer_c = SubVisaStreamDetailsSerializer(data_c, many=True)
            dic = serializer_c.data
        if self.request.GET.get("service_category"):
            data_c = SubVisaStream.objects.filter(stid=self.request.GET.get("service_category"))
            serializer_c = SubVisaStreamDetailsSerializer(data_c, many=True)
            dic = serializer_c.data

        return Response({"message": "success", "status": "OK", "data": dic}, status=200)

class PassportSaveCreateViewset(generics.CreateAPIView):
    """
    Create: a recipe
    """
    queryset = PassportVision.objects.all()
    serializer_class = PassportVisionSerializer


class PassportVisionRetrieve(generics.RetrieveAPIView):
    lookup_url_kwarg = "id"
    def get(self,request, id):

        result = image_recognization(id,request)
        return Response({"message": "success", "status": "OK", "data": result}, status=200)

class PassportRetrieve(generics.RetrieveAPIView):
    serializer_class = PassportVisionSerializer
    queryset = PassportVision.objects.all()
    lookup_url_kwarg = "id"



class VisaDetails(APIView):
    def get(self, request):
        dic = {}
        if self.request.GET.get("stid"):
            data_c = VisaStream.objects.filter(id=self.request.GET.get("stid"))

            serializer_c = VisaStreamDetailsSerializer(data_c, many=True)
            dic["vist_stream"] = serializer_c.data

            data_b = VisaAvailability.objects.filter(stid=self.request.GET.get("stid"))
            serializer_b = VisaAvailabilitySerializer(data_b, many=True)
            dic["visa_availability"] = serializer_b.data

            data_a = SubVisaStream.objects.filter(stid=self.request.GET.get("stid"))
            serializer_a = SubVisaStreamDetailsSerializer(data_a, many=True)
            dic["sub_visa_stream"] = serializer_a.data

        
        return Response({"message": "success", "status": "OK", "data": dic}, status=200)


class VisaStreamGlobalZonesArabianAPiView(ListAPIView):
    serializer_class = VisaStreamForCountryQuerySerializer

    def get_queryset(self):
        return VisaStream.objects.filter(country__zone_nr__title_latin='Arabian countries')


class GetCountryHasVisa(APIView):
    def get(self, request, format=None):
        visas = VisaStream.objects.all()
        country_ids = visas.values_list('country_id', flat=True).distinct()
        countries = Country.objects.filter(id__in=country_ids)
        serializer = CountryFlagSerializer(countries, many=True)
        
        return Response(serializer.data)
    