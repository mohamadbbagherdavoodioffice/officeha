from django.db import models
from Raw_root.models import EventCat

class HeaderEmergencyNote(models.Model):
    text_latin = models.CharField(max_length=500, null=True, blank=True)
    text_farsi = models.CharField(max_length=500, null=True, blank=True)
    from_date = models.DateField(null=True, blank=True)
    to_date = models.DateField(null=True, blank=True)
    color_code_hex = models.CharField(max_length=20, null=True, blank=True)

    def __str__(self):
        return str(self.text_latin)


def get_file_header_logo(instance, filename):
    return f"file_header_logo/','{filename}"


def get_file_footer_logo(instance, filename):
    return f"file_footer_logo/','{filename}"


def get_file_footer_icon1_svg(instance, filename):
    return f"file_footer_icon1_svg/','{filename}"


def get_file_footer_icon2_svg(instance, filename):
    return f"file_footer_icon2_svg','{filename}"


def get_file_footer_icon3_svg(instance, filename):
    return f"file_footer_icon3_svg/','{filename}"


def get_file_media_icon1(instance, filename):
    return f"file_media_icon1/','{filename}"


def get_file_media_icon2(instance, filename):
    return f"file_media_icon2/','{filename}"


def get_file_media_icon3(instance, filename):
    return f"file_media_icon3/','{filename}"


def get_file_footer_icon4_svg(instance, filename):
    return f"file_footer_icon4_svg/','{filename}"


def get_file_footer_icon6_svg(instance, filename):
    return f"file_footer_icon6_svg/','{filename}"


def get_file_footer_icon5_svg(instance, filename):
    return f"file_footer_icon5_svg/','{filename}"



def get_file_header_media_icon1(instance, filename):
    return f"file_header_media_icon1/','{filename}"


def get_file_header_media_icon2(instance, filename):
    return f"file_header_media_icon1/','{filename}"


def get_file_header_media_icon3(instance, filename):
    return f"file_header_media_icon1/','{filename}"



def get_file_header_media_icon7(instance, filename):
    return f"file_header_media_icon7/','{filename}"

def get_file_header_media_icon8(instance, filename):
    return f"file_header_media_icon8/','{filename}"


def get_file_header_media_icon4(instance, filename):
    return f"file_header_media_icon4/','{filename}"


def get_file_header_media_icon5(instance, filename):
    return f"file_header_media_icon5/','{filename}"

def get_file_media_icon4(instance, filename):
    return f"file_media_icon4/','{filename}"

def get_file_media_icon5(instance, filename):
    return f"file_media_icon5/','{filename}"

def get_file_header_fav_icon(instance, filename):
    return f"file_header_fav_icon/','{filename}"
class FooterHeaderSrc(models.Model):
    favIcon = models.ImageField(upload_to=get_file_header_fav_icon, null=True, blank=True)
    about_text_latin = models.CharField(max_length=500, null=True, blank=True)
    about_text_farsi = models.CharField(max_length=500, null=True, blank=True)
    about_text_latin_2 = models.CharField(max_length=500, null=True, blank=True)
    about_text_farsi_2 = models.CharField(max_length=500, null=True, blank=True)
    header_logo_svg = models.FileField(upload_to=get_file_header_logo, null=True, blank=True)
    footer_color_code_hex = models.CharField(max_length=20, null=True, blank=True)
    footer_logo_svg = models.FileField(upload_to=get_file_footer_logo, null=True, blank=True)
    tel_number_1 = models.CharField(max_length=20, null=True, blank=True)
    tel_number_2 = models.CharField(max_length=20, null=True, blank=True)
    tel_number_3 = models.CharField(max_length=20, null=True, blank=True)
    footer_address_latin = models.CharField(max_length=500, null=True, blank=True)
    footer_address_farsi = models.CharField(max_length=500, null=True, blank=True)
    footer_icon1_svg = models.FileField(upload_to=get_file_footer_icon1_svg, null=True, blank=True)
    footer_icon_url1 = models.CharField(max_length=500, null=True, blank=True)
    footer_icon_html1 = models.TextField(null=True, blank=True)
    footer_icon2_svg = models.FileField(upload_to=get_file_footer_icon2_svg, null=True, blank=True)
    footer_icon_url2 = models.CharField(max_length=500, null=True, blank=True)
    footer_icon_html2 = models.TextField(null=True, blank=True)
    footer_icon3_svg = models.FileField(upload_to=get_file_footer_icon3_svg, null=True, blank=True)
    footer_icon_url3 = models.CharField(max_length=500, null=True, blank=True)
    footer_icon_html3 = models.TextField(null=True, blank=True)
    footer_icon4_svg = models.FileField(upload_to=get_file_footer_icon4_svg, null=True, blank=True)
    footer_icon_url4 = models.CharField(max_length=500, null=True, blank=True)
    footer_icon_html4 = models.TextField(null=True, blank=True)
    footer_icon5_svg = models.FileField(upload_to=get_file_footer_icon5_svg, null=True, blank=True)
    footer_icon_url5 = models.CharField(max_length=500, null=True, blank=True)
    footer_icon_html5 = models.TextField(null=True, blank=True)
    footer_icon6_svg = models.FileField(upload_to=get_file_footer_icon6_svg, null=True, blank=True)
    footer_icon_url6 = models.CharField(max_length=500, null=True, blank=True)
    footer_icon_html6 = models.TextField(null=True, blank=True)
    media_icon1 = models.FileField(upload_to=get_file_media_icon1, null=True, blank=True)
    media_url1 = models.CharField(max_length=500, null=True, blank=True)
    media_icon2 = models.FileField(upload_to=get_file_media_icon2, null=True, blank=True)
    media_url2 = models.CharField(max_length=500, null=True, blank=True)
    media_icon3 = models.FileField(upload_to=get_file_media_icon3, null=True, blank=True)
    media_url3 = models.CharField(max_length=500, null=True, blank=True)

    header_media_icon1 = models.FileField(upload_to=get_file_header_media_icon1, null=True, blank=True)
    header_media_url1 = models.CharField(max_length=500, null=True, blank=True)

    header_media_icon2 = models.FileField(upload_to=get_file_header_media_icon2, null=True, blank=True)
    header_media_url2 = models.CharField(max_length=500, null=True, blank=True)

    header_media_icon3 = models.FileField(upload_to=get_file_header_media_icon3, null=True, blank=True)
    header_media_url3 = models.CharField(max_length=500, null=True, blank=True)

    footer_icon7 = models.FileField(upload_to=get_file_header_media_icon7, null=True, blank=True)
    footer_icon_url7 = models.CharField(max_length=500, null=True, blank=True)
    footer_icon_html7 = models.TextField(null=True, blank=True)

    footer_icon8 = models.FileField(upload_to=get_file_header_media_icon8, null=True, blank=True)
    footer_icon_url8 = models.CharField(max_length=500, null=True, blank=True)
    footer_icon_html8 = models.TextField(null=True, blank=True)

    header_media_icon4 = models.FileField(upload_to=get_file_header_media_icon4, null=True, blank=True)
    header_media_url4 = models.CharField(max_length=500, null=True, blank=True)

    header_media_icon5 = models.FileField(upload_to=get_file_header_media_icon5, null=True, blank=True)
    header_media_url5 = models.CharField(max_length=500, null=True, blank=True)

    media_icon4 = models.FileField(upload_to=get_file_media_icon4, null=True, blank=True)
    media_url4 = models.CharField(max_length=500, null=True, blank=True)

    media_icon5 = models.FileField(upload_to=get_file_media_icon5, null=True, blank=True)
    media_url5 = models.CharField(max_length=500, null=True, blank=True)

    service_category_1 = models.ForeignKey('Services.ServiceCategory', on_delete=models.CASCADE,
                                           related_name='footer_header_service_s_1', null=True, blank=True, )
    service_category_2 = models.ForeignKey('Services.ServiceCategory', on_delete=models.CASCADE,
                                           related_name='footer_header_service_s_2', null=True, blank=True, )
    service_category_3 = models.ForeignKey('Services.ServiceCategory', on_delete=models.CASCADE,
                                           related_name='footer_header_service_s_3', null=True, blank=True, )
    service_category_4 = models.ForeignKey('Services.ServiceCategory', on_delete=models.CASCADE,
                                           related_name='footer_header_service_s_4', null=True, blank=True, )
    service_category_5 = models.ForeignKey('Services.ServiceCategory', on_delete=models.CASCADE,
                                           related_name='footer_header_service_s_5', null=True, blank=True, )
    subscribe_latin = models.CharField(max_length=300, null=True, blank=True)
    subscribe_farsi = models.CharField(max_length=300, null=True, blank=True)




def get_file_banner(instance, filename):
    return f"file_banner/','{filename}"


class StaticText(models.Model):
    TYPE = (('Our services', 'Our services'),
            ('Passenger right main', 'Passenger right main'),
            ('feedback', 'feedback'),
            ('Claim your rights', 'Claim your rights'),
            ('Submit payments', 'Submit payments'),
            ('payment Status', 'payment Status'),
            ('Retrieve Application Number', 'Retrieve Application Number'),
            ('Application Status Check', 'Application Status Check'),
            ('Explore programs', 'Explore programs'),
            ('Travel requirements upper', 'Travel requirements upper'),
            ('Travel requirements lower', 'Travel requirements lower'),
            ('Careers', 'Careers'),
            ('Visa forms', 'Visa forms'),
            ('Asia and Middle', 'Asia and Middle'),
            ('Articles', 'Articles'),
            ('Things to do', 'Things to do'),
            ('Training', 'Training'),
            ('PAX-topics', 'PAX-topics'),
            ('Consultation','Consultation'),
            ('Evaluation','Evaluation'),
            ('Job Form','Job Form'))
    type = models.CharField(max_length=50, choices=TYPE, null=True, blank=True, )
    alt_farsi = models.CharField(max_length=300, null=True, blank=True)
    banner = models.FileField(upload_to=get_file_banner, null=True, blank=True)
    title_latin = models.CharField(max_length=250, null=True, blank=True)
    title_farsi = models.CharField(max_length=250, null=True, blank=True)
    text_latin = models.TextField(null=True, blank=True)
    text_farsi = models.TextField(null=True, blank=True)
    


    def __str__(self):
        return "%s | %s" % (self.type, self.title_latin)


def get_file_icon(instance, filename):
    return f"file_icon/','{filename}"


def get_file_info_graph(instance, filename):
    return f"file_info_graph/','{filename}"


class Hint(models.Model):
    ITEM = (('News cat', 'News cat'),
            ('news share', 'news share'),
            ('tour search bar', 'tour search bar'),
            ('Booking till', 'Booking till'),
            ('Departure', 'Departure'),
            ('leg', 'leg'),
            ('itinerary', 'itinerary'),
            ('packing list', 'packing list'),
            ('Not recommended', 'Not recommended'),
            ('Included services', 'Included services'),
            ('Sponsor', 'Sponsor'),
            ('Price list', 'Price list'),
            ('education center', 'education center'),
            ('each program', 'each program'),
            ('Apply form', 'Apply form'),
            ('apply now services', 'apply now services'),
            ('declaration services', 'declaration services'),
            ('declaration visa', 'declaration visa'),
            ('Applicants identity table', 'Applicants identity table'),
            ('checklist', 'checklist'),
            ('Submission fee', 'Submission fee'),
            ('Submission Successful case number', 'Submission Successful case number'),
            ('Arrival flights', 'Arrival flights'),
            ('Flight Notification', 'Flight Notification'),
            ('Submit payments', 'Submit payments'),
            ('Financial Status Check', 'Financial Status Check'),
            ('Retrieve Application Number', 'Retrieve Application Number'),
            ('Application Status Check', 'Application Status Check'),
            ('travel requirements', 'travel requirements'),
            ('consultation step1', 'consultation step1'),
            ('consultation calendar', 'consultation calendar'),
            ('consultation time', 'consultation time'),
            ('consultation form', 'consultation form'),
            ('consultation pay', 'consultation pay'),
            ('signin', 'signin'),
            ('forgot step1', 'forgot step1'),
            ('forgot step2', 'forgot step2'),
            ('forgot new password', 'forgot new password'),
            ('forgot failed', 'forgot failed'),
            ('submit external payment', 'submit external payment'),
            ('payment status', 'payment status'),
            ('application status check', 'application status check'))
    item = models.CharField(max_length=50, choices=ITEM, null=True, blank=True, )
    icon = models.FileField(upload_to=get_file_icon, null=True, blank=True)
    info_graph = models.FileField(upload_to=get_file_info_graph, null=True, blank=True)
    title_latin = models.CharField(max_length=250, null=True, blank=True)
    title_farsi = models.CharField(max_length=250, null=True, blank=True)
    text_latin = models.TextField(null=True, blank=True)
    text_farsi = models.TextField(null=True, blank=True)
    


def get_file_menu_consultation(instance, filename):
    return f"file_menu_consultation/','{filename}"


def get_file_menu_caq(instance, filename):
    return f"file_menu_caq/','{filename}"


def get_file_menu_visa_pick_up(instance, filename):
    return f"file_menu_visa_pick_up/','{filename}"


def get_file_menu_assessment(instance, filename):
    return f"file_menu_assessment/','{filename}"


def get_file_menu_cv_resume(instance, filename):
    return f"file_menu_cv_resume/','{filename}"


def get_file_menu_filling_forms(instance, filename):
    return f"file_menu_filling_forms/','{filename}"


def get_file_menu_us_lottery(instance, filename):
    return f"file_menu_us_lottery/','{filename}"


def get_file_menu_apply_for_study(instance, filename):
    return f"file_menu_apply_for_study/','{filename}"


def get_file_menu_visa_chance(instance, filename):
    return f"file_menu_visa_chance/','{filename}"


def get_file_menu_asia_middle_east(instance, filename):
    return f"ile_menu_asia_middle_east/','{filename}"


def get_file_menu_explore_visas(instance, filename):
    return f"file_menu_explore_visas/','{filename}"


def get_file_menu_visa_forms(instance, filename):
    return f"file_menu_visa_forms/','{filename}"


def get_file_menu_group_tours(instance, filename):
    return f"file_menu_group_tours/','{filename}"


def get_file_menu_kish_tours(instance, filename):
    return f"file_menu_kish_tours/','{filename}"


def get_file_menu_mashhad_tours(instance, filename):
    return f"file_menu_mashhad_tours/','{filename}"


def get_file_menu_outbound_tours(instance, filename):
    return f"file_menu_outbound_tours/','{filename}"


def get_file_menu_exhibition_tours(instance, filename):
    return f"file_menu_exhibition_tours/','{filename}"


def file_menu_contact_us(instance, filename):
    return f"file_menu_contact_us/','{filename}"


def get_file_menu_privacyـpolicy(instance, filename):
    return f"file_menu_privacyـpolicy/','{filename}"


def get_file_menu_terms_conditions(instance, filename):
    return f"file_menu_terms_conditions/','{filename}"


def get_file_menu_passenger_rights(instance, filename):
    return f"file_menu_passenger_rights/','{filename}"


def get_file_menu_dashboard(instance, filename):
    return f"file_menu_dashboard/','{filename}"


def get_file_menu_sign_in_sign_up(instance, filename):
    return f"file_menu_sign_in_sign_up/','{filename}"


def get_file_menu_language(instance, filename):
    return f"file_menu_language/','{filename}"


def get_file_menu_help(instance, filename):
    return f"file_menu_help/','{filename}"


def get_file_menu_send_feedback(instance, filename):
    return f"file_menu_send_feedback/','{filename}"


def get_file_menu_travel_requirements(instance, filename):
    return f"file_menu_travel_requirements/','{filename}"


def get_file_menu_submit_payments(instance, filename):
    return f"file_menu_submit_payments/','{filename}"


def get_file_menu_payment_status(instance, filename):
    return f"file_menu_payment_status/','{filename}"


def get_file_menu_application_status(instance, filename):
    return f"file_menu_application_status/','{filename}"


def get_file_menu_retrieve_app_no(instance, filename):
    return f"file_menu_retrieve_app_no/','{filename}"


def get_file_menu_contact_us(instance, filename):
    return f"file_menu_contact_us/','{filename}"


def get_file_menu_about_us(instance, filename):
    return f"file_menu_about_us/','{filename}"


def get_file_menu_career(instance, filename):
    return f"get_file_menu_career/','{filename}"

def get_file_menu_article(instance, filename):
    return f"file_menu_article/','{filename}"

def get_file_menu_things_to_do(instance, filename):
    return f"file_menu_things_to_do/','{filename}"

def get_file_menu_training(instance, filename):
    return f"file_menu_training/','{filename}"

def get_file_menu_pax_topic(instance, filename):
    return f"file_menu_pax_topic/','{filename}"

def get_file_menu_apply_for_study(instance, filename):
    return f"file_menu_apply_for_study/','{filename}"

def get_file_menu_asia_middle(instance, filename):
    return f"file_menu_asia_middle/','{filename}"

def get_file_evaluation_forms(instance, filename):
    return f"file_evaluation_forms/','{filename}"



class MenuIcon(models.Model):
    explore_visas = models.FileField(upload_to=get_file_menu_explore_visas, null=True, blank=True)
    visa_forms = models.FileField(upload_to=get_file_menu_visa_forms, null=True, blank=True)
    # group_tours = models.FileField(upload_to=get_file_menu_group_tours, null=True, blank=True)
    # kish_tours = models.FileField(upload_to=get_file_menu_kish_tours, null=True, blank=True)
    # mashhad_tours = models.FileField(upload_to=get_file_menu_mashhad_tours, null=True, blank=True)
    # outbound_tours = models.FileField(upload_to=get_file_menu_outbound_tours, null=True, blank=True)
    # exhibition_tours = models.FileField(upload_to=get_file_menu_exhibition_tours, null=True, blank=True)
    contact_us = models.FileField(upload_to=get_file_menu_contact_us, null=True, blank=True)
    about_us = models.FileField(upload_to=get_file_menu_about_us, null=True, blank=True)
    career = models.FileField(upload_to=get_file_menu_career, null=True, blank=True)
    privacyـpolicy = models.FileField(upload_to=get_file_menu_privacyـpolicy, null=True, blank=True)
    terms_conditions = models.FileField(upload_to=get_file_menu_terms_conditions, null=True, blank=True)
    passenger_rights = models.FileField(upload_to=get_file_menu_passenger_rights, null=True, blank=True)
    dashboard = models.FileField(upload_to=get_file_menu_dashboard, null=True, blank=True)
    sign_in_sign_up = models.FileField(upload_to=get_file_menu_sign_in_sign_up, null=True, blank=True)
    language = models.FileField(upload_to=get_file_menu_language, null=True, blank=True)
    help = models.FileField(upload_to=get_file_menu_help, null=True, blank=True)
    send_feedback = models.FileField(upload_to=get_file_menu_send_feedback, null=True, blank=True)
    travel_requirements = models.FileField(upload_to=get_file_menu_travel_requirements, null=True, blank=True)
    submit_payments = models.FileField(upload_to=get_file_menu_submit_payments, null=True, blank=True)
    payment_status = models.FileField(upload_to=get_file_menu_payment_status, null=True, blank=True)
    application_status = models.FileField(upload_to=get_file_menu_application_status, null=True, blank=True)
    retrieve_app_no = models.FileField(upload_to=get_file_menu_retrieve_app_no, null=True, blank=True)
    article = models.FileField(upload_to=get_file_menu_article, null=True, blank=True)
    things_to_do = models.FileField(upload_to=get_file_menu_things_to_do, null=True, blank=True)
    training = models.FileField(upload_to=get_file_menu_training, null=True, blank=True)
    pax_topic = models.FileField(upload_to=get_file_menu_pax_topic, null=True, blank=True)
    apply_for_study = models.FileField(upload_to=get_file_menu_apply_for_study, null=True, blank=True)
    visa_chance = models.FileField(upload_to=get_file_menu_visa_chance, null=True, blank=True)
    asia_middle = models.FileField(upload_to=get_file_menu_asia_middle, null=True, blank=True)
    evaluation_form = models.FileField(upload_to=get_file_evaluation_forms, null=True, blank=True)


class Branch(models.Model):
    type = models.CharField(choices=(('Headquarter', 'Headquarter'), ('branch', 'branch')), max_length=20, null=True, blank=True)
    branch_title_latin = models.CharField(max_length=500, null=True, blank=True)
    branch_title_farsi = models.CharField(max_length=500, null=True, blank=True)
    address_title_latin = models.CharField(max_length=500, null=True, blank=True)
    address_title_farsi = models.CharField(max_length=500, null=True, blank=True)
    postal_code = models.CharField(max_length=500, null=True, blank=True)
    working_days = models.CharField(max_length=500, null=True, blank=True)
    hours_of_operation = models.CharField(max_length=500, null=True, blank=True)


class BranchNumbers(models.Model):
    tel_number = models.CharField(max_length=500, null=True, blank=True)
    branch = models.ForeignKey(Branch, on_delete=models.PROTECT, null=True, blank=True, )


def get_file_home_page_banner_banner_image(instance, filename):
    return f"file_home_page_banner_banner_image/','{filename}"




class BackLink(models.Model):
    title = models.CharField(max_length=500, null=True, blank=True)
    color_code = models.CharField(max_length=100, null=True, blank=True)
    url = models.CharField(max_length=500, null=True, blank=True)


def get_file_home_page_attention_banner(instance, filename):
    return f"file_home_page_attention_banner/','{filename}"


class HomePageAttentionBoard(models.Model):
    active = models.BooleanField(default=False)
    date = models.DateField(null=True, blank=True)
    date_color_code = models.CharField(max_length=100, null=True, blank=True)
    title_latin = models.CharField(max_length=500, null=True, blank=True)
    title_farsi = models.CharField(max_length=500, null=True, blank=True)
    title_size = models.CharField(max_length=50, choices=(('M', 'M'), ('L', 'L')), null=True, blank=True, )
    title_color_code = models.CharField(max_length=100, null=True, blank=True)
    image = models.ImageField(upload_to=get_file_home_page_attention_banner, null=True, blank=True)
    text_latin = models.TextField(null=True, blank=True)
    text_farsi = models.TextField(null=True, blank=True)
    text_size = models.CharField(max_length=4, choices=(('S', 'S'), ('M', 'M'), ('L', 'L')), null=True, blank=True, )
    text_background_color_code = models.CharField(max_length=100, null=True, blank=True)
    text_color_code = models.CharField(max_length=100, null=True, blank=True)
    text_button_latin = models.CharField(max_length=300, null=True, blank=True)
    text_button_farsi = models.CharField(max_length=300, null=True, blank=True)
    button_color_code = models.CharField(max_length=100, null=True, blank=True)
    url = models.CharField(max_length=400, null=True, blank=True)
    url_color_code = models.CharField(max_length=100, null=True, blank=True)
    url_size = models.CharField(max_length=4, choices=(('S', 'S'), ('M', 'M'), ('L', 'L')), null=True, blank=True, )
    dash_color_code = models.CharField(max_length=100, null=True, blank=True)
    background_color_code = models.CharField(max_length=100, null=True, blank=True)


class MenuColumnService(models.Model):
    service_category_1 = models.ForeignKey('Services.ServiceCategory', on_delete=models.CASCADE,
                                           related_name='menu_column_service_s_1', null=True, blank=True, )
    service_category_2 = models.ForeignKey('Services.ServiceCategory', on_delete=models.CASCADE,
                                           related_name='menu_column_service_s_2', null=True, blank=True, )
    service_category_3 = models.ForeignKey('Services.ServiceCategory', on_delete=models.CASCADE,
                                           related_name='menu_column_service_s_3', null=True, blank=True, )
    service_category_4 = models.ForeignKey('Services.ServiceCategory', on_delete=models.CASCADE,
                                           related_name='menu_column_service_s_4', null=True, blank=True, )
    service_category_5 = models.ForeignKey('Services.ServiceCategory', on_delete=models.CASCADE,
                                           related_name='menu_column_service_s_5', null=True, blank=True, )
    service_category_6 = models.ForeignKey('Services.ServiceCategory', on_delete=models.CASCADE,
                                           related_name='menu_column_service_s_6', null=True, blank=True, )
    service_category_7 = models.ForeignKey('Services.ServiceCategory', on_delete=models.CASCADE,
                                           related_name='menu_column_service_s_7', null=True, blank=True, )
    service_category_8 = models.ForeignKey('Services.ServiceCategory', on_delete=models.CASCADE,
                                           related_name='menu_column_service_s_8', null=True, blank=True, )
    service_category_9 = models.ForeignKey('Services.ServiceCategory', on_delete=models.CASCADE,
                                           related_name='menu_column_service_s_9', null=True, blank=True, )
    service_category_10 = models.ForeignKey('Services.ServiceCategory', on_delete=models.CASCADE,
                                           related_name='menu_column_service_s_10', null=True, blank=True, )


# class BookingWizardStatic(models.Model):
#     for_what = models.CharField(max_length=50, choices=(('service cat','service cat'),('tour','tour')), null=True, blank=True,)
#     decleration_text_latin = models.TextField(null=True, blank=True)
#     decleration_text_farsi =  models.TextField(null=True, blank=True)
#     yellow_notice_apply_latin = models.TextField(null=True, blank=True)
#     yellow_notice_apply_farsi =  models.TextField(null=True, blank=True)
#     checklist_latin = models.TextField(null=True, blank=True)
#     checklist_farsi =  models.TextField(null=True, blank=True)
#     submission_text_latin = models.TextField(null=True, blank=True)
#     submission_text_farsi =  models.TextField(null=True, blank=True)
#     appointment_text_latin = models.TextField(null=True, blank=True)
#     appointment_text_farsi =  models.TextField(null=True, blank=True)
#     submission_successful_text_latin = models.TextField(null=True, blank=True)
#     submission_successful_text_farsi =  models.TextField(null=True, blank=True)

def get_file_menu_tours_image(instance, filename):
    return f"get_file_menu_tours_image/','{filename}"
class MenuTours(models.Model):
    title_latin = models.CharField(max_length=250, null=True, blank=True)
    title_farsi = models.CharField(max_length=250, null=True, blank=True)
    icon = models.FileField(upload_to=get_file_menu_tours_image, null=True, blank=True)
    event_cat = models.ForeignKey(EventCat, on_delete = models.CASCADE, related_name = 'event_cat_foreignkey', null = True, blank = True,)
    section = models.IntegerField(choices=list(zip(range(1, 11), range(1, 11))), null=True, blank=True)
    tag = models.CharField(max_length=50, null=True, blank=True)

    def __str__(self):
        return str(self.title_latin)
    

def get_file_evaluation_banner(instance, filename):
    return f"file_evaluation_banner/','{filename}"

class Evaluation(models.Model):
    title_latin = models.CharField(max_length=250, null=True, blank=True)
    title_farsi = models.CharField(max_length=250, null=True, blank=True)
    
    country = models.ForeignKey('Raw_root.Country', on_delete=models.PROTECT, null=True, blank=True)
    def __str__(self):
        return str(self.title_latin)
    


class HintPortal(models.Model):
    ITEM = (('Dashboard', 'Dashboard'),
            ('My Applicant', 'My Applicant'),
            ('New App', 'New App'),
            )
    item = models.CharField(max_length=50, choices=ITEM, null=True, blank=True, )
    icon = models.FileField(upload_to=get_file_icon, null=True, blank=True)
    info_graph = models.FileField(upload_to=get_file_info_graph, null=True, blank=True)
    title_latin = models.CharField(max_length=250, null=True, blank=True)
    title_farsi = models.CharField(max_length=250, null=True, blank=True)
    text_latin = models.TextField(null=True, blank=True)
    text_farsi = models.TextField(null=True, blank=True)
    