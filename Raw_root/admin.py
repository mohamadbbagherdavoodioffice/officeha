from django.contrib import admin
from django.apps import apps
from django.contrib import admin
from django.contrib.admin.sites import AlreadyRegistered
from import_export import resources
from import_export.admin import ImportExportModelAdmin
from django.apps import apps
from django.contrib import admin
from django.contrib.admin.sites import AlreadyRegistered
from .models import *
from django_summernote.admin import SummernoteModelAdmin

app_models = apps.get_app_config('Raw_root').get_models()
for model in app_models:
    try:

        class AAAResource(resources.ModelResource):
            class Meta:
                model = model


        class AAAAdmin(ImportExportModelAdmin):
            resource_class = AAAResource


        admin.site.register(model, AAAAdmin)
    except AlreadyRegistered:
        pass




admin.site.unregister(RespondNote)
class RespondNoteAdmin(SummernoteModelAdmin, ImportExportModelAdmin):
    summernote_fields = ('failed_text_latin', 'failed_text_farsi','successful_text_latin','successful_text_farsi')
admin.site.register(RespondNote, RespondNoteAdmin)


admin.site.unregister(PassengerRelatedCategory)
class PassengerRelatedCategoryAdmin(SummernoteModelAdmin, ImportExportModelAdmin):
    summernote_fields = '__all__'
admin.site.register(PassengerRelatedCategory, PassengerRelatedCategoryAdmin)