from rest_framework import serializers
from .models import *
from Raw_root.serializers import CurrencySerializer, ExternalPaymentStatusSerializer, ActionSerializer
from Visa.serializers import *
from Services.models import ServiceCategory
from PublicPages.serializers import ServiceCategoryDetailsSerializer
from PublicPages.models import FooterHeaderSrc

class RemoveMediaPrefixField(serializers.Field):
    def to_representation(self, value):
        if value and value.url.startswith('/media/'):
            return value.url.replace('/media/', '')
        return None
    

class FooterHeaderSrcSerializer(serializers.ModelSerializer):
    favIcon = RemoveMediaPrefixField()
    header_logo_svg = RemoveMediaPrefixField()
    footer_logo_svg = RemoveMediaPrefixField()
    footer_icon1_svg = RemoveMediaPrefixField()
    footer_icon2_svg = RemoveMediaPrefixField()
    footer_icon3_svg = RemoveMediaPrefixField()
    footer_icon4_svg = RemoveMediaPrefixField()
    footer_icon5_svg = RemoveMediaPrefixField()
    footer_icon6_svg = RemoveMediaPrefixField()
    media_icon1 = RemoveMediaPrefixField()
    media_icon2 = RemoveMediaPrefixField()
    media_icon3 = RemoveMediaPrefixField()
    header_media_icon1 = RemoveMediaPrefixField()
    header_media_icon2 = RemoveMediaPrefixField()
    header_media_icon3 = RemoveMediaPrefixField()
    footer_icon7 = RemoveMediaPrefixField()
    footer_icon8 = RemoveMediaPrefixField()
    header_media_icon4 = RemoveMediaPrefixField()
    header_media_icon5 = RemoveMediaPrefixField()
    media_icon4 = RemoveMediaPrefixField()
    media_icon5 = RemoveMediaPrefixField()
    service_category_1 = ServiceCategoryDetailsSerializer()
    service_category_2 = ServiceCategoryDetailsSerializer()
    service_category_3 = ServiceCategoryDetailsSerializer()
    service_category_4 = ServiceCategoryDetailsSerializer()
    service_category_5 = ServiceCategoryDetailsSerializer()

    class Meta:
        model = FooterHeaderSrc
        fields = '__all__'

class ImmigrationSectionConfigSerializer(serializers.ModelSerializer):
    class Meta:
        model = ImmigrationSectionConfig
        fields = '__all__'