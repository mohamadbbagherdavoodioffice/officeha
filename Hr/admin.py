from django.contrib import admin
from django_summernote.admin import SummernoteModelAdmin
from django.apps import apps
from django.contrib import admin
from django.contrib.admin.sites import AlreadyRegistered
from .models import *
from import_export import resources
from import_export.admin import ImportExportModelAdmin
from django.apps import apps
from django.contrib import admin
from django.contrib.admin.sites import AlreadyRegistered
from .models import *

app_models = apps.get_app_config('Hr').get_models()
for model in app_models:
    try:
        # admin.site.register(model)

        class AAAResource(resources.ModelResource):
            class Meta:
                model = model


        class AAAAdmin(ImportExportModelAdmin):
            resource_class = AAAResource


        admin.site.register(model, AAAAdmin)
    except AlreadyRegistered:
        pass



admin.site.unregister(Job)
class JobAdmin(SummernoteModelAdmin, ImportExportModelAdmin):
    summernote_fields = '__all__'
admin.site.register(Job, JobAdmin)