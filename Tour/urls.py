from django.urls import path, include, re_path

from SpecificPages.views import AboutUsCertALLAPIView
from .views import *
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('get_tour_conf', TourPageConfigALLAPIView.as_view()),
    path('get_tour_details_for_search', TourGetListCountryEventCatAPIView.as_view()),
    path('get_tour_details_criteria', TourGetSearchCriteriaAPIView.as_view()),
    path('get_tour_details_query', TourQueryAPIView.as_view()),
    path('get_tour_with_event_cat', TourDepartureFareQueryEventCatAPIVIEW.as_view()),

    path('get_each_tour', TourDetailsAPIView.as_view()),
    path('get_tour_query', TourDepartureFareQueryRegionAndEventCatAPIVIEW.as_view()),
    path('get_tour_query_or', TourDepartureFareQueryRegionAndEventCatAPIVIEWOR.as_view()),
    path('list_tour', TourAllListAPIView.as_view()),
    path('get_tour_dept_with_tour_id', TourDetailsAPIViewQueryWithTourID.as_view()),
    path('each_departure_fare', TourDepartureFareDetailsAPIView.as_view()),
    path('query_faq', QueryFAQ.as_view()),
    path('itinerary_with_tour_id', TourDetails2IdAPIView.as_view()),

    path('get_tour_list', EventCatAPIView.as_view()),
    



]

urlpatterns = urlpatterns + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
