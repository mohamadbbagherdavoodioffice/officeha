from django.contrib import admin
from django.apps import apps
from django.contrib import admin
from django.contrib.admin.sites import AlreadyRegistered
from import_export import resources
from import_export.admin import ImportExportModelAdmin
from django.apps import apps
from django.contrib import admin
from django.contrib.admin.sites import AlreadyRegistered
from .models import *
from django_summernote.admin import SummernoteModelAdmin


from SpecificPages.models import EvaluationConfig
admin.site.register(EvaluationConfig)

app_models = apps.get_app_config('PublicPages').get_models()
for model in app_models:
    try:
        class AAAResource(resources.ModelResource):
            class Meta:
                model = model
        class AAAAdmin(ImportExportModelAdmin):
            resource_class = AAAResource
        admin.site.register(model, AAAAdmin)
    except AlreadyRegistered:
        pass

admin.site.unregister(Hint)
class HintAdmin(SummernoteModelAdmin, ImportExportModelAdmin):
    summernote_fields = ('text_latin', 'text_farsi')
admin.site.register(Hint, HintAdmin)

admin.site.unregister(StaticText)
class StaticTextAdmin(SummernoteModelAdmin, ImportExportModelAdmin):
    summernote_fields = ('text_latin', 'text_farsi')
admin.site.register(StaticText, StaticTextAdmin)



admin.site.unregister(HeaderEmergencyNote)
class HeaderEmergencyNoteAdmin(SummernoteModelAdmin, ImportExportModelAdmin):
    summernote_fields = '__all__'
admin.site.register(HeaderEmergencyNote, HeaderEmergencyNoteAdmin)


admin.site.unregister(HomePageAttentionBoard)
class HomePageAttentionBoardAdmin(SummernoteModelAdmin, ImportExportModelAdmin):
    summernote_fields = '__all__'
admin.site.register(HomePageAttentionBoard, HomePageAttentionBoardAdmin)

