from django.apps import AppConfig


class RawrootConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'Raw_root'
    verbose_name = "Raw Root"
