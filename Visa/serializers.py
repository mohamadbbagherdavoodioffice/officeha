from Content.models import SpecificFaq
from Content.serializers import ServiceCategoryAllSerializer
from Organization.serializers import ContractProfileSerializer, DeclarationFormsSerializer, GuaranteeAmountSerializer
from Raw_root.models import VisaSubCat
from Raw_root.serializers import RefundConditionSerializer, EntryTypeSerializer, ProvidingModeSerializer
from Raw_root.serializers import VisaMainCatSerializer, CountrySerializer, StayTimeCatSerializer, \
    WorkPermitSerializer, CurrencySerializer
from Raw_root.serializers import VisaSubCatSerializer
from Services.models import Aboute
# from Services.serializers import AbouteSerializer
from Services.models import Eligibility, RequiredDocumnet, Guide, Instalment
# from Services.serializers import InstalmentSerializer
from Services.serializers import SubServiceCategoryTitleSerializer
from Tour.models import Tour
# from Tour.serializers import TourSerializer
from django.utils import timezone
from rest_framework import serializers
from .models import *
from django.db.models import Q
import json
from collections import OrderedDict
from Raw_root.serializers import CountryBreifSerializer

class TourSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tour
        fields = '__all__'


class TourDetailsForArticlesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tour
        fields = ['id', 'title_latin', 'title_farsi']


class SpecificFAQUniversalSerializer(serializers.ModelSerializer):
    class Meta:
        model = SpecificFaq
        fields = '__all__'


class InstalmentdataSerializer(serializers.ModelSerializer):
    refund = RefundConditionSerializer()

    class Meta:
        model = Instalment
        fields = '__all__'


class InstalmentServiceCatdataSerializer(serializers.ModelSerializer):
    # refund = RefundConditionSerializer()
    refund_policy = RefundConditionSerializer()
    mode_of_payment = ProvidingModeSerializer()
    currency = CurrencySerializer()

    class Meta:
        model = Instalment
        fields = ['seq', 'title_latin', 'title_farsi', 'currency', 'unit_fee', 'full_fee', 'refundable',
                  'mode_of_payment', 'refund_policy']


class AboutedataSerializer(serializers.ModelSerializer):
    class Meta:
        model = Aboute
        fields = '__all__'


class EligibilitydataSerializer(serializers.ModelSerializer):
    class Meta:
        model = Eligibility
        fields = '__all__'


class RequiredDocumnetdataSerializer(serializers.ModelSerializer):
    provide_mode = ProvidingModeSerializer()

    class Meta:
        model = RequiredDocumnet
        fields = '__all__'


class GuidedataSerializer(serializers.ModelSerializer):
    class Meta:
        model = Guide
        fields = '__all__'


class VisaStreamSerializer(serializers.ModelSerializer):
    visa_sub_cat = serializers.SerializerMethodField()

    class Meta:
        model = VisaStream
        fields = '__all__'

    def get_visa_sub_cat(self, instance):
        ids = int(instance.id)
        data = VisaSubCat.objects.filter(pk=ids)
        serializer = VisaSubCatSerializer(data, many=True)
        return serializer.data


class VisaStreamDetailsSerializer(serializers.ModelSerializer):
    country = CountryBreifSerializer()
    entry_type = EntryTypeSerializer()
    stay_time_cat = StayTimeCatSerializer()
    embassy_currency = CurrencySerializer()
    visa_sub_cat = VisaSubCatSerializer()
    class Meta:
        model = VisaStream
        fields = ['id','title_latin', 'title_farsi', 'stay_time_cat',  "country",'embassy_fee_main_applicant', 'thumbnail',
                  'banner', 'entry_type', 'embassy_currency', 'embassy_fee_main_applicant','abbreviation_code','visa_sub_cat',
                  'processing_time_latin','processing_time_farsi', 'interview']


class VisaAvailabilitySerializer(serializers.ModelSerializer):
    class Meta:
        model = VisaAvailability
        fields = '__all__'


class VisaStreamSubAllDetailsSerializer(serializers.ModelSerializer):
    visa_sub_cat = VisaSubCatSerializer()
    # visa_main_cat = VisaMainCatSerializer()
    country = CountrySerializer()
    stay_time_cat = StayTimeCatSerializer()
    work_permit = WorkPermitSerializer()
    contract = ContractProfileSerializer()
    declaration_forms = DeclarationFormsSerializer()
    guarantee = GuaranteeAmountSerializer()
    agn_currency = CurrencySerializer()
    embassy_currency = CurrencySerializer()
    entry_type = EntryTypeSerializer()

    class Meta:
        model = VisaStream
        fields = ['id', 'alt_farsi', 'abbreviation_code', 'publicity', 'is_popular', 'appointment',
                  'icon', 'last_edit', 'visa_sub_cat', 'title_latin', 'title_farsi',
                  'country', 'sub_class_nr', 'entry_type', 'stay_time_cat',
                  'processing_time_latin', 'processing_time_farsi', 'work_permit',
                  'interview', 'contract', 'declaration_forms', 'guarantee',
                  'agn_currency', 'agn_share_amount', 'embassy_currency', 'embassy_fee_main_applicant',
                  'emergency_note', 'expiry_date_emergency_note', 'banner', 'alt_text', 'tags']

    def get_emergency_note(self, instance):
        from dateutil import parser
        import datetime
        now = datetime.datetime.now()

        now = timezone.now()
        if instance.expiry_date_emergency_note is not None:
            new_date = parser.parse(instance.expiry_date_emergency_note)
            if now > new_date:
                instance.emergency_note = {}
                return None

    def get_instalment(self, instance):
        ids = int(instance.id)
        data = Instalment.objects.filter(stid_id=ids)
        serializer = InstalmentdataSerializer(data, many=True)
        return serializer.data

    def get_visa_availability(self, instance):
        ids = int(instance.id)
        now = timezone.now().date()
        data = VisaAvailability.objects.filter(Q(stid_id=ids) & Q(from_date__lte=now) & Q(to_date__gte=now))
        serializer = VisaAvailabilitySerializer(data, many=True)
        return serializer.data

    def get_tour(self, instance):
        ids = int(instance.id)
        data = Tour.objects.filter(stid_id=ids)
        serializer = TourSerializer(data, many=True)
        return serializer.data

    def get_aboute(self, instance):
        ids = int(instance.id)
        data = Aboute.objects.filter(stid_id=ids)
        serializer = AboutedataSerializer(data, many=True)
        return serializer.data

    def get_eligibility(self, instance):
        ids = int(instance.id)
        data = Eligibility.objects.filter(stid_id=ids)
        serializer = EligibilitydataSerializer(data, many=True)
        return serializer.data

    def get_guide(self, instance):
        ids = int(instance.id)
        data = Guide.objects.filter(stid_id=ids)
        serializer = GuidedataSerializer(data, many=True)
        return serializer.data

    def get_required_documnet(self, instance):
        ids = int(instance.id)
        data = RequiredDocumnet.objects.filter(stid_id=ids)
        serializer = RequiredDocumnetdataSerializer(data, many=True)
        return serializer.data

    def get_specific_faq(self, instance):
        ids = int(instance.id)
        data = SpecificFaq.objects.filter(stid_id=ids)
        serializer = SpecificFAQUniversalSerializer(data, many=True)
        return serializer.data


class VisaStreamSubForAsiaMiddleDetailsSerializer(serializers.ModelSerializer):
    entry_type = EntryTypeSerializer()
    stay_time_cat = StayTimeCatSerializer()
    country = CountrySerializer()

    class Meta:
        model = VisaStream
        fields = ['title_latin', 'title_farsi', 'thumbnail', 'country', 'entry_type', 'stay_time_cat']


class VisaStreamForCountryQuerySerializer(serializers.ModelSerializer):
    country = CountrySerializer()
    entry_type = EntryTypeSerializer()
    stay_time_cat = StayTimeCatSerializer()
    embassy_currency = CurrencySerializer()

    class Meta:
        model = VisaStream
        fields = ['id','alt_farsi', 'last_edit', 'title_latin', 'title_farsi', 'thumbnail', 'country', 'entry_type',
                  'stay_time_cat', 'embassy_currency', 'embassy_fee_main_applicant', 'alt_text']


class VisaStreamTitleSerializer(serializers.ModelSerializer):
    class Meta:
        model = VisaStream
        fields = ['id', 'title_latin', 'title_farsi']


class InstalmentGETSerializer(serializers.ModelSerializer):
    refund = RefundConditionSerializer()
    mode_of_payment = ProvidingModeSerializer()
    currency = CurrencySerializer()

    class Meta:
        model = Instalment
        fields = ['seq', 'title_latin', 'title_farsi', 'currency', 'unit_fee', 'full_fee', 'refundable', 'refund',
                  'mode_of_payment', 'account_number']


class SubVisaStreamSerializer(serializers.ModelSerializer):
    class Meta:
        model = SubVisaStream
        fields = ['id', 'title_latin', 'title_farsi']


class TourTitleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tour
        fields = ['title_latin', 'title_farsi']


class InstalmentVisaStreamCatdataSerializer(serializers.ModelSerializer):
    refund_policy = RefundConditionSerializer()
    mode_of_payment = ProvidingModeSerializer()
    currency = CurrencySerializer()
    stid = VisaStreamTitleSerializer()
    # sub_stid = SubVisaStreamSerializer()
    # currency = CurrencySerializer()
    service_category = ServiceCategoryAllSerializer()
    sub_service_cat = SubServiceCategoryTitleSerializer()
    tour = TourTitleSerializer()

    class Meta:
        model = Instalment
        fields = '__all__'


class VisaStreamTitleSerializer(serializers.ModelSerializer):
    class Meta:
        model = VisaStream
        fields = ['id', 'title_latin', 'title_farsi']


class SubVisaStreamDetailsSerializer(serializers.ModelSerializer):
    stid = VisaStreamTitleSerializer()
    currency = CurrencySerializer()
    guarantee = GuaranteeAmountSerializer()
    work_permit = WorkPermitSerializer()
    stay_time_cat = StayTimeCatSerializer()

    class Meta:
        model = SubVisaStream
        fields = '__all__'


class TourTitleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tour
        fields = ['title_latin', 'title_farsi', 'id']



class PassportVisionSerializer(serializers.ModelSerializer):
    class Meta:
        model = PassportVision
        fields = '__all__'
