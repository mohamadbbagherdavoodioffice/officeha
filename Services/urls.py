from django.urls import path, include, re_path
from .views import *
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('get_list_service_cat', ServiceCatListAPIView.as_view()),
    path('get_service_cat', ServiceCatAPIView.as_view()),
    path('get_service_cat_priority', ServiceCatProirityListAPIView.as_view()),
    path('get_declaration_form', DeclarationFormWizardAPIView.as_view()),
    path('list_service_cat', ServiceCategoryAllListAPIView.as_view()),
    path('get_static_text_consultation', StaticConsultationAPIView.as_view()),
    path('get_consultation', GETConsultationAPIView.as_view()),
    path('get_consultation_time_frame', ConsultCapacityDateTimeFrame.as_view()),
    path('get_tour_in_ex', TourIncludedExcludedWithTourAPIView.as_view()),
    path('get_about_filter', AbouteFilterAPIView.as_view()),
    path('get_eligibility_filter', EligibilityFilterAPIView.as_view()),
    path('get_guide_filter', GuideFilterAPIView.as_view()),
    path('get_required_documnet_filter', RequiredDocumnetFilterAPIView.as_view()),
    path('get_service_category_in_ex', IncludedExcludedWithMultiFilterAPIView.as_view()),
    path('get_subservice_bymainservice', SubServiceCategoryGETServiceCatAPIView.as_view()),
    path('get_instalment_filter', InstalmentListAPIView.as_view()),
    path('workshops', WorkshopListView.as_view(), name='workshop-list'),
    path('get_consult_schedule', GetConsultScheduleView.as_view(), name='get_consult_schedule'),
]

urlpatterns = urlpatterns + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
