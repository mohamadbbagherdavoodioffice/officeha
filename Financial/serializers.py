from rest_framework import serializers
from .models import *
from Raw_root.serializers import CurrencySerializer, ExternalPaymentStatusSerializer, ActionSerializer
from Tour.serializers import TourInvoiceSerializer

class ExternalPaymentSerializer(serializers.ModelSerializer):
    invoice = TourInvoiceSerializer()
    currency = CurrencySerializer()
    external_payment_status = ExternalPaymentStatusSerializer()
    action = ActionSerializer()
    class Meta:
        model = ExternalPayment
        fields = '__all__'

class ExternalPaymentDetailsSerializer(serializers.ModelSerializer):
    # invoice = TourInvoiceSerializer()
    # currency = CurrencySerializer()
    # external_payment_status = ExternalPaymentStatusSerializer()
    # action = ActionSerializer()
    class Meta:
        model = ExternalPayment
        fields = ['first_name','last_name','nid','tel','customer_note','debit_card_number','payment_receipt_file','payment_code']

class ExternalPaymentSerializer(serializers.ModelSerializer):

    class Meta:
        model = ExternalPayment
        fields = '__all__'

class ExternalPaymentStatusSerializer(serializers.ModelSerializer):
    external_payment_status = ExternalPaymentStatusSerializer()
    action = ActionSerializer()
    class Meta:
        model = ExternalPayment
        fields = ['payment_code','external_payment_status', 'refused_note', 'action', ]

class InvoiceSerializer(serializers.ModelSerializer):

    class Meta:
        model = Invoice
        fields = '__all__'