from django.db import models


class ContractSection(models.Model):
    section_latin = models.CharField(max_length=500, null=True, blank=True)
    section_farsi = models.CharField(max_length=500, null=True, blank=True)

    def __str__(self):
        return self.section_latin


class ContractProfile(models.Model):
    title_latin = models.CharField(max_length=500, null=True, blank=True)
    title_farsi = models.CharField(max_length=500, null=True, blank=True)


    def __str__(self):
        return self.title_latin


class ContractClauses(models.Model):
    contract = models.ForeignKey(ContractProfile, on_delete=models.CASCADE, null=True, blank=True, default=None)
    contract_section_seq = models.PositiveIntegerField(null=True, blank=True)
    contract_section = models.ForeignKey(ContractSection, on_delete=models.CASCADE, null=True, blank=True, default=None)
    clause_text_latin = models.CharField(max_length=500, null=True, blank=True)
    clause_text_farsi = models.CharField(max_length=500, null=True, blank=True)


class DeclarationForms(models.Model):
    title_latin = models.CharField(max_length=500, null=True, blank=True)
    title_farsi = models.CharField(max_length=500, null=True, blank=True)

    def __str__(self):
        return str(self.title_latin)


class DeclarationQuestion(models.Model):
    # decleration_form = models.ManyToManyField(DeclarationForms, blank=True,)
    service_category = models.ForeignKey("Services.ServiceCategory", on_delete=models.CASCADE, null=True, blank=True)
    stid = models.ForeignKey('Visa.VisaStream', on_delete=models.CASCADE, null=True, blank=True)
    tour = models.ForeignKey('Tour.Tour', on_delete=models.CASCADE, null=True, blank=True)
    ques_seq = models.IntegerField( null=True, blank=True)
    ques_title_latin = models.TextField(null=True, blank=True)
    ques_title_farsi = models.TextField(null=True, blank=True)
    ANSWERMODE = (("Boolean", "Boolean"), ("Text", "Text"),)
    answer_mode = models.CharField(max_length=300, choices=ANSWERMODE, null=True, blank=True)

    def __str__(self):
        datas= ""
        if self.service_category:
            datas=self.service_category
        if self.stid:
            datas=self.stid
        if self.tour:
            datas=self.tour
        return " %s | %s | %s " % (str(datas) ,self.ques_title_latin, self.ques_seq,)

class NotaryText(models.Model):
    title = models.CharField(max_length=300, null=True, blank=True)
    title_farsi = models.CharField(max_length=300, null=True, blank=True)
    commitment = models.TextField(null=True, blank=True)

    def __str__(self):
        return str(self.title_farsi)


class GuaranteeAmount(models.Model):
    date_time = models.DateTimeField(null=True, blank=True)
    currency = models.ForeignKey("Raw_root.Currency", on_delete=models.CASCADE, null=True, blank=True, default=None)
    amount = models.DecimalField(max_digits=15, decimal_places=2, null=True, blank=True)

    def __str__(self):
        return "%s / %s " % (self.currency, self.amount)


class ExRate(models.Model):
    date_time = models.DateTimeField(null=True, blank=True)
    currency_1 = models.ForeignKey("Raw_root.Currency", on_delete=models.CASCADE, related_name="exrate_currency_set_1",
                                 null=True, blank=True, )
    x_amount_in_irr_1 = models.DecimalField(max_digits=10, decimal_places=2, null=True, blank=True)
    currency_2 = models.ForeignKey("Raw_root.Currency", on_delete=models.CASCADE, related_name="exrate_currency_set_2",
                                 null=True, blank=True, )
    x_amount_in_irr_2 = models.DecimalField(max_digits=10, decimal_places=2, null=True, blank=True)
    currency_3 = models.ForeignKey("Raw_root.Currency", on_delete=models.CASCADE, related_name="exrate_currency_set_3",
                                 null=True, blank=True, )
    x_amount_in_irr_3 = models.DecimalField(max_digits=10, decimal_places=2, null=True, blank=True)
    currency_4 = models.ForeignKey("Raw_root.Currency", on_delete=models.CASCADE, related_name="exrate_currency_set_4",
                                 null=True, blank=True, )
    x_amount_in_irr_4 = models.DecimalField(max_digits=10, decimal_places=2, null=True, blank=True)
    currency_5 = models.ForeignKey("Raw_root.Currency", on_delete=models.CASCADE, related_name="exrate_currency_set_5",
                                 null=True, blank=True, )
    x_amount_in_irr_5 = models.DecimalField(max_digits=10, decimal_places=2, null=True, blank=True)


    def __str__(self):
        return str(self.date_time)


def get_bank_logo(instance, filename):
    return f"file_bank_logo/{filename}"


class Bank(models.Model):
    country = models.ForeignKey("Raw_root.Country", on_delete=models.CASCADE, related_name="bank_country_set",
                                null=True, blank=True, default=None)
    name_latin = models.CharField(max_length=500, null=True, blank=True)
    name_farsi = models.CharField(max_length=500, null=True, blank=True)
    bank_logo = models.ImageField(upload_to=get_bank_logo, null=True, blank=True)

    def __str__(self):
        return str(self.name_latin)


class AccountNumber(models.Model):
    bank = models.ForeignKey(Bank, on_delete=models.CASCADE, null=True, blank=True, default=None)
    card_number = models.CharField(max_length=100, null=True, blank=True)
    iban_number = models.CharField(max_length=100, null=True, blank=True)
    holder_Name = models.CharField(max_length=100, null=True, blank=True)

    def __str__(self):
        return "%s | %s | %s , %s " % (self.bank.name_latin, self.card_number, self.iban_number, self.holder_Name)


class DefaultWarranty(models.Model):
    TYPE = (('Bank', 'Bank'),
            ('Notary', 'Notary'),
            ('Cheque', 'Cheque'),
            ('Promissory', 'Promissory'),
            ('note', 'note'))

    type = models.CharField(max_length=33, choices=TYPE, null=True, blank=True, default=None)
    notary_text = models.ForeignKey(NotaryText, on_delete=models.CASCADE, null=True, blank=True, default=None)
    amount = models.DecimalField(max_digits=10, decimal_places=2, null=True, blank=True)
    action = models.ForeignKey('Raw_root.Action', on_delete=models.CASCADE, null=True, blank=True)

    def __str__(self):
        return "%s | %s | %s , %s " % (self.type, self.notary_text.title, self.amount, self.action.title_latin)


class OperationTask(models.Model):
    service_cat = models.ForeignKey("Services.ServiceCategory", on_delete=models.CASCADE, null=True, blank=True)
    visa_sub_cat = models.ForeignKey("Raw_root.VisaSubCat", on_delete=models.CASCADE, null=True, blank=True)
    country = models.ForeignKey("Raw_root.Country", on_delete=models.PROTECT, null=True, blank=True)
    seq = models.PositiveIntegerField(null=True, blank=True)
    task = models.ForeignKey("Organization.Task", on_delete=models.PROTECT, null=True, blank=True)


    def __str__(self):
        return "%s | %s | %s , %s " % (
        self.service_cat.title_latin, self.visa_sub_cat.title_latin, self.country.name_latin,
        self.task.task_title_latin)


class Task(models.Model):
    task_title_latin = models.CharField(max_length=500, null=True, blank=True, default=None)
    task_title_farsi = models.CharField(max_length=500, null=True, blank=True, default=None)

    def __str__(self):
        return str(self.task_title_latin)


class CheckListInFront(models.Model):
    service_cat = models.ForeignKey("Services.ServiceCategory", on_delete=models.CASCADE, null=True, blank=True,
                                    default=None)
    stid = models.ForeignKey('Visa.VisaStream', on_delete=models.PROTECT, null=True, blank=True)
    tour = models.ForeignKey('Tour.Tour', on_delete=models.PROTECT, null=True, blank=True, default=None)
    app_residency_travel_history = models.BooleanField(default=False)
    app_perferred_appointment = models.BooleanField(default=False)
    app_sponsor = models.BooleanField(default=False)
    app_edu_social_background = models.BooleanField(default=False)
    app_certificates = models.BooleanField(default=False)
    app_paper = models.BooleanField(default=False)
    app_family_info = models.BooleanField(default=False)
    app_answered_decleration = models.BooleanField(default=False)
    app_socail_media = models.BooleanField(default=False)
    app_references = models.BooleanField(default=False)
    app_work_history = models.BooleanField(default=False)
    app_volunteer_job_background = models.BooleanField(default=False)
    app_financial_data = models.BooleanField(default=False)
    app_immi_problem_history = models.BooleanField(default=False)

    def __str__(self):
        return str(self.service_cat)


class TourOperationTask(models.Model):
    tour = models.ForeignKey('Tour.Tour', on_delete=models.PROTECT, null=True, blank=True, default=None)
    t_service = models.ForeignKey('Raw_root.Tservice', on_delete=models.PROTECT, null=True, blank=True, default=None)


def get_file_general_our_buy_financial_evidence(instance, filename):
    return f"file_general_our_buy_financial_evidence/{filename}"


# class GeneralOurBuy(models.Model):
#     date_time = models.DateTimeField(null=True, blank=True)
#     currency = models.ForeignKey("Raw_root.Currency", on_delete=models.CASCADE, null=True, blank=True, )
#     amount = models.DecimalField(max_digits=10, decimal_places=2, null=True, blank=True)
#     TYPE = (
#         ('IBAN', 'IBAN'),
#         ('CC', 'CC'),
#         ('Online', 'Online'),
#         ('Cash', 'Cash'),
#     )
#     payment_methods = models.CharField(max_length=30, choices=TYPE, null=True, blank=True)
#     reason = models.ForeignKey("Raw_root.FinancialCat", on_delete=models.CASCADE, null=True, blank=True, )
#     beneficiary_business_user = models.ForeignKey('OfficeHauser.OfficeUser', on_delete=models.CASCADE, primary_key=False,
#                                       related_name='general_our_buy_beneficiary_business_user_set', null=True, blank=True)
#     account_number = models.ForeignKey('Organization.AccountNumber', on_delete=models.CASCADE, null=True,
#                                        blank=True)
#
#     financial_evidence = models.FileField(upload_to=get_file_general_our_buy_financial_evidence, null=True, blank=True)
#     benefinicary_account_iban = models.CharField(max_length=500, null=True, blank=True)
#     benefinicary_cc = models.CharField(max_length=500, null=True, blank=True)
#     by_staff_user = models.ForeignKey('OfficeHauser.OfficeUser', on_delete=models.CASCADE, primary_key=False,
#                                       related_name='general_our_buy_beneficiary_by_staff_user_set', null=True, blank=True)
#     self_note = models.TextField(null=True, blank=True)
#     last_updated_date = models.DateTimeField(auto_now=True)


class Vat(models.Model):
    sign_date_time = models.DateTimeField(null=True, blank=True)
    by_staff_user = models.ForeignKey('OfficeHauser.OfficeUser', on_delete=models.CASCADE, primary_key=False,
                                      related_name='vat_by_staff_user_set', null=True, blank=True)
    vat = models.DecimalField(max_digits=10, decimal_places=2, null=True, blank=True)


