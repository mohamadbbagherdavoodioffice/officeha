from django.urls import path, include, re_path
from .views import *
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('get_submit_payment_statics', SubmitPaymentStaticAPIView.as_view()),
    # path('create_external_payment', ExternalPaymentCreateAPIView.as_view()),
    path('get_application_status_statics', ApplicationStatusCheckStaticAPIView.as_view()),
    path('retrieve_profile_status/<str:id>/', ApplicationProfileRetriveAPIView.as_view()),
    path('get_app_number_statics', ApplicationNumberStaticAPIView.as_view()),
    path('retrieve_app_number', SearchForApplicationNumberAndUserAvailableAPIView.as_view()),
    path('query_app_profile', ApplicationProfileQueryAPIView.as_view()),
    path('query_app_profile_status/<str:id>/', ApplicationProfileRetriveStatusAPIView.as_view()),
    path('create_consultation_process', ConsultationProcessCreateAPIView.as_view()),
    path('create_application_profile', ApplicationProfileCreateView.as_view()),
    path('applicant', ApplicantCreateViewset.as_view()),

    path('list_application_profile', ApplicationProfileListView.as_view()),
    path('test', Test.as_view()),
    path('application_profile/count', ApplicationProfileCountAPIView.as_view(), name='application_profile_count_api'),
    path('pay_consultation_process', PayConsultationProcessView.as_view(), name='pay_consultation_process'),


]

urlpatterns = urlpatterns + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)


