from django.shortcuts import render
from rest_framework.generics import ListAPIView
from rest_framework.response import Response
from rest_framework.views import APIView
from Organization.models import ExRate
from Organization.serializers import ExRateSerializer


class ExRateAPIVIEW(APIView):
    def get(self, request):
        dic = {}
        ex_rate = ExRate.objects.all()
        serializer_ssc = ExRateSerializer(ex_rate, many=True)
        print(serializer_ssc.data)
        dic["ex_rate"] = serializer_ssc.data
        print(dic)
        # try:
        #     ex_rate = ExRate.objects.values().latest('id')
        #     serializer_ssc = ExRateSerializer(ex_rate, many=True)
        #     dic["ex_rate"] = serializer_ssc
        # except:
        #     dic["ex_rate"] = []
        return Response({"message": "success", "status": "OK", "data": dic}, status=200)

