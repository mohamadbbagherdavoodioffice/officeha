# Create your views here.
from PublicPages.models import Hint
from PublicPages.models import StaticText
from Raw_root.models import RespondNote
from rest_framework import generics
from rest_framework.response import Response
from rest_framework.views import APIView

from .serializers import *

class AppAnsweredDeclarationCreateViewset(generics.ListCreateAPIView):
    queryset = AppAnsweredDeclaration.objects.all()
    serializer_class = AppAnsweredDeclarationSerializer

