from django.db import models


def get_file_app_status_block_img(instance, filename):
    return f"file_app_status_block_img/{filename}"


class AppStatusBlock(models.Model):
    app_status_block_activate = models.BooleanField(default=False)
    app_status_block_img = models.ImageField(upload_to=get_file_app_status_block_img, null=True, blank=True)
    app_status_block_title_latin = models.CharField(max_length=200, null=True, blank=True)
    app_status_block_title_farsi = models.CharField(max_length=200, null=True, blank=True)
    app_status_block_text_latin = models.TextField(null=True, blank=True)
    app_status_block_text_farsi = models.TextField(null=True, blank=True)


def get_file_layout_expertise_cv_img(instance, filename):
    return f"file_layout_expertise_cv_img/{filename}"


def layout_consultation_img(instance, filename):
    return f"file_layout_consultation_img/{filename}"


def get_file_layout_service_cat1(instance, filename):
    return f"file_layout_service_cat1/{filename}"


def get_file_layout_service_cat2(instance, filename):
    return f"file_layout_service_cat2/{filename}"


def get_file_layout_service_cat3(instance, filename):
    return f"file_layout_service_cat3/{filename}"


# class LayoutBlock(models.Model):
#     layout_block_activate = models.BooleanField(default=False)
#     service_cat1 = models.ImageField(upload_to=get_file_layout_service_cat1, null=True, blank=True)
#     service_cat_1 = models.ForeignKey('Services.ServiceCategory', on_delete=models.CASCADE, null=True, blank=True,
#                                       related_name='layout_block_service_cat_1')
#     layout_service_cat1_title_latin = models.CharField(max_length=200, null=True, blank=True)
#     layout_service_cat1_title_farsi = models.CharField(max_length=200, null=True, blank=True)
#     layout_service_cat1_text_latin = models.TextField(null=True, blank=True)
#     layout_service_cat1_text_farsi = models.TextField(null=True, blank=True)
#
#     service_cat2 = models.ImageField(upload_to=get_file_layout_service_cat2, null=True, blank=True)
#     service_cat_2 = models.ForeignKey('Services.ServiceCategory', on_delete=models.CASCADE, null=True, blank=True,
#                                       related_name='layout_block_service_cat_2')
#     layout_service_cat2_title_latin = models.CharField(max_length=200, null=True, blank=True)
#     layout_service_cat2_title_farsi = models.CharField(max_length=200, null=True, blank=True)
#     layout_service_cat2_text_latin = models.TextField(null=True, blank=True)
#     layout_service_cat2_text_farsi = models.TextField(null=True, blank=True)
#
#     service_cat3 = models.ImageField(upload_to=get_file_layout_service_cat3, null=True, blank=True)
#     service_cat_3 = models.ForeignKey('Services.ServiceCategory', on_delete=models.CASCADE, null=True, blank=True,
#                                       related_name='layout_block_service_cat_3')
#     layout_service_cat3_title_latin = models.CharField(max_length=200, null=True, blank=True)
#     layout_service_cat3_title_farsi = models.CharField(max_length=200, null=True, blank=True)
#     layout_service_cat3_text_latin = models.TextField(null=True, blank=True)
#     layout_service_cat3_text_farsi = models.TextField(null=True, blank=True)
#
#     layout_consultation_img = models.ImageField(upload_to=layout_consultation_img, null=True, blank=True)
#     layout_consultation_title_latin = models.CharField(max_length=200, null=True, blank=True)
#     layout_consultation_title_fars = models.CharField(max_length=200, null=True, blank=True)
#     layout_consultation_text_latin = models.CharField(max_length=200, null=True, blank=True)
#     layout_consultation_text_farsi = models.CharField(max_length=200, null=True, blank=True)


def apply_now_visa_img(instance, filename):
    return f"apply_now_visa_img/{filename}"




def checkstatus_app_chk_thumbnail_thumbnail(instance, filename):
    return f"checkstatus_app_chk_thumbnail/{filename}"


def visa_chance_thumbnail_chk_thumbnail(instance, filename):
    return f"visa_chance_thumbnail_chk_thumbnail/{filename}"


def payment_status_thumbnail_chk_thumbnail(instance, filename):
    return f"payment_status_thumbnail_chk_thumbnail/{filename}"


class CheckStatus(models.Model):
    check_status_activate = models.BooleanField(default=False)
    app_chk_thumbnail = models.ImageField(upload_to=checkstatus_app_chk_thumbnail_thumbnail, null=True, blank=True)
    app_status_chk_title_latin = models.CharField(max_length=200, null=True, blank=True)
    app_status_chk_title_farsi = models.CharField(max_length=200, null=True, blank=True)
    app_status_chk_text_latin = models.TextField(null=True, blank=True)
    app_status_chk_text_farsi = models.TextField(null=True, blank=True)

    visa_chance_thumbnail = models.ImageField(upload_to=visa_chance_thumbnail_chk_thumbnail, null=True, blank=True)
    visa_chance_title_latin = models.CharField(max_length=200, null=True, blank=True)
    visa_chance_title_farsi = models.CharField(max_length=200, null=True, blank=True)
    visa_chance_text_latin = models.TextField(null=True, blank=True)
    visa_chance_text_farsi = models.TextField(null=True, blank=True)

    payment_status_thumbnail = models.ImageField(upload_to=payment_status_thumbnail_chk_thumbnail, null=True,
                                                 blank=True)
    payment_status_title_latin = models.CharField(max_length=200, null=True, blank=True)
    payment_status_title_farsi = models.CharField(max_length=200, null=True, blank=True)
    payment_status_text_latin = models.TextField(null=True, blank=True)
    payment_status_text_farsi = models.TextField(null=True, blank=True)


class InternationalService(models.Model):
    international_service_activate = models.BooleanField(default=False)
    block_1 = models.ForeignKey('Services.ServiceCategory', on_delete=models.CASCADE, null=True, blank=True,
                                related_name='international_service_block_1_set')
    block_2 = models.ForeignKey('Services.ServiceCategory', on_delete=models.CASCADE, null=True, blank=True,
                                related_name='international_service_block_2_set')
    block_3 = models.ForeignKey('Services.ServiceCategory', on_delete=models.CASCADE, null=True, blank=True,
                                related_name='international_service_block_3_set')
    block_4 = models.ForeignKey('Services.ServiceCategory', on_delete=models.CASCADE, null=True, blank=True,
                                related_name='international_service_block_4_set')


def block_tour_img(instance, filename):
    return f"block_tour_img/{filename}"


class BlockOutnboundTour(models.Model):
    activate = models.BooleanField(default=False)
    tour = models.ForeignKey('Tour.Tour', on_delete=models.PROTECT, null=True, blank=True)
    block_tour_img = models.ImageField(upload_to=block_tour_img, null=True, blank=True)
    block_tour_title_latin = models.CharField(max_length=200, null=True, blank=True)
    block_tour_title_farsi = models.CharField(max_length=200, null=True, blank=True)
    block_tour_text_latin = models.TextField(null=True, blank=True)
    block_tour_text_farsi = models.TextField(null=True, blank=True)


def block_school_img(instance, filename):
    return f"block_school_img/{filename}"


def app_status_img(instance, filename):
    return f"app_status_img/{filename}"


def submit_payment_img(instance, filename):
    return f"submit_payment_img/{filename}"


def travel_req_img(instance, filename):
    return f"travel_req_img/{filename}"


def consultation_img(instance, filename):
    return f"consultation_img/{filename}"



def get_file_explore_course(instance, filename):
    return f"file_explore_course/{filename}"


class ExploreCourse(models.Model):
    program = models.ForeignKey('Edu.Program', on_delete=models.CASCADE, null=True, blank=True, )
    activate = models.BooleanField(default=False)
    # explore_course = models.ImageField(upload_to=get_file_explore_course, null=True, blank=True)
    explore_course_title_latin = models.CharField(max_length=400, null=True, blank=True)
    explore_course_title_farsi = models.CharField(max_length=400, null=True, blank=True)
    explore_course_text_latin = models.TextField(null=True, blank=True)
    explore_course_text_farsi = models.TextField(null=True, blank=True)


def get_file_block_visa_chance_block_chance_img(instance, filename):
    return f"file_block_visa_chance_block_chance_img/{filename}"


# class BlockVisaChance(models.Model):
#     activate = models.BooleanField(default=False)
#     block_chance = models.CharField(max_length=200, null=True, blank=True)
#     block_chance_img = models.ImageField(upload_to=get_file_block_visa_chance_block_chance_img, null=True, blank=True)
#     block_chance_title_latin = models.CharField(max_length=400, null=True, blank=True)
#     block_chance_title_farsi = models.CharField(max_length=400, null=True, blank=True)
#     block_chance_text_latin = models.TextField(null=True, blank=True)
#     block_chance_text_farsi = models.TextField(null=True, blank=True)


def quick_access_submit_payment_thumb(instance, filename):
    return f"file_quick_access_submit_payment_thumb/{filename}"


def quick_access_consultation_service_thumb(instance, filename):
    return f"file_quick_access_submit_payment_thumb/{filename}"


def quick_access_international_service_thumb(instance, filename):
    return f"file_quick_access_submit_payment_thumb/{filename}"


def quickacess_block_career_thumb_img(instance, filename):
    return f"quickacess_block_career_thumb_img/{filename}"


def quickacess_block_study_abroad_img(instance, filename):
    return f"quickacess_block_study_abroad_img/{filename}"


def quickacess_block_faq_thumb(instance, filename):
    return f"quickacess_block_faq_thumb/{filename}"


def quickacess_new_app_thumb(instance, filename):
    return f"quickacess_new_app_thumb/{filename}"


class QuickAccessBlock(models.Model):

    # activate = models.BooleanField(default=False)

    new_app_thumb = models.ImageField(upload_to=quickacess_new_app_thumb, null=True, blank=True)
    new_app_title_latin = models.CharField(max_length=200, null=True, blank=True)
    new_app_title_farsi = models.CharField(max_length=200, null=True, blank=True)
    new_app_text_latin = models.TextField(null=True, blank=True)
    new_app_text_farsi = models.TextField(null=True, blank=True)

    track_application_thumb = models.ImageField(upload_to=quickacess_block_faq_thumb, null=True, blank=True)
    track_application_title_farsi = models.CharField(max_length=200, null=True, blank=True)
    track_application_title_latin = models.CharField(max_length=200, null=True, blank=True)
    track_application_text_latin = models.TextField(null=True, blank=True)
    track_application_text_farsi = models.TextField(null=True, blank=True)
    consultation_service_thumb = models.ImageField(upload_to=quick_access_consultation_service_thumb, null=True,blank=True)
    consultation_service_title_latin = models.CharField(max_length=200, null=True, blank=True)
    consultation_service_text_latin = models.TextField(null=True, blank=True)
    consultation_service_title_farsi = models.CharField(max_length=200, null=True, blank=True)
    consultation_service_text_farsi = models.TextField(null=True, blank=True)

    find_tour_thumb = models.ImageField(upload_to=quick_access_international_service_thumb, null=True, blank=True)
    find_tour_title_latin = models.CharField(max_length=200, null=True, blank=True)
    find_tour_text_latin = models.TextField(null=True, blank=True)
    find_tour_title_farsi = models.CharField(max_length=200, null=True, blank=True)
    find_tour_text_farsi = models.TextField(null=True, blank=True)

    international_service_thumb = models.ImageField(upload_to=checkstatus_app_chk_thumbnail_thumbnail, null=True,
                                                    blank=True)
    international_service_title_latin = models.CharField(max_length=200, null=True, blank=True)
    international_service_text_latin = models.TextField(null=True, blank=True)
    international_service_title_farsi = models.CharField(max_length=200, null=True, blank=True)
    international_service_text_farsi = models.TextField(null=True, blank=True)

    study_abroad_img = models.ImageField(upload_to=quickacess_block_study_abroad_img, null=True, blank=True)
    study_abroad_title_latin = models.CharField(max_length=200, null=True, blank=True)
    study_abroad_title_farsi = models.CharField(max_length=200, null=True, blank=True)
    study_abroad_text_latin = models.TextField(null=True, blank=True)
    study_abroad_text_farsi = models.TextField(null=True, blank=True)

    submit_payment_thumb = models.ImageField(upload_to=quick_access_submit_payment_thumb, null=True, blank=True)
    submit_payment_title_latin = models.CharField(max_length=200, null=True, blank=True)
    submit_payment_text_latin = models.TextField(null=True, blank=True)
    submit_payment_title_farsi = models.CharField(max_length=200, null=True, blank=True)
    submit_payment_text_farsi = models.TextField(null=True, blank=True)

    track_payment_thumb_img = models.ImageField(upload_to=quickacess_block_career_thumb_img, null=True, blank=True)
    track_payment_title_latin = models.CharField(max_length=200, null=True, blank=True)
    track_payment_title_farsi = models.CharField(max_length=200, null=True, blank=True)
    track_payment_text_latin = models.TextField(null=True, blank=True)
    track_payment_text_farsi = models.TextField(null=True, blank=True)









def get_block_tour_img(instance, filename):
    return f"files_block_tour_img/{filename}"


class BlockTour(models.Model):
    block_tour_id = models.ForeignKey('Tour.Tour', on_delete=models.PROTECT, null=True, blank=True)
    block_tour_img = models.ImageField(upload_to=get_block_tour_img, null=True, blank=True)
    block_tour_title_latin = models.CharField(max_length=200, null=True, blank=True)
    block_tour_title_farsi = models.CharField(max_length=200, null=True, blank=True)
    block_tour_text_latin = models.TextField(null=True, blank=True)
    block_tour_text_farsi = models.TextField(null=True, blank=True)




def get_image_thuumbnail_upcomming(instance, filename):
    return f"image_thuumbnail_upcomming/{filename}"


class UpCommingTour(models.Model):
    tour_1 = models.ForeignKey('Tour.Tour', on_delete=models.PROTECT, null=True, blank=True,related_name='tour_up_comming_1')
    thumbnail_1 = models.ImageField(upload_to=get_image_thuumbnail_upcomming, null=True, blank=True)
    title_latin_1 = models.CharField(max_length=200, null=True, blank=True)
    title_farsi_1 = models.CharField(max_length=200, null=True, blank=True)
    text_latin_1 = models.TextField(null=True, blank=True)
    text_farsi_1 = models.TextField(null=True, blank=True)
    price_1 = models.DecimalField(max_digits=15, decimal_places=2, null=True, blank=True)
    currency_1 = models.ForeignKey('Raw_root.Currency', on_delete=models.PROTECT, null=True, blank=True,related_name="currency_up_comming1")

    tour_2 = models.ForeignKey('Tour.Tour', on_delete=models.PROTECT, null=True, blank=True,related_name='tour_up_comming_2')
    thumbnail_2 = models.ImageField(upload_to=get_image_thuumbnail_upcomming, null=True, blank=True)
    title_latin_2 = models.CharField(max_length=200, null=True, blank=True)
    title_farsi_2 = models.CharField(max_length=200, null=True, blank=True)
    text_latin_2 = models.TextField(null=True, blank=True)
    text_farsi_2 = models.TextField(null=True, blank=True)
    price_2 = models.DecimalField(max_digits=15, decimal_places=2, null=True, blank=True)
    currency_2 = models.ForeignKey('Raw_root.Currency', on_delete=models.PROTECT, null=True, blank=True,related_name="currency_up_comming2")

    tour_3 = models.ForeignKey('Tour.Tour', on_delete=models.PROTECT, null=True, blank=True,related_name='tour_up_comming_3')
    thumbnail_3 = models.ImageField(upload_to=get_image_thuumbnail_upcomming, null=True, blank=True)
    title_latin_3 = models.CharField(max_length=200, null=True, blank=True)
    title_farsi_3 = models.CharField(max_length=200, null=True, blank=True)
    text_latin_3 = models.TextField(null=True, blank=True)
    text_farsi_3 = models.TextField(null=True, blank=True)
    price_3 = models.DecimalField(max_digits=15, decimal_places=2, null=True, blank=True)
    currency_3 = models.ForeignKey('Raw_root.Currency', on_delete=models.PROTECT, null=True, blank=True,related_name="currency_up_comming3")

    tour_4 = models.ForeignKey('Tour.Tour', on_delete=models.PROTECT, null=True, blank=True,related_name='tour_up_comming_4')
    thumbnail_4 = models.ImageField(upload_to=get_image_thuumbnail_upcomming, null=True, blank=True)
    title_latin_4 = models.CharField(max_length=200, null=True, blank=True)
    title_farsi_4 = models.CharField(max_length=200, null=True, blank=True)
    text_latin_4 = models.TextField(null=True, blank=True)
    text_farsi_4 = models.TextField(null=True, blank=True)
    price_4 = models.DecimalField(max_digits=15, decimal_places=2, null=True, blank=True)
    currency_4 = models.ForeignKey('Raw_root.Currency', on_delete=models.PROTECT, null=True, blank=True,related_name="currency_up_comming4")




class UpCommingTourDom(models.Model):
    tour_1 = models.ForeignKey('Tour.Tour', on_delete=models.PROTECT, null=True, blank=True,related_name='tour_up_comming_11')
    thumbnail_1 = models.ImageField(upload_to=get_image_thuumbnail_upcomming, null=True, blank=True)
    title_latin_1 = models.CharField(max_length=200, null=True, blank=True)
    title_farsi_1 = models.CharField(max_length=200, null=True, blank=True)
    text_latin_1 = models.TextField(null=True, blank=True)
    text_farsi_1 = models.TextField(null=True, blank=True)
    price_1 = models.DecimalField(max_digits=15, decimal_places=2, null=True, blank=True)
    currency_1 = models.ForeignKey('Raw_root.Currency', on_delete=models.PROTECT, null=True, blank=True,related_name="currency_up_comming11")

    tour_2 = models.ForeignKey('Tour.Tour', on_delete=models.PROTECT, null=True, blank=True,related_name='tour_up_comming_21')
    thumbnail_2 = models.ImageField(upload_to=get_image_thuumbnail_upcomming, null=True, blank=True)
    title_latin_2 = models.CharField(max_length=200, null=True, blank=True)
    title_farsi_2 = models.CharField(max_length=200, null=True, blank=True)
    text_latin_2 = models.TextField(null=True, blank=True)
    text_farsi_2 = models.TextField(null=True, blank=True)
    price_2 = models.DecimalField(max_digits=15, decimal_places=2, null=True, blank=True)
    currency_2 = models.ForeignKey('Raw_root.Currency', on_delete=models.PROTECT, null=True, blank=True,related_name="currency_up_comming21")

    tour_3 = models.ForeignKey('Tour.Tour', on_delete=models.PROTECT, null=True, blank=True,related_name='tour_up_comming_31')
    thumbnail_3 = models.ImageField(upload_to=get_image_thuumbnail_upcomming, null=True, blank=True)
    title_latin_3 = models.CharField(max_length=200, null=True, blank=True)
    title_farsi_3 = models.CharField(max_length=200, null=True, blank=True)
    text_latin_3 = models.TextField(null=True, blank=True)
    text_farsi_3 = models.TextField(null=True, blank=True)
    price_3 = models.DecimalField(max_digits=15, decimal_places=2, null=True, blank=True)
    currency_3 = models.ForeignKey('Raw_root.Currency', on_delete=models.PROTECT, null=True, blank=True,related_name="currency_up_comming31")

    tour_4 = models.ForeignKey('Tour.Tour', on_delete=models.PROTECT, null=True, blank=True,related_name='tour_up_comming_41')
    thumbnail_4 = models.ImageField(upload_to=get_image_thuumbnail_upcomming, null=True, blank=True)
    title_latin_4 = models.CharField(max_length=200, null=True, blank=True)
    title_farsi_4 = models.CharField(max_length=200, null=True, blank=True)
    text_latin_4 = models.TextField(null=True, blank=True)
    text_farsi_4 = models.TextField(null=True, blank=True)
    price_4 = models.DecimalField(max_digits=15, decimal_places=2, null=True, blank=True)
    currency_4 = models.ForeignKey('Raw_root.Currency', on_delete=models.PROTECT, null=True, blank=True,related_name="currency_up_comming41")

class ImmigrationSectionConfig(models.Model):
    active = models.BooleanField(default=False)
    
    main_title_latin = models.CharField(max_length=300, null=True, blank=True)
    main_title_farsi = models.CharField(max_length=300, null=True, blank=True)
    
    evaluation_title_latin = models.CharField(max_length=300, null=True, blank=True)
    evaluation_title_farsi = models.CharField(max_length=300, null=True, blank=True)
    evaluation_desc_latin = models.CharField(max_length=300, null=True, blank=True)
    evaluation_desc_farsi = models.CharField(max_length=300, null=True, blank=True)
    
    consultation_title_latin = models.CharField(max_length=300, null=True, blank=True)
    consultation_title_farsi = models.CharField(max_length=300, null=True, blank=True)
    consultation_desc_latin = models.CharField(max_length=300, null=True, blank=True)
    consultation_desc_farsi = models.CharField(max_length=300, null=True, blank=True)
    
    case_number_title_latin = models.CharField(max_length=300, null=True, blank=True)
    case_number_title_farsi = models.CharField(max_length=300, null=True, blank=True)
    case_number_text_latin = models.TextField(max_length=1000, null=True, blank=True)
    case_number_text_farsi = models.TextField(max_length=1000, null=True, blank=True)
    
    support_title_latin = models.CharField(max_length=300, null=True, blank=True)
    support_title_farsi = models.CharField(max_length=300, null=True, blank=True)
    support_text_latin = models.TextField(max_length=1000, null=True, blank=True)
    support_text_farsi = models.TextField(max_length=1000, null=True, blank=True)
    
    post_immi_title_latin = models.CharField(max_length=300, null=True, blank=True)
    post_immi_title_farsi = models.CharField(max_length=300, null=True, blank=True)
    post_immi_text_latin = models.TextField(max_length=1000, null=True, blank=True)
    post_immi_text_farsi = models.TextField(max_length=1000, null=True, blank=True)
    
    school_title_latin = models.CharField(max_length=300, null=True, blank=True)
    school_title_farsi = models.CharField(max_length=300, null=True, blank=True)
    school_text_latin = models.CharField(max_length=300, null=True, blank=True)
    school_text_farsi = models.CharField(max_length=300, null=True, blank=True)
    
    admission_title_latin = models.CharField(max_length=300, null=True, blank=True)
    admission_title_farsi = models.CharField(max_length=300, null=True, blank=True)
    admission_text_latin = models.CharField(max_length=300, null=True, blank=True)
    admission_text_farsi = models.CharField(max_length=300, null=True, blank=True)
    
    skilled_title_latin = models.CharField(max_length=300, null=True, blank=True)
    skilled_title_farsi = models.CharField(max_length=300, null=True, blank=True)
    skilled_text_latin = models.CharField(max_length=300, null=True, blank=True)
    skilled_text_farsi = models.CharField(max_length=300, null=True, blank=True)
    
    court_title_latin = models.CharField(max_length=300, null=True, blank=True)
    court_title_farsi = models.CharField(max_length=300, null=True, blank=True)
    court_text_latin = models.CharField(max_length=300, null=True, blank=True)
    court_text_farsi = models.CharField(max_length=300, null=True, blank=True)
    
    in_destination_title_latin = models.CharField(max_length=300, null=True, blank=True)
    in_destination_title_farsi = models.CharField(max_length=300, null=True, blank=True)
    in_destination_text_latin = models.CharField(max_length=300, null=True, blank=True)
    in_destination_text_farsi = models.CharField(max_length=300, null=True, blank=True)
    
    global_title_latin = models.CharField(max_length=300, null=True, blank=True)
    global_title_farsi = models.CharField(max_length=300, null=True, blank=True)
    global_text_latin = models.CharField(max_length=300, null=True, blank=True)
    global_text_farsi = models.CharField(max_length=300, null=True, blank=True)
    
    workshop_date_icon = models.ImageField(null=True, blank=True)
    workshop_location_icon = models.ImageField(null=True, blank=True)
    workshop_hotline_icon = models.ImageField(null=True, blank=True)
    workshop_delivery_icon = models.ImageField(null=True, blank=True)
    workshop_fee_icon = models.ImageField(null=True, blank=True)
    
    calculate_chance_title_latin = models.CharField(max_length=300, null=True, blank=True)
    calculate_chance_title_farsi = models.CharField(max_length=300, null=True, blank=True)
    calculate_line1_latin = models.CharField(max_length=300, null=True, blank=True)
    calculate_line1_farsi = models.CharField(max_length=300, null=True, blank=True)
    calculate_line2_latin = models.CharField(max_length=300, null=True, blank=True)
    calculate_line2_farsi = models.CharField(max_length=300, null=True, blank=True)