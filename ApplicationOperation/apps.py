from django.apps import AppConfig


class ApplicationOperationConfig(AppConfig):
    name = 'ApplicationOperation'
