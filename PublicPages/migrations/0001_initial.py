# Generated by Django 3.2.10 on 2023-09-21 13:08

import PublicPages.models
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('Raw_root', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='BackLink',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(blank=True, max_length=500, null=True)),
                ('color_code', models.CharField(blank=True, max_length=100, null=True)),
                ('url', models.CharField(blank=True, max_length=500, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Branch',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('type', models.CharField(blank=True, choices=[('Headquarter', 'Headquarter'), ('branch', 'branch')], max_length=20, null=True)),
                ('branch_title_latin', models.CharField(blank=True, max_length=500, null=True)),
                ('branch_title_farsi', models.CharField(blank=True, max_length=500, null=True)),
                ('address_title_latin', models.CharField(blank=True, max_length=500, null=True)),
                ('address_title_farsi', models.CharField(blank=True, max_length=500, null=True)),
                ('postal_code', models.CharField(blank=True, max_length=500, null=True)),
                ('working_days', models.CharField(blank=True, max_length=500, null=True)),
                ('hours_of_operation', models.CharField(blank=True, max_length=500, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='BranchNumbers',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('tel_number', models.CharField(blank=True, max_length=500, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Evaluation',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title_latin', models.CharField(blank=True, max_length=250, null=True)),
                ('title_farsi', models.CharField(blank=True, max_length=250, null=True)),
                ('banner', models.FileField(blank=True, null=True, upload_to=PublicPages.models.get_file_evaluation_banner)),
            ],
        ),
        migrations.CreateModel(
            name='FooterHeaderSrc',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('favIcon', models.ImageField(blank=True, null=True, upload_to=PublicPages.models.get_file_header_fav_icon)),
                ('about_text_latin', models.CharField(blank=True, max_length=500, null=True)),
                ('about_text_farsi', models.CharField(blank=True, max_length=500, null=True)),
                ('about_text_latin_2', models.CharField(blank=True, max_length=500, null=True)),
                ('about_text_farsi_2', models.CharField(blank=True, max_length=500, null=True)),
                ('header_logo_svg', models.FileField(blank=True, null=True, upload_to=PublicPages.models.get_file_header_logo)),
                ('footer_color_code_hex', models.CharField(blank=True, max_length=20, null=True)),
                ('footer_logo_svg', models.FileField(blank=True, null=True, upload_to=PublicPages.models.get_file_footer_logo)),
                ('tel_number_1', models.CharField(blank=True, max_length=20, null=True)),
                ('tel_number_2', models.CharField(blank=True, max_length=20, null=True)),
                ('tel_number_3', models.CharField(blank=True, max_length=20, null=True)),
                ('footer_address_latin', models.CharField(blank=True, max_length=500, null=True)),
                ('footer_address_farsi', models.CharField(blank=True, max_length=500, null=True)),
                ('footer_icon1_svg', models.FileField(blank=True, null=True, upload_to=PublicPages.models.get_file_footer_icon1_svg)),
                ('footer_icon_url1', models.CharField(blank=True, max_length=500, null=True)),
                ('footer_icon_html1', models.TextField(blank=True, null=True)),
                ('footer_icon2_svg', models.FileField(blank=True, null=True, upload_to=PublicPages.models.get_file_footer_icon2_svg)),
                ('footer_icon_url2', models.CharField(blank=True, max_length=500, null=True)),
                ('footer_icon_html2', models.TextField(blank=True, null=True)),
                ('footer_icon3_svg', models.FileField(blank=True, null=True, upload_to=PublicPages.models.get_file_footer_icon3_svg)),
                ('footer_icon_url3', models.CharField(blank=True, max_length=500, null=True)),
                ('footer_icon_html3', models.TextField(blank=True, null=True)),
                ('footer_icon4_svg', models.FileField(blank=True, null=True, upload_to=PublicPages.models.get_file_footer_icon4_svg)),
                ('footer_icon_url4', models.CharField(blank=True, max_length=500, null=True)),
                ('footer_icon_html4', models.TextField(blank=True, null=True)),
                ('footer_icon5_svg', models.FileField(blank=True, null=True, upload_to=PublicPages.models.get_file_footer_icon5_svg)),
                ('footer_icon_url5', models.CharField(blank=True, max_length=500, null=True)),
                ('footer_icon_html5', models.TextField(blank=True, null=True)),
                ('footer_icon6_svg', models.FileField(blank=True, null=True, upload_to=PublicPages.models.get_file_footer_icon6_svg)),
                ('footer_icon_url6', models.CharField(blank=True, max_length=500, null=True)),
                ('footer_icon_html6', models.TextField(blank=True, null=True)),
                ('media_icon1', models.FileField(blank=True, null=True, upload_to=PublicPages.models.get_file_media_icon1)),
                ('media_url1', models.CharField(blank=True, max_length=500, null=True)),
                ('media_icon2', models.FileField(blank=True, null=True, upload_to=PublicPages.models.get_file_media_icon2)),
                ('media_url2', models.CharField(blank=True, max_length=500, null=True)),
                ('media_icon3', models.FileField(blank=True, null=True, upload_to=PublicPages.models.get_file_media_icon3)),
                ('media_url3', models.CharField(blank=True, max_length=500, null=True)),
                ('header_media_icon1', models.FileField(blank=True, null=True, upload_to=PublicPages.models.get_file_header_media_icon1)),
                ('header_media_url1', models.CharField(blank=True, max_length=500, null=True)),
                ('header_media_icon2', models.FileField(blank=True, null=True, upload_to=PublicPages.models.get_file_header_media_icon2)),
                ('header_media_url2', models.CharField(blank=True, max_length=500, null=True)),
                ('header_media_icon3', models.FileField(blank=True, null=True, upload_to=PublicPages.models.get_file_header_media_icon3)),
                ('header_media_url3', models.CharField(blank=True, max_length=500, null=True)),
                ('footer_icon7', models.FileField(blank=True, null=True, upload_to=PublicPages.models.get_file_header_media_icon7)),
                ('footer_icon_url7', models.CharField(blank=True, max_length=500, null=True)),
                ('footer_icon_html7', models.TextField(blank=True, null=True)),
                ('footer_icon8', models.FileField(blank=True, null=True, upload_to=PublicPages.models.get_file_header_media_icon8)),
                ('footer_icon_url8', models.CharField(blank=True, max_length=500, null=True)),
                ('footer_icon_html8', models.TextField(blank=True, null=True)),
                ('header_media_icon4', models.FileField(blank=True, null=True, upload_to=PublicPages.models.get_file_header_media_icon4)),
                ('header_media_url4', models.CharField(blank=True, max_length=500, null=True)),
                ('header_media_icon5', models.FileField(blank=True, null=True, upload_to=PublicPages.models.get_file_header_media_icon5)),
                ('header_media_url5', models.CharField(blank=True, max_length=500, null=True)),
                ('media_icon4', models.FileField(blank=True, null=True, upload_to=PublicPages.models.get_file_media_icon4)),
                ('media_url4', models.CharField(blank=True, max_length=500, null=True)),
                ('media_icon5', models.FileField(blank=True, null=True, upload_to=PublicPages.models.get_file_media_icon5)),
                ('media_url5', models.CharField(blank=True, max_length=500, null=True)),
                ('subscribe_latin', models.CharField(blank=True, max_length=300, null=True)),
                ('subscribe_farsi', models.CharField(blank=True, max_length=300, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='HeaderEmergencyNote',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('text_latin', models.CharField(blank=True, max_length=500, null=True)),
                ('text_farsi', models.CharField(blank=True, max_length=500, null=True)),
                ('from_date', models.DateField(blank=True, null=True)),
                ('to_date', models.DateField(blank=True, null=True)),
                ('color_code_hex', models.CharField(blank=True, max_length=20, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Hint',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('item', models.CharField(blank=True, choices=[('News cat', 'News cat'), ('news share', 'news share'), ('tour search bar', 'tour search bar'), ('Booking till', 'Booking till'), ('Departure', 'Departure'), ('leg', 'leg'), ('itinerary', 'itinerary'), ('packing list', 'packing list'), ('Not recommended', 'Not recommended'), ('Included services', 'Included services'), ('Sponsor', 'Sponsor'), ('Price list', 'Price list'), ('education center', 'education center'), ('each program', 'each program'), ('Apply form', 'Apply form'), ('apply now services', 'apply now services'), ('declaration services', 'declaration services'), ('declaration visa', 'declaration visa'), ('Applicants identity table', 'Applicants identity table'), ('checklist', 'checklist'), ('Submission fee', 'Submission fee'), ('Submission Successful case number', 'Submission Successful case number'), ('Arrival flights', 'Arrival flights'), ('Flight Notification', 'Flight Notification'), ('Submit payments', 'Submit payments'), ('Financial Status Check', 'Financial Status Check'), ('Retrieve Application Number', 'Retrieve Application Number'), ('Application Status Check', 'Application Status Check'), ('travel requirements', 'travel requirements'), ('consultation step1', 'consultation step1'), ('consultation calendar', 'consultation calendar'), ('consultation time', 'consultation time'), ('consultation form', 'consultation form'), ('consultation pay', 'consultation pay'), ('signin', 'signin'), ('forgot step1', 'forgot step1'), ('forgot step2', 'forgot step2'), ('forgot new password', 'forgot new password'), ('forgot failed', 'forgot failed'), ('submit external payment', 'submit external payment'), ('payment status', 'payment status'), ('application status check', 'application status check')], max_length=50, null=True)),
                ('icon', models.FileField(blank=True, null=True, upload_to=PublicPages.models.get_file_icon)),
                ('info_graph', models.FileField(blank=True, null=True, upload_to=PublicPages.models.get_file_info_graph)),
                ('title_latin', models.CharField(blank=True, max_length=250, null=True)),
                ('title_farsi', models.CharField(blank=True, max_length=250, null=True)),
                ('text_latin', models.TextField(blank=True, null=True)),
                ('text_farsi', models.TextField(blank=True, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='HomePageAttentionBoard',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('active', models.BooleanField(default=False)),
                ('date', models.DateField(blank=True, null=True)),
                ('date_color_code', models.CharField(blank=True, max_length=100, null=True)),
                ('title_latin', models.CharField(blank=True, max_length=500, null=True)),
                ('title_farsi', models.CharField(blank=True, max_length=500, null=True)),
                ('title_size', models.CharField(blank=True, choices=[('M', 'M'), ('L', 'L')], max_length=50, null=True)),
                ('title_color_code', models.CharField(blank=True, max_length=100, null=True)),
                ('image', models.ImageField(blank=True, null=True, upload_to=PublicPages.models.get_file_home_page_attention_banner)),
                ('text_latin', models.TextField(blank=True, null=True)),
                ('text_farsi', models.TextField(blank=True, null=True)),
                ('text_size', models.CharField(blank=True, choices=[('S', 'S'), ('M', 'M'), ('L', 'L')], max_length=4, null=True)),
                ('text_background_color_code', models.CharField(blank=True, max_length=100, null=True)),
                ('text_color_code', models.CharField(blank=True, max_length=100, null=True)),
                ('text_button_latin', models.CharField(blank=True, max_length=300, null=True)),
                ('text_button_farsi', models.CharField(blank=True, max_length=300, null=True)),
                ('button_color_code', models.CharField(blank=True, max_length=100, null=True)),
                ('url', models.CharField(blank=True, max_length=400, null=True)),
                ('url_color_code', models.CharField(blank=True, max_length=100, null=True)),
                ('url_size', models.CharField(blank=True, choices=[('S', 'S'), ('M', 'M'), ('L', 'L')], max_length=4, null=True)),
                ('dash_color_code', models.CharField(blank=True, max_length=100, null=True)),
                ('background_color_code', models.CharField(blank=True, max_length=100, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='MenuColumnService',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
            ],
        ),
        migrations.CreateModel(
            name='MenuIcon',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('explore_visas', models.FileField(blank=True, null=True, upload_to=PublicPages.models.get_file_menu_explore_visas)),
                ('visa_forms', models.FileField(blank=True, null=True, upload_to=PublicPages.models.get_file_menu_visa_forms)),
                ('contact_us', models.FileField(blank=True, null=True, upload_to=PublicPages.models.get_file_menu_contact_us)),
                ('about_us', models.FileField(blank=True, null=True, upload_to=PublicPages.models.get_file_menu_about_us)),
                ('career', models.FileField(blank=True, null=True, upload_to=PublicPages.models.get_file_menu_career)),
                ('privacyـpolicy', models.FileField(blank=True, null=True, upload_to=PublicPages.models.get_file_menu_privacyـpolicy)),
                ('terms_conditions', models.FileField(blank=True, null=True, upload_to=PublicPages.models.get_file_menu_terms_conditions)),
                ('passenger_rights', models.FileField(blank=True, null=True, upload_to=PublicPages.models.get_file_menu_passenger_rights)),
                ('dashboard', models.FileField(blank=True, null=True, upload_to=PublicPages.models.get_file_menu_dashboard)),
                ('sign_in_sign_up', models.FileField(blank=True, null=True, upload_to=PublicPages.models.get_file_menu_sign_in_sign_up)),
                ('language', models.FileField(blank=True, null=True, upload_to=PublicPages.models.get_file_menu_language)),
                ('help', models.FileField(blank=True, null=True, upload_to=PublicPages.models.get_file_menu_help)),
                ('send_feedback', models.FileField(blank=True, null=True, upload_to=PublicPages.models.get_file_menu_send_feedback)),
                ('travel_requirements', models.FileField(blank=True, null=True, upload_to=PublicPages.models.get_file_menu_travel_requirements)),
                ('submit_payments', models.FileField(blank=True, null=True, upload_to=PublicPages.models.get_file_menu_submit_payments)),
                ('payment_status', models.FileField(blank=True, null=True, upload_to=PublicPages.models.get_file_menu_payment_status)),
                ('application_status', models.FileField(blank=True, null=True, upload_to=PublicPages.models.get_file_menu_application_status)),
                ('retrieve_app_no', models.FileField(blank=True, null=True, upload_to=PublicPages.models.get_file_menu_retrieve_app_no)),
                ('article', models.FileField(blank=True, null=True, upload_to=PublicPages.models.get_file_menu_article)),
                ('things_to_do', models.FileField(blank=True, null=True, upload_to=PublicPages.models.get_file_menu_things_to_do)),
                ('training', models.FileField(blank=True, null=True, upload_to=PublicPages.models.get_file_menu_training)),
                ('pax_topic', models.FileField(blank=True, null=True, upload_to=PublicPages.models.get_file_menu_pax_topic)),
                ('apply_for_study', models.FileField(blank=True, null=True, upload_to=PublicPages.models.get_file_menu_apply_for_study)),
                ('visa_chance', models.FileField(blank=True, null=True, upload_to=PublicPages.models.get_file_menu_visa_chance)),
                ('asia_middle', models.FileField(blank=True, null=True, upload_to=PublicPages.models.get_file_menu_asia_middle)),
                ('evaluation_form', models.FileField(blank=True, null=True, upload_to=PublicPages.models.get_file_evaluation_forms)),
            ],
        ),
        migrations.CreateModel(
            name='StaticText',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('type', models.CharField(blank=True, choices=[('Our services', 'Our services'), ('Passenger right main', 'Passenger right main'), ('feedback', 'feedback'), ('Claim your rights', 'Claim your rights'), ('Submit payments', 'Submit payments'), ('payment Status', 'payment Status'), ('Retrieve Application Number', 'Retrieve Application Number'), ('Application Status Check', 'Application Status Check'), ('Explore programs', 'Explore programs'), ('Travel requirements upper', 'Travel requirements upper'), ('Travel requirements lower', 'Travel requirements lower'), ('Careers', 'Careers'), ('Visa forms', 'Visa forms'), ('Asia & Middle', 'Asia & Middle'), ('Articles', 'Articles'), ('Things to do', 'Things to do'), ('Training', 'Training'), ('PAX-topics', 'PAX-topics'), ('Consultation', 'Consultation'), ('Evaluation', 'Evaluation'), ('Job Form', 'Job Form')], max_length=50, null=True)),
                ('alt_farsi', models.CharField(blank=True, max_length=300, null=True)),
                ('banner', models.FileField(blank=True, null=True, upload_to=PublicPages.models.get_file_banner)),
                ('title_latin', models.CharField(blank=True, max_length=250, null=True)),
                ('title_farsi', models.CharField(blank=True, max_length=250, null=True)),
                ('text_latin', models.TextField(blank=True, null=True)),
                ('text_farsi', models.TextField(blank=True, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='MenuTours',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title_latin', models.CharField(blank=True, max_length=250, null=True)),
                ('title_farsi', models.CharField(blank=True, max_length=250, null=True)),
                ('icon', models.FileField(blank=True, null=True, upload_to=PublicPages.models.get_file_menu_tours_image)),
                ('section', models.IntegerField(blank=True, choices=[(1, 1), (2, 2), (3, 3), (4, 4), (5, 5), (6, 6), (7, 7), (8, 8), (9, 9), (10, 10)], null=True)),
                ('tag', models.CharField(blank=True, max_length=50, null=True)),
                ('event_cat', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='event_cat_foreignkey', to='Raw_root.eventcat')),
            ],
        ),
    ]
