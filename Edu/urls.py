from django.urls import path, include, re_path
from .views import *
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('get_school', SchoolAPIView.as_view()),
    path('get_school_alphabet', SchoolAlphabetList.as_view()),
    path('get_list_all_alphabet_school', SchoolAllAlphabetList.as_view()),
    path('get_program', ProgramSchoolList.as_view()),
    re_path(r'^retrieve_program/(?P<program_id>\d+)$', ProgramRetrieveAPIView.as_view()),
    path('get_list_program', ProgramList.as_view()),
    path('get_query_program',ProgramQueryList.as_view()),
    path('get_query_school',ProgramSchoolQueryList.as_view()),

    path('get_static_explore_program',StaticExploreProgramAPIView.as_view()),
    path('get_query_program_school',QueryProgramSchool.as_view()),
    path('get_apply_forms',ApplyFormsAPIView.as_view()),
    path('get_school_by_id',SchoolDetailsIDAPIView.as_view()),
    path('get_list_country_apply',CountryGetListForApplySchool.as_view()),
    path('get_list_skill_match',SchoolListSkillMatch.as_view()),

]

urlpatterns = urlpatterns + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
