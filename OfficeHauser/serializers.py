from rest_framework import serializers

from PublicPages.serializers import ServiceCategoryDetailsSerializer
from Raw_root.serializers import ExpertiseCatSerializer, EventCatTourpageConfigListSerializer
from Services.models import ServiceCategory
from Visa.models import VisaStream
from django.contrib.auth.password_validation import validate_password
from rest_framework import serializers, status
from rest_framework.response import Response
from rest_framework_simplejwt.tokens import RefreshToken, TokenError
from django.contrib.auth import authenticate
from rest_framework import exceptions, serializers
from .models import OfficeUser, Role
from .permissions import IsHeadAgent, IsAnonymousForPost
from .models import *
from Raw_root.views import RecoveryQuestionsSerializer
import random
import string

from officeha.settings import EMAIL_HOST_USER, EMAIL_VERIFY_URL, EMAIL_LOGIN_URL
from django.core.mail import send_mail
from django.template.loader import render_to_string


class VisaStreamBriefSerializer(serializers.ModelSerializer):

    class Meta:
        model = VisaStream
        fields = ['id','title_latin','title_farsi','icon']



class OfficeUserDetailsSerializer(serializers.ModelSerializer):

    class Meta:
        model = OfficeUser
        fields = ['first_name','last_name','avatar']
    #   from date to date date of birth Service cat id yes/no IP Device type OTP 6 digits


class OfficeUserSerializer(serializers.ModelSerializer):

    class Meta:
        model = OfficeUser
        fields = ['id','first_name','last_name','username', 'password','email','cell_number','nid']
    #   from date to date date of birth Service cat id yes/no IP Device type OTP 6 digits

class OfficeUserUsernameSerializer(serializers.ModelSerializer):

    class Meta:
        model = OfficeUser
        fields = ['username']


class OfficeUserLinkDetailsSerializer(serializers.ModelSerializer):

    class Meta:
        model = OfficeUser
        fields = ['id','username','country','province','instagram_1'
            ,'instagram_2','website_url','linkedin','username',]

class OfficeUserBriefSerializer(serializers.ModelSerializer):

    class Meta:
        model = OfficeUser
        fields = ['id','avatar','username','position','first_name',
                  'last_name',
                  'username',
                  'role',
                  'last_login']
    #   from date to date date of birth Service cat id yes/no IP Device type OTP 6 digits


class BusinessProfileBriefSerializer(serializers.ModelSerializer):
    holder_user = OfficeUserBriefSerializer()
    expertise_tour_type_1 = ExpertiseCatSerializer()
    expertise_tour_type_2 = ExpertiseCatSerializer()

    class Meta:
        model = BusinessProfile
        fields = ['id','business_type','business_name','expertise_tour_type_1','expertise_tour_type_2','holder_user']



class StaffUserBriefSerializer(serializers.ModelSerializer):
    event_cat = EventCatTourpageConfigListSerializer(many=True)
    service_cat = ServiceCategoryDetailsSerializer(many=True)
    stid = VisaStreamBriefSerializer(many=True)

    class Meta:
        model = StaffUser
        fields = ['id','username','avatar','position','event_cat','service_cat','stid']



class UserSubscribeSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserSubscribe
        fields = '__all__'
    







































class RoleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Role
        fields = ['id', 'name']

class OfficeUserInfoSerializer(serializers.ModelSerializer):
    permission_classes = [IsHeadAgent]
    role = RoleSerializer()
    class Meta:
        model = OfficeUser
        fields = ['id', 'username', 'first_name', 'last_name', 'role', 'last_login']



class OfficeUserForPostSerializer(serializers.ModelSerializer):
    permission_classes = [IsHeadAgent]
    password = serializers.CharField(
        write_only=True,
    )

    class Meta:
        model = OfficeUser
        fields = ('password', 'username', 'first_name', 'last_name', 'role')


class OfficeUserSerializer(serializers.ModelSerializer):
    permission_classes = [IsHeadAgent]
    role = RoleSerializer()
    password = serializers.CharField(
        write_only=True,
    )

    class Meta:
        model = OfficeUser
        fields = ('password', 'username', 'first_name', 'last_name', 'role', 'last_login')

    def create(self, validated_data):
        validated_data["is_staff"] = True
        user = super(OfficeUserSerializer, self).create(validated_data)
        if 'password' in validated_data:
            user.set_password(validated_data['password'])
            user.save()
        return user


class OfficeUserForCreateUserSerializer(serializers.ModelSerializer):
    permission_classes = [IsAnonymousForPost]
   

    password = serializers.CharField(write_only=True, validators=[validate_password], required=False, allow_blank=True)
    password_confirm = serializers.CharField(write_only=True, required=False, allow_blank=True)
    activation_code = serializers.CharField(read_only=True)

    class Meta:
        model = OfficeUser
        fields = ('password', 'password_confirm', 'username','email', 'first_name', 'last_name', 'role','recovery_question','recovery_answer','dob','activation_code')

    def validate(self, attrs):
        if 'password' in attrs:
            if attrs['password'] == '':
                return attrs
            if attrs['password'] != attrs['password_confirm']:
                raise serializers.ValidationError({"password": "Password fields didn't match."})
            attrs.pop('password_confirm')
            return attrs
        else:
            return attrs

    def create(self, instance):
        password = instance.pop('password')
        instance = self.Meta.model(**instance)
        if password is not None:
            instance.set_password(password)
        instance.activation_code = ''.join(random.choices(string.ascii_uppercase + string.digits, k=20)) 
        instance.is_active =False
        instance.username=instance.email
        instance.save()

        msg_html = EMAIL_VERIFY_URL+"?activation_code="+instance.activation_code 
        
        dd= send_mail(subject="Timank.ir : Account Verification",
            from_email=EMAIL_HOST_USER,
            message=msg_html,
            recipient_list=[instance.email],
            html_message=msg_html,
            fail_silently=False)
        
        return instance

class ChangePasswordSerializer(serializers.ModelSerializer):
    password = serializers.CharField(write_only=True, required=True, validators=[validate_password])
    password2 = serializers.CharField(write_only=True, required=True)
    old_password = serializers.CharField(write_only=True, required=True)

    class Meta:
        model = OfficeUser
        fields = ('old_password', 'password', 'password2')

    def validate(self, attrs):
        if attrs['password'] != attrs['password2']:
            raise serializers.ValidationError({"password": "Password fields didn't match."})

        return attrs
    def update(self, instance, validated_data):
        user = self.context['request'].user

        if user.pk != instance.pk:
            raise serializers.ValidationError({"authorize": "You dont have permission for this user."})

        instance.set_password(validated_data['password'])
        instance.save()

        return instance
    def validate_old_password(self, value):
        user = self.context['request'].user
        if not user.check_password(value):
            raise serializers.ValidationError({"old_password": "Old password is not correct"})
        return value


class OfficeUserBriefForUpdateSerializer(serializers.ModelSerializer):
    class Meta:
        model = OfficeUser
        fields = [
            'first_name',
            'last_name',
            'username',
        ]


# class OfficeUserBriefSerializer(serializers.ModelSerializer):
#     role = RoleSerializer()

#     class Meta:
#         model = OfficeUser
#         fields = ['id',
#                   'first_name',
#                   'last_name',
#                   'username',
#                   'role',
#                   'last_login'
#                   ]


class OfficeUserRoleSerializer(serializers.ModelSerializer):
    role = RoleSerializer()

    class Meta:
        model = OfficeUser
        fields = [
            "role",
        ]

    def create(self, validated_data):
        roles = validated_data.pop('role')
        user = self.context['request'].user
        role_model = Role.objects.create(**roles)
        user.role = role_model
        user.save()
        return user



# class LogoutSerializer(serializers.Serializer[OfficeUser]):
#     refresh = serializers.CharField()

#     def validate(self, attrs):  # type: ignore
#         """Validate token."""
#         self.token = attrs['refresh']
#         return attrs

#     def save(self, **kwargs):  # type: ignore
#         """Validate save backlisted token."""
#         try:
#             RefreshToken(self.token).blacklist()

#         except TokenError as ex:
#             raise exceptions.AuthenticationFailed(ex)


class OfficeUserNameSerializer(serializers.ModelSerializer):

    class Meta:
        model = OfficeUser
        fields = [
            'first_name',
            'last_name',

        ]

class OfficeUserBriefForUpdateSerializer(serializers.ModelSerializer):
    class Meta:
        model = OfficeUser
        fields = [
            'first_name',
            'last_name',
            'username',
        ]
