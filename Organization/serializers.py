from rest_framework import serializers

from Services.models import ServiceCategory
from Tour.models import Tour
from Visa.models import VisaStream
from .models import *
from Raw_root.serializers import CurrencySerializer

class DeclarationFormsSerializer(serializers.ModelSerializer):
    class Meta:
        model = DeclarationForms
        fields = '__all__'

class ContractProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = ContractProfile
        fields = '__all__'


class GuaranteeAmountSerializer(serializers.ModelSerializer):
    class Meta:
        model = GuaranteeAmount
        fields = '__all__'

class AccountNumberSerializer(serializers.ModelSerializer):
    class Meta:
        model = AccountNumber
        fields = '__all__'


class ExRateSerializer(serializers.ModelSerializer):
    currency_1 = CurrencySerializer()
    currency_2 = CurrencySerializer()
    currency_3 = CurrencySerializer()
    currency_4 = CurrencySerializer()
    currency_5 = CurrencySerializer()

    class Meta:
        model = ExRate
        fields = ['id','date_time','currency_1','x_amount_in_irr_1','currency_2','x_amount_in_irr_2','currency_3','x_amount_in_irr_3','currency_4','x_amount_in_irr_4','currency_4','x_amount_in_irr_5','currency_5']

class ServiceCategoryDeclarationAllSerializer(serializers.ModelSerializer):
    declaration_forms = DeclarationFormsSerializer()
    class Meta:
        model = ServiceCategory
        fields = ['id','declaration_forms']

class VisaStreamDeclarationAllSerializer(serializers.ModelSerializer):
    declaration_forms = DeclarationFormsSerializer()
    class Meta:
        model = VisaStream
        fields = ['id','declaration_forms']

class TourDeclarationAllSerializer(serializers.ModelSerializer):
    declaration_forms = DeclarationFormsSerializer()
    class Meta:
        model = Tour
        fields = ['id','declaration_forms']

class DeclarationQuestionSerializer(serializers.ModelSerializer):
    # decleration_form = DeclarationFormsSerializer()
    service_category = ServiceCategoryDeclarationAllSerializer()
    stid = VisaStreamDeclarationAllSerializer()
    tour = TourDeclarationAllSerializer()
    class Meta:
        model = DeclarationQuestion
        fields = '__all__'
