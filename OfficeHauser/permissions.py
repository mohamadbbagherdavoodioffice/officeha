from rest_framework.permissions import BasePermission, SAFE_METHODS
from rest_framework import permissions
from .models import OfficeUser

class IsStaff(permissions.BasePermission):

    def has_permission(self, request, view):
        if request.user.is_staff:
            return True
        return False

    def has_object_permission(self, request, view, obj):
        if request.user.is_staff:
            return True
        return False


class IsOwner(permissions.BasePermission):

    def has_permission(self, request, view):
        if request.user.is_authenticated:
            return True
        return False

    def has_object_permission(self, request, view, obj):
        if obj.author == request.user:
            return True
        return False


class IsHeadAgent(BasePermission):
    message = 'Only head agent can access this page'

    def has_permission(self, request, view):
        if request.user.is_authenticated:
            qs = OfficeUser.objects.filter(id=request.user.id)
            ip_addr = request.META['REMOTE_ADDR']
            blacklisted = not OfficeUser.objects.filter(ip_addr=ip_addr).exists()
            return qs.exists() and qs.first().is_head and blacklisted
        return False

class IsAnonymousForPost(BasePermission):
    def has_permission(self, request, view):
        # ip_addr = request.META['REMOTE_ADDR']
        # blacklisted = not Blacklist.objects.filter(ip_addr=ip_addr).exists()
        # return not request.user.is_authenticated and request.method == "POST" and blacklisted
        return True

class IsMy(permissions.BasePermission):

    def has_permission(self, request, view):
        if request.user.is_authenticated:
            return True
        return False

    def has_object_permission(self, request, view, obj):
        if obj == request.user:
            return True
        return False

class IsAdmin(permissions.BasePermission):
    def has_permission(self, request, view):
        try:
            if str(request.user.role.name) == "admin":
                return True
            return False
        except:
            return False


class IsVisitor(permissions.BasePermission):

    def has_permission(self, request, view):
        try:
            if str(request.user.role.name) == "visitor":
                return True
            return False
        except:
            return False

