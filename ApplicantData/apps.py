from django.apps import AppConfig

class ApplicantDataConfig(AppConfig):
    name = 'ApplicantData'
    verbose_name = 'Applicant Data'
