# Generated by Django 3.2.18 on 2024-01-14 13:56

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Content', '0005_specificfaq_approval'),
    ]

    operations = [
        migrations.AddField(
            model_name='generalfaq',
            name='approval',
            field=models.BooleanField(default=False),
        ),
    ]
