from django.db import models

class ApplicationResidencyTravelHistory(models.Model):

    TYPE = (
        ('Travel history', 'Travel history'),
        ('Residency history', 'Residency history'),
        ('Travel Doc', 'Travel Doc'),
    )
    applicant = models.ForeignKey('ApplicationOperation.Applicant', on_delete=models.CASCADE, null=True, blank=True,)
    country = models.ForeignKey("Raw_root.Country", on_delete=models.CASCADE, related_name="app_residency_travel_hist_country_set",
                                null=True, blank=True, )
    type = models.CharField(choices=TYPE,max_length=20,null=True,blank=True)
    year = models.PositiveIntegerField(null=True,blank=True)
    residency_status = models.ForeignKey('Raw_root.ResidencyStatus', on_delete=models.CASCADE,null=True, blank=True,)
    from_date = models.DateTimeField(null=True,blank=True)
    to_date = models.DateTimeField(null=True,blank=True)
    travel_doc_type = models.ForeignKey('Raw_root.TravelDocType', on_delete=models.CASCADE, null=True, blank=True,)
    expiry_date_of_travel_doc = models.DateTimeField(null=True,blank=True)
    travel_doc_number = models.CharField(choices=(('1','1'),('2','2'),('3','3')),max_length=20,null=True,blank=True)

class AppPreferredAppointment(models.Model):
    # applicant = models.ForeignKey('ApplicationOperation.Applicant', on_delete=models.CASCADE, null=True, blank=True,)
    applicant_profile = models.ForeignKey('ApplicationOperation.ApplicationProfile', on_delete=models.CASCADE,
                                           null=True, blank=True)
    from_date = models.DateTimeField(null=True, blank=True)
    to_date = models.DateTimeField(null=True, blank=True)
    day_of_work = models.PositiveIntegerField(null=True, blank=True)

def get_file_sponsor_invitation_file(instance,filename):
    return f"file_sponsor_invitation_file/{filename}"

def get_file_sponsor_sponsor_id(instance,filename):
    return f"file_sponsor_sponsor_id/{filename}"

def get_file_sponsor_sponsor_bank_statement_file(instance,filename):
    return f"file_sponsor_sponsor_bank_statement_file/{filename}"


def get_file_sponsor_tax_return(instance,filename):
    return f"file_sponsor_tax_return/{filename}"

class AppSponsor(models.Model):
    applicant = models.ForeignKey('ApplicationOperation.Applicant', on_delete=models.CASCADE, null=True, blank=True,)
    f_name = models.CharField(max_length=300, null=True, blank=True)
    l_name = models.CharField(max_length=300, null=True, blank=True)
    relation = models.ForeignKey('Raw_root.Relationship', on_delete=models.CASCADE,null=True, blank=True,)
    why_they_pay_for_you = models.TextField( null=True, blank=True)
    tel = models.CharField(max_length=300, null=True, blank=True)
    email = models.EmailField(null=True,blank=True)
    invitation_file = models.FileField(upload_to=get_file_sponsor_invitation_file,null=True,blank=True)
    residency_status = models.ForeignKey('Raw_root.ResidencyStatus', on_delete=models.CASCADE,null=True, blank=True,)
    sponsor_id = models.ImageField(upload_to=get_file_sponsor_sponsor_id,null=True,blank=True)
    sponsor_tax_return = models.FileField(upload_to=get_file_sponsor_tax_return,null=True,blank=True)
    sponsor_bank_statement_file = models.ImageField(upload_to=get_file_sponsor_sponsor_bank_statement_file,null=True,blank=True)
    company_name = models.CharField(max_length=300,null=True,blank=True)

class AppEduSocialBackground(models.Model):
    applicant = models.ForeignKey('ApplicationOperation.Applicant', on_delete=models.CASCADE, null=True, blank=True,)
    from_date = models.DateTimeField(null=True, blank=True)
    to_date = models.DateTimeField(null=True, blank=True)
    day_of_work = models.PositiveIntegerField(null=True, blank=True)
    TYPE = (('Edu Back', 'Edu Back'),
        ('Social Exp', 'Social Exp'),)
    type = models.CharField(choices=TYPE,max_length=20,null=True,blank=True)
    from_date = models.DateTimeField(null=True, blank=True)
    to_date = models.DateTimeField(null=True, blank=True)
    where = models.CharField(max_length=300, null=True, blank=True)
    degree = models.ForeignKey("Raw_root.Degree", on_delete=models.CASCADE, null=True, blank=True, )

class AppCertificate(models.Model):
    applicant = models.ForeignKey('ApplicationOperation.Applicant', on_delete=models.CASCADE, null=True, blank=True,)
    title = models.CharField(max_length=300,null=True,blank=True)
    school_name = models.CharField(max_length=300,null=True,blank=True)
    date_of_issue = models.DateField(null=True, blank=True)
    duration = models.PositiveIntegerField(null=True, blank=True)

class AppPaper(models.Model):
    applicant = models.ForeignKey('ApplicationOperation.Applicant', on_delete=models.CASCADE, null=True, blank=True,)
    title = models.CharField(max_length=300, null=True, blank=True)
    journal_name = models.CharField(max_length=400, null=True, blank=True)
    date_of_issue = models.DateField(null=True, blank=True)
    duration = models.PositiveIntegerField(null=True, blank=True)
    indexed = models.BooleanField(default=False)
    issn = models.CharField(max_length=400, null=True, blank=True)
    date = models.DateField(null=True, blank=True)
    doi = models.CharField(max_length=200, null=True, blank=True)
    TYPE = (
        ('Full - Length', 'Full - Length'),
        ('Communication', 'Communication'),
        ('Technical', 'Technical'),
        ('Note', 'Note'),
        ('Data bank', 'Data bank'),
        ('Viewpoint', 'Viewpoint'),
        ('Review', 'Review'),
        ('Letter', 'Letter'),
        )
    type = models.CharField(choices=TYPE,max_length=20,null=True,blank=True)

class APPFamilyInfo(models.Model):
    TYPE = (
        ('Home', 'Home'),
        ('In destination', 'In destination'),
    )
    applicant = models.ForeignKey('ApplicationOperation.Applicant', on_delete=models.CASCADE, null=True, blank=True,)
    relation = models.ForeignKey('Raw_root.Relationship', on_delete=models.CASCADE,null=True, blank=True,)
    first_name = models.CharField(max_length=80, null=True, blank=True, )
    last_name = models.CharField(max_length=80, null=True, blank=True, )
    type = models.CharField(max_length=30, choices=TYPE, null=True, blank=True, )
    country_id_of_birth = models.ForeignKey('Raw_root.Country', on_delete=models.CASCADE,null=True, blank=True,)
    occupation = models.ForeignKey('Raw_root.Occupation', on_delete=models.CASCADE,null=True, blank=True,)
    dob = models.DateField(null=True, blank=True)
    present_address = models.CharField(max_length=300, null=True, blank=True,)
    will_accompany = models.BooleanField(default=False)
    residency_status = models.ForeignKey('Raw_root.ResidencyStatus', on_delete=models.CASCADE,null=True, blank=True,)

class AppAnsweredDeclaration(models.Model):
    application_number = models.ForeignKey('ApplicationOperation.ApplicationProfile', on_delete=models.CASCADE, null=True, blank=True)
    declaration_form = models.ForeignKey('Organization.DeclarationForms', on_delete=models.CASCADE,null=True, blank=True)
    question_seq = models.PositiveIntegerField(null=True, blank=True)
    answer = models.BooleanField(default=False)
    note = models.TextField(null=True, blank=True)

    def __str__(self):
        return "%s | %s" % (self.applicant, self.answer)

class AppSocialMedia(models.Model):
    applicant = models.ForeignKey('ApplicationOperation.Applicant', on_delete=models.CASCADE, null=True, blank=True)
    user_name = models.CharField(max_length=300, null=True, blank=True,)
    media_id = models.CharField(max_length=300, null=True, blank=True,)
    def __str__(self):
        return "%s | %s" % (self.user_name, self.media_id)

def get_reference_recommendation(instance,filename):
    return f"file_reference_recommendation/{filename}"

class AppReference(models.Model):
    applicant = models.ForeignKey('ApplicationOperation.Applicant', on_delete=models.CASCADE, null=True, blank=True)
    rank = models.CharField(max_length=100, null=True, blank=True,)
    referee_org_name = models.CharField(max_length=100, null=True, blank=True,)
    referee_first_name = models.CharField(max_length=50, null=True, blank=True,)
    last_name = models.CharField(max_length=50, null=True, blank=True,)
    tel = models.CharField(max_length=40, null=True, blank=True,)
    email = models.EmailField(null=True, blank=True)
    ref_type = models.CharField(max_length=40, choices=(('ACD','ACD'),('WRK','WRK')), null=True, blank=True,)
    file = models.FileField(upload_to=get_reference_recommendation, null=True, blank=True,)
    def __str__(self):
        return "%s | %s" % (self.applicant, self.rank)

class AppWorkHistory(models.Model):
    applicant = models.ForeignKey('ApplicationOperation.Applicant', on_delete=models.CASCADE, null=True, blank=True,default=None)
    type = models.CharField(max_length=40, choices=(('Work', 'Work'), ('Internship', 'Internship')))
    from_date = models.DateField (null=True,blank=True)
    to_date = models.DateField (null=True,blank=True)
    co_name = models.CharField(max_length=50,null=True,blank=True)
    website = models.CharField(max_length=100,null=True,blank=True)
    tel = models.CharField(max_length=40,null=True,blank=True)
    email = models.EmailField(null=True,blank=True)
    content_person_name = models.CharField(max_length=50,null=True,blank=True)
    position_latin = models.TextField(null=True,blank=True)
    position_farsi = models.TextField(null=True,blank=True)

    def __str__(self):
        return "%s | %s" % (self.applicant, self.position_latin )

class AppVolunteerJobBackground(models.Model):
    applicant = models.ForeignKey('ApplicationOperation.Applicant', on_delete=models.CASCADE, null=True, blank=True)
    type = models.CharField(max_length=40, choices=(
    ('Volunteer', 'Volunteer'), ('Job', 'Job'),), null=True, blank=True)
    title = models.CharField(max_length=200, null=True, blank=True,)
    organization_name = models.CharField(max_length=200, null=True, blank=True,)
    duty_latin = models.TextField(null=True,blank=True)
    duty_farsi = models.TextField(null=True,blank=True)

    def __str__(self):
        return self.title

class AppFinancialData(models.Model):
    applicant = models.ForeignKey('ApplicationOperation.Applicant', on_delete=models.CASCADE, null=True, blank=True)
    salary_in_year = models.DecimalField(max_digits=13, decimal_places=2, null=True, blank=True,)
    regular_monthly = models.DecimalField(max_digits=13, decimal_places=2, null=True, blank=True,)
    amount_in_saving = models.DecimalField(max_digits=13, decimal_places=2, null=True, blank=True,)
    amount_spend_per_month = models.DecimalField(max_digits=13, decimal_places=2, null=True, blank=True,)
    amount_planning_to_spend = models.DecimalField(max_digits=13, decimal_places=2, null=True, blank=True,)
    income_amount = models.DecimalField(max_digits=13, decimal_places=2, null=True, blank=True,)

    def __str__(self):
        return "%s | %s" % (self.applicant, self.salary_in_year)

def get_where_to_stay(instance,filename):
    return f"file_where_to_stay/{filename}"

class WhereToStay(models.Model):
    applicant = models.ForeignKey('ApplicationOperation.Applicant', on_delete=models.CASCADE, null=True, blank=True)
    hotel_name = models.CharField(max_length=100, null=True, blank=True,)
    invitation_file = models.FileField(upload_to=get_where_to_stay, null=True, blank=True,)
    from_date = models.DateField(null=True, blank=True)
    to_date = models.DateField(null=True, blank=True)
    reservation_code = models.CharField(max_length=200, null=True, blank=True, )
    def __str__(self):
        return "%s | %s" % (self.applicant, self.hotel_name)

def get_immi_problem_history(instance,filename):
    return f"file_immi_problem_history/{filename}"

class AppImmiProblemHistory(models.Model):
    applicant = models.ForeignKey('ApplicationOperation.Applicant', on_delete=models.CASCADE, null=True, blank=True)
    country = models.ForeignKey('Raw_root.Country', on_delete=models.CASCADE, related_name='app_raw_root_country_set_immi_set',null=True,blank=True)
    date = models.DateField (null=True,blank=True)
    desc = models.TextField(null=True,blank=True)
    file = models.FileField(upload_to=get_immi_problem_history)

    def __str__(self):
        return "%s | %s" % (self.applicant, self.country)