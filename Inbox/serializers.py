from rest_framework import serializers
from rest_framework.relations import PrimaryKeyRelatedField

from .models import *
from Edu.models import Program

from Raw_root.models import AgeRange
import random

class ContactUsSerializer(serializers.ModelSerializer):
    digital_code = serializers.SerializerMethodField()

    class Meta:
        model = ContactUs
        fields = ['date_time', 'contact_us_type', 'name', 'tel', 'email', 'note', 'star_rate', 'digital_code']

    def get_digital_code(self, obj):
        # Generate a random 10-digit digital code
        return ''.join(random.choices('0123456789', k=10))


class TellUsWhatYouThinkSerializer(serializers.ModelSerializer):
    class Meta:
        model = TellUsWhatYouThink
        fields = '__all__'

class FeedbackSerializer(serializers.ModelSerializer):
    class Meta:
        model = Feedback
        fields = ['date_time','name','tel','email','note']






class EduApplyRequestSerializer(serializers.ModelSerializer):
    class Meta:
        model = EduApplyRequest
        fields =['date_time','program',  'full_name',  'age_range', 'degree', 'gpa',
        'edu_subject',  'available_fund',  'province', 'tel',  'email']


class JobSeekerCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = JobSeeker
        fields = ['job_code','first_name','last_name','region','cell_number', 'upload_id','upload_cv','upload_app_form','skills']


class EvaluationFormSerializer(serializers.ModelSerializer):
    class Meta:
        model = EvaluationForm
        exclude = ['followup_status', 'staff', 'staff_note']
        

class TourRequestSerializer(serializers.ModelSerializer):
    class Meta:
        model = TourRequest
        fields = '__all__'
        
class WorkshopsParticipantSerializer(serializers.ModelSerializer):
    class Meta:
        model = WorkshopsParticipant
        fields = [
            'workshop',
            'status',
            'mode_of_payment',
            'first_name',
            'last_name',
            'cell_number',
            'email_address',
            'pos_bank_id',
            'debit_card_number',
            'payment_date_time',
            'account_number',
            'trace_code',
            'bank_receipt_file',
        ]
        read_only_fields = ['payment_date_time']
