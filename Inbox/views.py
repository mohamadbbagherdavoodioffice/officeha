from django.shortcuts import render
import random
from django.http import JsonResponse, HttpResponse
from django.contrib.auth.hashers import check_password
from django.db.models import Q
from rest_framework import status, viewsets, pagination, generics
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet
from rest_framework.generics import CreateAPIView, get_object_or_404, ListAPIView, RetrieveAPIView, ListCreateAPIView

from Hr.models import HrSkill
from .models import *
from .serializers import *
from Raw_root.models import RespondNote, Country, Region


# class ContactUsAPIView(generics.CreateAPIView):
#     queryset = ContactUs.objects.all()
#     serializer_class = ContactUsSerializer
#
#     def create(self, request, *args, **kwargs):
#         response = super().create(request, *args, **kwargs)
#
#         respond_success = RespondNote.objects.filter(category='Contact Us').values('successful_title_latin',
#                                                                                    'successful_title_farsi',
#                                                                                    'successful_text_latin',
#                                                                                    'successful_text_farsi',
#                                                                                    'successful_signature_latin',
#                                                                                    'successful_signature_farsi')
#         respond_fail = RespondNote.objects.filter(category='Contact Us').values('failed_title_latin',
#                                                                                 'failed_title_farsi',
#                                                                                 'failed_text_latin',
#                                                                                 'failed_text_farsi',
#                                                                                 'failed_signature_latin',
#                                                                                 'failed_signature_farsi')
#         if response.status_code == 201:
#             return Response({
#                 'status': 201,
#                 'message': 'success',
#                 'respond': respond_success,
#                 'data': response.data
#             })
#         else:
#             return Response({
#                 'status': 404,
#                 'message': 'fail',
#                 'respond': respond_fail,
#                 'data': response.data
#             })


class ContactUsAPIView(generics.CreateAPIView):
    queryset = ContactUs.objects.all()
    serializer_class = ContactUsSerializer

    def create(self, request, *args, **kwargs):
        response = super().create(request, *args, **kwargs)
        respond_success = RespondNote.objects.filter(category='Contact Us/Create').values('successful_title_latin',
                                                                                          'successful_title_farsi',
                                                                                          'successful_text_latin',
                                                                                          'successful_text_farsi',
                                                                                          'successful_signature_latin',
                                                                                          'successful_signature_farsi')
        respond_fail = RespondNote.objects.filter(category='Contact Us/Create').values('failed_title_latin',
                                                                                       'failed_title_farsi',
                                                                                       'failed_text_latin',
                                                                                       'failed_text_farsi',
                                                                                       'failed_signature_latin',
                                                                                       'failed_signature_farsi')
        if response.status_code == 201:
            return Response({
                'status': 201,
                'message': 'success',
                'respond': respond_success,
                'data': response.data
            })
        else:
            return Response({
                'status': 404,
                'message': 'fail',
                'respond': respond_fail,
                'data': response.data
            })


class TellUsWhatYouThinkAPIView(generics.CreateAPIView):
    queryset = TellUsWhatYouThink.objects.all()
    serializer_class = TellUsWhatYouThinkSerializer

    def create(self, request, *args, **kwargs):
        response = super().create(request, *args, **kwargs)
        respond_success = RespondNote.objects.filter(category='Tell Us/Create').values('successful_title_latin',
                                                                                       'successful_title_farsi',
                                                                                       'successful_text_latin',
                                                                                       'successful_text_farsi',
                                                                                       'successful_signature_latin',
                                                                                       'successful_signature_farsi')
        respond_fail = RespondNote.objects.filter(category='Tell Us/Create').values('failed_title_latin',
                                                                                    'failed_title_farsi',
                                                                                    'failed_text_latin',
                                                                                    'failed_text_farsi',
                                                                                    'failed_signature_latin',
                                                                                    'failed_signature_farsi')
        if response.status_code == 201:
            return Response({
                'status': 201,
                'message': 'success',
                'respond': respond_success,
                'data': response.data
            })
        else:
            return Response({
                'status': 404,
                'message': 'fail',
                'respond': respond_fail,
                'data': response.data
            })


class FeedbackCreateAPIView(generics.CreateAPIView):
    queryset = Feedback.objects.all()
    serializer_class = FeedbackSerializer

    def create(self, request, *args, **kwargs):
        response = super().create(request, *args, **kwargs)
        respond_success = RespondNote.objects.filter(category='Feedback/Create').values('successful_title_latin',
                                                                                        'successful_title_farsi',
                                                                                        'successful_text_latin',
                                                                                        'successful_text_farsi',
                                                                                        'successful_signature_latin',
                                                                                        'successful_signature_farsi')
        respond_fail = RespondNote.objects.filter(category='Feedback/Create').values('failed_title_latin',
                                                                                     'failed_title_farsi',
                                                                                     'failed_text_latin',
                                                                                     'failed_text_farsi',
                                                                                     'failed_signature_latin',
                                                                                     'failed_signature_farsi')
        if response.status_code == 201:
            return Response({
                'status': 201,
                'message': 'success',
                'respond': respond_success,
                'data': response.data
            })
        else:
            return Response({
                'status': 404,
                'message': 'fail',
                'respond': respond_fail,
                'data': response.data
            })


class dataAPIView(APIView):
    def get(self, request):
        from fpdf import FPDF
        pdf = FPDF()
        pdf.add_page()
        pdf.set_font("Arial", size=12)
        pdf.cell(300, 10, txt="Welcome to Timnak Office !", ln=1, align="C")
        pdf.line(420, 420, 40, 100)
        pdf.set_line_width(2)
        pdf.set_draw_color(255, 0, 0)
        pdf.line(20, 20, 100, 20)
        pdf.image('https://www.google.com/images/branding/googlelogo/1x/googlelogo_color_272x92dp.png', x=10, y=8,
                  w=100)
        pdf.output("media/simple_demo.pdf")
        return Response({"message": "success", "status": "OK", "data": ""}, status=200)


class EduApplyRequestCreateAPIView(generics.CreateAPIView):
    queryset = EduApplyRequest.objects.all()
    serializer_class = EduApplyRequestSerializer

    def create(self, request, *args, **kwargs):
        response = super().create(request, *args, **kwargs)
        respond_success = RespondNote.objects.filter(category='Edu Apply/Create').values('successful_title_latin',
                                                                                         'successful_title_farsi',
                                                                                         'successful_text_latin',
                                                                                         'successful_text_farsi',
                                                                                         'successful_signature_latin',
                                                                                         'successful_signature_farsi')
        respond_fail = RespondNote.objects.filter(category='Edu Apply/Create').values('failed_title_latin',
                                                                                      'failed_title_farsi',
                                                                                      'failed_text_latin',
                                                                                      'failed_text_farsi',
                                                                                      'failed_signature_latin',
                                                                                      'failed_signature_farsi')
        if response.status_code == 201:
            return Response({
                'status': 201,
                'message': 'success',
                'respond': respond_success,
                'data': response.data
            })
        else:
            return Response({
                'status': 404,
                'message': 'fail',
                'respond': respond_fail,
                'data': response.data
            })


class JobApplicationFormStaticAPIView(APIView):
    def get(self, request):
        dic = {}

        country = Region.objects.filter(country__name_latin="IRAN").all().values().values('id','title_latin','title_farsi')
        dic["country"] = list(country)

        hr_skill = HrSkill.objects.all().values('skill_latin', 'skill_farsi', 'id')
        dic["hr_skill"] = list(hr_skill)

        return Response({"message": "success", "status": "OK", "data": dic}, status=200)


class JobSeekerCreateAPIView(generics.CreateAPIView):
    queryset = JobSeeker.objects.all()
    serializer_class = JobSeekerCreateSerializer
    def create(self, request, *args, **kwargs):
        response = super().create(request, *args, **kwargs)

        respond_success = RespondNote.objects.filter(category='Job application').values(
            'successful_title_latin',
            'successful_title_farsi',
            'successful_text_latin',
            'successful_text_farsi',
            'successful_signature_latin',
            'successful_signature_farsi')
        respond_fail = RespondNote.objects.filter(category='Job application').values('failed_title_latin',
                                                                                             'failed_title_farsi',
                                                                                             'failed_text_latin',
                                                                                             'failed_text_farsi',
                                                                                             'failed_signature_latin',
                                                                                             'failed_signature_farsi')
        if response.status_code == 201:
            return Response({
                'status': 201,
                'message': 'success',
                'respond': respond_success,
                'data': response.data
            })
        else:
            return Response({
                'status': 404,
                'message': 'fail',
                'respond': respond_fail,
                'data': response.data
            })


class EvaluationFormAPIView(generics.CreateAPIView):
    queryset = EvaluationForm.objects.all()
    serializer_class = EvaluationFormSerializer

    def create(self, request, *args, **kwargs):
        response = super().create(request, *args, **kwargs)
        print(response)
        response.data = {"trace_code":response.data['trace_code']}
        # respond_success['trace_code']=
        respond_success = RespondNote.objects.filter(category='Evaluation').values(
            'successful_title_latin',
            'successful_title_farsi',
            'successful_text_latin',
            'successful_text_farsi',
            'successful_signature_latin',
            'successful_signature_farsi')
        respond_fail = RespondNote.objects.filter(category='Evaluation').values('failed_title_latin',
                                                                                             'failed_title_farsi',
                                                                                             'failed_text_latin',
                                                                                             'failed_text_farsi',
                                                                                             'failed_signature_latin',
                                                                                             'failed_signature_farsi')
        if response.status_code == 201:
            return Response({
                'status': 201,
                'message': 'success',
                'respond': respond_success,
                'data': response.data
            })
        else:
            return Response({
                'status': 404,
                'message': 'fail',
                'respond': respond_fail,
                'data': response.data
            })


class TourRequestAPIView(generics.CreateAPIView):
    queryset = TourRequest.objects.all()
    serializer_class = TourRequestSerializer

class CreateWorkshopParticipantView(generics.CreateAPIView):
    queryset = WorkshopsParticipant.objects.all()
    serializer_class = WorkshopsParticipantSerializer

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        return Response(serializer.data, status=status.HTTP_201_CREATED)