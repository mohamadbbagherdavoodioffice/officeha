# Generated by Django 3.2.18 on 2024-07-16 14:44

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('HomePageConfig', '0002_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='ImmigrationSectionConfig',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('active', models.IntegerField(default=0)),
                ('main_title_latin', models.CharField(blank=True, max_length=300, null=True)),
                ('main_title_farsi', models.CharField(blank=True, max_length=300, null=True)),
                ('evaluation_title_latin', models.CharField(blank=True, max_length=300, null=True)),
                ('evaluation_title_farsi', models.CharField(blank=True, max_length=300, null=True)),
                ('evaluation_desc_latin', models.CharField(blank=True, max_length=300, null=True)),
                ('evaluation_desc_farsi', models.CharField(blank=True, max_length=300, null=True)),
                ('consultation_title_latin', models.CharField(blank=True, max_length=300, null=True)),
                ('consultation_title_farsi', models.CharField(blank=True, max_length=300, null=True)),
                ('consultation_desc_latin', models.CharField(blank=True, max_length=300, null=True)),
                ('consultation_desc_farsi', models.CharField(blank=True, max_length=300, null=True)),
                ('case_number_title_latin', models.CharField(blank=True, max_length=300, null=True)),
                ('case_number_title_farsi', models.CharField(blank=True, max_length=300, null=True)),
                ('case_number_text_latin', models.TextField(blank=True, max_length=1000, null=True)),
                ('case_number_text_farsi', models.TextField(blank=True, max_length=1000, null=True)),
                ('support_title_latin', models.CharField(blank=True, max_length=300, null=True)),
                ('support_title_farsi', models.CharField(blank=True, max_length=300, null=True)),
                ('support_text_latin', models.TextField(blank=True, max_length=1000, null=True)),
                ('support_text_farsi', models.TextField(blank=True, max_length=1000, null=True)),
                ('post_immi_title_latin', models.CharField(blank=True, max_length=300, null=True)),
                ('post_immi_title_farsi', models.CharField(blank=True, max_length=300, null=True)),
                ('post_immi_text_latin', models.TextField(blank=True, max_length=1000, null=True)),
                ('post_immi_text_farsi', models.TextField(blank=True, max_length=1000, null=True)),
                ('school_title_latin', models.CharField(blank=True, max_length=300, null=True)),
                ('school_title_farsi', models.CharField(blank=True, max_length=300, null=True)),
                ('school_text_latin', models.CharField(blank=True, max_length=300, null=True)),
                ('school_text_farsi', models.CharField(blank=True, max_length=300, null=True)),
                ('admission_title_latin', models.CharField(blank=True, max_length=300, null=True)),
                ('admission_title_farsi', models.CharField(blank=True, max_length=300, null=True)),
                ('admission_text_latin', models.CharField(blank=True, max_length=300, null=True)),
                ('admission_text_farsi', models.CharField(blank=True, max_length=300, null=True)),
                ('skilled_title_latin', models.CharField(blank=True, max_length=300, null=True)),
                ('skilled_title_farsi', models.CharField(blank=True, max_length=300, null=True)),
                ('skilled_text_latin', models.CharField(blank=True, max_length=300, null=True)),
                ('skilled_text_farsi', models.CharField(blank=True, max_length=300, null=True)),
                ('court_title_latin', models.CharField(blank=True, max_length=300, null=True)),
                ('court_title_farsi', models.CharField(blank=True, max_length=300, null=True)),
                ('court_text_latin', models.CharField(blank=True, max_length=300, null=True)),
                ('court_text_farsi', models.CharField(blank=True, max_length=300, null=True)),
                ('in_destination_title_latin', models.CharField(blank=True, max_length=300, null=True)),
                ('in_destination_title_farsi', models.CharField(blank=True, max_length=300, null=True)),
                ('in_destination_text_latin', models.CharField(blank=True, max_length=300, null=True)),
                ('in_destination_text_farsi', models.CharField(blank=True, max_length=300, null=True)),
                ('global_title_latin', models.CharField(blank=True, max_length=300, null=True)),
                ('global_title_farsi', models.CharField(blank=True, max_length=300, null=True)),
                ('global_text_latin', models.CharField(blank=True, max_length=300, null=True)),
                ('global_text_farsi', models.CharField(blank=True, max_length=300, null=True)),
                ('workshop_date_icon', models.ImageField(blank=True, null=True, upload_to='')),
                ('workshop_location_icon', models.ImageField(blank=True, null=True, upload_to='')),
                ('workshop_hotline_icon', models.ImageField(blank=True, null=True, upload_to='')),
                ('workshop_delivery_icon', models.ImageField(blank=True, null=True, upload_to='')),
                ('workshop_fee_icon', models.ImageField(blank=True, null=True, upload_to='')),
                ('calculate_chance_title_latin', models.CharField(blank=True, max_length=300, null=True)),
                ('calculate_chance_title_farsi', models.CharField(blank=True, max_length=300, null=True)),
                ('calculate_line1_latin', models.CharField(blank=True, max_length=300, null=True)),
                ('calculate_line1_farsi', models.CharField(blank=True, max_length=300, null=True)),
                ('calculate_line2_latin', models.CharField(blank=True, max_length=300, null=True)),
                ('calculate_line2_farsi', models.CharField(blank=True, max_length=300, null=True)),
            ],
        ),
    ]
