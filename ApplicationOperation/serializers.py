from rest_framework import serializers

from Financial.models import ExternalPayment
from Services.serializers import ConsultCapacitySerializer, ServiceCategoryTitleSerializer
from Visa.serializers import TourTitleSerializer
from .models import *
from Raw_root.serializers import ApplicationProcessStatusSerializer, ConsultationModeSerializer, OccupationSerializer, \
    MinEmploymentYearSerializer, DegreeSerializer, AgeRangeSerializer, ActionSerializer
from OfficeHauser.serializers import OfficeUserSerializer, VisaStreamBriefSerializer


class AppGuaranteeOptSerializer(serializers.ModelSerializer):
    class Meta:
        model = AppGuaranteeOpt
        fields = '__all__'


# class ExternalPaymentSerializer(serializers.ModelSerializer):
#     class Meta:
#         model = ExternalPayment
#         fields = '__all__'

class ApplicationProfileSerializer(serializers.ModelSerializer):
    app_processing_status = ApplicationProcessStatusSerializer()
    action =  ActionSerializer()

    class Meta:
        model = ApplicationProfile
        fields = ['id','application_number', 'app_processing_status','action']


class ApplicationProfileALLSerializer(serializers.ModelSerializer):
    creator_user = OfficeUserSerializer()
    application_processing_status = ApplicationProcessStatusSerializer()

    class Meta:
        model = ApplicationProfile
        fields = '__all__'


class ConsultationProcessCreateSerializer(serializers.ModelSerializer):
    # consult_capacity = ConsultCapacitySerializer()
    # consultation_mode = ConsultationModeSerializer()
    # service_category = ServiceCategoryTitleSerializer()
    # stid = VisaStreamBriefSerializer()
    # tour = TourTitleSerializer()
    # age_range = AgeRangeSerializer()
    # degree = DegreeSerializer()
    # min_employment_year = MinEmploymentYearSerializer()
    # occupation = OccupationSerializer()
    class Meta:
        model = ConsultationProcess
        fields = ['id', 'payment_details', 'occupation', 'visit_chance_code', 'min_employment_year', 'degree',
                  'age_range', 'cell_number', 'applicant_full_name',
                   'stid', 'service_category', 'consultation_mode']


class CreateApplicationProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = ApplicationProfile
        fields = "__all__"


class CreateApplicantSerializer(serializers.ModelSerializer):
    def __init__(self, *args, **kwargs):
        many = kwargs.pop('many', True)
        super(CreateApplicantSerializer, self).__init__(many=many, *args, **kwargs)

    class Meta:
        model = Applicant
        fields = "__all__"


# class ModelApplicationProfileSerializers(serializers.ModelSerializer):
#     class ModelApplicantSerializer(serializers.ModelSerializer):
#         class Meta:
#             model = Applicant
#             # fields = ["first_name", "last_name", "date_of_birth", "postal_code"]
#             fields = "__all__"
#
#     model_applicant = ModelApplicantSerializer()
#
#     class Meta:
#         model = ApplicationProfile
#         fields = "__all__"
#
#     def create(self, validated_data):
#         model_applicant_data = validated_data.pop("model_applicant")
#         model_application_profile_instance = ApplicationProfile.objects.create(**validated_data)
#         Applicant.objects.create(application_profile_id=model_application_profile_instance,
#                                  **model_applicant_data)
#         return model_application_profile_instance
# ----------------
class ApplicantCreateSerializers(serializers.ModelSerializer):
    class Meta:
        model = Applicant
        fields = '__all__'


class ApplicationProfileSerializers(serializers.ModelSerializer):
    applicants = ApplicantCreateSerializers(many=True)

    class Meta:
        model = ApplicationProfile
        fields = ["service_category", "applicants"]

    def create(self, validated_data):
        applicants_data = validated_data.pop("applicants")
        application_profile = ApplicationProfile.objects.create(**validated_data)
        # if application_profile.group_size == len(applicants_data)
        for applicant_data in applicants_data:
            Applicant.objects.create( **applicant_data)
        return application_profile

class ApplicantSerializer(serializers.ModelSerializer):
    class Meta:
        model = Applicant
        fields = '__all__'

class ConsultationProcessCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = ConsultationProcess
        fields = [
            'consultation_category',
            'consultation_mode',
            'consult_date',
            # 'time_frame',
            'consult_team',
            'applicant_full_name',
            'cell_number',
            'email',
            'evaluation_form_trace_code',
            'visa_chance_case_number',
            'heard_about_us',
            'payment_date_time',
            'trace_code',
            'bank_details',
            'status',
        ]