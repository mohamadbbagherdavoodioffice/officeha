from django.contrib import admin

from django.apps import apps
from django.contrib import admin
from django.contrib.admin.sites import AlreadyRegistered
from import_export import resources
from import_export.admin import ImportExportModelAdmin
from django.apps import apps
from django.contrib import admin
from django.contrib.admin.sites import AlreadyRegistered
from .models import *
from django_summernote.admin import SummernoteModelAdmin

app_models = apps.get_app_config('Content').get_models()
for model in app_models:
    try:
        # admin.site.register(model)

        class AAAResource(resources.ModelResource):
            class Meta:
                model = model


        class AAAAdmin(ImportExportModelAdmin):
            resource_class = AAAResource


        admin.site.register(model, AAAAdmin)
    except AlreadyRegistered:
        pass



admin.site.unregister(Article)

class ArticleAdmin(SummernoteModelAdmin, ImportExportModelAdmin):
    summernote_fields = ('text',)


admin.site.register(Article, ArticleAdmin)

admin.site.unregister(GeneralFAQ)
class GeneralFAQAdmin(SummernoteModelAdmin, ImportExportModelAdmin):
    summernote_fields = ('question_farsi', 'answer_farsi')
admin.site.register(GeneralFAQ, GeneralFAQAdmin)


admin.site.unregister(PassengerrelatedTopic)
class PassengerrelatedTopicAdmin(SummernoteModelAdmin, ImportExportModelAdmin):
    summernote_fields = '__all__'
admin.site.register(PassengerrelatedTopic, PassengerrelatedTopicAdmin)



admin.site.unregister(PassengerRight)
class PassengerRightAdmin(SummernoteModelAdmin, ImportExportModelAdmin):
    summernote_fields = '__all__'
admin.site.register(PassengerRight, PassengerRightAdmin)


admin.site.unregister(SpecificFaq)
class SpecificFaqAdmin(SummernoteModelAdmin, ImportExportModelAdmin):
    summernote_fields = '__all__'
admin.site.register(SpecificFaq, SpecificFaqAdmin)


admin.site.unregister(TravelRequirement)
class TravelRequirementAdmin(SummernoteModelAdmin, ImportExportModelAdmin):
    summernote_fields = '__all__'
admin.site.register(TravelRequirement, TravelRequirementAdmin)

admin.site.unregister(VisaForms)
class VisaFormsCountryAdmin(admin.ModelAdmin,):
    list_filter = ['country']
admin.site.register(VisaForms, VisaFormsCountryAdmin)

