from django.urls import path, include, re_path
from .views import *
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    # path('', include(router.urls)),
    path('get_hint_news', HintNewsAPIView.as_view()),
    path('get_branch_number', BranchNewAPI.as_view()),
    path('get_static_feedback', StaticTextFeedbackAPIView.as_view()),
    path('get_hint_apply_form', HintApplyFormAPIView.as_view()),
    path('get_static_our_services', StaticTextOurServicesAPIView.as_view()),
    path('get_static_visa_forms', StaticTextVisaFormsAPIView.as_view()),
    path('get_static_asia_middle', StaticAsiaMiddleAPIView.as_view()),
    path('get_hint_wizard', HintWizardAPIView.as_view()),
    path('get_list_type_static_text', StaticTextListTypeAPIView.as_view()),
    path('get_static_text_with_type', StaticDyanamicAPIView.as_view()),
    path('get_list_evaluation', EvaluationAPIView.as_view()),
    path('get_country_has_evaluation', EvaluationCountryAPIView.as_view()),
    
    

]

urlpatterns = urlpatterns + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
