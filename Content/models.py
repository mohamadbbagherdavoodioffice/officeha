from django.db import models

import Raw_root.models


def get_file_visa_form(instance, filename):
    return f"file_visa_form/{filename}"


class VisaForms(models.Model):
    title_latin = models.CharField(max_length=500, null=True, blank=True)
    file = models.FileField(upload_to=get_file_visa_form, null=True, blank=True)
    country = models.ForeignKey("Raw_root.Country", on_delete=models.CASCADE, related_name="visa_form_country_set",
                                null=True, blank=True, )
    main_cat = models.ForeignKey("Raw_root.VisaMainCat", on_delete=models.CASCADE, null=True, blank=True, )

    def __str__(self):
        return str(self.country)+" |  "+str(self.title_latin)


class LegalSection(models.Model):
    seq = models.PositiveIntegerField(null=True, blank=True)
    TYPE = (("Term", "Term"), ("Privacy", "Privacy"))
    type = models.CharField(max_length=30, choices=TYPE, null=True, blank=True, )
    title_latin = models.CharField(max_length=500, null=True, blank=True)
    title_farsi = models.CharField(max_length=500, null=True, blank=True)

    def __str__(self):
        return "%s | %s" % (self.title_latin, self.id)






class Glossary(models.Model):
    title_latin = models.CharField(max_length=500, null=True, blank=True)
    title_farsi = models.CharField(max_length=500, null=True, blank=True)
    desc_farsi = models.CharField(max_length=500, null=True, blank=True)
    tags = models.TextField( null=True, blank=True)

    def __str__(self):
        return str(self.title_latin)


class TermSection(models.Model):
    seq = models.PositiveIntegerField(null=True, blank=True)
    title_latin = models.CharField(max_length=500, null=True, blank=True)
    title_farsi = models.CharField(max_length=500, null=True, blank=True)

    def __str__(self):
        return "%s | %s" % (self.title_latin, self.id)


class TermClauses(models.Model):
    # term_section = models.ForeignKey(LegalSection, on_delete=models.CASCADE, null=True, blank=True, )
    clause_seq = models.PositiveIntegerField(null=True, blank=True)
    clause_text_latin = models.TextField(null=True, blank=True)
    clause_text_farsi = models.TextField(null=True, blank=True)
    term_section = models.ManyToManyField(TermSection,related_name='term_section_set1',blank=True)

    def __str__(self):
        return "%s | %s" % (self.clause_text_latin, self.id)


def get_file_passenger_right_thumbnail(instance, filename):
    return f"file_passenger_right_thumbnail/{filename}"


def get_file_passenger_right_thumbnail(instance, filename):
    return f"file_passenger_right_thumbnail/{filename}"


def get_file_passenger_right_banner(instance, filename):
    return f"file_passenger_right_banner/{filename}"


class PassengerRight(models.Model):
    title_latin = models.CharField(max_length=500, null=True, blank=True)
    title_farsi = models.CharField(max_length=500, null=True, blank=True)
    thumbnail = models.FileField(upload_to=get_file_passenger_right_thumbnail, null=True, blank=True)
    banner = models.FileField(upload_to=get_file_passenger_right_banner, null=True, blank=True)
    html_text = models.TextField(null=True, blank=True)
    tags = models.TextField( null=True, blank=True)

    def __str__(self):
        return "%s " % (self.title_latin)


def get_file_travel_requirements_icon(instance, filename):
    return f"file_travel_requirements_icon/{filename}"


class TravelRequirementsCat(models.Model):
    name = models.CharField(max_length=500, null=True, blank=True)
    icon = models.FileField(upload_to=get_file_travel_requirements_icon, null=True, blank=True)

    def __str__(self):
        return str(self.name)


class GeneralFAQ(models.Model):
    question_farsi = models.TextField(null=True, blank=True)
    answer_farsi = models.TextField( null=True, blank=True)
    tags = models.TextField( null=True, blank=True)
    approval = models.BooleanField( default=False)

    def __str__(self):
        return str(self.question_farsi)


def get_file_travel_req_icon(instance, filename):
    return f"file_travel_req_icon/{filename}"


class TravelRequirement(models.Model):
    country = models.ForeignKey("Raw_root.Country", on_delete=models.CASCADE,
                                related_name="travel_requirement_country_set", null=True, blank=True, )
    travel_req_cat =  models.ForeignKey("Content.TravelRequirementsCat", on_delete=models.CASCADE, null=True, blank=True, )
    topic_latin = models.CharField(max_length=500, null=True, blank=True)
    topic_farsi = models.CharField(max_length=500, null=True, blank=True)
    desc_latin = models.TextField(null=True, blank=True)
    desc_farsi = models.TextField(null=True, blank=True)
    tags = models.TextField( null=True, blank=True)


    def __str__(self):
        return "%s | %s | %s" % (self.country.name_latin,self.travel_req_cat, str(self.topic_latin))

def get_file_attachment_file_new_icon(instance, filename):
    return f"file_attachment_file_new_icon/{filename}"

class NewCat(models.Model):
    title_latin = models.CharField(max_length=500, null=True, blank=True)
    title_farsi = models.CharField(max_length=500, null=True, blank=True)
    icon = models.FileField(upload_to=get_file_attachment_file_new_icon, null=True, blank=True)

    def __str__(self):
        return str(self.title_latin)


def get_file_attachment_file_new(instance, filename):
    return f"file_attachment_file_new/{filename}"


class New(models.Model):
    country = models.ForeignKey("Raw_root.Country", on_delete=models.CASCADE, related_name="new_country_set", null=True,blank=True,)
    new_cat = models.ForeignKey(NewCat, on_delete=models.PROTECT, null=True, blank=True, )
    title_latin = models.CharField(max_length=500, null=True, blank=True)
    title_farsi = models.CharField(max_length=500, null=True, blank=True)
    text_latin = models.TextField(null=True, blank=True)
    text_farsi = models.TextField(null=True, blank=True)
    tags = models.CharField(max_length=500, null=True, blank=True)
    creator_staff = models.ForeignKey('OfficeHauser.OfficeUser',on_delete=models.CASCADE,related_name='new_model_create_staff_user_set', null=True,blank=True)
    create_date_time = models.DateTimeField(null=True, blank=True)
    attachment_file = models.FileField(upload_to=get_file_attachment_file_new, null=True, blank=True)

    def __str__(self):
        return str(self.title_latin)

import re
# as per recommendation from @freylis, compile once only
CLEANR = re.compile('<.*?>')

def cleanhtml(raw_html):
  cleantext = re.sub(CLEANR, '', raw_html)
  return cleantext

class SpecificFaq(models.Model):
    tour = models.ForeignKey('Tour.Tour', on_delete=models.PROTECT, null=True, blank=True, )
    stid = models.ForeignKey('Visa.VisaStream', on_delete=models.PROTECT, null=True, blank=True, )
    service_category = models.ForeignKey('Services.ServiceCategory', on_delete=models.CASCADE, null=True, blank=True, )
    question_farsi = models.TextField( null=True, blank=True)
    answer_farsi = models.TextField(null=True, blank=True)
    tags = models.TextField(null=True, blank=True)
    approval = models.BooleanField( default=False)
    def __str__(self):
        print()
        if self.stid == None:
            s1 = ""
        else:
            s1 = self.stid.title_latin
        if self.service_category == None:
            s2 = ""
        else:
            s2 = self.service_category.title_latin

        if self.tour == None:
            s3 = ""
        else:
            s3 = self.tour.title_latin

        return f'{s1}, {s2}, {s3},{cleanhtml(self.question_farsi)} '

class Centers(models.Model):
    CENTER_TYPE = (
        ("Embassy", "Embassy"),
        ("VFS", "VFS"),
        ("Test", "Test"),
        ("TM", "TM"),
        ("MED", "MED"),
        ("JUD", "JUD"),
        ("IMMI", "IMMI"),
        ("Branch", "Branch"),

    )
    seq = models.PositiveIntegerField(null=True, blank=True)
    type = models.CharField(max_length=400, choices=CENTER_TYPE, null=True, blank=True)
    country = models.ForeignKey('Raw_root.Country', on_delete=models.CASCADE,
                                related_name='centers_history_country_set', null=True, blank=True, verbose_name="Visa Country")
    bio_in_country = models.ForeignKey('Raw_root.Country', on_delete=models.CASCADE,
                                related_name='bio_incountry_id_raw_root', null=True, blank=True,)
    region = models.ForeignKey("Raw_root.Region", on_delete=models.CASCADE, null=True, blank=True, )
    email = models.EmailField(null=True, blank=True)
    address = models.CharField(max_length=400, null=True, blank=True)
    tel = models.CharField(max_length=100, null=True, blank=True)
    whats_app = models.CharField(max_length=100, null=True, blank=True)
    location_gps = models.CharField(max_length=400, null=True, blank=True)
    working = models.BooleanField(default=False)

    def __str__(self):
        return str(self.type)


def get_file_passenger_topic_thumbnail(instance, filename):
    return f"file_passenger_topic_thumbnail/{filename}"

class PassengerrelatedTopic(models.Model):
    country = models.ForeignKey("Raw_root.PassengerRelatedCategory", on_delete=models.CASCADE, related_name="new_country_set", null=True,blank=True,)
    title_latin = models.CharField(max_length=500, null=True, blank=True)
    title_farsi = models.CharField(max_length=500, null=True, blank=True)
    text_latin = models.TextField(null=True, blank=True)
    text_farsi = models.TextField(null=True, blank=True)
    thumbnail = models.FileField(upload_to=get_file_passenger_topic_thumbnail, null=True, blank=True)


def get_file_article_banner(instance, filename):
    return f"file_article_banner/{filename}"

def get_file_article_thumbnail(instance, filename):
    return f"file_article_thumbnail/{filename}"


class Article(models.Model):
    approval = models.BooleanField( default=False)
    title_latin = models.CharField(max_length=500, null=True, blank=True)
    title_farsi = models.CharField(max_length=500, null=True, blank=True)
    thumbnail = models.FileField(upload_to=get_file_article_thumbnail, null=True, blank=True)
    banner = models.FileField(upload_to=get_file_article_banner, null=True, blank=True)
    tour_element = models.ManyToManyField("Raw_root.TourElement", blank=True)
    service_category = models.ManyToManyField("Services.ServiceCategory", blank=True)
    stid = models.ManyToManyField('Visa.VisaStream', blank=True, )
    tour = models.ManyToManyField('Tour.Tour', blank=True)
    program = models.ManyToManyField('Edu.Program', blank=True, )
    school = models.ManyToManyField('Edu.School', blank=True, )
    job = models.ManyToManyField('Hr.job', blank=True)
    by_staff_user = models.ForeignKey('OfficeHauser.OfficeUser', on_delete=models.CASCADE, primary_key=False,
                                      related_name='article_by_user_set', null=True, blank=True)
    text = models.TextField(null=True, blank=True)
    last_updated_date = models.DateTimeField(auto_now=True)

    tags = models.TextField( null=True, blank=True)

    def __str__(self):
        return "%s | %s | %s" % (self.by_staff_user,self.title_latin, str(self.last_updated_date))



def get_file_thingtodo_banner(instance, filename):
    return f"file_thingtodo_banner/{filename}"

class ThingToDo(models.Model):
    thing_to_do_cat = models.ForeignKey("Raw_root.ThingsToDo", on_delete=models.CASCADE, null=True,blank=True,)
    title_latin = models.CharField(max_length=500, null=True, blank=True)
    title_farsi = models.CharField(max_length=500, null=True, blank=True)
    region = models.ForeignKey("Raw_root.Region", on_delete=models.CASCADE, null=True, blank=True, )
    description = models.TextField( null=True, blank=True)
    banner = models.FileField(upload_to=get_file_thingtodo_banner, null=True, blank=True)
    visiting_time_in_min = models.PositiveIntegerField( null=True, blank=True)
    currency = models.ForeignKey("Raw_root.Currency", on_delete=models.CASCADE, null=True, blank=True, default=None)
    ticket_fee = models.PositiveIntegerField(null=True, blank=True)
    photo_by = models.ForeignKey('OfficeHauser.OfficeUser', on_delete=models.CASCADE, null=True, blank=True)
    upload_date_time = models.DateTimeField(null=True, blank=True)
    approval = models.BooleanField(default=False)

    def __str__(self):
        return "%s" % (self.title_latin)
    class Meta:
        verbose_name = "Thing To Do"



def get_file_gallery_image(instance, filename):
    return f"file_gallery_image/{filename}"

class Gallery(models.Model):
    image = models.FileField(upload_to=get_file_gallery_image, null=True, blank=True)
    title_latin = models.CharField(max_length=500, null=True, blank=True)
    title_farsi = models.CharField(max_length=500, null=True, blank=True)
    description = models.TextField( null=True, blank=True)
    tour = models.ForeignKey('Tour.Tour', on_delete=models.PROTECT, null=True, blank=True, )
    service_category = models.ForeignKey('Services.ServiceCategory', on_delete=models.CASCADE, null=True, blank=True, )
    photo_by = models.ForeignKey('OfficeHauser.OfficeUser', on_delete=models.CASCADE, null=True, blank=True)
    upload_date_time = models.DateTimeField(null=True,blank=True)
    approval = models.BooleanField( default=False)


    def __str__(self):
        return "%s" % (self.title_latin)


class PrivacySection(models.Model):
    seq = models.PositiveIntegerField(null=True, blank=True)
    title_latin = models.CharField(max_length=500, null=True, blank=True)
    title_farsi = models.CharField(max_length=500, null=True, blank=True)

    def __str__(self):
        return "%s | %s" % (self.title_latin, self.id)
    
class PrivacyClauses(models.Model):
    clause_seq = models.PositiveIntegerField(null=True, blank=True)
    clause_text_latin = models.TextField(null=True, blank=True)
    clause_text_farsi = models.TextField(null=True, blank=True)
    privacy_section = models.ManyToManyField(PrivacySection, blank=True,related_name='privacy_section_set1' )
    
    def __str__(self):
        return str(self.clause_text_latin)
    
class PortalNote(models.Model):
    PAGE = (("Dashboard", "Dashboard"), ("Profile", "Profile"), ("App", "App")
            , ("New_App_Q", "New_App_Q")
            , ("New_App_Service", "New_App_Service")
            , ("New_App_Applicant", "New_App_Applicant")
            , ("New_App_Invoice", "New_App_Invoice")
            , ("New_App_Payresult", "New_App_Payresult")
            , ("App_Process", "App_Process")
            )
    page = models.CharField(max_length=80, choices=PAGE, null=True, blank=True, )

    title_latin = models.CharField(max_length=500, null=True, blank=True)
    title_farsi = models.CharField(max_length=500, null=True, blank=True)
