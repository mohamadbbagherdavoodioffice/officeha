from django.contrib.postgres.indexes import GinIndex
from django.db import models
from django.db.models import Q
from django.contrib.postgres.search import SearchVectorField, SearchVector


def get_file_school_banner_image(instance, filename):
    return f"file_school_banner_image/{filename}"


def get_file_school_banner_thumbnail(instance, filename):
    return f"file_school_banner_thumbnail/{filename}"

class School(models.Model):
    TYPE = (
        ('Uni', 'Uni'),
        ('College', 'College'),
        ('School', 'School'),
    )
    IRZONE = (
        ('State capital', 'State capital'),
        ('Azad Capital', 'Azad Capital'),
        ('Towns', 'Towns'),
        ('None', 'None'),
    )
    country = models.ForeignKey('Raw_root.Country', on_delete=models.CASCADE, related_name='school_country_set',null=True,blank=True, )
    region = models.ForeignKey('Raw_root.Region', on_delete=models.CASCADE, related_name='school_region_set',null=True,blank=True )
    name_latin = models.CharField(max_length=500,null=True,blank=True)
    name_farsi = models.CharField(max_length=500,null=True,blank=True)
    pgwp = models.BooleanField(default=False)
    dli = models.CharField(max_length=12,null=True,blank=True)
    type = models.CharField(max_length=30, choices=TYPE,null=True,blank=True)
    website = models.URLField(max_length=300,null=True,blank=True)
    admission_staff_name = models.CharField(max_length=200,null=True,blank=True)
    admission_staff_email = models.CharField(max_length=100, null=True, blank=True)
    school_banner_image = models.ImageField(upload_to=get_file_school_banner_image,null=True,blank=True)
    school_banner_thumbnail = models.ImageField(upload_to=get_file_school_banner_thumbnail, null=True, blank=True)
    sevis_code = models.CharField(max_length=200,null=True,blank=True)
    ir_edu_zone = models.ForeignKey('Raw_root.IrGraduationZones', on_delete=models.PROTECT, null=True, blank=True)
    ir_gov_approval = models.BooleanField(default=False)
    tags = models.TextField( null=True, blank=True)


    def __str__(self):
        return "%s | %s | %s" % (self.country.name_latin,self.region.title_latin, str(self.name_latin))



def get_file_program_banner_img(instance, filename):
    return f"file_program_banner_img/{filename}"


def get_file_program_thumbnail_img(instance, filename):
    return f"file_program_thumbnail_img/{filename}"

class Program(models.Model):
    banner = models.FileField(upload_to=get_file_program_banner_img, null=True, blank=True)
    thumbnail = models.FileField(upload_to=get_file_program_thumbnail_img, null=True, blank=True)
    edu_subject = models.ForeignKey('Raw_root.EduSubject', on_delete=models.PROTECT, null=True, blank=True, )
    required_degree = models.ForeignKey('Raw_root.Degree', on_delete=models.CASCADE,
                                        related_name='program_required_degree_set', null=True, blank=True, )
    degree = models.ForeignKey('Raw_root.EduProgramDegree', on_delete=models.PROTECT, null=True, blank=True,related_name='program_degreee_set')
    name_latin = models.CharField(max_length=300, null=True, blank=True, )
    name_farsi = models.CharField(max_length=300, null=True, blank=True, )
    supervisor_support = models.BooleanField(default=False)

    tags = models.TextField( null=True, blank=True)


    def __str__(self):
        return "%s | %s" % ( self.name_latin,self.degree.title_latin )


class ProgramSchool(models.Model):
    MONTH =  (('Jan.','Jan.'),('Feb.','Feb.'),('Mar.','Mar.'),('Apr.','Apr.'),('May','May'),('Jun.','Jun.'),
              ('Jul.','Jul.'),('Aug.','Aug.'),('Sep.','Sep.'),('Oct.','Oct.'),('Nov.','Nov.'),('Dec.','Dec.'))
    TYPE = (('Courses bases', 'Courses bases'),('Thesis base', 'Thesis base'),)
    school = models.ForeignKey(School, on_delete=models.CASCADE, null=True, blank=True, )
    program = models.ManyToManyField(Program,blank=True,)


    # intake_month = models.CharField(max_length=30, choices=MONTH, null=True, blank=True, )
    # days_1 = models.CharField(max_length=30,choices=[(str(x), str(x)) for x in range(1, 32)], null=True, blank=True, )
    # # month_2 = models.CharField(max_length=30, choices=MONTH, null=True, blank=True, )
    # # days_2 = models.CharField(max_length=30, choices=[(str(x), str(x)) for x in range(1, 32)], null=True, blank=True, )
    # # month_3 = models.CharField(max_length=30, choices=MONTH, null=True, blank=True, )
    # # days_3 = models.CharField(max_length=30, choices=[(str(x), str(x)) for x in range(1, 32)], null=True, blank=True, )
    # # month_4 = models.CharField(max_length=30, choices=MONTH, null=True, blank=True, )
    # # days_4 = models.CharField(max_length=30, choices=[(str(x), str(x)) for x in range(1, 32)], null=True, blank=True, )
    # # month_5 = models.CharField(max_length=30, choices=MONTH, null=True, blank=True, )
    # # days_5 = models.CharField(max_length=30, choices=[(str(x), str(x)) for x in range(1, 32)], null=True, blank=True, )
    # # month_6 = models.CharField(max_length=30, choices=MONTH, null=True, blank=True, )
    # # days_6 = models.CharField(max_length=30, choices=[(str(x), str(x)) for x in range(1, 32)], null=True, blank=True, )
    #
    # currency = models.ForeignKey('Raw_root.Currency', on_delete=models.CASCADE, null=True, blank=True,)
    # tuition = models.DecimalField(max_digits=10, decimal_places=2,null=True,blank=True)
    # scholarship = models.BooleanField(default=False)
    # teaching_lang_1 = models.ForeignKey('Raw_root.Language', null=True, blank=True, related_name='program_school_lang1_set',on_delete=models.CASCADE)
    # teaching_lang_2 = models.ForeignKey('Raw_root.Language', null=True, blank=True, related_name='program_school_lang2_set',on_delete=models.CASCADE)
    # admission_fee = models.DecimalField(max_digits=10, decimal_places=2, null=True, blank=True,)
    # all_degrees_internship = models.BooleanField(default=False, null=True, blank=True)
    # all_degrees_co_op = models.BooleanField(default=False, null=True, blank=True)
    # type = models.CharField(max_length=30, choices=TYPE, null=True, blank=True, )
    # duration = models.CharField(max_length=300, null=True, blank=True, )
    # apply_month = models.ManyToManyField('Raw_root.MonthOfYear', blank=True, )
    #
    def __str__(self):
        return " %s | %s" % ( self.program, self.school )





