from rest_framework import serializers

from Services.models import ServiceCategory
from .models import *
class RemoveMediaPrefixField(serializers.Field):
    def to_representation(self, value):
        return value.url.replace('/media/', '')
    

class ServiceCategoryDetailsSerializer(serializers.ModelSerializer):
    icon = RemoveMediaPrefixField()
    class Meta:
        model = ServiceCategory
        fields = ['id','title_latin', 'title_farsi', 'icon']
        




class HintSerializer(serializers.ModelSerializer):
    class Meta:
        model = Hint
        fields = '__all__'


class BranchSerializer(serializers.ModelSerializer):
    class Meta:
        model = Branch
        fields = '__all__'


class BranchNumbersSerializer(serializers.ModelSerializer):
    branch = serializers.SerializerMethodField()

    class Meta:
        model = BranchNumbers
        fields = '__all__'

    def get_branch(self, instance):
        ids = int(instance.id)
        branches = Branch.objects.filter(pk=ids)
        branches_serializer = BranchSerializer(branches, many=True)
        return branches_serializer.data

class StaticTextSerializer(serializers.ModelSerializer):
    class Meta:
        model = StaticText
        fields = '__all__'


class MenuColumnServiceDetailsSerializer(serializers.ModelSerializer):
    service_category_1 = ServiceCategoryDetailsSerializer()
    service_category_2 = ServiceCategoryDetailsSerializer()
    service_category_3 = ServiceCategoryDetailsSerializer()
    service_category_4 = ServiceCategoryDetailsSerializer()
    service_category_5 = ServiceCategoryDetailsSerializer()
    service_category_6 = ServiceCategoryDetailsSerializer()
    service_category_7 = ServiceCategoryDetailsSerializer()
    service_category_8 = ServiceCategoryDetailsSerializer()
    service_category_9 = ServiceCategoryDetailsSerializer()
    service_category_10 = ServiceCategoryDetailsSerializer()
    class Meta:
        model = MenuColumnService
        fields = '__all__'

class BranchNewSerializer(serializers.ModelSerializer):
    branch_numbers = BranchNumbersSerializer(source='branchnumbers_set', many=True)

    class Meta:
        model = Branch
        fields = (
            'id', 'type', 'branch_title_latin', 'branch_title_farsi', 'address_title_latin',
            'address_title_farsi', 'postal_code', 'working_days', 'hours_of_operation', 'branch_numbers'
        )


class EvaluationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Evaluation
        fields = ['id','title_latin','title_farsi','country']
