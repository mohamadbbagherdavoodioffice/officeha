from django.contrib import admin
from django_summernote.admin import SummernoteModelAdmin
from import_export import resources
from import_export.admin import ImportExportModelAdmin
from django.apps import apps
from django.contrib import admin
from django.contrib.admin.sites import AlreadyRegistered
from .models import *
app_models = apps.get_app_config('Organization').get_models()
for model in app_models:
    try:
        # admin.site.register(model)

        class AAAResource(resources.ModelResource):
            class Meta:
                model = model


        class AAAAdmin(ImportExportModelAdmin):
            resource_class = AAAResource


        admin.site.register(model, AAAAdmin)
    except AlreadyRegistered:
        pass



admin.site.unregister(DeclarationQuestion)
class DeclarationQuestionAdmin(SummernoteModelAdmin, ImportExportModelAdmin,admin.ModelAdmin):
    list_filter = ('id','service_category__title_latin','tour__title_latin','stid__title_latin')
    summernote_fields = '__all__'
admin.site.register(DeclarationQuestion, DeclarationQuestionAdmin)


admin.site.unregister(NotaryText)
class NotaryTextAdmin(SummernoteModelAdmin, ImportExportModelAdmin):
    summernote_fields = '__all__'
admin.site.register(NotaryText, NotaryTextAdmin)
