from django.conf import settings
from django.conf.urls.static import static
from django.urls import path, re_path

from .views import *

urlpatterns = [
    path('reterive_stid_by_country', VisaStreamAPIVIEW.as_view()),
    path('get_stid_popular', VisaStreamPopularAPIVIEW.as_view()),
    re_path('retrieve_stid_details', VisaStreamSubAllDetailsAPIVIEW.as_view()),
    path('get_list_visa', VisaStreamListAllAPIVIEW.as_view()),
    re_path(r'^retrieve_instalment_details_by_service_cat/(?P<idd>\d+)$', InstalmentGETServiceCatDetailsAPIVIEW.as_view()),
    re_path(r'^retrieve_instalment_details_by_visastream/(?P<idd>\d+)$', InstalmentGETStidDetailsAPIVIEW.as_view()),
    path('get_alert_note_with_visa', VisaStreamAlertNoteAPIView.as_view()),
    path('get_guarantee_with_visa', VisaStreamGuaranteeAPIView.as_view()),
    path('get_instalment_details', InstalmentSubAllDetailsAPIVIEW.as_view()),
    path('get_sub_visastream', SubVisaStreamDetailsAPIView.as_view()),
    path('passport-save', PassportSaveCreateViewset.as_view()),
    path('passport-vision/<int:id>', PassportVisionRetrieve.as_view()),
    path('passport/<int:id>', PassportRetrieve.as_view()),
    path('visa_details', VisaDetails.as_view()),
    path('get_list_visa_asia_middle', VisaStreamGlobalZonesArabianAPiView.as_view()),
    path('get_country_has_visa', GetCountryHasVisa.as_view()),

]

urlpatterns = urlpatterns + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
