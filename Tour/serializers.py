from rest_framework import serializers

# from Content.serializers import ArticleTourpageConfigListSerializer
from Content.models import Article, Gallery
from Content.serializers import ThingToDoSerializer, ThingToDoTitleSerializer
from Financial.models import Invoice
from OfficeHauser.models import OfficeUser
from OfficeHauser.serializers import BusinessProfileBriefSerializer, OfficeUserDetailsSerializer, \
    OfficeUserBriefSerializer, OfficeUserLinkDetailsSerializer
from Raw_root.serializers import EventCatTourpageConfigListSerializer, CountrySerializer, ProvinceSerializer, \
    DifficultyLevelSerializer, MealPatternSerializer, AccomTypeSerializer, EventCatSerializer, VehicleSerializer, \
    AgeRangeSerializer, \
    AllowedBaggageSerializer, CurrencySerializer, ClassRangesSerializer, TransCoSerializer, TransModeSerializer, \
    RegionSerializer, ToPackSerializer, ThingsToDoSerializer, CancellationRateSerializer
from .models import *


class ArticleTourpageConfigListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Article
        fields = ['id','title_latin','title_farsi','thumbnail']



class AttractionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Attraction
        fields = '__all__'


class TourInvoiceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Invoice
        fields = '__all__'

class TourTourPageConfigSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tour
        fields = ['id','title_latin', 'title_farsi', 'magazine_cover']

class TourPageConfigALLSerializer(serializers.ModelSerializer):
    article_1 = ArticleTourpageConfigListSerializer()
    article_2 = ArticleTourpageConfigListSerializer()
    event_cat = EventCatTourpageConfigListSerializer(many=True)
    popular_tour_1 =TourTourPageConfigSerializer()
    popular_tour_2 = TourTourPageConfigSerializer()
    popular_tour_3 = TourTourPageConfigSerializer()
    popular_tour_4 = TourTourPageConfigSerializer()
    popular_tour_5 = TourTourPageConfigSerializer()
    guide_1 = BusinessProfileBriefSerializer()
    guide_2 = BusinessProfileBriefSerializer()
    guide_3 = BusinessProfileBriefSerializer()
    guide_4 = BusinessProfileBriefSerializer()
    guide_5 = BusinessProfileBriefSerializer()
    guide_6 = BusinessProfileBriefSerializer()
    staff_1 = OfficeUserBriefSerializer()
    staff_2 = OfficeUserBriefSerializer()
    staff_3 = OfficeUserBriefSerializer()
    staff_4 = OfficeUserBriefSerializer()
    staff_5 = OfficeUserBriefSerializer()
    staff_6 = OfficeUserBriefSerializer()
    random_images_from = OfficeUserLinkDetailsSerializer( many=True)
    gallery = serializers.SerializerMethodField()
    class Meta:
        model = TourPageConfig
        fields = ['id','title_latin', 'title_farsi',
                  'banner', 'slogan_title_1_latin', 'slogan_title_1_farsi',
                  'slogan_desc_1_latin', 'slogan_desc_1_farsi', 'article_1',
                  'article_2', 'slogan_title_2_latin', 'slogan_title_2_farsi',
                  'slogan_desc_2_latin', 'slogan_desc_2_farsi', 'event_cat',
                  'magazine_background', 'magazine_title_latin',
                  'magazine_title_farsi', 'magazine_title_color_code',
                  'popular_tour_1', 'popular_tour_2', 'popular_tour_3',
                  'popular_tour_4', 'popular_tour_5', 'expert_block_title_latin',
                  'expert_block_title_farsi', 'expert_block_1_latin',
                  'expert_block_1_farsi', 'guide_1', 'guide_2',
                  'guide_3', 'guide_4', 'guide_5', 'guide_6',
                  'staff_1', 'staff_2', 'staff_3', 'staff_4',
                  'staff_5', 'staff_6', 'routing_block_img', 'button_routing', 'service_cat_1',
                  'visa', 'article', 'thing_to_do_cat', 'routing_title_title',
                  'routing_title_farsi', 'routing_desc_latin', 'routing_desc_farsi', 'photos_of_client_title_latin', 'photos_of_client_title_farsi',
                  'photos_of_client_desc_title_latin',
                  'photos_of_client_desc_title_farsi', 'no_of_random_images',
                  'random_images_from', 'subscribe_title_latin', 'subscribe_title_farsi',
                  'subscribe_desc_latin', 'subscribe_desc_farsi','gallery']


    def get_gallery(self, instance):
        dict = {}
        id_user = TourPageConfig.objects.filter(id=instance.id).values_list('random_images_from',flat=True)
        count = TourPageConfig.objects.filter(id=instance.id).values_list('no_of_random_images', flat=True)
        dict = Gallery.objects.filter(photo_by__in=id_user).filter(approval=True).values('id','image', 'title_latin',
                                     'title_farsi', 'description',
                                     'tour', 'service_category',
                                      'upload_date_time', 'approval',
                                     'photo_by__id', 'photo_by__username', 'photo_by__country',
                                     'photo_by__province', 'photo_by__instagram_1'
                                     , 'photo_by__instagram_2', 'photo_by__website_url',
                                     'photo_by__linkedin', 'photo_by__username',
                                     )[:max(list(count))]

        return dict
    # def to_representation(self, data):
    #     data = data.filter(pk=1)
    #     return super(TourPageConfigALLSerializer, self).to_representation(data)

class SystemSerializer(serializers.ModelSerializer):

    class Meta:
        model = TourPageConfig
        list_serializer_class = TourPageConfigALLSerializer
        fields = '__all__'

    # def to_representation(self, data):
    #     data = data.filter(id==2)
    #     return super(TourPageConfigALLSerializer, self).to_representation(data)


class TourDetailsForQuerySerializer(serializers.ModelSerializer):
    # meal_pattern
    class Meta:
        model = Tour
        fields = ['id','thumbnail','title_latin', 'title_farsi', 'trip_length_day']

class TourDetailsSerializer(serializers.ModelSerializer):
    country = CountrySerializer()
    age_range = AgeRangeSerializer(many=True)
    province = ProvinceSerializer()
    difficulty_level = DifficultyLevelSerializer()
    meal_pattern = MealPatternSerializer()
    accom_type = AccomTypeSerializer()
    vechile = VehicleSerializer()
    event_cat = EventCatSerializer(many=True)
    supervisor_staff_user = OfficeUserBriefSerializer()
    tour_guide = OfficeUserBriefSerializer()
    thing_to_do = ThingToDoTitleSerializer(many=True)
    attractions = AttractionSerializer(many=True)
    class Meta:
        model = Tour
        fields = ['id','title_latin','title_farsi','tour_code','country','province'
            ,'trip_length_day','difficulty_level','meal_pattern','accom_type','arrvial_pick_up',
                  'departure_drop','night_drive','group','outbound','brief_text','gathering_address',
                  'hot_line','calling_time_from','calling_time_to','hash_tags','vechile',
                  'banner','alt_text_banner','video_upload','video_title','video_desc','supervisor_staff_user',
                  'tour_guide','event_cat','age_range','thing_to_do','attractions']


class TourDepratureFareDetailsSerializer(serializers.ModelSerializer):
    allowed_luggage = AllowedBaggageSerializer()
    allowed_carry_on = AllowedBaggageSerializer()
    currency = CurrencySerializer()
    class_range = ClassRangesSerializer()
    trans_co = TransCoSerializer()
    trans_mode = TransModeSerializer()
    destination = RegionSerializer()
    cancellation_rate = CancellationRateSerializer()
    class Meta:
        model = TourDepartureFare
        fields = ['id','validity_to','validity_from','destination',
                  'capacity','departure_date_time','fee_dbl','fee_dbl_discounted','fee_sgl'
            ,'fee_shared','fee_child','fee_infant','allowed_luggage','allowed_carry_on','cancellation_rate'
            ,'currency','class_range','trans_co','trans_mode','comment','flight_adult','flight_chd']


class TourAllListSerializer(serializers.ModelSerializer):
    country = CountrySerializer()
    meal_pattern = MealPatternSerializer()
    accom_type = AccomTypeSerializer()

    class Meta:
        model = Tour
        fields = ['id','title_latin','title_farsi','country','tour_code','thumbnail','trip_length_day','meal_pattern','accom_type','title_seo','slug']

class TourToPackSerializer(serializers.ModelSerializer):
    to_pack = ToPackSerializer(many=True)
    class Meta:
        model = TourToPack
        fields =['id','to_pack']



class DayTourDetails2Serializer(serializers.ModelSerializer):
    day_region = RegionSerializer()
    night_region = RegionSerializer()
    accom_type = AccomTypeSerializer()
    meal = MealPatternSerializer()
    thing_to_do = ThingToDoSerializer(many=True)
    attraction = AttractionSerializer(many=True)
    local_guide = OfficeUserBriefSerializer()
    class Meta:
        model = DayTour
        fields = '__all__'

