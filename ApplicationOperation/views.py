# Create your views here.
import json
from Services.serializers import SubServiceCategorySerializer
from Financial.serializers import ExternalPaymentSerializer
from PublicPages.models import Hint
from PublicPages.models import StaticText
from Raw_root.models import RespondNote, ConsultationMode
from Services.models import ServiceCategory, ConsultCapacity
from rest_framework import generics
from rest_framework.generics import RetrieveAPIView
from rest_framework.response import Response
from rest_framework.views import APIView
from OfficeHauser.models import OfficeUser
from Tour.models import Tour
from .serializers import *
import datetime
from rest_framework import status
from rest_framework import viewsets
from django.db.models import Q, Count
from drf_yasg.utils import swagger_auto_schema
from drf_yasg import openapi

class SubmitPaymentStaticAPIView(APIView):
    def get(self, request):
        dic = {}

        static_text = StaticText.objects.filter(type='Submit payments').values('banner', 'title_latin', 'title_farsi',
                                                                               'text_latin', 'text_farsi')
        dic["static_text"] = list(static_text)

        hint = Hint.objects.filter(item='submit external payment').all().values()
        dic["hint"] = list(hint)

        return Response({"message": "success", "status": "OK", "data": dic}, status=200)


# class ExternalPaymentCreateAPIView(generics.CreateAPIView):
#     queryset = ExternalPayment.objects.all()
#     serializer_class = ExternalPaymentSerializer
#
#     def create(self, request, *args, **kwargs):
#         response = super().create(request, *args, **kwargs)
#
#         respond_success = RespondNote.objects.filter(category='External Payment/Create').values(
#             'successful_title_latin',
#             'successful_title_farsi',
#             'successful_text_latin',
#             'successful_text_farsi',
#             'successful_signature_latin',
#             'successful_signature_farsi')
#         respond_fail = RespondNote.objects.filter(category='External Payment/Create').values('failed_title_latin',
#                                                                                              'failed_title_farsi',
#                                                                                              'failed_text_latin',
#                                                                                              'failed_text_farsi',
#                                                                                              'failed_signature_latin',
#                                                                                              'failed_signature_farsi')
#         if response.status_code == 201:
#             return Response({
#                 'status': 201,
#                 'message': 'success',
#                 'respond': respond_success,
#                 'data': response.data
#             })
#         else:
#             return Response({
#                 'status': 404,
#                 'message': 'fail',
#                 'respond': respond_fail,
#                 'data': response.data
#             })


class ApplicationStatusCheckStaticAPIView(APIView):
    @swagger_auto_schema(
        operation_summary="Get application status check static data",
        operation_description="Returns the static text and hint data for the application status check feature.",
        responses={
            200: "Success",
            400: "Bad Request",
            401: "Unauthorized",
            403: "Forbidden",
            404: "Not Found",
            500: "Internal Server Error"
        }
    )
    def get(self, request):
        dic = {}

        static_text = StaticText.objects.filter(type='Application Status Check').values('banner', 'title_latin',
                                                                                        'title_farsi', 'text_latin',
                                                                                        'text_farsi')
        dic["static_text"] = list(static_text)

        hint = Hint.objects.filter(item='Application Status Check').all()
        dic["hint"] = list(hint)

        return Response({"message": "success", "status": "OK", "data": dic}, status=200)


class ApplicationProfileRetriveAPIView(APIView):
    def get(self, request, id):
        dic = {}
        profile_id = str(id)
        try:
            app = ApplicationProfile.objects.filter(id=profile_id).values('id', 'app_processing_status__title_latin',
                                                                          'app_processing_status__title_farsi',
                                                                          'app_processing_status__file',
                                                                          'action__title_latin', 'action__title_farsi')
            if len(app) >= 1:
                dic = list(app)
                return Response({
                    'status': 200,
                    'message': 'success',
                    'respond': "",
                    'data': dic
                })
        except:
            a = 1
        app = ApplicationProfile.objects.filter(trace_code=profile_id).values('id', 'app_processing_status__title_latin'
                                                                              , 'app_processing_status__title_farsi',
                                                                              'app_processing_status__file',
                                                                              'action__title_latin',
                                                                              'action__title_farsi')
        if len(app) >= 1:
            dic = list(app)
            return Response({
                'status': 200,
                'message': 'success',
                'respond': "",
                'data': dic
            })
        app = ApplicationProfile.objects.filter(application_number=profile_id).values('id',
                                                                                      'app_processing_status__title_latin'
                                                                                      ,
                                                                                      'app_processing_status__title_farsi'
                                                                                      , 'app_processing_status__file',
                                                                                      'action__title_latin',
                                                                                      'action__title_farsi')
        if len(app) >= 1:
            dic = list(app)
            return Response({
                'status': 200,
                'message': 'success',
                'respond': "",
                'data': dic
            })

        if len(app) <= 0:
            respond_fail = RespondNote.objects.filter(category='application status no record').values(
                'failed_title_latin',
                'failed_title_farsi',
                'failed_text_latin',
                'failed_text_farsi',
                'failed_signature_latin',
                'failed_signature_farsi')
            return Response({
                'status': 404,
                'message': 'fail',
                'respond': respond_fail,
                'data': ''
            })


# class ApplicationProfileRetriveAPIView(RetrieveAPIView):
#     serializer_class = ApplicationProfileSerializer
#     # queryset = ApplicationProfile.objects.values('application_processing_status__title_latin','application_processing_status__title_farsi')
#     lookup_url_kwarg = "profile_id"
#
#     def get(self, request, profile_id):
#         app = ApplicationProfile.objects.filter(id=profile_id)
#         if len(app) >= 1:
#             serializer = ApplicationProfileSerializer(app)
#             print(serializer.data,"|!||!|!|!|!|")
#
#         try:
#             print(profile_id)
#
#             print(app,"++++",len(app))
#
#             # app = ApplicationProfile.objects.filter(trace_code=profile_id)
#             # app = ApplicationProfile.objects.filter(trace_code=profile_id)
#             # if len(app) >= 1:
#             #     serializer = ApplicationProfileSerializer(app)
#             # app = ApplicationProfile.objects.filter(application_number=profile_id)
#             # if len(app) >= 1:
#             #     serializer = ApplicationProfileSerializer(app)
#             # return Response({
#             #     'result': serializer.data,
#             #     'message': 'success',
#             #     'status': 200,
#             # })
#             return Response({
#                 'status': 200,
#                 'message': 'success',
#                 'respond': "",
#                 'data': serializer.data
#             })
#         except:
#             respond_fail = RespondNote.objects.filter(category='application status no record').values(
#                 'failed_title_latin',
#                 'failed_title_farsi',
#                 'failed_text_latin',
#                 'failed_text_farsi',
#                 'failed_signature_latin',
#                 'failed_signature_farsi')
#             return Response({
#                 'status': 404,
#                 'message': 'fail',
#                 'respond': respond_fail,
#                 'data': ''
#             })


class ApplicationNumberStaticAPIView(APIView):
    def get(self, request):
        dic = {}

        static_text = StaticText.objects.filter(type='Retrieve Application Number').values('banner', 'title_latin',
                                                                                           'title_farsi', 'text_latin',
                                                                                           'text_farsi')
        dic["static_text"] = list(static_text)

        hint = Hint.objects.filter(item='Retrieve Application Number').all()
        dic["hint"] = list(hint)

        service_cat = ServiceCategory.objects.all()
        json_data = SubServiceCategorySerializer(service_cat, many=True)
        dic["service_cat"] = json_data.data

        return Response({"message": "success", "status": 200, "data": dic}, status=200)


class SearchForApplicationNumberAndUserAvailableAPIView(APIView):
    def post(self, request):
        dic = {}
        serializer_sc = {}
        dic = serializer_sc
        # first_name = self.request.GET.get("first_name")
        first_name = self.request.GET.get("first_name")
        last_name = self.request.GET.get("last_name")

        dob = self.request.GET.get("date_of_birth")
        nid = self.request.GET.get("national_id_number")
        service_cat = self.request.GET.get("service_cat")
        email = self.request.GET.get("email")
        cell_number = self.request.GET.get("cell_number")
        security = self.request.GET.get("security")
        otp = self.request.GET.get("otp")
        date_from = self.request.GET.get("date_from")
        date_to = self.request.GET.get("date_to")
        ip = self.request.GET.get("ip")
        device_type = self.request.GET.get("device_type")
        stid = self.request.GET.get("stid")
        tour = self.request.GET.get("tour")
        if stid==None: stid=-1000
        if tour==None: tour=-1000
        if service_cat==None: service_cat=-1000
        # check identity
        # date of birth badara
        # email or phone mizane
        #  otp migiram baad
        #  if match user :

        # if otp is not Null:
        #     check otp
        #     query.filter(otp)
        # serializer_sc.data=[]
        if otp != "Null":
            # check otp
            #     query.filter(otp)
            #  if otp is valid
            # data = OfficeUser.objects.filter(cell_number=cell_number)
            if 1 == 1:
                data_sc = ApplicationProfile.objects.filter(Q(delegate_tel=cell_number) & Q(delegate_last_name__icontains=last_name) & Q(delegate_nid=nid)
                & ( Q(service_category=service_cat) | Q(stid=stid)  | Q(tour=tour)) )
         
                serializer_sc = ApplicationProfileSerializer(data_sc, many=True)

                static_text = StaticText.objects.filter(type='Retrieve Application Number').values('banner', 'title_latin','title_farsi', 'text_latin', 'text_farsi')
                
                if len(serializer_sc.data) >= 1:
                #     print(dict(serializer_sc.data[0]))
                #     dict["app_number"] = list(dict(serializer_sc.data[0]))
                    return Response({"message": "success", "status": 200, "data": serializer_sc.data,}, status=200)
                else:
                    respond_fail = RespondNote.objects.filter(category='Retrieve Application Number').values(
                'failed_title_latin',
                'failed_title_farsi',
                'failed_text_latin',
                'failed_text_farsi',
                'failed_signature_latin',
                'failed_signature_farsi')
                    return Response({"message": "fail", "status": 400, "data": respond_fail,}, status=200)

            else:

                return Response({"message": "send otp", "status": 200, "data": []}, status=200)
                # fail
        # else:
        # if security yes : ip device ok bood
        if security == "yes":
            data_sc = ApplicationProfile.objects.filter(creator_user_id=data[0].id, ip=ip, device_type=device_type)
            serializer_sc = ApplicationProfileALLSerializer(data_sc, many=True)
            return Response({"message": "send otp", "status": 200, "data": serializer_sc.data}, status=200)
        else:
            return Response({"message": "send otp", "status": 200, "data": []}, status=200)

        return Response({"message": "send otp", "status": 200, "data": []}, status=200)


class ApplicationProfileQueryAPIView(APIView):
    def post(self, request):
        current_datetime = datetime.datetime.utcnow().date()
        print(current_datetime)
        dic = {}
        js = json.loads(request.body)

        # {
        # "from_date": "2011-02-20",
        # "to_date":"2031-02-01",
        # "date_of_birth":"2022-01-30",
        # "service_category":12
        # }
        # service cat

        if js.get('service_category') is not None:
            app = ApplicationProfile.objects.filter(date_of_creation__gte=js.get('from_date')) \
                .filter(date_of_creation__lte=js.get('to_date')).filter(
                creator_user__date_of_birth=js.get('date_of_birth')). \
                filter(creator_user__nid=js.get('nid')).filter(service_category=js.get('service_category')).values('id',
                                                                                                                   'application_number',
                                                                                                                   'action__title_latin',
                                                                                                                   'action__title_farsi',
                                                                                                                   'app_processing_status__title_latin',
                                                                                                                   'app_processing_status__title_farsi')
            dic = app
            if len(app) >= 1:
                return Response({
                    'status': 200,
                    'message': 'success',
                    'respond': [],
                    'data': dic
                })

            # Tour
        if js.get('tour') is not None:
            app = ApplicationProfile.objects.filter(date_of_creation__gte=js.get('from_date')) \
                .filter(date_of_creation__lte=js.get('to_date')).filter(
                creator_user__date_of_birth=js.get('date_of_birth')). \
                filter(creator_user__nid=js.get('nid')).filter(tour=js.get('tour')).values('id',
                                                                                           'application_number',
                                                                                           'action__id',
                                                                                           'action__title_latin',
                                                                                           'action__title_farsi',
                                                                                           'app_processing_status__title_latin',
                                                                                           'app_processing_status__title_farsi')
            dic = app
            if len(app) >= 1:
                return Response({
                    'status': 200,
                    'message': 'success',
                    'data': dic
                })

        # stid
        if js.get('stid') is not None:
            app = ApplicationProfile.objects.filter(date_of_creation__gte=js.get('from_date')) \
                .filter(date_of_creation__lte=js.get('to_date')).filter(
                creator_user__date_of_birth=js.get('date_of_birth')). \
                filter(creator_user__nid=js.get('nid')).filter(stid=js.get('stid')).values('id', 'application_number',
                                                                                           'action__id',
                                                                                           'action__title_latin',
                                                                                           'action__title_farsi',
                                                                                           'app_processing_status__title_latin',
                                                                                           'app_processing_status__title_farsi')
            dic = app
            if len(app) >= 1:
                return Response({
                    'status': 200,
                    'message': 'success',
                    'data': dic
                })

        if len(app) <= 0:
            respond_fail = RespondNote.objects.filter(category='application status no record').values(
                'failed_title_latin',
                'failed_title_farsi',
                'failed_text_latin',
                'failed_text_farsi',
                'failed_signature_latin',
                'failed_signature_farsi')
            return Response({
                'status': 200,
                'message': 'fail',
                'respond': respond_fail,
                'data': dic
            })

        # return Response({"message": "success", "status": "OK", "data": dic}, status=200)


class ApplicationProfileRetriveStatusAPIView(APIView):
    def get(self, request, id):
        dic = {}
        profile_id = str(id)
        app = ApplicationProfile.objects.filter(application_number=profile_id).values('id',
                                                                                      'app_processing_status__title_latin',
                                                                                      'app_processing_status__title_farsi')
        if len(app) >= 1:
            dic = list(app)
            return Response({
                'status': 200,
                'message': 'success',
                'respond': "",
                'data': dic
            })

        if len(app) <= 0:
            respond_fail = RespondNote.objects.filter(category='application status no record').values(
                'failed_title_latin',
                'failed_title_farsi',
                'failed_text_latin',
                'failed_text_farsi',
                'failed_signature_latin',
                'failed_signature_farsi')
            return Response({
                'status': 404,
                'message': 'fail',
                'respond': respond_fail,
                'data': ''
            })


class ConsultationProcessCreateAPIView(generics.CreateAPIView):
    queryset = ConsultationProcess.objects.all()
    serializer_class = ConsultationProcessCreateSerializer

    def create(self, request, *args, **kwargs):

        dic = {}

        if request.POST.get('service_category'):
            dic["service_category"] = ServiceCategory.objects.filter(id=request.POST.get('service_category')).values(
                "title_latin", "title_farsi", "id")
        if request.POST.get('stid'):
            dic["stid"] = ServiceCategory.objects.filter(id=request.POST.get('stid')).values("title_latin",
                                                                                             "title_farsi", "id")

        if request.POST.get('tour'):
            dic["tour"] = Tour.objects.filter(id=request.POST.get('tour')).values("title_latin", "title_farsi", "id")
        dic["consultation_mode"] = ConsultationMode.objects.filter(id=request.POST.get('consultation_mode')).values(
            "title_latin", "title_farsi", "id")
        dic["date"] = str(datetime.datetime.now())
        dic["consult_capacity"] = ConsultCapacity.objects.filter(id=request.POST.get('consult_capacity')).values("id",
                                                                                                                 "consultation_date",
                                                                                                                 "time_frame__time_frame",
                                                                                                                 "time_frame__id",
                                                                                                                 "center__branch_title_latin",
                                                                                                                 "center__branch_title_farsi",
                                                                                                                 "center__address_title_latin",
                                                                                                                 "center__address_title_farsi",
                                                                                                                 "center__postal_code",
                                                                                                                 "center__working_days",
                                                                                                                 "center__hours_of_operation")
        dic["applicant_full_name"] = request.POST.get('applicant_full_name')
        dic["payment_details"] = request.POST.get('payment_details')
        dic["payment_details"] = request.POST.get('cell_number')
        response = super().create(request, *args, **kwargs)

        respond_success = RespondNote.objects.filter(category='Appointment').values(
            'successful_title_latin',
            'successful_title_farsi',
            'successful_text_latin',
            'successful_text_farsi',
            'successful_signature_latin',
            'successful_signature_farsi')
        respond_fail = RespondNote.objects.filter(category='Appointment').values('failed_title_latin',
                                                                                 'failed_title_farsi',
                                                                                 'failed_text_latin',
                                                                                 'failed_text_farsi',
                                                                                 'failed_signature_latin',
                                                                                 'failed_signature_farsi')
        if response.status_code == 201:
            return Response({
                'status': 201,
                'message': 'success',
                'respond': respond_success,
                'data': response.data,
                'result': dic
            })
        else:
            return Response({
                'status': 404,
                'message': 'fail',
                'respond': respond_fail,
                'data': response.data
            })


class ApplicationProfileListView(APIView):
    def get(self, request):
        application = ApplicationProfile.objects.all()
        ser_data = CreateApplicationProfileSerializer(instance=application, many=True)
        return Response(data=ser_data.data, status=status.HTTP_200_OK)


class ApplicationProfileCreateView(APIView):
    serializer_class = CreateApplicationProfileSerializer
    def post(self, request, *args, **kwargs):
        # group_size = int(request.data["group_size"])
        ser_data1 = CreateApplicationProfileSerializer(data=request.POST)
        ser_data2 = CreateApplicantSerializer(data=request.POST, many=True)
        if ser_data2.is_valid():
            ser_data2.save()
            if ser_data1.is_valid():
                ser_data1.save()
                return Response({"Message": "OK"}, status=status.HTTP_200_OK)
        return Response({"errors": [ser_data1.errors, ser_data2.errors]}, status=status.HTTP_400_BAD_REQUEST)


class Test(generics.CreateAPIView):
    queryset = ApplicationProfile.objects.all()
    serializer_class = ApplicationProfileSerializers


class ApplicantCreateViewset(generics.ListCreateAPIView):
    queryset = Applicant.objects.all()
    serializer_class = ApplicantSerializer

class ApplicationProfileCountAPIView(APIView):
       def get(self, request):
           count = ApplicationProfile.objects.count()
           return Response({'count': count})

class PayConsultationProcessView(generics.CreateAPIView):
    queryset = ConsultationProcess.objects.all()
    serializer_class = ConsultationProcessCreateSerializer

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        # Log the incoming data
        consultation_process = serializer.save()
        
        payment_successful =True
        # Check if the payment is successful
        if payment_successful:
            consultation_process.status = 'PAID'
            consultation_process.payment_date_time = serializer.validated_data['payment_date_time']
            consultation_process.trace_code = serializer.validated_data['trace_code']
            consultation_process.bank_details = serializer.validated_data['bank_details']
            consultation_process.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        else:
            # Delete the entire log if the payment fails
            consultation_process.delete()
            return Response({'error': 'Payment failed'}, status=status.HTTP_400_BAD_REQUEST)