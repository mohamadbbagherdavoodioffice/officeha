from django.urls import path, include, re_path
from .views import *
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    # path('', include(router.urls)),
    path('get_news', NewAPIView.as_view()),
    path('get_news_cat', NewCatAPIView.as_view()),
    path('get_list_visa_form', VisaFormsList.as_view()),
    re_path(r'^get_article_with_program/(?P<program>\d+)$', ArticleProgramIDListAPIView.as_view()),
    re_path(r'^get_article_with_service_category/(?P<service_category>\d+)$', ArticleServiceCategoryIDListAPIView.as_view()),
    re_path(r'^get_article_with_stid/(?P<stid>\d+)$', ArticleSTIDListAPIView.as_view()),
    re_path(r'^get_article_with_tour/(?P<tour>\d+)$', ArticleTourIDListAPIView.as_view()),
    re_path(r'^get_article_with_job/(?P<job>\d+)$', ArticleJobIDListAPIView.as_view()),
    re_path(r'^get_article_with_school/(?P<school>\d+)$', ArticleSchoolIDListAPIView.as_view()),
    path('get_list_country_for_visa_from', CountryGetListForApplyVisaForm.as_view()),
    path('get_list_term_clause', TermClausesListAPIView.as_view()),
    path('get_list_privacy_clause', PrivacyClausesListAPIView.as_view()),
    path('get_list_article_user', ArticleUsersAPIVIEW.as_view()),
    re_path(r'^get_article_with_id/(?P<id>\d+)$', ArticleEachIDAPIView.as_view()),
    path('get_list_article_have_value', ArticleListIsNotNullAPIView.as_view()),
    path('get_list_article_have_job', ArticleListJobVALUEAPIView.as_view()),
    path('get_list_article_have_service_category', ArticleListServiceCategoryVALUEAPIView.as_view()),
    path('get_list_article_have_program', ArticleListProgramVALUEAPIView.as_view()),
    path('get_list_article_have_school', ArticleListSchoolVALUEAPIView.as_view()),
    path('get_list_article_have_visa', ArticleListStidVALUEAPIView.as_view()),
    path('get_list_article_have_tour', ArticleListTourVALUEAPIView.as_view()),
    path('get_article_tour_element', ArticleEachTourElementAPIView.as_view()),
    path('get_list_general_faq', GeneralFAQListApiView.as_view()),
    path('get_travel_requirement_with_country', TravelRequirementDetailsAPIView.as_view()),
    path('get_travelreq_countries', TravelRequirementListCountriesAPIView.as_view()),

    path('get_list_specific_faq', SpecificFaqListApiView.as_view()),
    path('get_list_passenger_right', PassengerRightListApiView.as_view()),
    re_path(r'^get_passenger_right/(?P<id>\d+)$', PassengerRightRetrieveApiView.as_view()),

]

urlpatterns = urlpatterns + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
