from rest_framework import serializers


from OfficeHauser.serializers import StaffUserBriefSerializer
from Raw_root.serializers import DocumentSerializer, HotelSerializer, TourServiceSerializer
from .models import *
from PublicPages.serializers import EvaluationSerializer

class BookWizardConfigSerializer(serializers.ModelSerializer):
    class Meta:
        model = BookWizardConfig
        fields = '__all__'

class AboutUsCertSerializer(serializers.ModelSerializer):
    class Meta:
        model = AboutUsCert
        fields = '__all__'

class AboutUsTeamSerializer(serializers.ModelSerializer):
    staff = StaffUserBriefSerializer()
    class Meta:
        model = AboutUsTeam
        fields = '__all__'



class AboutUsSerializer(serializers.ModelSerializer):
    class Meta:
        model = AboutUs
        fields = ['id','banner', 'background',
                  'alt_text_farsi_banner', 'how_we_started_text_latin', 'how_we_started_text_farsi',
                  'image_desc', 'our_name_image', 'our_name_title_latin', 'our_name_title_farsi',
                  'our_name_text_latin', 'our_name_text_farsi', 'cert_title_latin', 'cert_title_farsi',
                  'cert_text_latin', 'cert_text_farsi', 'our_team_title_latin',
                  'our_team_title_farsi', 'our_team_text_latin', 'our_team_text_farsi', ]
        


class EvaluationConfigSerializer(serializers.ModelSerializer):
    evaluation = EvaluationSerializer()
    class Meta:
        model = EvaluationConfig
        fields = '__all__'


class ConsultWizardConfigSerializer(serializers.ModelSerializer):
    class Meta:
        model = ConsultWizardConfig
        fields = '__all__'
    

class BiometricConfigSerializer(serializers.ModelSerializer):
    hotel = HotelSerializer(many=True, read_only=True)  # Include hotel information
    documents = DocumentSerializer()
    tour_services = TourServiceSerializer()
    class Meta:
        model = BiometricConfig
        fields = ['visa_country', 'center', 'banner', 'title_latin', 'title_farsi', 
                  'duration_latin', 'duration_farsi', 'service_category', 'documents', 
                  'tour_services', 'booking_condition', 'transfer_company1', 
                  'departure_flight1_number', 'departure_flight1_time', 
                  'return_flight1_number', 'return_flight1_time', 
                  'transfer_company2', 'departure_flight2_number', 
                  'departure_flight2_time', 'return_flight2_number', 
                  'return_flight2_time', 'bio_in_country', 'hotel']