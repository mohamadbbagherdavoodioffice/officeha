from django.db import models


def get_file_job_application_pdf_form(instance, filename):
    return f"job_application_pdf_form/{filename}"


# class CareerPageConfig(models.Model):
#     first_text_latin = models.CharField(max_length=500, null=True, blank=True)
#     first_text_farsi = models.CharField(max_length=500, null=True, blank=True)
#     job_openings_text_latin = models.TextField(null=True, blank=True)
#     job_openings_text_farsi = models.TextField(null=True, blank=True)
#     self_assessment_text_latin = models.TextField(null=True, blank=True)
#     self_assessment_text_farsi = models.TextField(null=True, blank=True)
#     job_families_text_latin = models.TextField(null=True, blank=True)
#     job_families_text_farsi = models.TextField(null=True, blank=True)
#     job_application_form_text_latin = models.TextField(null=True, blank=True)
#     job_application_form_text_farsi = models.TextField(null=True, blank=True)
#     job_application_pdf_form = models.FileField(upload_to=get_file_job_application_pdf_form,null=True, blank=True)
#
#     def __str__(self):
#         return self.first_text_latin

# class ConsultationPage(models.Model):
#     text_latin = models.CharField(max_length=500, null=True, blank=True)
#     text_farsi = models.CharField(max_length=500, null=True, blank=True)
#
#
#     def __str__(self):
#         return self.text_latin

# class ConsultationPageIntroduction(models.Model):
#     seq = models.PositiveIntegerField(null=True, blank=True)
#     text_latin = models.CharField(max_length=500, null=True, blank=True)
#     text_farsi = models.CharField(max_length=500, null=True, blank=True)
#
#     def __str__(self):
#         return self.text_latin


class SignInPages(models.Model):
    intro_text_latin = models.CharField(max_length=500, null=True, blank=True)
    intro_text_farsi = models.CharField(max_length=500, null=True, blank=True)
    forgot_text_latin = models.CharField(max_length=500, null=True, blank=True)
    forgot_text_farsi = models.CharField(max_length=500, null=True, blank=True)

    def __str__(self):
        return self.intro_text_latin


# class ServiceBookingWizardConfig(models.Model):
#     TYPE = (('service', 'service'),
#             ('visa', 'visa'),
#             ('tour', 'tour'))
#     APPTYPE = (('all fields', 'all fields'),
#             ('some fields', 'some fields'),
#             ('some fields+doc', 'some fields+doc'),
#                ('name+doc', 'name+doc'))
#     general_type = models.CharField(max_length=30, choices=TYPE, null=True, blank=True, )
#     service_category = models.ForeignKey('Services.ServiceCategory', on_delete=models.CASCADE, null=True, blank=True, )
#     stid = models.ForeignKey('Visa.VisaStream', on_delete=models.PROTECT, null=True, blank=True, )
#     tour = models.ForeignKey('Tour.Tour', on_delete=models.PROTECT, )
#     applicant_block_doc = models.CharField(max_length=60, choices=APPTYPE, null=True, blank=True, )
#     declaration_activate = models.BooleanField(default=False)
#     text_latin = models.TextField(null=True, blank=True)
#     text_farsi = models.TextField(null=True, blank=True)
#
#     def __str__(self):
#         return self.text_latin

def get_file_aboutus_how_we_started(instance, filename):
    return f"aboutus_how_we_started/{filename}"


def get_file_aboutus_banner(instance, filename):
    return f"aboutus_banner/{filename}"


def get_file_aboutus_background(instance, filename):
    return f"file_aboutus_background/{filename}"


def get_file_aboutus_our_name_image(instance, filename):
    return f"file_aboutus_our_name_image/{filename}"


class AboutUs(models.Model):
    banner = models.FileField(upload_to=get_file_aboutus_banner, null=True, blank=True)
    background = models.ImageField(upload_to=get_file_aboutus_background, null=True, blank=True)
    alt_text_farsi_banner = models.TextField(null=True, blank=True)
    how_we_started_text_latin = models.TextField(null=True, blank=True)
    how_we_started_text_farsi = models.TextField(null=True, blank=True)
    image_desc = models.TextField(null=True, blank=True)
    our_name_image = models.ImageField(upload_to=get_file_aboutus_our_name_image, null=True, blank=True)
    our_name_title_latin = models.CharField(max_length=300, null=True, blank=True)
    our_name_title_farsi = models.CharField(max_length=300, null=True, blank=True)

    our_name_text_latin = models.TextField(null=True, blank=True)
    our_name_text_farsi = models.TextField(null=True, blank=True)

    cert_title_latin = models.CharField(max_length=300, null=True, blank=True)
    cert_title_farsi = models.CharField(max_length=300, null=True, blank=True)

    cert_text_latin = models.TextField(null=True, blank=True)
    cert_text_farsi = models.TextField(null=True, blank=True)

    our_team_title_latin = models.CharField(max_length=300, null=True, blank=True)
    our_team_title_farsi = models.CharField(max_length=300, null=True, blank=True)

    our_team_text_latin = models.TextField(null=True, blank=True)
    our_team_text_farsi = models.TextField(null=True, blank=True)

    def __str__(self):
        return self.alt_text_farsi_banner


class BookWizardConfig(models.Model):
    TYPE = (('service', 'service'),
            ('visa', 'visa'),
            ('tour', 'tour'))
    APPTYPE = (('all fields', 'all fields'),
               ('some fields', 'some fields'),
               ('some fields+doc', 'some fields+doc'),
               ('name+doc', 'name+doc'))
    general_type = models.CharField(max_length=30, choices=TYPE, null=True, blank=True, )

    service_cat = models.ManyToManyField("Services.ServiceCategory", blank=True)
    visa_stream = models.ManyToManyField('Visa.VisaStream', blank=True, )
    tour = models.ManyToManyField('Tour.Tour', blank=True)
    applicant_block_doc = models.CharField(max_length=60, choices=APPTYPE, null=True, blank=True, )
    declaration_activate = models.BooleanField(default=False)

    declaration_text_latin = models.TextField(null=True, blank=True)
    declaration_text_farsi = models.TextField(null=True, blank=True)

    delegated_title_latin = models.CharField(max_length=500, null=True, blank=True)
    delegated_title_farsi = models.CharField(max_length=500, null=True, blank=True)

    delegated_note_latin = models.CharField(max_length=500, null=True, blank=True)
    delegated_note_farsi = models.CharField(max_length=500, null=True, blank=True)

    applicant_step_activate = models.BooleanField(default=False)
    applicant_emergancy_text_latin = models.TextField(null=True, blank=True)
    applicant_emergancy_text_farsi = models.TextField(null=True, blank=True)

    applicant_text_latin = models.TextField(null=True, blank=True)
    applicant_text_farsi = models.TextField(null=True, blank=True)

    payment_text_latin = models.TextField(null=True, blank=True)
    payment_text_farsi = models.TextField(null=True, blank=True)

    invoice_text_latin = models.TextField(null=True, blank=True)
    invoice_text_farsi = models.TextField(null=True, blank=True)

    def __str__(self):
        b = [a.title_latin for a in self.service_cat.all()]
        c = [a.title_latin for a in self.visa_stream.all()]
        d = [a.title_latin for a in self.tour.all()]
        h = b + c + d
        h = h[:3]
        h = ','.join(h)
        return f"{self.general_type},{h}"
        # if b and c and d:
        #     return f"{self.general_type},{b},{c},{d}"
        # elif b and c:
        #     return f"{self.general_type},{b},{c}"
        # elif b and d:
        #     return f"{self.general_type},{b},{d}"
        # elif c and d:
        #     return f"{self.general_type},{c},{d}"
        # elif b:
        #     return f"{self.general_type},{b}"
        # elif c:
        #     return f"{self.general_type},{c}"
        # elif d:
        #     return f"{self.general_type},{d}"
        # else:
        #     return f"{self.general_type}"

class AboutUsCert(models.Model):
    seq = models.PositiveIntegerField(null=True, blank=True)
    title_latin = models.CharField(max_length=500, null=True, blank=True)
    title_farsi = models.CharField(max_length=500, null=True, blank=True)
    year = models.PositiveIntegerField(null=True, blank=True)
    school = models.CharField(max_length=500, null=True, blank=True)


    def __str__(self):
        return self.title_latin


class AboutUsTeam(models.Model):
    staff = models.ForeignKey('OfficeHauser.StaffUser', on_delete=models.PROTECT, null=True, blank=True)

    def __str__(self):
        return str(self.staff)



def get_file_evaluation_banner(instance, filename):
    return f"file_evaluation_banner/','{filename}"

class EvaluationConfig(models.Model):
    evaluation = models.ForeignKey('PublicPages.Evaluation', on_delete=models.PROTECT, null=True, blank=True)
    
    language = models.BooleanField(default=False)
    language_q_latin = models.CharField(max_length=500, null=True, blank=True)
    language_q_farsi = models.CharField(max_length=500, null=True, blank=True)
    
    military_service = models.BooleanField(default=False)
    military_q_latin = models.CharField(max_length=500, null=True, blank=True)
    military_q_farsi = models.CharField(max_length=500, null=True, blank=True)
    
    paper = models.BooleanField(default=False)
    paper_q_latin = models.CharField(max_length=500, null=True, blank=True)
    paper_q_farsi = models.CharField(max_length=500, null=True, blank=True)
    
    visa_chance = models.BooleanField(default=False)
    visachance_q_latin = models.CharField(max_length=500, null=True, blank=True)
    visachance_q_farsi = models.CharField(max_length=500, null=True, blank=True)
    
    occupation = models.BooleanField(default=False)
    occupation_q_latin = models.CharField(max_length=500, null=True, blank=True)
    occupation_q_farsi = models.CharField(max_length=500, null=True, blank=True)

    min_employ_years = models.BooleanField(default=False)
    min_employ_years_q_latin = models.CharField(max_length=500, null=True, blank=True)
    min_employ_years_q_farsi = models.CharField(max_length=500, null=True, blank=True)
    
    degree = models.BooleanField(default=False)
    degree_q_latin = models.CharField(max_length=500, null=True, blank=True)
    degree_q_farsi = models.CharField(max_length=500, null=True, blank=True)
   
    avalibale_funds = models.BooleanField(default=False)
    avalibale_funds_q_latin = models.CharField(max_length=500, null=True, blank=True)
    avalibale_funds_q_farsi = models.CharField(max_length=500, null=True, blank=True)
    
    edu_subject = models.BooleanField(default=False)
    edu_subject_q_latin = models.CharField(max_length=500, null=True, blank=True)
    edu_subject_q_farsi = models.CharField(max_length=500, null=True, blank=True)
    
    marital = models.BooleanField(default=False)
    marital_q_latin = models.CharField(max_length=500, null=True, blank=True)
    marital_q_farsi = models.CharField(max_length=500, null=True, blank=True)
    
    family = models.BooleanField(default=False)
    family_q_latin = models.CharField(max_length=500, null=True, blank=True)
    family_q_farsi = models.CharField(max_length=500, null=True, blank=True)
    
    visa_refesal = models.BooleanField(default=False)
    visa_refesal_q_latin = models.CharField(max_length=500, null=True, blank=True)
    visa_refesal_q_farsi = models.CharField(max_length=500, null=True, blank=True)
    
    travel_history = models.BooleanField(default=False)
    travel_history_q_latin = models.CharField(max_length=500, null=True, blank=True)
    travel_history_q_farsi = models.CharField(max_length=500, null=True, blank=True)

    banner = models.FileField(upload_to=get_file_evaluation_banner, null=True, blank=True)
    def __str__(self):
        return str(self.evaluation)


class ConsultWizardConfig(models.Model):
    icon1 = models.ImageField(null=True, blank=True)
    text1_latin = models.CharField(max_length=300, null=True, blank=True)
    text1_farsi = models.CharField(max_length=300, null=True, blank=True)
    
    icon2 = models.ImageField(null=True, blank=True)
    text2_latin = models.CharField(max_length=300, null=True, blank=True)
    text2_farsi = models.CharField(max_length=300, null=True, blank=True)
    
    icon3 = models.ImageField(null=True, blank=True)
    text3_latin = models.CharField(max_length=300, null=True, blank=True)
    text3_farsi = models.CharField(max_length=300, null=True, blank=True)
    
    icon4 = models.ImageField(null=True, blank=True)
    text4_latin = models.CharField(max_length=300, null=True, blank=True)
    text4_farsi = models.CharField(max_length=300, null=True, blank=True)
    
    icon5 = models.ImageField(null=True, blank=True)
    text5_latin = models.CharField(max_length=300, null=True, blank=True)
    text5_farsi = models.CharField(max_length=300, null=True, blank=True)
    
    step1_desc_latin = models.CharField(max_length=300, null=True, blank=True)
    step1_desc_farsi = models.CharField(max_length=300, null=True, blank=True)
    
    step2_click_latin = models.CharField(max_length=300, null=True, blank=True)
    step2_click_farsi = models.CharField(max_length=300, null=True, blank=True)
    step2_consult_time_latin = models.CharField(max_length=300, null=True, blank=True)
    step2_consult_time_farsi = models.CharField(max_length=300, null=True, blank=True)
    
    step3_declaration_latin = models.CharField(max_length=300, null=True, blank=True)
    step3_declaration_farsi = models.CharField(max_length=300, null=True, blank=True)
    step3_point1_latin = models.CharField(max_length=300, null=True, blank=True)
    step3_point1_farsi = models.CharField(max_length=300, null=True, blank=True)
    step3_point2_latin = models.CharField(max_length=300, null=True, blank=True)
    step3_point2_farsi = models.CharField(max_length=300, null=True, blank=True)
    step3_point3_latin = models.CharField(max_length=300, null=True, blank=True)
    step3_point3_farsi = models.CharField(max_length=300, null=True, blank=True)
    step3_point4_latin = models.CharField(max_length=300, null=True, blank=True)
    step3_point4_farsi = models.CharField(max_length=300, null=True, blank=True)
    step3_point5_latin = models.CharField(max_length=300, null=True, blank=True)
    step3_point5_farsi = models.CharField(max_length=300, null=True, blank=True)
    step3_point6_latin = models.CharField(max_length=300, null=True, blank=True)
    step3_point6_farsi = models.CharField(max_length=300, null=True, blank=True)




class BiometricConfig(models.Model):
    visa_country = models.ForeignKey('Raw_root.Country', on_delete=models.CASCADE, related_name='bio_visa_country')
    bio_in_country = models.ForeignKey('Raw_root.Country', on_delete=models.CASCADE, related_name='bio_bio_in_country')
    center = models.ForeignKey('Content.Centers', on_delete=models.CASCADE)
    banner = models.ImageField(null=True, blank=True)
    title_latin = models.CharField(max_length=300, null=True, blank=True)
    title_farsi = models.CharField(max_length=300, null=True, blank=True)
    duration_latin = models.CharField(max_length=300, null=True, blank=True)
    duration_farsi = models.CharField(max_length=300, null=True, blank=True)
    service_category = models.ForeignKey('Services.ServiceCategory', on_delete=models.CASCADE)
    documents = models.ManyToManyField('Raw_root.Document', blank=True)
    tour_services = models.ManyToManyField('Raw_root.Tourservice', blank=True)
    booking_condition = models.ForeignKey('Raw_root.BookingCondition', on_delete=models.CASCADE, null=True, blank=True)
    transfer_company1 = models.ForeignKey('Raw_root.TransCo', on_delete=models.CASCADE, related_name='transfer_company1')
    transfer_company1 = models.ForeignKey('Raw_root.TransCo', on_delete=models.CASCADE, related_name='transfer_company1')
    departure_flight1_number = models.CharField(max_length=50, null=True, blank=True)
    departure_flight1_time = models.CharField(max_length=50, null=True, blank=True)
    return_flight1_number = models.CharField(max_length=50, null=True, blank=True)
    return_flight1_time = models.CharField(max_length=50, null=True, blank=True)
    transfer_company2 = models.ForeignKey('Raw_root.TransCo', on_delete=models.CASCADE, related_name='transfer_company2')
    departure_flight2_number = models.CharField(max_length=50, null=True, blank=True)
    departure_flight2_time = models.CharField(max_length=50, null=True, blank=True)
    return_flight2_number = models.CharField(max_length=50, null=True, blank=True)
    return_flight2_time = models.CharField(max_length=50, null=True, blank=True)    
    hotel = models.ManyToManyField('Raw_root.Hotel', blank=True)
    def __str__(self):
        return f"{self.title_latin} - {self.visa_country} - {self.center}"
