import datetime

from rest_framework.generics import ListAPIView

from HomePageConfig.models import AppStatusBlock, \
    CheckStatus, ImmigrationSectionConfig, \
    InternationalService, BlockOutnboundTour, \
    QuickAccessBlock, BlockTour, UpCommingTour, UpCommingTourDom
from Organization.models import ExRate
from PublicPages.models import FooterHeaderSrc, MenuIcon, MenuColumnService, \
    HomePageAttentionBoard
from PublicPages.models import HeaderEmergencyNote, BackLink, MenuTours
from Raw_root.models import EventCat
from Services.models import ServiceCategory, ServiceCatFeature
from Tour.models import Tour, DayTour
from django.utils import timezone
from rest_framework.response import Response
from rest_framework.views import APIView
from HomePageConfig.serializer import FooterHeaderSrcSerializer, ImmigrationSectionConfigSerializer

from PublicPages.serializers import MenuColumnServiceDetailsSerializer
from rest_framework import generics

class HomePageAPIView(APIView):
    def get(self, request):

        dic = {}
        dt = timezone.now()
        
        up_comming_tour = UpCommingTour.objects.select_related(
                'tour_1', 'currency_1',
                'tour_2', 'currency_2',
                'tour_3', 'currency_3',
                'tour_4', 'currency_4',
            ).values('tour_1__id','tour_2__id','tour_3__id','tour_4__id',
                'tour_1__title_latin', 'tour_1__title_farsi',
                'currency_1__currency_latin', 'currency_1__currency_farsi',
                'tour_2__title_latin', 'tour_2__title_farsi',
                'currency_2__currency_latin', 'currency_2__currency_farsi',
                'tour_3__title_latin', 'tour_3__title_farsi',
                'currency_3__currency_latin', 'currency_3__currency_farsi',
                'tour_4__title_latin', 'tour_4__title_farsi',
                'currency_4__currency_latin', 'currency_4__currency_farsi',
                'thumbnail_1', 'thumbnail_2', 'thumbnail_3', 'thumbnail_4',
                'title_latin_1', 'title_latin_2', 'title_latin_3', 'title_latin_4',
                'title_farsi_1', 'title_farsi_2', 'title_farsi_3', 'title_farsi_4',
                'text_latin_1', 'text_latin_2', 'text_latin_3', 'text_latin_4',
                'text_farsi_1', 'text_farsi_2', 'text_farsi_3', 'text_farsi_4',
                'price_1', 'price_2', 'price_3', 'price_4'
            )
        dic["upcomming_tour"] = up_comming_tour



        up_comming_tour_dom = UpCommingTourDom.objects.select_related(
                'tour_1', 'currency_1',
                'tour_2', 'currency_2',
                'tour_3', 'currency_3',
                'tour_4', 'currency_4',
            ).values('tour_1__id','tour_2__id','tour_3__id','tour_4__id',
                'tour_1__title_latin', 'tour_1__title_farsi',
                'currency_1__currency_latin', 'currency_1__currency_farsi',
                'tour_2__title_latin', 'tour_2__title_farsi',
                'currency_2__currency_latin', 'currency_2__currency_farsi',
                'tour_3__title_latin', 'tour_3__title_farsi',
                'currency_3__currency_latin', 'currency_3__currency_farsi',
                'tour_4__title_latin', 'tour_4__title_farsi',
                'currency_4__currency_latin', 'currency_4__currency_farsi',
                'thumbnail_1', 'thumbnail_2', 'thumbnail_3', 'thumbnail_4',
                'title_latin_1', 'title_latin_2', 'title_latin_3', 'title_latin_4',
                'title_farsi_1', 'title_farsi_2', 'title_farsi_3', 'title_farsi_4',
                'text_latin_1', 'text_latin_2', 'text_latin_3', 'text_latin_4',
                'text_farsi_1', 'text_farsi_2', 'text_farsi_3', 'text_farsi_4',
                'price_1', 'price_2', 'price_3', 'price_4'
            )
        dic["upcomming_tour_dom"] = up_comming_tour_dom

        attention_baord = HomePageAttentionBoard.objects.all().values()
        dic["attention_baord"] = attention_baord

        
        back_link = BackLink.objects.all().values()
        dic["back_link"] = back_link

        try:
            block_tour_data = BlockTour.objects.values().latest('id')
            dic["block_tour_data"] = block_tour_data
        except:
            dic["block_tour_data"] = []

        try:
            app_status_block = AppStatusBlock.objects.filter(app_status_block_activate=True).values()
            dic["app_status_block"] = app_status_block
        except:
            dic["app_status_block"] = []



        try:
            check_status = CheckStatus.objects.filter(check_status_activate=True).values().latest('id')
            dic["check_status"] = check_status
        except:
            dic["check_status"] = []

        try:
            block_tour = BlockOutnboundTour.objects.filter(activate=True).values().latest('id')
            dic["block_tour"] = block_tour
        except:
            dic["block_tour"] = []


        # try:
        #     asia_and_middle_east = AsiaAndMiddleEastVisas.objects.filter(activate=True).values().latest('id')
        #     dic["asia_and_middle_east"] = list(asia_and_middle_east)
        # except :
        #     dic["asia_and_middle_east"] = []

        # try:
        #     block_visa_chance = BlockVisaChance.objects.filter(activate=True).values().latest('id')
        #     dic["block_visa_chance"] = block_visa_chance
        # except:
        #     dic["block_visa_chance"] = []

        try:
            quick_access_block = QuickAccessBlock.objects.all().values()
            dic["quick_access_block"] = quick_access_block
        except:
            dic["quick_access_block"] = []


        count = 0
        dic['InternationalService'] = {}
        inter_service = InternationalService.objects.filter(international_service_activate=True).values()
        for datas in inter_service:
            count += 1
            block1 = datas['block_1_id']
            block2 = datas['block_2_id']
            block3 = datas['block_3_id']
            block4 = datas['block_4_id']
            i_block1 = \
                ServiceCategory.objects.filter(pk=block1).values('id', 'title_latin', 'title_farsi', 'fee',
                                                                 'country__name_latin',
                                                                 'country__name_farsi', 'currency_id')[0]
            try:
                xrate1 = ExRate.objects.filter(currency_1=i_block1['currency_id']).values().latest('id')
                FEE = xrate1['x_amount_in_irr_1'] * i_block1['fee']
                i_block1['fee'] = FEE
            except:
                e1 = 2
            try:
                xrate2 = ExRate.objects.filter(currency_2=i_block1['currency_id']).values().latest('id')
                FEE = xrate2['x_amount_in_irr_2'] * i_block1['fee']
                i_block1['fee'] = FEE
            except:
                e1 = 1
            try:
                xrate3 = ExRate.objects.filter(currency_3=i_block1['currency_id']).values().latest('id')
                FEE = xrate3['x_amount_in_irr_3'] * i_block1['fee']
                i_block1['fee'] = FEE
            except:
                e1 = 2
            try:
                xrate4 = ExRate.objects.filter(currency_4=i_block1['currency_id']).values().latest('id')
                FEE = xrate4['x_amount_in_irr_4'] * i_block1['fee']
                i_block1['fee'] = FEE
            except:
                e1 = 1
            try:
                xrate5 = ExRate.objects.filter(currency_5=i_block1['currency_id']).values().latest('id')
                FEE = xrate5['x_amount_in_irr_5'] * i_block1['fee']
                i_block1['fee'] = FEE
            except:
                e1 = 1

            i_block1['service_cat_feature'] = ServiceCatFeature.objects.filter(service_category=block1).values()

            i_block2 = \
                ServiceCategory.objects.filter(pk=block2).values('id', 'title_latin', 'title_farsi', 'fee',
                                                                 'country__name_latin',
                                                                 'country__name_farsi', 'currency_id')[0]
            try:
                xrate1 = ExRate.objects.filter(currency_1=i_block2['currency_id']).values().latest('id')
                FEE = xrate1['x_amount_in_irr_1'] * i_block2['fee']
                i_block2['fee'] = FEE
            except:
                e1 = 2
            try:
                xrate2 = ExRate.objects.filter(currency_2=i_block2['currency_id']).values().latest('id')
                FEE = xrate2['x_amount_in_irr_2'] * i_block2['fee']
                i_block2['fee'] = FEE
            except:
                e1 = 1
            try:
                xrate3 = ExRate.objects.filter(currency_3=i_block2['currency_id']).values().latest('id')
                FEE = xrate3['x_amount_in_irr_3'] * i_block2['fee']
                i_block2['fee'] = FEE
            except:
                e1 = 2
            try:
                xrate4 = ExRate.objects.filter(currency_4=i_block2['currency_id']).values().latest('id')
                FEE = xrate4['x_amount_in_irr_4'] * i_block2['fee']
                i_block2['fee'] = FEE
            except:
                e1 = 1
            try:
                xrate5 = ExRate.objects.filter(currency_5=i_block2['currency_id']).values().latest('id')
                FEE = xrate5['x_amount_in_irr_5'] * i_block2['fee']
                i_block2['fee'] = FEE
            except:
                e1 = 1

            i_block2['service_cat_feature'] = ServiceCatFeature.objects.filter(service_category=block2).values()
            i_block3 = \
                ServiceCategory.objects.filter(pk=block3).values('id', 'title_latin', 'title_farsi', 'fee',
                                                                 'country__name_latin',
                                                                 'country__name_farsi', 'currency_id')[0]
            try:
                xrate1 = ExRate.objects.filter(currency_1=i_block3['currency_id']).values().latest('id')
                FEE = xrate1['x_amount_in_irr_1'] * i_block3['fee']
                i_block3['fee'] = FEE
            except:
                e1 = 2
            try:
                xrate2 = ExRate.objects.filter(currency_2=i_block3['currency_id']).values().latest('id')
                FEE = xrate2['x_amount_in_irr_2'] * i_block3['fee']
                i_block3['fee'] = FEE
            except:
                e1 = 1
            try:
                xrate3 = ExRate.objects.filter(currency_3=i_block3['currency_id']).values().latest('id')
                FEE = xrate3['x_amount_in_irr_3'] * i_block3['fee']
                i_block3['fee'] = FEE
            except:
                e1 = 2
            try:
                xrate4 = ExRate.objects.filter(currency_4=i_block3['currency_id']).values().latest('id')
                FEE = xrate4['x_amount_in_irr_4'] * i_block3['fee']
                i_block3['fee'] = FEE
            except:
                e1 = 1
            try:
                xrate5 = ExRate.objects.filter(currency_5=i_block3['currency_id']).values().latest('id')
                FEE = xrate5['x_amount_in_irr_5'] * i_block3['fee']
                i_block3['fee'] = FEE
            except:
                e1 = 1
            i_block3['service_cat_feature'] = ServiceCatFeature.objects.filter(service_category=block3).values()



           





            i_block4 = \
                ServiceCategory.objects.filter(pk=block4).values('id', 'title_latin', 'title_farsi', 'fee',
                                                                 'country__name_latin',
                                                                 'country__name_farsi', 'currency_id')[0]
            try:
                xrate1 = ExRate.objects.filter(currency_1=i_block4['currency_id']).values().latest('id')
                FEE = xrate1['x_amount_in_irr_1'] * i_block4['fee']
                i_block3['fee'] = FEE
            except:
                e1 = 2
            try:
                xrate2 = ExRate.objects.filter(currency_2=i_block4['currency_id']).values().latest('id')
                FEE = xrate2['x_amount_in_irr_2'] * i_block4['fee']
                i_block4['fee'] = FEE
            except:
                e1 = 1
            try:
                xrate3 = ExRate.objects.filter(currency_3=i_block4['currency_id']).values().latest('id')
                FEE = xrate3['x_amount_in_irr_3'] * i_block4['fee']
                i_block4['fee'] = FEE
            except:
                e1 = 2
            try:
                xrate4 = ExRate.objects.filter(currency_4=i_block4['currency_id']).values().latest('id')
                FEE = xrate4['x_amount_in_irr_4'] * i_block4['fee']
                i_block4['fee'] = FEE
            except:
                e1 = 1
            try:
                xrate5 = ExRate.objects.filter(currency_5=i_block4['currency_id']).values().latest('id')
                FEE = xrate5['x_amount_in_irr_5'] * i_block4['fee']
                i_block4['fee'] = FEE
            except:
                e1 = 1
            i_block4['service_cat_feature'] = ServiceCatFeature.objects.filter(service_category=block4).values()


            dic['InternationalService']['service_category' + str(count)] = [i_block1, i_block2, i_block3,i_block4]


        return Response({"message": "success", "status": "OK", "data": dic}, status=200)


class HeaderFooterAPIView(APIView):
    def get(self, request):
        dic = {}

        footer_header_src = FooterHeaderSrc.objects.all()
        dic["footer_services"] = list(FooterHeaderSrcSerializer(footer_header_src, many=True).data)


        menu_tour = MenuTours.objects.all().values()
        dic["menu_tour"] = list(menu_tour)

        menu_column_service = MenuColumnService.objects.all()
        dic["menu_column_service"] = list(MenuColumnServiceDetailsSerializer(menu_column_service, many=True).data)


        back_link = BackLink.objects.all().values()
        dic["back_link"] = list(back_link)

        menu_icon = MenuIcon.objects.all().values()
        dic["menu_icon"] = list(menu_icon)

        header_emergency_note = HeaderEmergencyNote.objects.all().values()
        dic["header_emergency_note"] = list(header_emergency_note)

        attention_board = HomePageAttentionBoard.objects.all().values()
        dic["attention_board"] = list(attention_board)

        # menu_column_event = EventCat.objects.all().order_by("seq").values('title_latin', 'title_farsi', 'icon', 'id')
        # dic["menu_column_event"] = list(menu_column_event)

        return Response({"message": "success", "status": "OK", "data": dic}, status=200)




class HomePageAPIInternationalServiceView(APIView):
    def get(self, request):

        dic = {}
        dt = timezone.now()


        count = 0
        dic['InternationalService'] = {}
        inter_service = InternationalService.objects.filter(international_service_activate=True).values()
        for datas in inter_service:
            count += 1
            block1 = datas['block_1_id']
            block2 = datas['block_2_id']
            block3 = datas['block_3_id']
            block4 = datas['block_4_id']
            i_block1 = \
                ServiceCategory.objects.filter(pk=block1).values('id', 'title_latin', 'title_farsi', 'fee',
                                                                 'country__name_latin',
                                                                 'country__name_farsi', 'currency_id')[0]
            try:
                xrate1 = ExRate.objects.filter(currency_1=i_block1['currency_id']).values().latest('id')
                FEE = xrate1['x_amount_in_irr_1'] * i_block1['fee']
                i_block1['fee'] = FEE
            except:
                e1 = 2
            try:
                xrate2 = ExRate.objects.filter(currency_2=i_block1['currency_id']).values().latest('id')
                FEE = xrate2['x_amount_in_irr_2'] * i_block1['fee']
                i_block1['fee'] = FEE
            except:
                e1 = 1
            try:
                xrate3 = ExRate.objects.filter(currency_3=i_block1['currency_id']).values().latest('id')
                FEE = xrate3['x_amount_in_irr_3'] * i_block1['fee']
                i_block1['fee'] = FEE
            except:
                e1 = 2
            try:
                xrate4 = ExRate.objects.filter(currency_4=i_block1['currency_id']).values().latest('id')
                FEE = xrate4['x_amount_in_irr_4'] * i_block1['fee']
                i_block1['fee'] = FEE
            except:
                e1 = 1
            try:
                xrate5 = ExRate.objects.filter(currency_5=i_block1['currency_id']).values().latest('id')
                FEE = xrate5['x_amount_in_irr_5'] * i_block1['fee']
                i_block1['fee'] = FEE
            except:
                e1 = 1

            i_block1['service_cat_feature'] = ServiceCatFeature.objects.filter(service_category=block1).values()

            i_block2 = \
                ServiceCategory.objects.filter(pk=block2).values('id', 'title_latin', 'title_farsi', 'fee',
                                                                 'country__name_latin',
                                                                 'country__name_farsi', 'currency_id')[0]
            try:
                xrate1 = ExRate.objects.filter(currency_1=i_block2['currency_id']).values().latest('id')
                FEE = xrate1['x_amount_in_irr_1'] * i_block2['fee']
                i_block2['fee'] = FEE
            except:
                e1 = 2
            try:
                xrate2 = ExRate.objects.filter(currency_2=i_block2['currency_id']).values().latest('id')
                FEE = xrate2['x_amount_in_irr_2'] * i_block2['fee']
                i_block2['fee'] = FEE
            except:
                e1 = 1
            try:
                xrate3 = ExRate.objects.filter(currency_3=i_block2['currency_id']).values().latest('id')
                FEE = xrate3['x_amount_in_irr_3'] * i_block2['fee']
                i_block2['fee'] = FEE
            except:
                e1 = 2
            try:
                xrate4 = ExRate.objects.filter(currency_4=i_block2['currency_id']).values().latest('id')
                FEE = xrate4['x_amount_in_irr_4'] * i_block2['fee']
                i_block2['fee'] = FEE
            except:
                e1 = 1
            try:
                xrate5 = ExRate.objects.filter(currency_5=i_block2['currency_id']).values().latest('id')
                FEE = xrate5['x_amount_in_irr_5'] * i_block2['fee']
                i_block2['fee'] = FEE
            except:
                e1 = 1

            i_block2['service_cat_feature'] = ServiceCatFeature.objects.filter(service_category=block2).values()
            i_block3 = \
                ServiceCategory.objects.filter(pk=block3).values('id', 'title_latin', 'title_farsi', 'fee',
                                                                 'country__name_latin',
                                                                 'country__name_farsi', 'currency_id')[0]
            try:
                xrate1 = ExRate.objects.filter(currency_1=i_block3['currency_id']).values().latest('id')
                FEE = xrate1['x_amount_in_irr_1'] * i_block3['fee']
                i_block3['fee'] = FEE
            except:
                e1 = 2
            try:
                xrate2 = ExRate.objects.filter(currency_2=i_block3['currency_id']).values().latest('id')
                FEE = xrate2['x_amount_in_irr_2'] * i_block3['fee']
                i_block3['fee'] = FEE
            except:
                e1 = 1
            try:
                xrate3 = ExRate.objects.filter(currency_3=i_block3['currency_id']).values().latest('id')
                FEE = xrate3['x_amount_in_irr_3'] * i_block3['fee']
                i_block3['fee'] = FEE
            except:
                e1 = 2
            try:
                xrate4 = ExRate.objects.filter(currency_4=i_block3['currency_id']).values().latest('id')
                FEE = xrate4['x_amount_in_irr_4'] * i_block3['fee']
                i_block3['fee'] = FEE
            except:
                e1 = 1
            try:
                xrate5 = ExRate.objects.filter(currency_5=i_block3['currency_id']).values().latest('id')
                FEE = xrate5['x_amount_in_irr_5'] * i_block3['fee']
                i_block3['fee'] = FEE
            except:
                e1 = 1
            i_block3['service_cat_feature'] = ServiceCatFeature.objects.filter(service_category=block3).values()






            i_block4 = \
                ServiceCategory.objects.filter(pk=block4).values('id', 'title_latin', 'title_farsi', 'fee',
                                                                 'country__name_latin',
                                                                 'country__name_farsi', 'currency_id')[0]
            try:
                xrate1 = ExRate.objects.filter(currency_1=i_block4['currency_id']).values().latest('id')
                FEE = xrate1['x_amount_in_irr_1'] * i_block4['fee']
                i_block3['fee'] = FEE
            except:
                e1 = 2
            try:
                xrate2 = ExRate.objects.filter(currency_2=i_block4['currency_id']).values().latest('id')
                FEE = xrate2['x_amount_in_irr_2'] * i_block4['fee']
                i_block4['fee'] = FEE
            except:
                e1 = 1
            try:
                xrate3 = ExRate.objects.filter(currency_3=i_block4['currency_id']).values().latest('id')
                FEE = xrate3['x_amount_in_irr_3'] * i_block4['fee']
                i_block4['fee'] = FEE
            except:
                e1 = 2
            try:
                xrate4 = ExRate.objects.filter(currency_4=i_block4['currency_id']).values().latest('id')
                FEE = xrate4['x_amount_in_irr_4'] * i_block4['fee']
                i_block4['fee'] = FEE
            except:
                e1 = 1
            try:
                xrate5 = ExRate.objects.filter(currency_5=i_block4['currency_id']).values().latest('id')
                FEE = xrate5['x_amount_in_irr_5'] * i_block4['fee']
                i_block4['fee'] = FEE
            except:
                e1 = 1
            i_block4['service_cat_feature'] = ServiceCatFeature.objects.filter(service_category=block4).values()


            dic['InternationalService']['service_category' + str(count)] = [i_block1, i_block2, i_block3,i_block4]

        return Response({"message": "success", "status": "OK", "data": dic}, status=200)
    
class GenericDataListView(generics.ListAPIView):
    queryset = ImmigrationSectionConfig.objects.all()
    serializer_class = ImmigrationSectionConfigSerializer