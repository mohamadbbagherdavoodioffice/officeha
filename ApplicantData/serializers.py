from rest_framework import serializers
from .models import *




class AppAnsweredDeclarationSerializer(serializers.ModelSerializer):
    class Meta:
        model = AppAnsweredDeclaration
        fields = '__all__'

