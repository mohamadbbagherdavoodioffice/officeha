from django.contrib import admin
from django.urls import path, include, re_path
from rest_framework_jwt.views import obtain_jwt_token, verify_jwt_token
from django.conf import settings
from django.conf.urls.static import static
from rest_framework import permissions
from drf_yasg.views import get_schema_view
from drf_yasg import openapi

schema_view = get_schema_view(
   openapi.Info(
      title="Snippets API",
      default_version='v1',
      description="Test description",
      terms_of_service="https://www.google.com/policies/terms/",
      contact=openapi.Contact(email="contact@snippets.local"),
      license=openapi.License(name="BSD License"),
   ),
   public=True,
   permission_classes=[permissions.AllowAny],
)

API_TITLE = 'Office'
API_DESCRIPTION = 'All of APIs'

urlpatterns = [
    path('api/login', obtain_jwt_token),
    path('api/verify', verify_jwt_token),
    path('admin/', admin.site.urls),
    path('api/home/', include('HomePageConfig.urls')),
    path('api/content/', include('Content.urls')),
    path('api/users/', include('OfficeHauser.urls')),
    path('api/public/', include('PublicPages.urls')),
    path('api/root/', include('Raw_root.urls')),
    path('api/edu/', include('Edu.urls')),
    path('api/inbox/', include('Inbox.urls')),
    path('api/service/', include('Services.urls')),
    path('api/visa/', include('Visa.urls')),
    path('api/appopt/', include('ApplicationOperation.urls')),
    path('api/appdata/', include('ApplicantData.urls')),
    path('api/financial/', include('Financial.urls')),
    path('api/hr/', include('Hr.urls')),
    path('api/tour/', include('Tour.urls')),
    path('api/spec/', include('SpecificPages.urls')),
    path('api/org/', include('Organization.urls')),
    path('summernote/', include('django_summernote.urls')),
    re_path(r'^swagger(?P<format>\.json|\.yaml)$', schema_view.without_ui(cache_timeout=0), name='schema-json'),
   re_path(r'^swagger/$', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
   re_path(r'^redoc/$', schema_view.with_ui('redoc', cache_timeout=0), name='schema-redoc'),
]

urlpatterns = urlpatterns + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
urlpatterns = urlpatterns + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

