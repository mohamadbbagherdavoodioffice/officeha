from rest_framework import serializers

from Content.models import TravelRequirement,TravelRequirementsCat
from Organization.serializers import GuaranteeAmountSerializer
from .models import *
from Raw_root.serializers import AgeRangeSerializer, ConsultationCategorySerializer, ConsultationModeSerializer, HrRequiredTestSerializer, HrPublicTrustSerializer, \
    HrContractModeSerializer, HrPositionSensitivitySerializer

class TravelRequirementsCatSerializer(serializers.ModelSerializer):

    class Meta:
        model = TravelRequirementsCat
        fields = '__all__'

class TravelRequirementSerializer(serializers.ModelSerializer):
    travel_req_cat = TravelRequirementsCatSerializer(required=False, allow_null=True)
    class Meta:
        model = TravelRequirement
        fields = '__all__'


class JobFamilySerializer(serializers.ModelSerializer):
    class Meta:
        model = JobFamily
        fields = '__all__'


class JobCategorySerializer(serializers.ModelSerializer):
    job_fam = JobFamilySerializer()
    class Meta:
        model = JobCategory
        fields = '__all__'


class DutyStationSerializer(serializers.ModelSerializer):
    class Meta:
        model = DutyStation
        fields = '__all__'

class WorkScheduleTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = WorkScheduleType
        fields = '__all__'

class GenderPrioritySerializer(serializers.ModelSerializer):
    class Meta:
        model = GenderPriority
        fields = '__all__'

class JobExpreienceSerializer(serializers.ModelSerializer):
    class Meta:
        model = JobExpreience
        fields = '__all__'

class HrTaskSerializer(serializers.ModelSerializer):
    class Meta:
        model = HrTask
        fields = '__all__'

class HrSkillSerializer(serializers.ModelSerializer):

    class Meta:
        model = HrSkill
        fields = ['id','skill_latin','skill_farsi']

class HrResponsibilitySerializer(serializers.ModelSerializer):
    class Meta:
        model = HrResponsibility
        fields = '__all__'

class HrQualificationSerializer(serializers.ModelSerializer):
    class Meta:
        model = HrQualification
        fields = '__all__'

class JobSerializer(serializers.ModelSerializer):
    job_cat = JobCategorySerializer()
    age_range = AgeRangeSerializer()
    guarantee = GuaranteeAmountSerializer()
    hr_required_test = HrRequiredTestSerializer(many=True)
    public_trust = HrPublicTrustSerializer()
    contract_mode = HrContractModeSerializer()
    duty_station = DutyStationSerializer()
    travel_required = TravelRequirementSerializer()
    work_schedule_type = WorkScheduleTypeSerializer()
    gender_priority = GenderPrioritySerializer()
    job_experience = JobExpreienceSerializer()
    position_sensitivity = HrPositionSensitivitySerializer(many=True)
    job_task = HrTaskSerializer(many=True)
    job_skill = HrSkillSerializer(many=True)
    job_reponsibility = HrResponsibilitySerializer(many=True)
    job_qualification = HrQualificationSerializer(many=True)


    class Meta:
        model = Job
        fields = '__all__'

    def get_job_skill_percent(self, obj):
        job_skill_count = obj.job_skill.count()
        if job_skill_count == 0:
            return 0
        else:
            job_skill_match_count = HrSkill.objects.filter(job__in=[obj]).count()
            job_skill_percent = (job_skill_match_count / job_skill_count) * 100
            return {
                'job_skill_percent': job_skill_percent,
                'job_skill_match_count': job_skill_match_count,
                'job_skill_count': job_skill_count,
            }



class JobSkillSerializer(serializers.ModelSerializer):
    job_skill = HrSkillSerializer(many=True)
    class Meta:
        model = Job
        fields = ['job_skill']
class JobListSerializer(serializers.ModelSerializer):

    class Meta:
        model = Job
        fields = ['id','job_title_latin','job_title_farsi']



class JobAgeSerializer(serializers.ModelSerializer):
    age_range = AgeRangeSerializer()
    class Meta:
        model = Job
        fields = ['age_range']



class JobForSkillsQuerySerializer(serializers.ModelSerializer):
    job_cat = JobCategorySerializer()
    age_range = AgeRangeSerializer()
    # job_skill = HrSkillSerializer(many=True)
   

    class Meta:
        model = Job
        fields = ['id','job_title_latin','job_title_farsi','job_cat','age_range',]

class ConsultTeamSerializer(serializers.ModelSerializer):
    consultation_categories = ConsultationCategorySerializer(many=True)
    consultation_modes = ConsultationModeSerializer()

    class Meta:
        model = ConsultTeam
        fields = ['full_name_latin', 'full_name_farsi', 'consultation_categories', 'consultation_modes']