from rest_framework import serializers
from .models import *


class CountrySerializer(serializers.ModelSerializer):
    class Meta:
        model = Country
        fields = '__all__'

class CountryBreifSerializer(serializers.ModelSerializer):
    class Meta:
        model = Country
        fields = ['flag','name_latin','name_farsi']

class ContactUsTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = ContactUsType
        fields = '__all__'


class TellUsWhatYouThinkCatSerializer(serializers.ModelSerializer):
    class Meta:
        model = TellUsWhatYouThinkCat
        fields = '__all__'


class AgeRangeSerializer(serializers.ModelSerializer):
    class Meta:
        model = AgeRange
        fields = '__all__'


class EduProgramDegreeSerializer(serializers.ModelSerializer):
    class Meta:
        model = EduProgramDegree
        fields = '__all__'


class EduSubjectSerializer(serializers.ModelSerializer):
    class Meta:
        model = EduSubject
        fields = '__all__'


class AvailableFundSerializer(serializers.ModelSerializer):
    class Meta:
        model = AvailableFund
        fields = '__all__'


class ProvinceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Province
        fields = ['id','title_latin','title_farsi']


class FollowUpStatusSerializer(serializers.ModelSerializer):
    class Meta:
        model = FollowUpStatus
        fields = '__all__'


class TserviceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tservice
        fields = '__all__'


class DocumnetSerializer(serializers.ModelSerializer):
    class Meta:
        model = Document
        fields = '__all__'


class TranslationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Translation
        fields = '__all__'


class CurrencySerializer(serializers.ModelSerializer):
    class Meta:
        model = Currency
        fields = '__all__'


class VisaMainCatSerializer(serializers.ModelSerializer):
    class Meta:
        model = VisaMainCat
        fields = '__all__'


class VisaSubCatSerializer(serializers.ModelSerializer):
    class Meta:
        model = VisaSubCat
        fields = '__all__'


class StayTimeCatSerializer(serializers.ModelSerializer):
    class Meta:
        model = StayTimeCat
        fields = '__all__'


class WorkPermitSerializer(serializers.ModelSerializer):
    class Meta:
        model = WorkPermit
        fields = '__all__'


class RefundConditionSerializer(serializers.ModelSerializer):
    class Meta:
        model = RefundCondition
        fields = '__all__'



class ExternalPaymentStatusSerializer(serializers.ModelSerializer):
    class Meta:
        model = ExternalPaymentStatus
        fields = '__all__'


class ApplicationProcessStatusSerializer(serializers.ModelSerializer):
    class Meta:
        model = ApplicationProcessStatus
        fields = '__all__'


class ActionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Action
        fields = '__all__'


class HrRequiredTestSerializer(serializers.ModelSerializer):
    class Meta:
        model = HrRequiredTest
        fields = '__all__'


class HrPublicTrustSerializer(serializers.ModelSerializer):
    class Meta:
        model = HrPublicTrust
        fields = '__all__'


class HrContractModeSerializer(serializers.ModelSerializer):
    class Meta:
        model = HrContractMode
        fields = '__all__'


class HrPositionSensitivitySerializer(serializers.ModelSerializer):
    class Meta:
        model = HrPositionSensitivity
        fields = '__all__'



class EduProgramDegreeSerializer(serializers.ModelSerializer):
    class Meta:
        model = EduProgramDegree
        fields = '__all__'

class EntryTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = EntryType
        fields = '__all__'


class EventCatTourpageConfigListSerializer(serializers.ModelSerializer):
    class Meta:
        model = EventCat
        fields = ['id','title_latin','title_farsi','thumbnail','icon']

class ExpertiseCatSerializer(serializers.ModelSerializer):
    class Meta:
        model = ExpertiseCat
        fields = '__all__'

class ModeOfPaymentSerializer(serializers.ModelSerializer):
    class Meta:
        model = ModeOfPayment
        fields = '__all__'



class ProvidingModeSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProvidingMode
        fields = '__all__'

class DifficultyLevelSerializer(serializers.ModelSerializer):
    class Meta:
        model = DifficultyLevel
        fields = '__all__'


class MealPatternSerializer(serializers.ModelSerializer):
    class Meta:
        model = MealPattern
        fields = '__all__'

class AccomTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = AccomType
        fields = '__all__'

class CancellationRateSerializer(serializers.ModelSerializer):
    class Meta:
        model = CancellationRate
        fields = '__all__'



class VehicleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Vehicle
        fields = '__all__'


class EventCatSerializer(serializers.ModelSerializer):
    class Meta:
        model = EventCat
        fields = [       "id",  "seq",    "title_latin",    "title_farsi",     "icon"]

class TourElementSerializer(serializers.ModelSerializer):
    class Meta:
        model = TourElement
        fields = '__all__'


class TimeFrameSerializer(serializers.ModelSerializer):
    class Meta:
        model = TimeFrame
        fields = '__all__'

class ProvinceByCountrySerializer(serializers.ModelSerializer):
    class Meta:
        model = Province
        fields = '__all__'

class AllowedBaggageSerializer(serializers.ModelSerializer):
    class Meta:
        model = AllowedBaggage
        fields = '__all__'

class ClassRangesSerializer(serializers.ModelSerializer):
    class Meta:
        model = ClassRanges
        fields = '__all__'

class TransModeSerializer(serializers.ModelSerializer):
    class Meta:
        model = TransMode
        fields = '__all__'


class TransCoSerializer(serializers.ModelSerializer):
    class Meta:
        model = TransCo
        fields = '__all__'

class RegionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Region
        fields = '__all__'

class ToPackSerializer(serializers.ModelSerializer):
    class Meta:
        model = ToPack
        fields = '__all__'

class RefundDeatailsConditionSerializer(serializers.ModelSerializer):
    class Meta:
        model = RefundCondition
        fields = '__all__'

class ThingsToDoSerializer(serializers.ModelSerializer):
    class Meta:
        model = ThingsToDo
        fields = '__all__'

class ConsultationModeSerializer(serializers.ModelSerializer):
    class Meta:
        model = ConsultationMode
        fields = '__all__'

class DegreeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Degree
        fields = '__all__'

class MinEmploymentYearSerializer(serializers.ModelSerializer):
    class Meta:
        model = MinEmploymentYear
        fields = '__all__'

class OccupationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Occupation
        fields = '__all__'

class OccupationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Occupation
        fields = '__all__'


class MaritalSerializer(serializers.ModelSerializer):
    class Meta:
        model = Marital
        fields = '__all__'

class OccupationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Occupation
        fields = '__all__'

class LanguageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Language
        fields = '__all__'



class RoomTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = RoomType
        fields = '__all__'


class CountryFlagSerializer(serializers.ModelSerializer):
    class Meta:
        model = Country
        fields = ['id','name_latin', 'name_farsi', 'flag']



class GlobalZoneSerializer(serializers.ModelSerializer):
    class Meta:
        model = GlobalZone
        fields = '__all__'

class RecoveryQuestionsSerializer(serializers.ModelSerializer):
    class Meta:
        model = RecoveryQuestions
        fields = '__all__'

class ConsultationCategorySerializer(serializers.ModelSerializer):
    currency = CurrencySerializer()

    class Meta:
        model = ConsultationCategory
        fields = ['title_latin', 'title_farsi', 'currency', 'fee']


class HeardAboutUsSerializer(serializers.ModelSerializer):
    class Meta:
        model = HeardAboutUs
        fields = '__all__'
    


class HotelSerializer(serializers.ModelSerializer):
    accom_type = AccomTypeSerializer() 
    class Meta:
        model = Hotel
        fields = '__all__'


class HotelDistanceSerializer(serializers.ModelSerializer):
    hotel = HotelSerializer()
    class Meta:
        model = HotelDistance
        fields = '__all__'

class DocumentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Document
        fields = ['id','title_latin','title_farsi']


class TourServiceSerializer(serializers.ModelSerializer):
    class Meta:
        model = TourService
        fields = ['id','title_latin','title_farsi']