from django.db.models import Count
import json

from Content.models import Article
from Hr.models import Job
from PublicPages.models import Hint
from PublicPages.models import StaticText
from PublicPages.serializers import StaticTextSerializer
from Raw_root.models import Degree, Language, EduSubject, Province, AvailableFund, AgeRange, Gpa
from django.db.models import Value
from django.db.models.functions import Concat
from rest_framework import status
from rest_framework.generics import ListAPIView, RetrieveAPIView
from rest_framework.response import Response
from rest_framework.views import APIView

from .serializers import *


class SchoolAPIView(ListAPIView):
    serializer_class = SchoolSerializer
    queryset = School.objects.all()

    def get_queryset(self):
        country_id = self.request.GET.get("country_id")
        queryset = School.objects.filter(country_id=country_id)
        return queryset


class SchoolAlphabetList(ListAPIView):
    serializer_class = SchoolSerializer

    def get_queryset(self):
        name_latin = self.request.GET.get("name_latin")
        queryset = School.objects.filter(name_latin__startswith=name_latin)
        return queryset


class SchoolAllAlphabetList(APIView):
    def get(self, request):
        alphabet = []
        alpha = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't',
                 'u', 'v',
                 'w', 'x', 'y', 'z']
        for x in alpha:
            queryset = School.objects.filter(name_latin__startswith=x)
            if len(queryset) != 0:
                alphabet.append(x)
        return Response(alphabet, status=status.HTTP_200_OK)


class ProgramSchoolList(ListAPIView):
    serializer_class = SchoolSerializer
    queryset = ProgramSchool.objects.all()
    lookup_field = "school"


class ProgramList(APIView):
    serializer_class = ProgramSerializer

    def get(self, request):
        latin = list(Program.objects.all().order_by('name_latin').values('name_latin', 'id'))
        farsi = list(Program.objects.all().order_by('name_farsi').values('name_farsi', 'id'))
        all = {'latin': latin, 'farsi': farsi}
        # print(all)
        return Response({"message": "success", "status": "OK", "data": all}, status=200)


class ProgramQueryList(APIView):
    serializer_class = ProgramSerializer

    def get(self, request):
        query = self.request.GET.get("query")
        queryset = ProgramSchool.objects.filter(
            Q(program__name_latin__icontains=query) | Q(program__name_farsi__icontains=query)).values(
            'program__name_latin', 'program__name_farsi'
            , 'school__name_latin', 'school__name_farsi'
            , 'teaching_lang_1__title_latin',
            'teaching_lang_1__title_farsi'
            , 'teaching_lang_2__title_latin',
            'teaching_lang_2__title_farsi'
            , 'tuition', 'duration'
            , 'currency__currency_latin', 'currency__currency_farsi'
            ).annotate(
            date_1=Concat('month_1', Value(' '), 'days_1')
            , date_2=Concat('month_2', Value(' '), 'days_2')
            , date_3=Concat('month_3', Value(' '), 'days_3')
            , date_4=Concat('month_4', Value(' '), 'days_4')
            , date_5=Concat('month_5', Value(' '), 'days_5')
            , date_6=Concat('month_6', Value(' '), 'days_6')
        )
        # vector = SearchVector('name_latin', weight='A')  + SearchVector('name_farsi', weight='B')
        # querys = SearchQuery('davoodi')
        # queryset = Program.objects.annotate(
        #     search=vector
        # ).filter(
        #     search=querys
        # )
        # queryset = Program.objects.annotate(rank=SearchRank(vector, querys)).order_by('rank')
        # Post.objects.annotate(search=SearchVector('title', 'overview')).filter(search='vortex')
        # queryset = Program.objects.annotate(
        #     similarity=TrigramSimilarity('name_latin', 'davodi'),
        # ).filter(similarity__gt=0.3).order_by('-similarity')

        # queryset = Program.objects.filter(Q(name_latin__contains = query ) | Q(name_farsi__contains = query)).values_list('id', flat=True)
        # print(list(queryset))

        return Response({"message": "success", "status": "OK", "data": list(queryset)}, status=200)


# class StaticExploreProgramAPIView(ListAPIView):
#     serializer_class = StaticTextSerializer
#     queryset = StaticText.objects.filter(type='Explore programs').values('banner', 'title_latin', 'title_farsi',
#                                                                          'text_latin', 'text_farsi', 'id')


class StaticExploreProgramAPIView(APIView):
    def get(self, request):
        dic = {}
        static_text = StaticText.objects.filter(type='Explore programs').values('banner','title_latin','title_farsi','text_latin','text_farsi','id')
        dic["static_text"] = list(static_text)

        # degree = Degree.objects.all().values('title_latin', 'title_farsi')
        # dic["degree"] = list(degree)
        #
        # language = Language.objects.all().values('title_latin', 'title_farsi')
        # dic["language"] = list(language)
        #
        # edu_subj = EduSubject.objects.all().values('title_latin', 'title_farsi')
        # dic["edu_subj"] = list(edu_subj)
        return Response({"message": "success", "status": "OK", "data": dic}, status=200)

#             # program__edu_subject_id=self.request.GET.get("edu_subject"),
#                                             # teaching_lang_1__in=json.loads(self.request.GET.get("language"))
class QueryProgramSchool(APIView):
    def post(self, request):
        objs = ProgramSchool.objects.filter(program_id=self.request.GET.get("program"),
                                            program__degree__in=json.loads(self.request.GET.get("degree"))
                                            ).values('currency__currency_latin','currency__currency_farsi','teaching_lang_1__title_latin','teaching_lang_1__title_farsi',
                                                     'teaching_lang_2__title_latin','teaching_lang_2__title_farsi')
        return Response({"message": "success", "status": "OK", "data": list(objs)}, status=200)


class ApplyFormsAPIView(APIView):
    def get(self, request):
        dic = {}
        hint = Hint.objects.filter(Q(item="Apply form")).all()

        age_range = AgeRange.objects.all().values()
        dic["age_range"] = list(age_range)

        degree = Degree.objects.all().values('title_latin', 'title_farsi')
        dic["degree"] = list(degree)

        gpa = Gpa.objects.all().values()
        dic["gpa"] = list(gpa)

        edu_subj = EduSubject.objects.all().values('title_latin', 'title_farsi')
        dic["edu_subj"] = list(edu_subj)

        available_fund = AvailableFund.objects.all().values()
        dic["available_fund"] = list(available_fund)

        province = Province.objects.filter(country__name_latin="iran").all().values()
        dic["province"] = list(province)

        return Response({"message": "success", "status": "OK", "data": dic}, status=200)


class SchoolDetailsIDAPIView(APIView):
    def get(self, request):
        dic = {}
        id = self.request.GET.get("id")
        school = School.objects.filter(id=id).values('country__name_latin', 'country__name_farsi',
                                                     'school_banner_image')
        dic["school"] = list(school)

        program_school = ProgramSchool.objects.filter(school_id=id).values('program__name_latin', 'program__name_farsi',
                                                                           'duration', 'teaching_lang_1__title_latin',
                                                                           'teaching_lang_1__title_farsi'
                                                                           , 'teaching_lang_2__title_latin',
                                                                           'teaching_lang_2__title_farsi'
                                                                           ).annotate(
            date_1=Concat('month_1', Value(' '), 'days_1')
            , date_2=Concat('month_2', Value(' '), 'days_2')
            , date_3=Concat('month_3', Value(' '), 'days_3')
            , date_4=Concat('month_4', Value(' '), 'days_4')
            , date_5=Concat('month_5', Value(' '), 'days_5')
            , date_6=Concat('month_6', Value(' '), 'days_6')

        )
        dic["program_school"] = list(program_school)
        article = Article.objects.filter(school__in=[id]).values('title_latin', 'title_farsi',
                                                                 'by_staff_user__first_name',
                                                                 'by_staff_user__last_name', 'banner')
        dic["article"] = list(article)
        return Response({"message": "success", "status": "OK", "data": dic}, status=200)

class ProgramRetrieveAPIView(RetrieveAPIView):
    serializer_class = ProgramRetreiveSerializer
    queryset = Program.objects.all()
    lookup_url_kwarg = "program_id"



class ProgramSchoolQueryList(APIView):
    serializer_class = ProgramSerializer

    def post(self, request):
        query = self.request.GET.get("program_id")
        queryset = ProgramSchool.objects.filter(
            Q(program_id=query) ).values('school_id',
            'program__name_latin', 'program__name_farsi'
            , 'school__name_latin', 'school__name_farsi'    )

        # queryset = ProgramSchool.objects.filter(
        #     Q(program_id=query)).values('school_id',
        #                                 'program__name_latin', 'program__name_farsi'
        #                                 , 'school__name_latin', 'school__name_farsi'
        #                                 , 'teaching_lang_1__title_latin',
        #                                 'teaching_lang_1__title_farsi'
        #                                 , 'teaching_lang_2__title_latin',
        #                                 'teaching_lang_2__title_farsi'
        #                                 , 'tuition', 'duration'
        #                                 , 'currency__currency_latin', 'currency__currency_farsi',
        #                                 ).annotate(
        #     date_1=Concat('month_1', Value(' '), 'days_1')
        #     , date_2=Concat('month_2', Value(' '), 'days_2')
        #     , date_3=Concat('month_3', Value(' '), 'days_3')
        #     , date_4=Concat('month_4', Value(' '), 'days_4')
        #     , date_5=Concat('month_5', Value(' '), 'days_5')
        #     , date_6=Concat('month_6', Value(' '), 'days_6')
        # )
        return Response({"message": "success", "status": "OK", "data": list(queryset)}, status=200)





class CountryGetListForApplySchool(APIView):
    def get(self, request):
        dic = {}

        country = School.objects.annotate(num_offerings=Count('id')).values('country__name_latin',
                                                                                    'country__name_farsi',
                                                                                    'country__id').distinct()
        dic["country"] = list(country)

        return Response({"message": "success", "status": "OK", "data": dic}, status=200)


class SchoolListSkillMatch(APIView):
    def get(self, request):
        dic = {}
        skill=[]
        query = Q()
        ager = self.request.GET.get("age_range")
        jobf = self.request.GET.get("job_family")
        jobs = self.request.GET.get("job_skill")

        jobs = [int(i) for i in jobs.split(',')]
        print(jobs,"#@3432423")
        if self.request.GET.get('gender'):
            if int(self.request.GET.get('gender'))<3:
                query &= Q(gender_priority__gender__id=self.request.GET.get('gender'))
                skill = Job.objects.filter(age_range__id=ager).filter(job_cat__job_fam=jobf).\
                    filter(job_skill__in=jobs).annotate(entries=Count('job_title_latin')).values('id','job_title_latin','job_title_farsi','job_code','job_cat__title_latin','job_cat__title_farsi'
                    ,'duty_station__title_latin','duty_station__title_farsi','expiry_date','entries')
            else:
                skill = Job.objects.filter(age_range=ager).filter(job_cat__job_fam=jobf).\
                    filter(job_skill__in=jobs).annotate(num_products=Count('job_title_latin')).values('id','job_title_latin','job_title_farsi','job_code','job_cat__title_latin','job_cat__title_farsi'
                    ,'duty_station__title_latin','duty_station__title_farsi','expiry_date','entries')

        if len(skill)>=1:
            import pandas as pd
            df = pd.DataFrame(skill)

            grouped = df.groupby(['job_title_latin','job_title_farsi','job_code','job_cat__title_latin','job_cat__title_farsi'
                        ,'duty_station__title_latin','duty_station__title_farsi','expiry_date'],as_index = True)['entries']\
                .count()
            # grouped=df.groupby(['job_title_latin','job_title_farsi','job_code','job_cat__title_latin','job_cat__title_farsi'
            #             ,'duty_station__title_latin','duty_station__title_farsi','expiry_date'],as_index = False)['entries'].agg('mean')



            skill =grouped.reset_index().to_dict('records')
            for data in skill:
                data['percent']= round(100*data['entries']/len(jobs))
            dic=data
        else:
            dic=[]

        return Response({"message": "success", "status": "OK", "data": dic}, status=200)

