from django.urls import path, include, re_path
from .views import *
from django.conf import settings
from django.conf.urls.static import static
from rest_framework.schemas import get_schema_view
from rest_framework_simplejwt.views import TokenVerifyView

from rest_framework_simplejwt.views import (
    TokenObtainPairView,
    TokenRefreshView,
)

from .views import  ChangePasswordView, OfficeUserGetInfo, OfficeUserGetUserInfo , OfficeUserCreateViewSet

urlpatterns = [
    path('user_subscribe', UserSubscribeGeneric.as_view()),
    re_path(r'^user_subscribe/(?P<pk>\d+)$', UserSubscribeDeleteGeneric.as_view()),

    path('', OfficeUserCreateViewSet.as_view()),
    path('account/user-info', OfficeUserGetUserInfo.as_view()),
    
    path('account/verify_account', verify_account),

    path('account/token/refresh', TokenRefreshView.as_view(), name='token_refresh'),
    path('account/token/verify', TokenVerifyView.as_view(), name='token_verify'),
    
    path('account/change_password', ChangePasswordView.as_view(), name='auth_change_password'),
    # path('account/logout', LogoutAPIView.as_view(), name='auth_logout'),
    path('account/login', TokenObtainPairView.as_view(), name='token_obtain_pair'),

]

urlpatterns = urlpatterns + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
