import datetime
import datetime
import random

from django.db import models
from django.utils.crypto import get_random_string
from persiantools.jdatetime import JalaliDateTime


class ApplicationProfile(models.Model):
    APPLICATION_MAIN_STATUS = (
        ('Questionnaire', 'Questionnaire'),
        ('Visa result', 'Visa result'),
        ('Applicants', 'Applicants'),
        ('Checklist', 'Checklist'),
        ('Pending', 'Pending'),
        ('Done', 'Done'),
        ('Expired', 'Expired'),
        ('Revoked', 'Revoked'),
    )
    ARCHIVED_DATE_BY_APPLICANT = (
        ('Done', 'Done'),
        ('Expired', 'Expired'),
        ('Revoked', 'Revoked'),

    )
    FUS = (
        ('New', 'New'),
        ('Called', 'Called'),
        ('unsuccessful', 'unsuccessful'),
        ('interested', 'interested'),
        ('meeting', 'meeting'),
        ('confirmed', 'confirmed'),
    )

    def create_new_ref_number():
        not_unique = True
        while not_unique:
            now = datetime.datetime.now()
            alpha = ['A', 'C', 'D', 'E', 'F', 'G', 'H', 'J', 'K', 'L', 'M', 'N', 'Q', 'S', 'T',
                     'U', 'V', 'W', 'X', 'Y', 'Z']
            raalpha = ['Q', 'E', 'R', 'T', 'U', 'O', 'P', 'A', 'D', 'F', 'H', 'J', 'K', 'Z', 'X', 'V', 'B',
                       'N', ]
            number = ['1', '2', '3', '4', '5', '6', '7', '8', '9']
            al1 = random.randint(0, len(alpha) - 1)
            nu1 = random.randint(0, len(number) - 1)
            al2 = random.randint(0, len(alpha) - 1)
            nu2 = random.randint(0, len(number) - 1)
            al3 = random.randint(0, len(alpha) - 1)
            nu3 = random.randint(0, len(number) - 1)
            al4 = random.randint(0, len(alpha) - 1)
            al5 = random.randint(0, len(alpha) - 1)
            nu4 = random.randint(0, len(number) - 1)
            ra1 = random.randint(0, len(raalpha) - 1)
            ra2 = random.randint(0, len(raalpha) - 1)

            unique_ref = str(now.year) + str(now.strftime("%B")[0]) + str(alpha[al1]) \
                         + alpha[al2] + number[nu1] + alpha[al3] \
                         + alpha[al4] + alpha[al5] + number[nu2] \
                         + number[nu3] + raalpha[ra1] + raalpha[ra2]

            if not ApplicationProfile.objects.filter(application_number=unique_ref):
                not_unique = False
        return str(unique_ref)

    application_number = models.CharField(
        max_length=20,
        blank=True,
        editable=False,
        unique=True,
        default=create_new_ref_number
    )
    date_of_creation = models.DateField(auto_now_add=True)
    service_category = models.ForeignKey('Services.ServiceCategory', on_delete=models.CASCADE, null=True, blank=True)
    sub_service_cat = models.ForeignKey('Services.SubServiceCategory', on_delete=models.CASCADE, null=True, blank=True)
    stid = models.ForeignKey('Visa.VisaStream', on_delete=models.PROTECT, null=True, blank=True,
                             related_name='application_profile_stid_set')
    sub_stid = models.ForeignKey('Visa.VisaStream', on_delete=models.PROTECT, null=True, blank=True,
                                 related_name='application_profile_sub_stid_set')
    tour = models.ForeignKey('Tour.Tour', on_delete=models.PROTECT, null=True, blank=True, default=None)
    creator_user = models.ForeignKey('OfficeHauser.OfficeUser', on_delete=models.PROTECT,
                                     related_name='application_profile_creator_user_set', null=True, blank=True)
    delegate_first_name = models.CharField(max_length=300, null=True, blank=True)
    delegate_last_name = models.CharField(max_length=300, null=True, blank=True)
    delegate_tel = models.CharField(max_length=300, null=True, blank=True)
    delegate_land_line = models.CharField(max_length=300, null=True, blank=True)
    delegate_nid = models.CharField(max_length=10, null=True, blank=True)

    assign_staff = models.ForeignKey('OfficeHauser.OfficeUser', on_delete=models.PROTECT,
                                     related_name='application_profile_assign_staff_set', null=True, blank=True)
    group_size = models.CharField(max_length=40, choices=(('1', '1'), ('2', '2'), ('3', '3'), ('4', '4'), ('5', '5')),
                                  default=1, null=True, blank=True)
    application_main_status = models.ForeignKey('Raw_root.AppMainStatus', on_delete=models.PROTECT, null=True,
                                                blank=True, default=None)
    app_processing_status = models.ForeignKey('Raw_root.ApplicationProcessStatus', on_delete=models.PROTECT, null=True,
                                              blank=True)
    follow_up_status = models.ForeignKey("Raw_root.FollowUpStatus", on_delete=models.CASCADE, null=True, blank=True,
                                         default=None)
    action = models.ForeignKey('Raw_root.Action', on_delete=models.CASCADE, null=True, blank=True)
    contract = models.ForeignKey('Organization.ContractProfile', on_delete=models.PROTECT, null=True, blank=True,
                                 default=None)
    contract_number = models.CharField(max_length=40, null=True, blank=True)
    country = models.ForeignKey("Raw_root.Country", on_delete=models.CASCADE, null=True, blank=True, default=None)
    task = models.ForeignKey('Organization.Task', on_delete=models.CASCADE, null=True, blank=True, default=None)
    trace_code = models.CharField(max_length=200, null=True, blank=True)
    accompanying_cell_num = models.CharField(max_length=40, null=True, blank=True)
    accompanying_email = models.EmailField(null=True, blank=True)

    device_type = models.CharField(max_length=300, null=True, blank=True)
    ip = models.CharField(max_length=300, null=True, blank=True)
    # for_immediate_fam = models.BooleanField(default=False, null=True, blank=True)
    archived_date_by_applicant = models.DateTimeField(null=True, blank=True)
    by_staff_user = models.ForeignKey('OfficeHauser.OfficeUser', on_delete=models.PROTECT,
                                      related_name='aaplicant_profile_by_user_set', null=True, blank=True)
    self_note = models.TextField(null=True, blank=True)
    last_update = models.DateTimeField(auto_now=True)
    submit_date_time = models.DateTimeField(null=True, blank=True)

    
    customer_service_type = models.ForeignKey("Raw_root.CustomerServiceType", on_delete=models.CASCADE, null=True, blank=True, default=None)
    relationship = models.ForeignKey("Raw_root.Relationship", on_delete=models.CASCADE, null=True, blank=True, default=None)
    revoke_date  = models.DateField(null=True, blank=True)


    def __str__(self):
        return "%s | %s __ %s , %s" % (
            self.application_number, self.service_category, self.group_size, self.application_main_status)
        # visatitle tourlatin creatoruser full name, assiginfull , application_processing_status


def get_applicant_doc(instance, filename):
    return f"file_applicant_doc/{filename}"


class Applicant(models.Model):
    GENDER_TYPES = (
        ("Male", "Male"),
        ("Female", "Female"),
        ("None", "None")
    )
    # application_profile_id = models.ForeignKey(ApplicationProfile, on_delete=models.CASCADE, null=True, blank=True,
    #                                            related_name="applicant_application")

    first_name = models.CharField(max_length=200, null=True, blank=True)
    last_name = models.CharField(max_length=200, null=True, blank=True)
    date_of_birth = models.DateField(null=True, blank=True)
    nid = models.CharField(max_length=200, null=True, blank=True)
    gender = models.CharField(max_length=10, choices=GENDER_TYPES, null=True, blank=True)
    postal_code = models.CharField(max_length=200, null=True, blank=True)
    dob = models.DateField(null=True, blank=True)
    doc = models.ImageField(upload_to=get_applicant_doc, null=True, blank=True)
    landline = models.CharField(max_length=200, null=True, blank=True)
    cell_number = models.CharField(max_length=200, null=True, blank=True)
    email = models.CharField(max_length=200, null=True, blank=True)
    residential_province = models.CharField(max_length=200, null=True, blank=True)
    residential_city = models.CharField(max_length=200, null=True, blank=True)
    father_name = models.CharField(max_length=200, null=True, blank=True)
    mother_name = models.CharField(max_length=200, null=True, blank=True)
    covert_email = models.CharField(max_length=200, null=True, blank=True)
    covert_password = models.CharField(max_length=200, null=True, blank=True)

    def __str__(self):
        return " %s __ %s , %s __ %s | %s __ %s , %s" % (
             self.first_name, self.last_name, self.nid, self.gender, self.dob, self.cell_number,
            self.residential_province)
        # residential_province


class AppAppointmentProcess(models.Model):
    STS = (('Booked', 'Booked'), ('Canceled', 'Canceled'),)

    # scac = models.ForeignKey('Services.ServiceCatAppointmentCapacity', on_delete=models.CASCADE, null=True, blank=True)
    user = models.ForeignKey('OfficeHauser.OfficeUser', on_delete=models.PROTECT,
                             related_name='app_appointment_user_set', null=True, blank=True)
    application = models.ForeignKey(Applicant, on_delete=models.CASCADE, null=True, blank=True, default=None)
    payment_details = models.CharField(max_length=200, null=True, blank=True)
    status = models.CharField(max_length=40, choices=STS, null=True, blank=True)
    feedback = models.TextField(null=True, blank=True)
    satisfaction_level = models.CharField(max_length=40,
                                          choices=(('1', '1'), ('2', '2'), ('3', '3'), ('4', '4'), ('5', '5')),
                                          default=1)

    def __str__(self):
        return "%s | %s  " % (self.application.first_name, self.status)
        # user full name Scac


def get_external_payment_bank_receipt_file(instance, filename):
    return f"file_external_payment_bank_receipt_file/{filename}"


# class ExternalPayment(models.Model):
#     payment_code = models.CharField(max_length=300, null=True, blank=True)
#     first_name = models.CharField(max_length=300, null=True, blank=True)
#     last_name = models.CharField(max_length=300, null=True, blank=True)
#     nid = models.CharField(max_length=300, null=True, blank=True)
#     tel = models.CharField(max_length=300, null=True, blank=True)
#
#     customer_note = models.TextField(null=True, blank=True)
#     debit_card_number = models.CharField(max_length=16, null=True, blank=True)
#     tour_invoice = models.ForeignKey('Tour.TourInvoice', on_delete=models.CASCADE, null=True, blank=True)
#
#     app_invoices = models.ForeignKey('ApplicationOperation.AppInvoice', on_delete=models.CASCADE, null=True, blank=True)
#     external_payment_status = models.ForeignKey('ApplicationOperation.ExternalPayment', on_delete=models.CASCADE,
#                                                 null=True, blank=True)
#     STS = (('New', 'New'), ('Refused', 'Refused'), ('Approved', 'Approved'),)
#     # status = models.CharField(max_length=40, choices=STS, null=True, blank=True)
#     refused_note = models.TextField(null=True, blank=True)
#     # bank_receipt_file = models.ImageField(upload_to=get_external_payment_bank_receipt_file, null=True, blank=True)
#     action = models.ForeignKey('Raw_root.Action', on_delete=models.CASCADE, null=True, blank=True, default=None)
#
#     def __str__(self):
#         return self.payment_code
#
#     def save(self, *args, **kwargs):
#         not_unique = True
#         while not_unique:
#             uni_random = "X" + str(datetime.datetime.now().year)[2:4] \
#                          + str(self.tel)[-3:] + str(get_random_string(length=6, allowed_chars='123456789'))
#             if not ExternalPayment.objects.filter(payment_code=uni_random):
#                 not_unique = False
#                 self.payment_code = uni_random
#                 super(ExternalPayment, self).save(*args, **kwargs)


class ConsultationProcess(models.Model):
    STATUS = (
        ('Pending', 'Pending'),
        ('PAID', 'PAID'),
        ('Bank failed', 'Bank failed'),
        ('Canceled', 'Canceled'),        
    )
    SATISFICATION_LEVEL = (
        ('1', '1'),
        ('2', '2'),
        ('3', '3'),
        ('4', '4'),
        ('5', '5'),
    )
    status = models.CharField(max_length=13, default='Booked', choices=STATUS, null=True, blank=True)
    # consult id
    # consult_capacity = models.ForeignKey('Services.ConsultCapacity', on_delete=models.PROTECT, null=True, blank=True)
    # time_frame = models.ForeignKey('Raw_root.TimeFrame', on_delete=models.PROTECT, null=True, blank=True)
    user = models.ForeignKey('OfficeHauser.OfficeUser', on_delete=models.PROTECT,
                             related_name='consultation_process_user_set', null=True, blank=True)
    consultation_mode = models.ForeignKey('Raw_root.ConsultationMode', on_delete=models.PROTECT, null=True,
                                          blank=True, )
    consult_date = models.DateField(null=True, blank=True)
    consult_team = models.ForeignKey('Hr.ConsultTeam', on_delete=models.PROTECT, null=True, blank=True, default=None)
    email = models.EmailField(null=True, blank=True)
    evaluation_form_trace_code = models.CharField(max_length=300, null=True, blank=True)
    consultation_category = models.ForeignKey('Raw_root.ConsultationCategory', on_delete=models.PROTECT, null=True, blank=True, default=None)
    
    consult_note = models.TextField(null=True, blank=True)
    
    visa_chance_case_number = models.CharField(max_length=250, blank=True, null=True)
    heard_about_us = models.ForeignKey('Raw_root.HeardAboutUs', on_delete=models.PROTECT, null=True, blank=True, default=None) 
    service_category = models.ForeignKey('Services.ServiceCategory', on_delete=models.CASCADE, null=True, blank=True)
    stid = models.ForeignKey('Visa.VisaStream', on_delete=models.PROTECT, null=True, blank=True, )
    # tour = models.ForeignKey('Tour.Tour', on_delete=models.PROTECT, null=True, blank=True)
    applicant_full_name = models.CharField(max_length=200, null=True, blank=True)
    cell_number = models.CharField(max_length=15, null=True, blank=True)
    age_range = models.ForeignKey('Raw_root.AgeRange', on_delete=models.PROTECT, null=True, blank=True, default=None)
    degree = models.ForeignKey('Raw_root.Degree', on_delete=models.PROTECT, null=True, blank=True, default=None)
    min_employment_year = models.ForeignKey('Raw_root.MinEmploymentYear', on_delete=models.PROTECT, null=True,
                                            blank=True, default=None)
    visit_chance_code = models.CharField(max_length=250, blank=True, null=True)
    occupation = models.ForeignKey('Raw_root.Occupation', on_delete=models.PROTECT, null=True, blank=True, default=None)
    payment_details = models.CharField(max_length=200, null=True, blank=True)
    
    feedback_text = models.TextField(null=True, blank=True)
    satisfaction_level = models.CharField(max_length=13, choices=SATISFICATION_LEVEL, default='1', null=True,
                                          blank=True)
    consult_image = models.ImageField(null=True, blank=True)
    payment_date_time = models.DateTimeField( null=True, blank=True)
    trace_code = models.CharField(max_length=300, null=True, blank=True)
    bank_details = models.TextField(null=True, blank=True)
    visit_id = models.CharField(max_length=250, blank=True, null=True)
    previous_advice = models.BooleanField(default=False)

    def __str__(self):
        return "%s | %s ,%s | %s , %s " % (
            self.application.first_name, self.application.last_name, self.cell_number, self.degree.title_latin,
            self.status)


def get_app_invoice(instance, filename):
    return f"file_app_invoice/{filename}"


def get_file_bank_receipt_file_image(instance, filename):
    return f"file_bank_receipt_file_image/{filename}"


# class AppInvoice(models.Model):
#     # METHOD_OF_PAYMENT = (
#     #     ('No action needed', 'No action needed'),
#     #     ('In-office', 'In-office'),
#     #     ('Online', 'Online'),
#     #     ('Cash', 'Cash'),
#     #     ('POS', 'POS'),
#     #     ('Bank-Transfer', 'Bank-Transfer'),
#     # )
#     #
#     # STATUS = (
#     #     ('Neutral', 'Neutral'),
#     #     ('Pay Queue', 'Pay Queue'),
#     #     ('Wait approval', 'Wait approval'),
#     #     ('PAID', 'PAID'),
#     #     ('Discarded', 'Discarded'),
#     # )
#     #
#     def create_new_ref_number():
#         not_unique = True
#         while not_unique:
#             number = ['1', '2', '3', '4', '5', '6', '7', '8', '9']
#             nu1 = random.randint(0, len(number) - 1)
#             nu2 = random.randint(0, len(number) - 1)
#             nu3 = random.randint(0, len(number) - 1)
#             c = AppInvoice.objects.count()
#
#             rna = 0
#             if int(str(JalaliDateTime.now())[5:7]) <= 6:
#                 rna = random.randint(5, 9)
#             if int(str(JalaliDateTime.now())[5:7]) > 6:
#                 rna = random.randint(0, 5)
#
#             uni_random = "IV" + "USVP" + str(JalaliDateTime.now())[5:7] + number[nu1] + number[nu2] \
#                          + number[nu3] + str(c).zfill(4) + str(rna)
#             if not AppInvoice.objects.filter(invoice_nr=uni_random):
#                 not_unique = False
#         return str(uni_random)
#
#     invoice_nr = models.CharField(max_length=20,
#                                   blank=True,
#                                   editable=False,
#                                   unique=True,
#                                   default=create_new_ref_number)
#     # TRACE_CODE = (
#     #     ('Neutral', 'Neutral'),
#     #     ('Pay Queue', 'Pay Queue'),
#     #     ('Waiting Approval', 'Waiting Approval'),
#     #     ('PAID', 'PAID'),
#     #     ('Discarded', 'Discarded'),
#     # )
#     # applicant = models.ForeignKey(Applicant, on_delete=models.CASCADE, null=True, blank=True, default=None)
#     # instalment = models.ForeignKey('Services.Instalment', on_delete=models.CASCADE, null=True, blank=True)
#     t_service = models.ForeignKey('Raw_root.Tservice', on_delete=models.PROTECT, null=True, blank=True, default=None)
#     # method_of_payment = models.CharField(max_length=40, choices=METHOD_OF_PAYMENT, null=True, blank=True)
#     # status = models.CharField(max_length=40, choices=STATUS, null=True, blank=True)
#     action = models.ForeignKey('Raw_root.Action', on_delete=models.CASCADE, null=True, blank=True, default=None)
#     currency = models.ForeignKey('Raw_root.Currency', on_delete=models.CASCADE, null=True, blank=True, default=None)
#     unit_fee = models.DecimalField(max_digits=10, decimal_places=2, null=True, blank=True)
#     ex_rate = models.ForeignKey('Organization.ExRate', on_delete=models.CASCADE, null=True, blank=True, default=None)
#     irr_amount = models.DecimalField(max_digits=10, decimal_places=2, null=True, blank=True, default=None)
#     vat = models.ForeignKey('Organization.Vat', on_delete=models.PROTECT, null=True, blank=True)
#     quantity = models.IntegerField(choices=((1, 1), (2, 2), (3, 3), (4, 4), (5, 5)), null=True, blank=True)
#     total = models.DecimalField(max_digits=12, decimal_places=2, null=True, blank=True)
#     account_number = models.ForeignKey('Organization.AccountNumber', on_delete=models.CASCADE, null=True, blank=True,
#                                        default=None)
#     pos_bank = models.ForeignKey('Organization.Bank', on_delete=models.CASCADE, null=True, blank=True)
#     payment_date_time = models.DateTimeField(null=True, blank=True)
#     bank_receipt_file_image = models.ImageField(upload_to=get_file_bank_receipt_file_image, null=True, blank=True)
#     # status = models.CharField(max_length=400, choices=TRACE_CODE, null=True, blank=True)
#     # trace_code = models.CharField(max_length=400, choices=TRACE_CODE, null=True, blank=True)
#     # external_payment = models.ForeignKey(ExternalPayment, on_delete=models.CASCADE, null=True, blank=True)
#     discard_date_time = models.DateTimeField(null=True, blank=True)
#     by_staff = models.ForeignKey('OfficeHauser.OfficeUser', on_delete=models.PROTECT,
#                                  related_name='app_invoice_user_set', null=True, blank=True)
#     self_note = models.TextField(null=True, blank=True)
#     last_edit = models.DateTimeField(null=True, blank=True)
#
#     def __str__(self):
#         return "%s | %s , %s  | %s" % (self.invoice_nr, self.method_of_payment, self.status, self.total)


def get_file_reservation_evidence(instance, filename):
    return f"file_reservation_evidence/{filename}"


def get_file_confirmation_evidence(instance, filename):
    return f"file_confirmation_evidence/{filename}"


class AppTourOperation(models.Model):
    tour = models.ForeignKey('Tour.Tour', on_delete=models.PROTECT, null=True, blank=True)
    t_service = models.ForeignKey('Raw_root.Tservice', on_delete=models.PROTECT, null=True, blank=True, default=None)
    region = models.ForeignKey("Raw_root.Region", on_delete=models.CASCADE, null=True, blank=True, default=None)
    due_date_time = models.DateTimeField(null=True, blank=True)
    by_staff_user = models.ForeignKey('OfficeHauser.OfficeUser', on_delete=models.PROTECT,
                                      related_name='app_tour_operation_user_set', null=True, blank=True)
    note = models.TextField(null=True, blank=True)
    app_our_buy_opt = models.ForeignKey('ApplicationOperation.AppOurBuyOpt', on_delete=models.PROTECT, null=True,
                                        blank=True, default=None)
    reservation_evidence = models.TextField(null=True, blank=True)
    reservation_evidence_file = models.ImageField(upload_to=get_file_reservation_evidence, null=True, blank=True)
    done_date_time = models.DateTimeField(null=True, blank=True)
    confirmation_evidence_file = models.ImageField(upload_to=get_file_confirmation_evidence, null=True, blank=True)
    confirmation_evidence = models.TextField(null=True, blank=True)
    status = models.CharField(max_length=30, choices=(
        ('Waiting', 'Waiting'), ('Pending', 'Pending'), ('Done', 'Done'), ('Failed', 'Failed')), null=True, blank=True)


class AppTourSeat(models.Model):
    tour_dept = models.ForeignKey('Tour.TourDepartureFare', on_delete=models.CASCADE, null=True, blank=True,
                                  default=None)
    tkt_number = models.PositiveIntegerField(null=True, blank=True)
    status = models.CharField(max_length=30, choices=(('Vacant', 'Vacant'), ('Reserved', 'Reserved'),
                                                      ('Paid', 'Paid'), ('Canceled', 'Canceled')), null=True,
                              blank=True)
    applicant = models.ForeignKey('ApplicationOperation.Applicant', on_delete=models.CASCADE, null=True, blank=True,
                                  default=None)


def get_file_financial_evidence(instance, filename):
    return f"file_financial_evidence/{filename}"


class AppOurBuyOpt(models.Model):
    REASON = (
        ('TKT', 'TKT'),
        ('Registration', 'Registration'),
        ('Visa', 'Visa'),
        ('Translation', 'Translation'),
        ('Car hire', 'Car hire'),
        ('Visa Pick up', 'Visa Pick up'),
        ('Print', 'Print'),
        ('Stamp', 'Stamp'),
        ('Other', 'Other'),
    )
    TYPE = (
        ('IBAN', 'IBAN'),
        ('CC', 'CC'),
        ('Online', 'Online'),
        ('Cash', 'Cash'),
    )
    tour = models.ForeignKey('Tour.Tour', on_delete=models.PROTECT, null=True, blank=True)
    applicant = models.ForeignKey('ApplicationOperation.Applicant', on_delete=models.CASCADE, null=True, blank=True,
                                  default=None)
    task = models.ForeignKey('Organization.Task', on_delete=models.CASCADE, null=True, blank=True, default=None)
    payment_method = models.CharField(max_length=30, choices=TYPE, null=True, blank=True)
    currency = models.ForeignKey('Raw_root.Currency', on_delete=models.CASCADE, null=True, blank=True, default=None)
    amount = models.DecimalField(max_digits=10, decimal_places=2, null=True, blank=True)
    from_account_number = models.ForeignKey('Organization.AccountNumber', on_delete=models.CASCADE, null=True,
                                            blank=True, default=None)
    financial_evidence = models.ImageField(upload_to=get_file_financial_evidence, null=True, blank=True)
    beneficiary_name = models.CharField(max_length=300, null=True, blank=True)
    beneficiary_business_user = models.ForeignKey('OfficeHauser.OfficeUser', on_delete=models.PROTECT,
                                                  related_name='app_our_by_benefiencery_user_set', null=True,
                                                  blank=True)
    beneficiary_account_iban = models.CharField(max_length=300, null=True, blank=True)
    from_account_bank = models.ForeignKey('Organization.Bank', on_delete=models.CASCADE, null=True, blank=True,
                                          default=None)
    by_staff = models.ForeignKey('OfficeHauser.OfficeUser', on_delete=models.CASCADE, primary_key=False,
                                 related_name='app_our_by_by_user_set', null=True, blank=True)
    self_note = models.TextField(null=True, blank=True)
    last_updated_date = models.DateTimeField(auto_now=True)

    def __str__(self):
        return "%s | %s ,%s | %s , %s  | %s| %s , %s  | %s" % (
            self.application_number, self.application.first_name, self.application.last_name,
            self.task.task_title_latin, self.method_of_payment, self.status,
            self.currency.currency_latin, self.amount, self.beneficiary_name)


class AppDeliveryOpt(models.Model):
    TYPE = (
        ('Pick Up', 'Pick Up'),
        ('Drop', 'Drop'),
    )
    STATUS = (
        ('Waiting', 'Waiting'),
        ('Done', 'Done'),
        ('Not Show', 'Not Show'),
    )

    applicant = models.ForeignKey('ApplicationOperation.Applicant', on_delete=models.CASCADE, null=True, blank=True,
                                  default=None)
    creation_date_time = models.DateTimeField(null=True, blank=True)
    creator_staff = models.ForeignKey('OfficeHauser.OfficeUser', on_delete=models.CASCADE, primary_key=False,
                                      related_name='app_delivery_out_create_user_set', null=True, blank=True)
    type = models.CharField(max_length=30, choices=TYPE, null=True)
    doc = models.ForeignKey('Raw_root.Document', on_delete=models.PROTECT, null=True, blank=True)
    ready_date_time = models.DateTimeField(null=True, blank=True)
    meeting_date_time = models.DateTimeField(null=True, blank=True)
    status = models.CharField(max_length=30, choices=STATUS, null=True, blank=True)
    by_staff_user = models.ForeignKey('OfficeHauser.OfficeUser', on_delete=models.CASCADE, primary_key=False,
                                      related_name='app_delivery_out_by_user_set', null=True, blank=True)
    self_note = models.TextField(null=True, blank=True)
    last_updated_date_time = models.DateTimeField(auto_now=True)

    def __str__(self):
        return "%s | %s ,%s | %s , %s  | %s| %s " % (
            self.application_number, self.application.first_name, self.application.last_name, self.type, self.status,
            self.ready_date_time, self.meeting_date_time)

    delivery_staff = models.ForeignKey('OfficeHauser.OfficeUser', on_delete=models.PROTECT,
                                       related_name='app_delivery_by_user_set', null=True, blank=True)


def get_file_guarantee_evidence(instance, filename):
    return f"file_guarantee_evidence/{filename}"


def get_file_provider_passport(instance, filename):
    return f"ile_filled_from_file/{filename}"


class AppGuaranteeOpt(models.Model):
    TYPE = (
        ('Bank', 'Bank'),
        ('Cheque', 'Cheque'),
    )
    applicant = models.ForeignKey('ApplicationOperation.Applicant', on_delete=models.CASCADE, null=True, blank=True,
                                  default=None)
    create_date_time = models.DateTimeField(null=True, blank=True)
    creator_bussiness_user = models.ForeignKey('OfficeHauser.OfficeUser', on_delete=models.CASCADE, primary_key=False,
                                               related_name='app_guarantee_out_creator_bussiness_user_set', null=True,
                                               blank=True)
    guarantee_evidence = models.ImageField(upload_to=get_file_guarantee_evidence, null=True)
    provider_passport = models.FileField(upload_to=get_file_provider_passport, null=True)
    type = models.CharField(max_length=30, choices=TYPE, null=True)
    guarantee = models.ForeignKey('Organization.GuaranteeAmount', on_delete=models.CASCADE, null=True, blank=True,
                                  default=None)
    STATUS = (
        ('Discarded', 'Discarded'),
        ('Providing', 'Providing'),
        ('Waiting', 'Waiting'))
    status = models.CharField(max_length=30, choices=STATUS, null=True, blank=True)
    date_time = models.DateTimeField(default=0)
    by_staff_user = models.ForeignKey('OfficeHauser.OfficeUser', on_delete=models.CASCADE, primary_key=False,
                                      related_name='app_guarantee_out_by_user_set', null=True, blank=True)
    self_note = models.TextField(null=True, blank=True)
    last_updated_date_time = models.DateTimeField(auto_now=True)


class AppProviderInvoice(models.Model):
    applicant = models.ForeignKey('ApplicationOperation.Applicant', on_delete=models.CASCADE, null=True, blank=True,
                                  default=None)
    create_date_time = models.DateTimeField(null=True, blank=True)
    creator_bussiness_user = models.ForeignKey('OfficeHauser.OfficeUser', on_delete=models.CASCADE, primary_key=False,
                                               related_name='app_provider_creator_bussiness_user_set', null=True,
                                               blank=True)

    unit_fee = models.DecimalField(max_digits=10, decimal_places=2, null=True, blank=True)
    quantity = models.PositiveIntegerField(null=True, blank=True)
    total = models.PositiveIntegerField(null=True, blank=True)
    approval = models.BooleanField(default=False)

    def __str__(self):
        return "%s | %s ,%s | %s , %s  " % (
            self.application_number, self.application.first_name, self.application.last_name, self.total, self.approval)
        #   creator bussiness name


class AppToDoList(models.Model):
    STATUS = (
        ('Waiting', 'Waiting'),
        ('Done', 'Done'),
        ('Discarded', 'Discarded'),
    )
    ACTION_MODALS = (
        ('Upload', 'Upload'),
        ('For information only', 'For information only'),
        ('Visiting notary office', 'Visiting notary office'),
        ('Bring Physically', 'Bring Physically'),
    )
    applicant = models.ForeignKey('ApplicationOperation.Applicant', on_delete=models.CASCADE, null=True, blank=True,
                                  default=None)
    create_date_time = models.DateTimeField(null=True, blank=True)
    creator_staff = models.ForeignKey('OfficeHauser.OfficeUser', on_delete=models.CASCADE,
                                      related_name='app_to_do_list_create_staff_user_set', null=True, blank=True)

    note = models.TextField(null=True, blank=True)
    doc = models.ForeignKey('Raw_root.Document', on_delete=models.CASCADE, null=True, blank=True, default=None)
    status = models.CharField(max_length=40, choices=STATUS, null=True, blank=True)
    action = models.ForeignKey('Raw_root.Action', on_delete=models.CASCADE, null=True, blank=True, default=None)
    action_modals = models.CharField(max_length=30, choices=ACTION_MODALS, null=True, blank=True)


def get_file_applicant_doc(instance, filename):
    return f"file_applicant_doc/{filename}"


class ApplicantDoc(models.Model):
    STATUS = (
        ('Not provided', 'Not provided'),
        ('Received', 'Received'),
    )
    applicant = models.ForeignKey('ApplicationOperation.Applicant', on_delete=models.CASCADE, null=True, blank=True,
                                  default=None)
    to_do = models.ForeignKey(AppToDoList, on_delete=models.CASCADE, null=True, blank=True)
    doc = models.ForeignKey('Raw_root.Document', on_delete=models.CASCADE, null=True, blank=True, default=None)
    file = models.FileField(upload_to=get_file_applicant_doc, null=True, blank=True)
    status = models.CharField(max_length=30, choices=STATUS, null=True, blank=True)


class AppStaffReminderOpt(models.Model):
    LABEL = (
        ('Pending', 'Pending'),
        ('Done', 'Done'),
        ('Failed', 'Failed'),
    )
    applicant = models.ForeignKey('ApplicationOperation.Applicant', on_delete=models.CASCADE, null=True, blank=True,
                                  default=None)
    due_date = models.DateField(null=True, blank=True)
    label = models.CharField(max_length=30, choices=LABEL, null=True, blank=True)
    by_staff = models.OneToOneField('OfficeHauser.OfficeUser', on_delete=models.CASCADE, null=True, blank=True,
                                    related_name='staff_reminder_by_staff_set')
    note = models.TextField(null=True)
    last_updated_date = models.DateTimeField(auto_now=True)


class AppOverviewOpt(models.Model):
    STATUS = (
        ('Editing at our office', 'Editing at our office'),
        ('Editing at third party', 'Editing at third party'),
        ('Pending at our office', 'Pending at our office'),
        ('Pending at third party  Neutral', 'Pending at third party  Neutral'),
        ('Done', 'Done'),
        ('Failed', 'Failed'),
        ('Submitted', 'Submitted'),
        ('Scanning', 'Scanning'),
        ('Sent to third party', 'Sent to third party'),
        ('Docs mailed', 'Docs mailed'),
        ('Docs with Authorities', 'Docs with Authorities'),
        ('Docs. Submitted', 'Docs. Submitted'),
        ('Processing at destination', 'Processing at destination'),
        ('Filling the forms', 'Filling the forms'),
        ('Decision has made', 'Decision has made'),
        ('Result file issued', 'Result file issued'),
        ('Docs. Assessedc', 'Docs. Assessedc'),
        ('Ref. forwarded', 'Ref. forwarded'),
        ('Applicant Registered', 'Applicant Registered'),
        ('Correspondence', 'Correspondence'),
        ('Arranging Online interview', 'Arranging Online interview'),
        ('Remittance the fee', 'Remittance the fee'),
        ('Flight Temp. reserved', 'Flight Temp. reserved'),
        ('Flight ticket issued', 'Flight ticket issued'),
        ('Insurance policy issued', 'Insurance policy issued'),
        ('Bio. Appointment booked', 'Bio. Appointment booked'),
        ('Passport to embassy', 'Passport to embassy'),
        ('Passport at embassy', 'Passport at embassy'),
        ('Passport on return', 'Passport on return'),
        ('Passport at office', 'Passport at office'),
        ('Processing at embassy', 'Processing at embassy'),
        ('Granted', 'Granted'),
        ('Refused', 'Refused'),)

    applicant = models.ForeignKey('ApplicationOperation.Applicant', on_delete=models.CASCADE, null=True, blank=True,
                                  default=None)
    task = models.ForeignKey('Organization.Task', on_delete=models.CASCADE, null=True, blank=True, default=None)
    by_staff = models.ForeignKey('OfficeHauser.OfficeUser', on_delete=models.CASCADE, primary_key=False,
                                 related_name='app_overview_staff_user_set', null=True, blank=True)
    self_note = models.TextField(null=True, blank=True)
    last_updated_date_time = models.DateTimeField(auto_now=True)

    def __str__(self):
        return "%s | %s ,%s | %s " % (
            self.application_number, self.application.first_name, self.application.last_name,
            self.task.task_title_latin)


def get_app_invoice_payment_by_user(instance, filename):
    return f"file_invoice_payment_by_user/{filename}"


class PaymentByUser(models.Model):
    payment_code = models.CharField(max_length=30, null=True, blank=True)
    first_name = models.CharField(max_length=200, null=True, blank=True)
    last_name = models.CharField(max_length=200, null=True, blank=True)
    nid = models.CharField(max_length=200, null=True, blank=True)
    tel = models.CharField(max_length=200, null=True, blank=True)
    note = models.TextField(null=True, blank=True)
    # app_invoice = models.ForeignKey(AppInvoice, on_delete=models.CASCADE, null=True, blank=True, default=None)
    status = models.CharField(max_length=40, choices=(('New', 'New'), ('Approved', 'Approved'), ('Refused', 'Refused')),
                              null=True, blank=True)
    refused_note = models.TextField(null=True, blank=True)
    bank_receipt_file_image = models.ImageField(upload_to=get_app_invoice_payment_by_user, null=True, blank=True)

class AppUserPassOpt(models.Model):
    applicant = models.ForeignKey('ApplicationOperation.Applicant', on_delete=models.CASCADE, null=True, blank=True,
                                  related_name='AppUserPassOpt_applicant_set')
    task = models.ForeignKey('Organization.Task', on_delete=models.CASCADE, null=True, blank=True)
    url = models.URLField(null=True, blank=True)
    user_name = models.CharField(max_length=300, null=True, blank=True)
    password = models.CharField(max_length=300, null=True, blank=True)
    by_staff = models.ForeignKey('OfficeHauser.OfficeUser', on_delete=models.CASCADE, primary_key=False,
                                 related_name='app_our_pass_by_user_staff_set', null=True, blank=True)
    self_note = models.TextField(null=True, blank=True)
    last_updated_date = models.DateTimeField(auto_now=True)

    def __str__(self):
        return "%s | %s | %s , %s" % (
            self.application_number, self.task.task_title_latin, self.applicant.firstname, self.applicant.lastname)


class AppMailServiceOpt(models.Model):
    applicant = models.ForeignKey('ApplicationOperation.Applicant', on_delete=models.CASCADE, null=True, blank=True,
                                  related_name='AppMailServiceOpt_app_set')
    prepared_doc_date = models.DateField(null=True, blank=True)
    send_date = models.DateField(null=True, blank=True)
    arrival_date = models.DateField(null=True, blank=True)
    return_date = models.DateField(null=True, blank=True)
    batch_number = models.CharField(max_length=300, null=True, blank=True)
    trace_code = models.CharField(max_length=300, null=True, blank=True)
    to = models.CharField(max_length=300, null=True, blank=True)
    by_staff = models.ForeignKey('OfficeHauser.OfficeUser', on_delete=models.CASCADE, primary_key=False,
                                 related_name='app_mail_service_staff_opt_set', null=True, blank=True)
    self_note = models.TextField(null=True, blank=True)
    last_updated_date = models.DateTimeField(auto_now=True)

    def __str__(self):
        return "%s | %s | %s , %s , %s" % (
            self.application_number, self.prepared_doc_date, self.send_date, self.arrival_date, self.return_date)


def get_file_result_file(instance, filename):
    return f"file_result_file/{filename}"


class AppResultOpt(models.Model):
    STATUS = (
        ('Pending', 'Pending'),
        ('Issued', 'Issued'),
        ('Granted', 'Granted'),
        ('Refused', 'Refused'),
        ('Discarded', 'Discarded'),
        ('Done', 'Done'),
    )
    applicant = models.ForeignKey('ApplicationOperation.Applicant', on_delete=models.CASCADE, null=True, blank=True,
                                  related_name='AppResultOpt_app_set')
    task = models.ForeignKey('Organization.Task', on_delete=models.CASCADE, null=True, blank=True,
                             related_name='AppResultOpt_task_set')
    title = models.CharField(max_length=200, null=True, blank=True)
    file = models.FileField(upload_to=get_file_result_file, null=True, blank=True)
    status = models.CharField(max_length=30, choices=STATUS, null=True)
    share_with_client = models.BooleanField(default=False)
    by_staff = models.ForeignKey('OfficeHauser.OfficeUser', on_delete=models.CASCADE, primary_key=False,
                                 related_name='app_result_opt_user_staff_set', null=True, blank=True)
    self_note = models.TextField(null=True, blank=True)
    last_updated_date = models.DateTimeField(auto_now=True)

    def __str__(self):
        return "%s | %s | %s , %s | %s" % (
            self.application_number, self.task.task_title_latin, self.applicant.firstname, self.applicant.lastname,
            self.status)


def get_file_bio(instance, filename):
    return f"file_caq_file/{filename}"


class AppBioAppointOpt(models.Model):
    applicant = models.ForeignKey('ApplicationOperation.Applicant', on_delete=models.CASCADE, null=True, blank=True,
                                  related_name='AppBioAppointOpt_applicant_set')
    task = models.ForeignKey('Organization.Task', on_delete=models.CASCADE, null=True, blank=True,
                             related_name='AppBioAppointOpt_task_set')
    appointment_date_time = models.DateTimeField()
    trace_code = models.CharField(max_length=200, null=True, blank=True)
    share_with_client = models.BooleanField(default=False)
    by_staff_user = models.ForeignKey('OfficeHauser.OfficeUser', on_delete=models.PROTECT,
                                      related_name='app_bio_user_staff_set', null=True, blank=True)
    self_note = models.TextField(null=True, blank=True)
    last_updated_date = models.DateTimeField(auto_now=True)

    def __str__(self):
        return "%s | %s | %s , %s | %s __ %s , %s" % (
            self.application_number, self.task.task_title_latin, self.applicant.firstname, self.applicant.lastname,
            self.appointment_date_time, self.trace_code, self.share_with_client)


class AppSecQOpt(models.Model):
    applicant = models.ForeignKey('ApplicationOperation.Applicant', on_delete=models.CASCADE, null=True, blank=True,
                                  related_name='AppSecQOpt_applicant_set')
    task = models.ForeignKey('Organization.Task', on_delete=models.CASCADE, null=True, blank=True,
                             related_name='AppSecOpt_task_set')
    sec_question = models.CharField(max_length=3000, null=True, blank=True)
    sec_answer = models.CharField(max_length=3000, null=True, blank=True)
    by_staff_user = models.ForeignKey('OfficeHauser.OfficeUser', on_delete=models.PROTECT,
                                      related_name='app_sec_qopt_by_user_set', null=True, blank=True)
    last_updated_date = models.DateTimeField(auto_now=True)

    def __str__(self):
        return "%s | %s | %s , %s  __ %s , %s" % (
            self.application_number, self.task.task_title_latin, self.applicant.firstname, self.applicant.lastname,
            self.sec_answer, self.sec_question)


class AppEmbassyopt(models.Model):
    STATUS = (
        ('Pending', 'Pending'),
        ('Submission', 'Submission'),
        ('Granted', 'Granted'),
        ('Refused', 'Refused'),
        ('Not started', 'Not started'),
    )
    applicant = models.ForeignKey('ApplicationOperation.Applicant', on_delete=models.CASCADE, null=True, blank=True,
                                  related_name='AppEmbassyopt_application_set')
    center = models.ForeignKey('Content.Centers', on_delete=models.CASCADE, null=True, blank=True,
                               related_name='AppEmbassyopt_center_set')
    status = models.CharField(max_length=30, choices=STATUS, null=True, blank=True)
    profile_number = models.CharField(max_length=30, choices=STATUS, null=True, blank=True)

    def __str__(self):
        return "%s | %s | %s , %s  __ %s , %s" % (
            self.application_number, self.task.task_title_latin, self.applicant.firstname, self.applicant.lastname,
            self.profile_number, self.center)


class AppNotaryOpt(models.Model):
    applicant = models.ForeignKey('ApplicationOperation.Applicant', on_delete=models.CASCADE, null=True, blank=True,
                                  related_name='AppNotaryOpt_application_set')
    notary_user = models.ForeignKey('OfficeHauser.OfficeUser', on_delete=models.PROTECT,
                                    related_name='app_notary_user_by_user_set', null=True, blank=True)
    notary_text = models.OneToOneField('Organization.NotaryText', on_delete=models.CASCADE, primary_key=False,
                                       related_name='AppNotaryOpt_notary_text_set')
    trace_code = models.CharField(max_length=200, null=True, blank=True)
    by_staff_user = models.ForeignKey('OfficeHauser.OfficeUser', on_delete=models.PROTECT,
                                      related_name='app_notary_by_user_set', null=True, blank=True)
    self_note = models.TextField(null=True, blank=True)
    last_updated_date = models.DateTimeField(auto_now=True)

    def __str__(self):
        return "%s | %s | %s , %s  __ %s , %s" % (
            self.application_number, self.task.task_title_latin, self.applicant.firstname, self.applicant.lastname,
            self.notary_text, self.trace_code)
        # doc latin


class AppFormOpt(models.Model):
    STATUS = (
        ('Waiting', 'Waiting'),
        ('Done', 'Done'),)
    applicant = models.ForeignKey('ApplicationOperation.Applicant', on_delete=models.CASCADE, null=True, blank=True,
                                  related_name='AppFormOpt_application_set')
    status = models.CharField(max_length=30, choices=STATUS, null=True, blank=True)
    forms = models.ForeignKey('Content.VisaForms', on_delete=models.PROTECT, null=True, blank=True,
                              related_name='AppFormOpt_Visa_form_set')
    by_staff_user = models.ForeignKey('OfficeHauser.OfficeUser', on_delete=models.PROTECT,
                                      related_name='app_form_opt_by_user_set', null=True, blank=True)
    self_note = models.TextField(null=True, blank=True)
    last_updated_date = models.DateTimeField(auto_now=True)


def get_file_word_cv(instance, filename):
    return f"file_word_cv/{filename}"


class AppTextOpt(models.Model):
    STATUS = (
        ('Just started', 'Just started'),
        ('Pending', 'Pending'),
        ('Done', 'Done'),
        ('Expired', 'Expired'),
    )
    applicant = models.ForeignKey('ApplicationOperation.Applicant', on_delete=models.CASCADE, null=True, blank=True,
                                  related_name='AppTextOpt_application_set')
    status = models.CharField(max_length=30, choices=STATUS, null=True, blank=True)
    file = models.FileField(upload_to=get_file_word_cv, null=True, blank=True)
    bussiness_user = models.ForeignKey('OfficeHauser.OfficeUser', on_delete=models.PROTECT,
                                       related_name='app_text_opt_bussiness_user_set', null=True, blank=True)
    by_staff_user = models.ForeignKey('OfficeHauser.OfficeUser', on_delete=models.PROTECT,
                                      related_name='app_text_opt_by_user_set', null=True, blank=True)

    self_note = models.TextField(null=True, blank=True)
    last_updated_date = models.DateTimeField(auto_now=True)


def get_file_flight_reserve(instance, filename):
    return f"file_flight_reserve/{filename}"


class AppFlightReserveOpt(models.Model):
    applicant = models.ForeignKey('ApplicationOperation.Applicant', on_delete=models.CASCADE, null=True, blank=True,
                                  related_name='AppFlightReserveOpt_application_set')
    dept_region = models.ForeignKey('Raw_root.Region', on_delete=models.CASCADE,
                                    related_name='flight_country_dept_regoin_set', null=True, blank=True)
    dest_city = models.ForeignKey('Raw_root.Region', on_delete=models.CASCADE,
                                  related_name='flight_country_regoin_dest_set', null=True, blank=True, )
    preferred_dept_date = models.DateField(null=True, blank=True)
    preferred_return_date = models.DateField(null=True, blank=True)
    assign_staff_agn = models.ForeignKey('OfficeHauser.OfficeUser', on_delete=models.PROTECT,
                                         related_name='app_flight_reser_opt_staff_agn_user_set', null=True, blank=True)
    by_staff_user = models.ForeignKey('OfficeHauser.OfficeUser', on_delete=models.PROTECT,
                                      related_name='app_flight_reser_opt_by_user_set', null=True, blank=True)
    self_note = models.TextField(null=True, blank=True)
    last_updated_date = models.DateTimeField(auto_now=True)


class AppAdmissionOpt(models.Model):
    RESULT = (
        ('Approved', 'Approved'),
        ('Conditional', 'Conditional'),
        ('Refused', 'Refused'),
    )

    PATH = (
        ('Direct', 'Direct'),
        ('Indirect', 'Indirect'),
    )

    TYPE = (
        ('New', 'New'),
        ('Defer1', 'Defer1'),
        ('Defer2', 'Defer2'),
        ('Defer3', 'Defer3'),
    )
    applicant = models.ForeignKey('ApplicationOperation.Applicant', on_delete=models.CASCADE, null=True, blank=True,
                                  related_name='AppAdmissionOpt_application_set')
    defer_type = models.CharField(max_length=300, choices=TYPE, null=True, blank=True)
    school = models.ForeignKey('Edu.School', on_delete=models.CASCADE, null=True, blank=True,
                               related_name='AppAdmissionOpt_school_set')
    degree = models.ForeignKey('Raw_root.Degree', on_delete=models.PROTECT, null=True, blank=True,
                               related_name='AppAdmissionOpt_degree_set')
    program = models.ForeignKey('Edu.Program', on_delete=models.PROTECT, null=True, blank=True,
                                related_name='AppAdmissionOpt_program_set')
    by_staff_user = models.ForeignKey('OfficeHauser.OfficeUser', on_delete=models.PROTECT,
                                      related_name='addmision_opt_user_set', null=True, blank=True)
    self_note = models.TextField(null=True, blank=True)
    last_updated_date = models.DateTimeField(auto_now=True)
    submission_trace_code = models.CharField(max_length=300, null=True, blank=True)
    student_number = models.CharField(max_length=300, null=True, blank=True)
    currency = models.ForeignKey('Raw_root.Currency', on_delete=models.PROTECT, null=True, blank=True,
                                 related_name='AppAdmissionOpt_degree_set')
    admission_fee = models.DecimalField(max_digits=10, decimal_places=2, null=True, blank=True)
    submission_date = models.DateField(null=True, blank=True)
    result = models.CharField(max_length=300, choices=RESULT, null=True)

    def __str__(self):
        return "%s | %s | %s , %s  __ %s , %s" % (
            self.application_number, self.task.task_title_latin, self.applicant.firstname, self.applicant.lastname,
            self.degree.title_latin, self.result)
        # school latin


def get_file_app_dynamic_log(instance, filename):
    return f"file_app_dynamic_log/{filename}"


class AppDynamicLog(models.Model):
    applicant = models.ForeignKey('ApplicationOperation.Applicant', on_delete=models.CASCADE, null=True, blank=True, )
    task = models.ForeignKey('Organization.Task', on_delete=models.CASCADE, null=True, blank=True, default=None)
    date_time = models.DateTimeField(null=True, blank=True)
    code = models.CharField(max_length=300, null=True, blank=True)
    evidence = models.ImageField(upload_to=get_file_app_dynamic_log)
    by_staff_user = models.ForeignKey('OfficeHauser.OfficeUser', on_delete=models.PROTECT,
                                      related_name='app_dynamic_by_user_set', null=True, blank=True)
    self_note = models.TextField(null=True, blank=True)
    last_updated_date = models.DateTimeField(auto_now=True)



class ProfileApplicant(models.Model):
    applicant = models.ForeignKey(Applicant, on_delete=models.CASCADE, null=True, blank=True,
                                  related_name='applicant_idd_1')
    application = models.ForeignKey(ApplicationProfile, on_delete=models.CASCADE, null=True, blank=True,  related_name='application_idd_2')
    def __str__(self):
        return " %s __ %s , %s  | %s " % (
             self.applicant.first_name, self.applicant.last_name,  self.applicant.cell_number, self.application.application_number)