from django.db.models import Q
import datetime
from django.utils import timezone
from django.shortcuts import render
import random
from django.http import JsonResponse, HttpResponse
from django.contrib.auth.hashers import check_password
from django.db.models import Q
from rest_framework import status, viewsets, pagination
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet
from rest_framework.generics import CreateAPIView, get_object_or_404, ListAPIView, RetrieveAPIView, ListCreateAPIView

from Organization.models import DeclarationQuestion
from Organization.serializers import DeclarationQuestionSerializer
from PublicPages.models import StaticText
from Raw_root.models import ConsultationMode
from Tour.models import Tour, TourIncludedExcluded
from Visa.models import VisaStream
from Visa.serializers import EligibilitydataSerializer, InstalmentServiceCatdataSerializer
from .models import *
from .serializers import *
from Content.models import SpecificFaq
from Content.serializers import SpecificFaqSerializer, SpecificFAQ2Serializer, ServiceCategoryAllSerializer


# from Content.serializers import SpecificFAQSerializer



class ServiceCatListAPIView(ListAPIView):
    serializer_class = ServiceCategoryBreifSerializer

    def list(self, request):
        dict = {}
        result_list = []
        queryset = ServiceCategory.objects.all().values()
        try:
            now = datetime.datetime.now()
            service_cat = ServiceCategory.objects.filter(availability_from_date_time__lte=now.date()).filter(
                availability_to_date_time__gte=now.date()).values()
            ser = service_cat
            for ser in service_cat:
                ser['availabile'] = 1
                result_list.append(ser)

            service_cat2 = ServiceCategory.objects.filter(
                Q(availability_from_date_time__gt=now.date()) | Q(availability_to_date_time__lt=now.date())).values()
            ser2 = service_cat2
            for ser2 in service_cat2:
                ser2['availabile'] = 0
                result_list.append(ser2)

        except:
            ser2['availabile'] = 0

        # newlist=sorted(map(lambda x: x['show_priority'], queryset))

        # print(len(queryset),len(newlist))
        res1 = []
        res2 = []
        for sub in result_list:
            if sub['show_priority'] == True:
                res1.append(sub)
        for sub in result_list:
            if sub['show_priority'] == False:
                res1.append(sub)


        return Response({"message": "success", "status": "OK", "data": res1+res2}, status=200)


class ServiceCatProirityListAPIView(ListAPIView):
    serializer_class = ServiceCategoryBreifSerializer
    queryset = ServiceCategory.objects.filter(show_priority=True)



class ServiceCategoryAllListAPIView(ListAPIView):
    serializer_class = ServiceCategoryAllSerializer
    queryset = ServiceCategory.objects.all()

class ServiceCatAPIView(ListAPIView):
    def list(self, request, *args, **kwargs):
        dict = {}

        id = self.request.GET.get("id")

        data_sc = ServiceCategory.objects.filter(pk=id)
        serializer_sc = ServiceCategorySerializer(data_sc, many=True)
        dict["service_category"] = serializer_sc.data

        dt = timezone.now()
        data_sc_serach_id = ServiceCategory.objects.filter(pk=id).filter(
            availability_from_date_time__lte=dt).filter(
            availability_to_date_time__gte=dt)
        data_emergency_serach_id = ServiceCategory.objects.filter(pk=id).filter(
            expiry_date_emergency_note__lte=dt)
        if len(dict["service_category"]) >= 1:
            if len(data_sc_serach_id) >= 1:

                dict["service_category"][0]["availability"] = True
            else:

                dict["service_category"][0]["availability"] = False
            if len(data_emergency_serach_id) >= 1:
                dict["service_category"][0]["emergency_note"] = None


        data_ssc = SubServiceCategory.objects.filter(service_category__id=id)
        serializer_ssc = SubServiceCategorySerializer(data_ssc, many=True)
        dict["sub_service_category"] = serializer_ssc.data
        validtiy_subsc = SubServiceCategory.objects.filter(service_category__id=id).filter(validity_to__gte=dt).filter(validity_from__lte=dt).values_list('id',flat=True)
        print(dt)
        # print(list(validtiy_subsc ))
        # print(len(data_ssc),len(validtiy_subsc))
        for subcc in range(0, len(dict["sub_service_category"])):
            dict["sub_service_category"][subcc]["availability"] = False
        if len(dict["sub_service_category"]) >= 1:
            for dats in range(0, len(validtiy_subsc)):
                for subcc in range(0,len(dict["sub_service_category"])):
                    # dict["sub_service_category"][subcc]["availability"] = False
                    if validtiy_subsc[dats]==dict["sub_service_category"][subcc]['id']:
                        dict["sub_service_category"][subcc]["availability"]=True
                    # else:
                    #     if dict["sub_service_category"][subcc].get("availability") is None:




                # for data in dict["sub_service_category"]:
                #     dict["sub_service_category"]["availability"] = False



        data_a = Aboute.objects.filter(service_category=id)
        serializer_a = AbouteSerializer(data_a, many=True)
        dict["about"] = serializer_a.data

        data_i = IncludedExcluded.objects.filter(service_category=id).filter(type='Included')
        serializer_i = IncludedExcludedSerializer(data_i, many=True)
        dict["included"] = serializer_i.data

        data_i = IncludedExcluded.objects.filter(service_category=id).filter(type='Excluded')
        serializer_i = IncludedExcludedSerializer(data_i, many=True)
        dict["excluded"] = serializer_i.data


        data_ie = Eligibility.objects.filter(service_category=id)
        serializer_ie = EligibilitydataSerializer(data_ie, many=True)
        dict["eligibility"] = serializer_ie.data

        data_ig = Guide.objects.filter(service_category=id)
        serializer_ig = GuideSerializer(data_ig, many=True)
        dict["guide"] = serializer_ig.data

        data_r = RequiredDocumnet.objects.filter(service_category=id)
        serializer_r = RequiredDocumnetSerializer(data_r, many=True)
        dict["required_documnet"] = serializer_r.data

        data_s = SpecificFaq.objects.filter(service_category=id)
        serializer_s = SpecificFAQ2Serializer(data_s, many=True)
        dict["specific_faq"] = serializer_s.data

        # data_ar = Article.objects.filter(service_category__in=[id])
        # serializer_s = ArticleServiceCatListSerializer(data_ar, many=True)
        # dict["article"] = serializer_s.data

        data_in = Instalment.objects.filter(service_category=id)
        serializer_in = InstalmentServiceCatdataSerializer(data_in, many=True)
        dict["instalment"] = serializer_in.data

        return Response({
            'status': 200,
            'message': 'success',
            'data': dict
        })



class DeclarationFormWizardAPIView(ListAPIView):
    def list(self, request, *args, **kwargs):
        dict = {}

        if self.request.GET.get("service_category"):

            data_sc = DeclarationQuestion.objects.filter(service_category__id=self.request.GET.get("service_category")).order_by('ques_seq')
            serializer_sc = DeclarationQuestionSerializer(data_sc, many=True)
            dict = serializer_sc.data
        if self.request.GET.get("tour"):
            data_sc = DeclarationQuestion.objects.filter(tour__id=self.request.GET.get("tour")).order_by('ques_seq')
            serializer_sc = DeclarationQuestionSerializer(data_sc, many=True)
            dict = serializer_sc.data

        if self.request.GET.get("stid"):
            data_sc = DeclarationQuestion.objects.filter(stid__id=self.request.GET.get("stid")).order_by('ques_seq')
            serializer_sc = DeclarationQuestionSerializer(data_sc, many=True)
            dict = serializer_sc.data
        return Response({
            'status': 200,
            'message': 'success',
            'data': dict
        })





class StaticConsultationAPIView(APIView):
    def get(self, request):
        dic = {}
        static_text = StaticText.objects.filter(type='Consultation').values('banner','title_latin','title_farsi','text_latin','text_farsi','id')
        dic["static_text"] = list(static_text)

        return Response({"message": "success", "status": "OK", "data": dic}, status=200)



class GETConsultationAPIView(APIView):
    def get(self, request):
        dic = {}
        service_category = ServiceCategory.objects.all().values('title_latin','title_farsi','id')
        dic["service_category"] = list(service_category)

        visa = VisaStream.objects.all().values('title_latin','title_farsi','id')
        dic["visa"] = list(visa)

        tour = Tour.objects.all().values('title_latin','title_farsi','id')
        dic["tour"] = list(tour)

        consultation_mode = ConsultationMode.objects.all().values('title_latin','title_farsi','id')
        dic["consultation_mode"] = list(consultation_mode)

        return Response({"message": "success", "status": "OK", "data": dic}, status=200)


class ConsultCapacityDateTimeFrame(ListAPIView):
    serializer_class = ConsultCapacitySerializer
    queryset = ConsultCapacity.objects.all()


class TourIncludedExcludedWithTourAPIView(ListAPIView):
    def list(self, request, *args, **kwargs):
        dict = {}
        id = self.request.GET.get("id")
        data_i = TourIncludedExcluded.objects.filter(tour=id)
        serializer_i = TourIncludedExcludedSerializer(data_i, many=True)
        dict["included"] = serializer_i.data

        data_e = TourIncludedExcluded.objects.filter(tour=id)
        serializer_e = TourIncludedExcludedSerializer(data_e, many=True)
        dict["excluded"] = serializer_e.data

        return Response({
            'status': 200,
            'message': 'success',
            'data': dict
        })


class AbouteFilterAPIView(APIView):
    def get(self, request, *args, **kwargs):
        dict = {}

        # id = self.request.GET.get("service_category")
        if self.request.GET.get("service_category"):
            data_a = Aboute.objects.filter(service_category__id=self.request.GET.get("service_category"))
            serializer_a = AbouteSerializer(data_a, many=True)
            dict["aboute"] = serializer_a.data

        if self.request.GET.get("stid"):
            data_a = Aboute.objects.filter(stid__id=self.request.GET.get("stid"))
            serializer_a = AbouteSerializer(data_a, many=True)
            dict["aboute"] = serializer_a.data

            # data_ie = Eligibility.objects.filter(service_category=id)
            # serializer_ie = EligibilitydataSerializer(data_ie, many=True)
            # dict["eligibility"] = serializer_ie.data
            #
            # data_ig = Guide.objects.filter(service_category=id)
            # serializer_ig = GuideSerializer(data_ig, many=True)
            # dict["guide"] = serializer_ig.data

        return Response({
            'status': 200,
            'message': 'success',
            'data': dict
        })



class EligibilityFilterAPIView(APIView):
    def get(self, request, *args, **kwargs):
        dict = {}
        # id = self.request.GET.get("service_category")
        if self.request.GET.get("service_category"):
            data_a = Eligibility.objects.filter(service_category__id=self.request.GET.get("service_category"))
            serializer_a = EligibilitydataSerializer(data_a, many=True)
            dict["eligibility"] = serializer_a.data

        if self.request.GET.get("stid"):
            data_a = Eligibility.objects.filter(stid__id=self.request.GET.get("stid"))
            serializer_a = EligibilitydataSerializer(data_a, many=True)
            dict["eligibility"] = serializer_a.data

            # data_ie = Eligibility.objects.filter(service_category=id)
            # serializer_ie = EligibilitydataSerializer(data_ie, many=True)
            # dict["eligibility"] = serializer_ie.data
            #
            # data_ig = Guide.objects.filter(service_category=id)
            # serializer_ig = GuideSerializer(data_ig, many=True)
            # dict["guide"] = serializer_ig.data

        return Response({
            'status': 200,
            'message': 'success',
            'data': dict
        })



class GuideFilterAPIView(APIView):
    def get(self, request, *args, **kwargs):
        dict = {}

        # id = self.request.GET.get("service_category")
        if self.request.GET.get("service_category"):
            data_a = Guide.objects.filter(service_category=self.request.GET.get("service_category"))
            serializer_a = GuideSerializer(data_a, many=True)
            dict["guide"] = serializer_a.data

        if self.request.GET.get("stid"):
            data_a = Guide.objects.filter(stid=self.request.GET.get("stid"))
            serializer_a = GuideSerializer(data_a, many=True)
            dict["guide"] = serializer_a.data

            # data_ie = Eligibility.objects.filter(service_category=id)
            # serializer_ie = EligibilitydataSerializer(data_ie, many=True)
            # dict["eligibility"] = serializer_ie.data
            #
            # data_ig = Guide.objects.filter(service_category=id)
            # serializer_ig = GuideSerializer(data_ig, many=True)
            # dict["guide"] = serializer_ig.data

        return Response({
            'status': 200,
            'message': 'success',
            'data': dict
        })


class RequiredDocumnetFilterAPIView(APIView):
    def get(self, request, *args, **kwargs):
        dict = {}

        if self.request.GET.get("service_category"):
            data_r = RequiredDocumnet.objects.filter(service_category=self.request.GET.get("service_category"))
            serializer_r = RequiredDocumnetSerializer(data_r, many=True)
            dict["required_documnet"] = serializer_r.data

        if self.request.GET.get("stid"):
            data_r = RequiredDocumnet.objects.filter(stid=self.request.GET.get("stid"))
            serializer_r = RequiredDocumnetSerializer(data_r, many=True)
            dict["required_documnet"] = serializer_r.data

        if self.request.GET.get("tour"):
            data_r = RequiredDocumnet.objects.filter(tour=self.request.GET.get("tour"))
            serializer_r = RequiredDocumnetSerializer(data_r, many=True)
            dict["required_documnet"] = serializer_r.data
        return Response({
            'status': 200,
            'message': 'success',
            'data': dict
        })

class IncludedExcludedWithMultiFilterAPIView(APIView):
    def post(self, request, *args, **kwargs):
        dict = {}
        if self.request.GET.get("tour"):

            data_i = IncludedExcluded.objects.filter(tour=self.request.GET.get("tour")).filter(type='Included')
            serializer_i = IncludedExcludedSerializer(data_i, many=True)
            dict["included"] = serializer_i.data

            data_e = IncludedExcluded.objects.filter(tour=self.request.GET.get("tour")).filter(type='Excluded')
            serializer_e = IncludedExcludedSerializer(data_e, many=True)
            dict["excluded"] = serializer_e.data

        if self.request.GET.get("service_category"):

            data_i = IncludedExcluded.objects.filter(service_category=self.request.GET.get("service_category")).filter(type='Included')
            serializer_i = IncludedExcludedSerializer(data_i, many=True)
            dict["included"] = serializer_i.data

            data_e = IncludedExcluded.objects.filter(service_category=self.request.GET.get("service_category")).filter(type='Excluded')
            serializer_e = IncludedExcludedSerializer(data_e, many=True)
            dict["excluded"] = serializer_e.data
        return Response({
            'status': 200,
            'message': 'success',
            'data': dict
        })



class SubServiceCategoryGETServiceCatAPIView(APIView):
    def get(self, request, *args, **kwargs):
        dict = {}
        if self.request.GET.get("servicecat"):

            data_i = SubServiceCategory.objects.filter(service_category=self.request.GET.get("servicecat"))
            serializer_i = SubServiceCategorySerializer(data_i, many=True)
            dict["service_category"] = serializer_i.data
        
        return Response({
            'status': 200,
            'message': 'success',
            'data': dict
        })


class InstalmentListAPIView(ListAPIView):
    serializer_class = InstalmentSerializer

    def list(self, request):
        dict = {}
        result_list = []

        query = Q()
        
        if self.request.GET.get('stid'):
            query = Q(stid=self.request.GET.get('stid'))
        if self.request.GET.get('service_category'):
            query = Q(service_category=self.request.GET.get('service_category'))
        if self.request.GET.get('tour'):
            query = Q(tour=self.request.GET.get('tour'))


        data_i = Instalment.objects.filter(query).order_by('seq')
        serializer_i = InstalmentSerializer(data_i, many=True)

        return Response({"message": "success", "status": "OK", "data":serializer_i.data }, status=200)


class WorkshopListView(ListAPIView):
    queryset = Workshop.objects.all()
    serializer_class = WorkshopSerializer
    

class GetConsultScheduleView(ListAPIView):
    queryset = ConsultScheduleGeneral.objects.all()
    serializer_class = ConsultScheduleGeneralSerializer

    def get(self, request, *args, **kwargs):
        consult_schedules = self.get_queryset()
        serializer = self.get_serializer(consult_schedules, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)